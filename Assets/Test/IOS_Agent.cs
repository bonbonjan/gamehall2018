﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class IOS_Agent : MonoBehaviour {

	[DllImport("__Internal")]
	private static extern void _DoCopy(string str);

	[DllImport("__Internal")]
	private static extern bool _CanOpenUrl(string str);

	[DllImport("__Internal")]
	private static extern string _TestString(string str);

	[DllImport("__Internal")]
	private static extern void _Send_UnityMessage(string gameObjectName, string methodName, string msg);

	[DllImport("__Internal")]
	private static extern string _GetVersion();

	[DllImport("__Internal")]
	private static extern string _GetPhoneSerials();

	[DllImport("__Internal")]
	private static extern string _GetphoneModel();

	[DllImport("__Internal")]
	private static extern string _GetStartOpenUrl();

	[DllImport("__Internal")]
	private static extern string _GetStartQuery();

	[DllImport("__Internal")]
	private static extern string _GetStartScheme();

	[DllImport("__Internal")]
	private static extern string _GetSourceApplication();


	static IOS_Agent g_instance;

	public static IOS_Agent Get()
	{
		return g_instance;
	}

	void Awake()
	{
		g_instance = this;
	}

	void Start () {

	}
	

	void Update () {
	
	}

	static string g_msg = "";
	public void ReceiveIOSMessage(string msg)
	{
		g_msg = msg;
		Debug.Log(msg);
	}

	public static string GetReceiveIOSMessage()
	{
		return g_msg;
	}

	public static string TestString(string str)
	{
		return _TestString(str);
	}

	public static string GetVersion()
	{
		return _GetVersion();
	}

	public static string GetPhoneSerials()
	{
		return _GetPhoneSerials();
	}

	public static string GetPhoneModel()
	{
		return _GetphoneModel();
	}

	//获取启动参数，由其他app传入
	public static string GetStartQuery()
	{
		return _GetStartQuery();
	}

	public static string GetStartScheme()
	{
		return _GetStartScheme();
	}

	//获取哪个app启动大厅的
	public static string GetStartSourceApp()
	{
		return _GetSourceApplication();
	}

	public static void DoCopy(string str)
	{
		_DoCopy(str);
	}

	//判断应用是否安装使用此方法
	public static bool CanOpenUrl(string url)
	{
		return _CanOpenUrl(url);
	}

	public static void Send_UnityMessage(string gameObjectName, string methodName, string msg)
	{
		_Send_UnityMessage(gameObjectName, methodName, msg);
	}
		
}
