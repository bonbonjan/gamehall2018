﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using JsonFx;
using Diagnostics = System.Diagnostics;

public class TestLogCollector : MonoBehaviour
{
    void Awake()
    {

    }

    void Start()
    {
        LogCollector.CreateInstantce(null, "http://localhost:3001/log");
    }

    void Update()
    {

    }

    public void OnBtnClick_Send()
    {
        Debug.Log("OnBtnClick_Send");
        //LogCollector.Get().Send("hello", "大家好");
        LogCollector.Get().Send("hello", new Dictionary<string, object>() {
            { "name","玩美创游"},
            { "age", 123}
        });
    }

    //练习，实验，字典操作（trial,practise,experiment,exercise)
    void Practise_Dic_Operation()
    {
        var dic1 = new Dictionary<string, object>() {
            { "name","玩美创游1"},
            { "age", 123},
            { "abc",true}
        };
        var dic2 = new Dictionary<string, object>() {
            { "name","玩美创游2"},
            { "age", 456}
        };

        //把dic1加到dic2上，相同key覆盖value
        foreach (var item in dic1)
        {
            dic2[item.Key] = dic1[item.Key];
        }
    }

    //备忘：获取客户端信息
    void Memo_HowToGetClientInfo()
    {
        Debug.LogFormat("version: {0}", Application.version);
        Debug.LogFormat("platform: {0}", Application.platform);
        Debug.LogFormat("productName: {0}", Application.productName);
        Debug.LogFormat("unityVersion: {0}", Application.unityVersion);
        Debug.LogFormat("unityVersion: {0}", SystemInfo.deviceModel);
        Debug.LogFormat("unityVersion: {0}", SystemInfo.deviceName);
        Debug.LogFormat("unityVersion: {0}", SystemInfo.deviceType);
        Debug.LogFormat("unityVersion: {0}", SystemInfo.deviceUniqueIdentifier);
        Debug.LogFormat("unityVersion: {0}", SystemInfo.graphicsDeviceType);
        Debug.LogFormat("unityVersion: {0}", SystemInfo.operatingSystem);
        Debug.LogFormat("unityVersion: {0}", SystemInfo.processorCount);
        Debug.LogFormat("unityVersion: {0}", SystemInfo.processorFrequency);
        Debug.LogFormat("unityVersion: {0}", SystemInfo.processorType);
        Debug.LogFormat("unityVersion: {0}", SystemInfo.systemMemorySize);

        //应用名称（几代大厅，子游戏）版本号，场地
        //deviceUniqueIdentifier,mac address
        //其他设备信息
        //帧率统计（统计频率，开始时间，结束时间，时长）
        //内存统计
        //时间对齐（客户端，服务器）
        //运行时长
        //断网事件统计(时间点，开始时间，结束时间，时长
        //心跳
    }
}

public class LogCollector : MonoBehaviour
{
    public string host;

    static LogCollector _instance;

    public static LogCollector Get()
    {
        return _instance;
    }

    public static LogCollector CreateInstantce(GameObject root = null, string host = null)
    {
        GameObject go = root;
        if (go == null)
        {
            go = new GameObject();
            go.name = "LogCollector";
        }
        LogCollector self = go.AddComponent<LogCollector>();
        self.host = host;
        return self;
    }

    void Awake()
    {
        _instance = this;
    }

    public void Send(string method, object body)
    {
        StartCoroutine(_SendAndReceive(method, body));
    }

    IEnumerator _SendAndReceive(string method, object arg)
    {
        var sw = new Diagnostics.Stopwatch();
        sw.Start();
        string sendData;
        var timestamp = System.DateTime.Now.Ticks;
        //sendData = JsonFx.Json.JsonWriter.Serialize(new { method = method, time = System.DateTime.Now.Ticks, args = arg });
        sendData = JsonFx.Json.JsonWriter.Serialize(new Dictionary<string, object>() {
            { "method",method },
            {"time",timestamp },
            {"args", arg }
        });
        Debug.Log(sendData);
        //Debug.Log(JsonUtility.ToJson(new { method = method, time = System.DateTime.Now.Ticks, args = arg }));
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(sendData);
        var headers = new Dictionary<string, string>();
        headers.Add("content-type", "application/json");
        //content-type "application/x-www-form-urlencoded"
        WWW www = new WWW(host, bytes, headers);
        yield return www;
        if (www.isDone)
        {

        }
        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.LogError(www.error);
        }
        else
        {
            try
            {
                string ret = System.Text.Encoding.UTF8.GetString(www.bytes);
                Debug.Log(ret);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
            finally
            {

            }
        }
        sw.Stop();
        Debug.Log(string.Format("Elapsed: {0} ms", sw.Elapsed.TotalMilliseconds.ToString("f0")));
        yield break;
    }
}
