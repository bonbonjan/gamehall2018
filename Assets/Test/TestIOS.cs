﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class TestIOS : MonoBehaviour {


	void Start () {
        ParseQuery();
        return;
        Debug.Log("PhoneSerials：" + IOS_Agent.GetPhoneSerials());
		Debug.Log("PhoneModel：" + IOS_Agent.GetPhoneModel());
		Debug.Log("StartQuery：" + IOS_Agent.GetStartQuery());
		Debug.Log("StartScheme：" + IOS_Agent.GetStartScheme());
		Debug.Log("StartSourceApp：" + IOS_Agent.GetStartSourceApp());
		Debug.Log("Version：" + IOS_Agent.GetVersion());
		Debug.Log("ReceiveIOSMessage：" + IOS_Agent.GetReceiveIOSMessage());

		string[] schemes = new string[] {
			"LuckyLion",
			"MoneyFish",
			"DanTiao",
			"WanFish",
			"BeautyFish",
			"QueYiMen",
			"Joy",
			"WaterMargin"
		};

		foreach(var game in schemes)
		{
			string url = game+"://www.xingli.com?";
			Debug.Log(game +": "+IOS_Agent.CanOpenUrl(url));
		}
        

    }


	void Update () {
	
	}

	public void OnBtnClick_Test1()
	{
		Debug.Log("OnBtnClick_Test1");
		Debug.Log(IOS_Agent.GetPhoneSerials());
	}

	public void OnBtnClick_Test2()
	{
		Debug.Log("OnBtnClick_Test2");
		Debug.Log(IOS_Agent.TestString("hello "));
	}

    public void ParseQuery()
    {
        string queryStr = "abc+NAME=我是XX+PWD=456123+IP=127.0.0.1+LAN=12";
        //string name = new Regex(@"(?<=\+NAME\=)\S+(?=\=)").Match(query).ToString();
        string name = new Regex(@"(?<=\+NAME\=)\S+?(?=\+)").Match(queryStr).ToString();
        string pwd = new Regex(@"(?<=\+PWD\=)\S+?(?=\+)").Match(queryStr).ToString();
        string language = new Regex(@"(?<=\+LAN\=)\S+$").Match(queryStr).ToString();
        string test = new Regex(@"(?<=\+TEST\=)\S+?(?=\+)").Match(queryStr).ToString();
        Debug.Log(name);
        Debug.Log(language);

    }
}
