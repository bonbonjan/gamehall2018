﻿using UnityEngine;
using System.Collections;
using System.IO;
using GGVars = WMCY.GameHall.GVars;

namespace TestScene
{
    public static class GVars
    {
        public static string appName = "TestScene";
        public static string dirName = "TestScene";
        public static string rootDir;
        public static string strVersion;
        public static int nVersion;

        //lua
        public const bool luaByteMode = true;                       //Lua字节码模式-默认关闭 
        public const bool luaBundleMode = true;                    //Lua代码AssetBundle模式
        public static bool DebugMode = false; //移动平台不可用
        public static bool StreamMode = true; //使用StreamingAssets中的资源

        public const string ABDir = "AssetBundles";           //素材目录 
        //public const string luaTempDir = "LuaTemp/";                    //临时目录
        //public const string extName = ".unity3d";                   //素材扩展名
        public const string extName = ".unity3d";                   //素材扩展名
        public const string prefabName = "";
        public const string baseABName = "main";

        public static string dataPath
        {
            get
            {
                string game = GVars.appName.ToLower();
                if (Application.isMobilePlatform)
                {
                    return Application.persistentDataPath + "/" + game + "/";
                }

                if (Application.platform == RuntimePlatform.OSXEditor)
                {
                    int i = Application.dataPath.LastIndexOf('/');
                    return Application.dataPath.Substring(0, i + 1) + game + "/";
                }

                //return "D:/GameHall/" + game + "/";
                //return "E:/GameHall/" + game + "/";
                string ret = Path.GetFullPath(Path.Combine(Application.dataPath, "../Down/" + game + "/")).Replace('\\', '/');
                return ret;
            }
        }

        public static string luaTempPath
        {
            get
            {
                string game = GVars.appName.ToLower();
                //string ret = Path.GetFullPath(Path.Combine(Application.dataPath, "../LuaTemp/" + game + "/")).Replace('\\', '/');
                string ret = Path.GetFullPath(Path.Combine(Application.dataPath, "LuaTemp/" + game + "/")).Replace('\\', '/');
                return ret;
            }
        }

        public static string AppContentPath
        {
            get
            {
                string path = string.Empty;
                //string game = GVars.appName.ToLower();
                string game = GVars.appName;
                switch (Application.platform)
                {
                    case RuntimePlatform.Android:
                        path = "jar:file://" + Application.dataPath + "!/assets/" + game + "/";
                        break;
                    case RuntimePlatform.IPhonePlayer:
                        path = Application.dataPath + "/Raw/" + game + "/";
                        break;
                    default:
                        path = "file://" + Application.dataPath + "/StreamingAssets/" + game + "/";
                        break;
                }
                return path;
                //return Application.streamingAssetsPath + '/' + game + '/';
            }
        }

        public static string streamBundlePath
        {
            get
            {
                return AppContentPath + platformDir;
            }
        }

        static GVars()
        {
            platformDir = GGVars.platformDir;
#if !UNITY_EDITOR
            DebugMode = false;
#endif
        }

        public static string platformDir;

    }
}
