﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Linq;
using System;

using GHUtil = WMCY.GameHall.Util;
using GGVars = WMCY.GameHall.GVars;

namespace TestScene
{

    public class Packager_TestScene
    {
        public static string platform = string.Empty;
        static List<string> paths = new List<string>();
        static List<string> files = new List<string>();
        static List<AssetBundleBuild> maps = new List<AssetBundleBuild>();
        static string outputPath = string.Empty;

        [MenuItem("Build Assets/Build [TestScene]/Android")]
        static void BuildAndroid()
        {
            //Debug.Log(string.Format("build game:[{0}], target: [Android] ", GVars.appName));
            BuildTargetAB(BuildTarget.Android);
        }

        [MenuItem("Build Assets/Build [TestScene]/IOS")]
        static void BuildIOS()
        {
            BuildTargetAB(BuildTarget.iOS);

        }

        [MenuItem("Build Assets/Build [TestScene]/Windows")]
        static void BuildWindows()
        {
            BuildTargetAB(BuildTarget.StandaloneWindows);

        }

        static void BuildTargetAB(BuildTarget target)
        {
            ClearLog();
            //Application.
            UnityEngine.Debug.Log(string.Format("build game:[{0}], target: [{1}] ", GVars.appName, target));
            outputPath = GetOutputPath(target);
            UnityEngine.Debug.Log(string.Format("outputPath:[{0}]", outputPath));
            if (Directory.Exists(outputPath))
            {
                Directory.Delete(outputPath, true);
            }

            maps.Clear();

            if (GVars.luaBundleMode)
                HandleLuaBundle(target);
            else
                HandleLuaFile();

            HandleCustomBundle();

            PrintAssetBundleBuildList(maps);
            if (!Directory.Exists(outputPath)) Directory.CreateDirectory(outputPath);

            BuildAssetBundleOptions options = BuildAssetBundleOptions.DeterministicAssetBundle |
                                              BuildAssetBundleOptions.UncompressedAssetBundle;
            AssetBundleManifest abManifest = BuildPipeline.BuildAssetBundles(outputPath, maps.ToArray(), options, target);
            BuildFileIndex();

            UnityEngine.Debug.Log(string.Format("outputPath: {0}", outputPath));
            //UnityEngine.Debug.Log(string.Format("maps: {0}", maps.Count));

            if (abManifest) UnityEngine.Debug.Log(string.Format("abManifest: {0}", abManifest.GetAllAssetBundles().Length));

            //清理临时目录
            //string tempDir = Application.dataPath + "/" + GVars.luaTempDir;
            //if (Directory.Exists(tempDir)) Directory.Delete(tempDir, true);
            //string tempDir = GVars.luaTempPath + GetTargetDir(target) + "/";
            string tempDir = GVars.luaTempPath;
            if (Directory.Exists(tempDir)) Directory.Delete(tempDir, true);

            AssetDatabase.Refresh();
        }

        static void PrintAssetBundleBuildList(List<AssetBundleBuild> list)
        {
            UnityEngine.Debug.Log(string.Format("AssetBundleBuild count: {0}", list.Count));
            for (int i = 0, len = list.Count; i < len; i++)
            {
                UnityEngine.Debug.Log(string.Format("assetBundleName: [{0}], asset count: [{1}], assetNames: \n[\n  {2}\n]",
                    list[i].assetBundleName, list[i].assetNames.Length, string.Join(",\n  ", list[i].assetNames)));

            }
        }

        public static void ClearLog()
        {
            //UnityEngine.Debug.ClearDeveloperConsole();不能在awake,start方法中工作，也不能在Editor环境下工作
            //http://answers.unity3d.com/questions/707636/clear-console-window.html
            var assembly = Assembly.GetAssembly(typeof(UnityEditor.ActiveEditorTracker));
            var type = assembly.GetType("UnityEditorInternal.LogEntries");
            var method = type.GetMethod("Clear");
            method.Invoke(new object(), null);
        }

        static string GetOutputPath(BuildTarget target)
        {
            //string baseDir = "Assets/" + GVars.ABDir + "/" + GVars.appName + "/";
            //string baseDir = "E:/pj/ULUA/TestAsset/" + "Assets/" + GVars.ABDir + "/" + GVars.appName + "/";
            //string baseDir = Path. + "Assets/" + GVars.ABDir + "/" + GVars.appName + "/";
            string baseDir;
            baseDir = Path.GetFullPath(Path.Combine(Application.dataPath, "../AB/" + GVars.appName + "/")).Replace('\\', '/');
            if (GVars.StreamMode) baseDir = Path.GetFullPath(Path.Combine(Application.dataPath, "StreamingAssets/" + GVars.appName + "/")).Replace('\\', '/');
            string targetDir = GetTargetDir(target);
            return baseDir + targetDir;
        }

        static string GetTargetDir(BuildTarget target)
        {
            string targetDir = string.Empty;
            switch (target)
            {
                case BuildTarget.StandaloneWindows:
                    targetDir = "Win";
                    break;
                case BuildTarget.Android:
                    targetDir = "Android";
                    break;
                case BuildTarget.iOS:
                    targetDir = "iOS";
                    break;
                default:
                    targetDir = "Win";
                    break;
            }
            return targetDir;
        }



        static void HandleLuaBundle(BuildTarget target)
        {
            string tempDir = GVars.luaTempPath + GetTargetDir(target);// +"/";
            UnityEngine.Debug.Log(string.Format("luaTempDir:[{0}]", tempDir));

            if (Directory.Exists(tempDir)) Directory.Delete(tempDir, true);
            Directory.CreateDirectory(tempDir);

            string[] srcDirs = { "" };
            //HashSet<string> fileSet = new HashSet<string>();
            Dictionary<string, string> fileMap = new Dictionary<string, string>();
            ///todo:排除文件同名和目录同名
            ///遍历源目录，挨个文件encode或者拷贝到临时文件夹
            for (int i = 0; i < srcDirs.Length; i++)
            {
                UnityEngine.Debug.Log(string.Format("srcDir:[{0}]", srcDirs[i]));
                if (GVars.luaByteMode)
                {
                    string sourceDir = srcDirs[i];
                    string[] files = Directory.GetFiles(sourceDir, "*.lua", SearchOption.AllDirectories);
                    int len = sourceDir.Length;

                    if (sourceDir[len - 1] == '/' || sourceDir[len - 1] == '\\')
                    {
                        --len;
                    }
                    for (int j = 0; j < files.Length; j++)
                    {
                        files[j] = files[j].Replace('\\', '/');
                        string str = files[j].Remove(0, len);
                        string dest = tempDir + str + ".bytes";
                        //if (fileMap.ContainsKey(str))
                        //{
                        //    throw new Exception(string.Format("路径冲突>文件 file:{0}, file:{1}", files[j], fileMap[dest]));
                        //}
                        //fileMap[str] = files[j];
                        string dir = Path.GetDirectoryName(dest).Replace('\\', '/'); ;
                        UnityEngine.Debug.Log(string.Format("dir:[{0}], file:[{1}]", dir, files[j]));
                        UnityEngine.Debug.Log(string.Format("str:[{0}], dest:[{1}]", str, dest));

                        string srcDir = Path.GetDirectoryName(files[j]).Replace('\\', '/'); ;
                        //if (fileMap.ContainsKey(dir))
                        //{
                        //    if (fileMap[dir] != srcDir)
                        //        throw new Exception(string.Format("路径冲突>目录 dir:{0}, dir:{1}", srcDir, fileMap[dir]));
                        //}
                        //else if (!streamDir.StartsWith(dir))
                        //    fileMap[dir] = srcDir;
                        if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
                        EncodeLuaFile(files[j], dest);
                    }
                }
                else
                {
                    //ToLuaMenu.CopyLuaBytesFiles(srcDirs[i], tempDir);
                }
            }

            string[] subDirs = Directory.GetDirectories(tempDir, "*", SearchOption.AllDirectories);
            //遍历输出文件夹，挨个文件打包
            for (int i = 0; i < subDirs.Length; i++)
            {
                subDirs[i] = subDirs[i].Replace('\\', '/');
                UnityEngine.Debug.Log(string.Format("subDir:[{0}]", subDirs[i]));

                string name = subDirs[i].Replace(tempDir + "/", string.Empty);
                UnityEngine.Debug.Log(string.Format("name:>{0}", name));

                name = name.Replace('\\', '_').Replace('/', '_');
                UnityEngine.Debug.Log(string.Format("name:>{0}", name));

                name = "Lua/Lua_" + name.ToLower() + GVars.extName;
                UnityEngine.Debug.Log(string.Format("name:[{0}]", name));


                string path;
                //path = subDirs[i];
                //path =
                path = "Assets" + subDirs[i].Replace(Application.dataPath, "");
                //AddBuildMap(name, "*.bytes", path);
                //string dir = dirs[i].Replace(Application.dataPath,"");
                AddBuildMap(name, "*.bytes", path);
            }
            string _tempDir;
            // _tempDir = tempDir;
            _tempDir = "Assets" + tempDir.Replace(Application.dataPath, "");
            UnityEngine.Debug.Log(string.Format("tempDir:[{0}], _tempDir:[{1}], dataPath:[{2}]", tempDir, _tempDir, Application.dataPath));

            AddBuildMap("Lua/Lua" + GVars.extName, "*.bytes", _tempDir);

            //-------------------------------处理非Lua文件----------------------------------
            string luaPath = outputPath + "/lua/";
            for (int i = 0; i < srcDirs.Length; i++)
            {
                paths.Clear(); files.Clear();
                string luaDataPath = srcDirs[i].ToLower();
                Recursive(luaDataPath);
                foreach (string f in files)
                {
                    if (f.EndsWith(".meta") || f.EndsWith(".lua")) continue;
                    string newfile = f.Replace(luaDataPath, "");
                    string path = Path.GetDirectoryName(luaPath + newfile);
                    if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                    string destfile = path + "/" + Path.GetFileName(f);
                    File.Copy(f, destfile, true);
                }
            }
            AssetDatabase.Refresh();
        }

        static void HandleLuaFile()
        {
        }


        static void HandleCustomBundle()
        {
            UnityEngine.Debug.Log("HandleCustomBundle");

            //string resPath = AppDataPath + "/" + GVars.AssetDir + "/";
            //if (!Directory.Exists(resPath)) Directory.CreateDirectory(resPath);
            //string sourcePath = "Assets/Test/TestSceneTransfer/TestScene/" + GVars.dirName + "/";
            string sourcePath = "Assets/Test/TestSceneTransfer/TestScene/";
            UnityEngine.Debug.Log("sourcePath: " + sourcePath);
            //return;
            AddBuildMap(GVars.baseABName + GVars.extName, "*.*", sourcePath + "Main");
            AddBuildMap(GVars.baseABName + GVars.extName, "*.*", sourcePath + "Prefabs");
            AddBuildMap(GVars.baseABName + GVars.extName, "*.*", sourcePath + "Textures");
            //AddBuildMap("big" + GVars.extName, "*.*", sourcePath + "Big");
            //AddBuildMap("mainx", "*.*", sourcePath + "Main");

        }

        static void BuildFileIndex()
        {

            ///----------------------创建文件列表-----------------------
            string newFilePath = outputPath + "/files.txt";
            if (File.Exists(newFilePath)) File.Delete(newFilePath);

            paths.Clear(); files.Clear();
            Recursive(outputPath);

            FileStream fs = new FileStream(newFilePath, FileMode.CreateNew);
            StreamWriter sw = new StreamWriter(fs);
            for (int i = 0; i < files.Count; i++)
            {
                string file = files[i];
                string ext = Path.GetExtension(file);
                if (file.EndsWith(".meta") || file.Contains(".DS_Store")) continue;

                string md5 = GHUtil.md5file(file);
                string value = file.Replace(outputPath + "/", string.Empty);
                //UnityEngine.Debug.Log("value:"+value);
                FileInfo fileInfo = new FileInfo(file);
                long size = fileInfo.Length;
                //sw.WriteLine(value + "|" + md5);
                sw.WriteLine(string.Format("{0}|{1}|{2}", value, md5, size));
            }
            sw.Close(); fs.Close();
        }

        #region common

        public static string AppDataPath = string.Empty;
        public static void EncodeLuaFile(string srcFile, string outFile)
        {
            if (!srcFile.ToLower().EndsWith(".lua"))
            {
                File.Copy(srcFile, outFile, true);
                return;
            }
            bool isWin = true;
            string luaexe = string.Empty;
            string args = string.Empty;
            string exedir = string.Empty;
            string currDir = Directory.GetCurrentDirectory();
            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                isWin = true;
                luaexe = "luajit.exe";
                args = "-b " + srcFile + " " + outFile;
                exedir = AppDataPath.Replace("assets", "") + "LuaEncoder/luajit/";
            }
            else if (Application.platform == RuntimePlatform.OSXEditor)
            {
                isWin = false;
                luaexe = "./luac";
                args = "-o " + outFile + " " + srcFile;
                exedir = AppDataPath.Replace("assets", "") + "LuaEncoder/luavm/";
            }
            Directory.SetCurrentDirectory(exedir);
            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = luaexe;
            info.Arguments = args;
            info.WindowStyle = ProcessWindowStyle.Hidden;
            info.ErrorDialog = true;
            info.UseShellExecute = isWin;
            //UnityEngine.Debug.Log(info.FileName + " " + info.Arguments);

            Process pro = Process.Start(info);
            pro.WaitForExit();
            Directory.SetCurrentDirectory(currDir);
        }

        static void Recursive(string path)
        {
            //UnityEngine.Debug.Log(path);
            string[] names = Directory.GetFiles(path);
            string[] dirs = Directory.GetDirectories(path);
            foreach (string filename in names)
            {
                string ext = Path.GetExtension(filename);
                if (ext.Equals(".meta")) continue;
                files.Add(filename.Replace('\\', '/'));
                //UnityEngine.Debug.Log(files[files.Count - 1]);
            }
            foreach (string dir in dirs)
            {
                paths.Add(dir.Replace('\\', '/'));
                Recursive(dir);
            }
        }

        static void UpdateProgress(int progress, int progressMax, string desc)
        {
            string title = "Processing...[" + progress + " - " + progressMax + "]";
            float value = (float)progress / (float)progressMax;
            EditorUtility.DisplayProgressBar(title, desc, value);
        }


        //?bundleName有些奇怪
        static void AddBuildMap(string bundleName, string pattern, string path)
        {
            UnityEngine.Debug.Log(string.Format("bundleName:{0}, pattern:[{1}], path:{2}", bundleName, pattern, path));
            //使用linq过滤
            try
            {
                var files = Directory.GetFiles(path, pattern).Where(s => !s.EndsWith(".meta")).ToArray();
                if (files.Length == 0) return;

                for (int i = 0; i < files.Length; i++)
                {
                    files[i] = files[i].Replace('\\', '/');
                    //UnityEngine.Debug.Log(files[i]);
                }
                AssetBundleBuild build = new AssetBundleBuild();
                build.assetBundleName = bundleName;
                build.assetNames = files;
                maps.Add(build);
            }
            catch (Exception e)
            {
                UnityEngine.Debug.Log(string.Format("bundleName:{0}, pattern:[{1}], path:{2}", bundleName, pattern, path));
                UnityEngine.Debug.LogException(e);
            }



        }

        #endregion

    }

    
    //}

}
