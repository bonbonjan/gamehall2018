﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class TestPlayer  {

    [MenuItem("Scene/Run")]
    static void Run()
    {
        EditorApplication.isPlaying = true;
    }
}
