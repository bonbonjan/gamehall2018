﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu()]
public class DataConfig : ScriptableObject {
    public string IP;//IP密文
    public string IP2;//IP明文，可以输入，用来本地生成IP密文
    public int Port;//Port明文
    public string Key_1;//IP public_key
    public string Key_2;//IP private_key
    public string Key_3;//加密下面key的RSA
    public string Key_4;//version.txt AES key
    public string Key_5;
    public string Key_6;
    public string Key_7;
    public string Key_8;//收集信息IP密文
    public string Key_9;//收集信息IP明文
    public string Key_a;//混淆字典a
    public string Key_b;//混淆字典b
    public string Key_c;
    public char[] KeyChars;

}
