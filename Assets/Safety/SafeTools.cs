﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Net;
using System.Net.Sockets;
//using SimpleJson;
//using System;
//using System.Collections;
//using System.Text;
//using System.Collections.Generic;
//using UnityEngine;

public class SafeTools
{

    //公钥加密，私钥加密均可
    public static string RSA_Encrypt(string key, string content)
    {
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        rsa.FromXmlString(key);
        byte[] cipherBytes = rsa.Encrypt(Encoding.UTF8.GetBytes(content), false);
        return Convert.ToBase64String(cipherBytes);
        //return "";
    }

    //私钥解密
    public static string RSA_Decrypt(string key, string source)
    {
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        rsa.FromXmlString(key);
        //rsa.ImportParameters();
        byte[] cipherBytes = Convert.FromBase64String(source);
        byte[] clearBytes = rsa.Decrypt(cipherBytes, false);
        return Encoding.UTF8.GetString(clearBytes);
        //return "";
    }

    //公钥无法解密，只能验证
    public static string RSA_Decrypt(RSAParameters publicParas, string source)
    {
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        rsa.ImportParameters(publicParas);
        byte[] cipherBytes = Convert.FromBase64String(source);
        byte[] clearBytes = rsa.Decrypt(cipherBytes, false);
        //rsa.VerifyData();
        return Encoding.UTF8.GetString(clearBytes);
        //return "";
    }

    public static string RSA_Decrypt(string strExponent, string strModulus, string source)
    {
        RSAParameters publicParas = new RSAParameters();
        publicParas.Exponent = Convert.FromBase64String(strExponent);
        publicParas.Modulus = Convert.FromBase64String(strModulus);
        return RSA_Decrypt(publicParas, source);
    }

    public static string RSA_Decrypt(string[] paras, string source)
    {
        RSAParameters publicParas = new RSAParameters();
        publicParas.Exponent = Convert.FromBase64String(paras[0]);
        publicParas.Modulus = Convert.FromBase64String(paras[1]);
        return RSA_Decrypt(publicParas, source);
    }

    public static string[] RSA_Resolve(RSACryptoServiceProvider rsa)
    {
        string[] ret = new string[2];
        var paras = rsa.ExportParameters(false);
        ret[0] = Convert.ToBase64String(paras.Exponent);
        ret[1] = Convert.ToBase64String(paras.Modulus);
        return ret;
    }

    public static string AES_Transform(byte[] key, byte[] IV, string content, bool isEncrypt = false)
    {
        string ret = "";
        byte[] toProcessArray;
        //Debug.Log("content: " + content);
        //Debug.Log("content.length: " + content.Length);
        //Debug.Log(string.Format("0:[{0}],1:[{1}]", content[0], content[content.Length-1]));
        if (isEncrypt)
            toProcessArray = Encoding.UTF8.GetBytes(content);
        else
            toProcessArray = Convert.FromBase64String(content);
        RijndaelManaged rDel = new RijndaelManaged();
        rDel.Key = key;
        rDel.IV = IV;
        rDel.Mode = CipherMode.CBC;
        rDel.Padding = PaddingMode.PKCS7;
        ICryptoTransform cTransform = isEncrypt ? rDel.CreateEncryptor() : rDel.CreateDecryptor();
        byte[] resultArray = cTransform.TransformFinalBlock(toProcessArray, 0, toProcessArray.Length);
        if(isEncrypt)
            ret = Convert.ToBase64String(resultArray);
        else
            ret = UTF8Encoding.UTF8.GetString(resultArray);
        return ret;
    }

    public static string AES_Transform(string keyStr, string IVStr, string content, bool isEncrypt = false)
    {
        byte[] keyArray = UTF8Encoding.UTF8.GetBytes(keyStr);
        byte[] IVArray = UTF8Encoding.UTF8.GetBytes(IVStr);
        return AES_Transform(keyArray, IVArray, content, isEncrypt);
    }

    public static string _Adjust_String_1(string source)
    {
        char[] chars = source.ToCharArray();
        int begin = 1;
        int end = chars.Length - 2;
        char tempChar;
        while (begin < end)
        {
            tempChar = chars[begin];
            chars[begin] = chars[begin + 1];
            chars[begin + 1] = tempChar;
            begin += 2;
        }
        begin = 1;
        end = chars.Length - 2;
        while (begin < end)
        {
            tempChar = chars[begin];
            chars[begin] = chars[end];
            chars[end] = tempChar;
            begin++;
            end--;
        }

        string ret = new string(chars);
        return ret;
    }

    public static string Adjust_String_1(string source)
    {
        return (string)GetValue(IE_Adjust_String_1(source), true);
    }

    static IEnumerator IE_Adjust_String_1(string source)
    {
        char[] chars = source.ToCharArray();
        int begin = 1;
        int end = chars.Length - 2;
        char tempChar;
        while (begin < end)
        {
            tempChar = chars[begin];
            chars[begin] = chars[begin + 1];
            chars[begin + 1] = tempChar;
            begin += 2;
        }
        begin = 1;
        end = chars.Length - 2;
        while (begin < end)
        {
            tempChar = chars[begin];
            chars[begin] = chars[end];
            chars[end] = tempChar;
            begin++;
            end--;
        }

        string ret = new string(chars);
        yield return ret;
    }

#if !UNITY_IOS && !UNITY_ANDROID || UNITY_EDITOR


    public static string Adjust_String_2(string source)
    {
        char[] chars = source.ToCharArray();
        Debug.Log("chars.Lenght: " + chars.Length);
        //return "";

        char[] map = GetMapA();
        //Debug.Log("map.Length: " + map.Length);
        //return "";

        HashSet<char> set1 = new HashSet<char>();
        set1.UnionWith(map);
        //Debug.Log("set1.count: " + set1.Count);
        char[] map2 = GetMapB();
        //Debug.Log("map2.Length: " + map2.Length);
        var set2 = new HashSet<char>(set1);
        set2.IntersectWith(map2);
        //Debug.Log("set2.count: " + set2.Count);
        //Debug.Log("set1.count: " + set1.Count);
        //Hashtable map3 = new Hashtable();
        Dictionary<char, int> dic = new Dictionary<char, int>();
        //int[] indexArray = new int[map2.Length];
        for (int i = 0, len = map2.Length; i < len; i++)
        {
            //map3.Add(map2[i], i);
            dic.Add(map2[i], i);
            //indexArray[i] = map2[i];
        }

        char[] retChars = new char[chars.Length];
        for (int i = 0, len = chars.Length; i < len; i++)
        {
            if (dic.ContainsKey(chars[i]))
            {
                retChars[i] = map[dic[chars[i]]];
            }
            else
            {
                retChars[i] = chars[i];
            }
        }

        string retStr = new string(retChars);
        return retStr;
    }

    public static string _Adjust_String_3(string source)
    {
        char[] chars = source.ToCharArray();
        //Debug.Log("chars.Lenght: " + chars.Length);
        //return "";

        char[] map = GetMapA();
        //Debug.Log("map.Length: " + map.Length);
        //return "";

        HashSet<char> set1 = new HashSet<char>();
        set1.UnionWith(map);
        //Debug.Log("set1.count: " + set1.Count);
        char[] map2 = GetMapB();
        //Debug.Log("map2.Length: " + map2.Length);
        var set2 = new HashSet<char>(set1);
        set2.IntersectWith(map2);
        //Debug.Log("set2.count: " + set2.Count);
        //Debug.Log("set1.count: " + set1.Count);
        //Hashtable map3 = new Hashtable();
        Dictionary<char, int> dic = new Dictionary<char, int>();
        //int[] indexArray = new int[map2.Length];
        for (int i = 0, len = map2.Length; i < len; i++)
        {
            //map3.Add(map2[i], i);
            dic.Add(map[i], i);
            //indexArray[i] = map2[i];
        }

        char[] retChars = new char[chars.Length];
        for (int i = 0, len = chars.Length; i < len; i++)
        {
            if (dic.ContainsKey(chars[i]))
            {
                retChars[i] = map2[dic[chars[i]]];
            }
            else
            {
                retChars[i] = chars[i];
            }
        }

        string retStr = new string(retChars);
        return retStr;
    }


   


    public static char[] GetMapA()
    {
        char[] map = new char[] {
            'а','б','в','г','д','е','ё','ж','з','и',
            'й','к','л','м','н','о','п','р','с','т',
            'у','ф','х','ц','ч','ш','щ','ъ','ы','ь',
            'э','ю','я','\n',' ',
            'ā','á','ǎ','à','ō','ó','ǒ','ò','ê','ē',
            'é','ě','è','ī','í','ǐ','ì','ū','ú','ǔ',
            'ù','ǖ','ǘ','ǚ','ǜ','ü'
        };
        return map;
    }

    public static char[] GetMapB()
    {
        char[] map = new char[] {
        '<','>','/','=',
        '0','1','2','3','4','5','6','7','8','9',
        'b','c','d','f','g',
        'h','j','k','m','n',
        'p','q','r','s','t',
        'u','v','w','x','y','z',
        'A','B','C','D','E','F','G',
        'H','J','K','L','M','N',
        'O','P','Q','R','S','T',
        'U','V','W','X','Y','Z'
        };
        return map;
    }


#endif
    public static IEnumerator IE_Adjust_String_3(string source)
    {
        char[] chars = source.ToCharArray();
        //Debug.Log("chars.Lenght: " + chars.Length);
        //return "";

        char[] map = m_mapA;
        //Debug.Log("map.Length: " + map.Length);
        //return "";

        HashSet<char> set1 = new HashSet<char>();
        set1.UnionWith(map);
        //Debug.Log("set1.count: " + set1.Count);
        char[] map2 = m_mapB;
        //Debug.Log("map2.Length: " + map2.Length);
        var set2 = new HashSet<char>(set1);
        set2.IntersectWith(map2);
        //Debug.Log("set2.count: " + set2.Count);
        //Debug.Log("set1.count: " + set1.Count);
        //Hashtable map3 = new Hashtable();
        Dictionary<char, int> dic = new Dictionary<char, int>();
        //int[] indexArray = new int[map2.Length];
        for (int i = 0, len = map2.Length; i < len; i++)
        {
            //map3.Add(map2[i], i);
            dic.Add(map[i], i);
            //indexArray[i] = map2[i];
        }

        char[] retChars = new char[chars.Length];
        for (int i = 0, len = chars.Length; i < len; i++)
        {
            if (dic.ContainsKey(chars[i]))
            {
                retChars[i] = map2[dic[chars[i]]];
            }
            else
            {
                retChars[i] = chars[i];
            }
        }

        string retStr = new string(retChars);
        yield return retStr;
    }

    public static string Adjust_String_3(string source)
    {
        return (string)GetValue(IE_Adjust_String_3(source), true);
    }

    public static bool CompareStr(string strA, string strB)
    {
        int lenA = strA.Length;
        int lenB = strB.Length;
        Debug.Log(string.Format("lenA:{0}, lenB:{1}", lenA, lenB));
        if (lenA != lenB)
        {
            return false;
        }
        bool isSame = true;
        for (int i = 0; i < lenA; i++)
        {
            if (strA[i] != strB[i])
            {
                Debug.Log(string.Format("pos:{0}, A:[{1}], B:[{2}]", i, strA[i], strB[i]));
                isSame = false;
            }
        }
        return isSame;
    }

    //public object result;
    public static IEnumerator IE_Demo(string strA, string strB)
    {
        string ret = strA + strB;
        yield return 123;
        yield return ret;
        //result = ret;
        yield break;
    }
    public static T GetValue<T>(IEnumerator ie, bool toEnd = false)
    {
        while (ie.MoveNext() && (toEnd || ie.Current == null))
            ;
        return (T)ie.Current;
    }

    public static object GetValue(IEnumerator ie, bool toEnd = false)
    {

        while (ie.MoveNext() && (toEnd || ie.Current == null))
            //Debug.Log(toEnd || ie.Current == null);
            ;
        return ie.Current;
    }

    public static DataConfig m_config;
    public static char[] m_mapA;
    public static char[] m_mapB;
    public static bool m_isInit = false;
    static string m_ip;
    static string m_url;
    public static void Init(DataConfig config)
    {
        Debug.Log("safetools.init");
        m_config = config;
        m_mapA = config.Key_a.ToCharArray();
        m_mapB = config.Key_b.ToCharArray();
        //m_ip = 
        m_ip = (string)GetValue(IE_ReadIp(), true);

        m_url = (string)GetValue(IE_ReadUrl(), true);
        Debug.Log("ip: "+m_ip);
        m_isInit = true;
        //Constants.IP = m_ip;
    }

    public static string GetIp()
    {
        if (!m_isInit) throw new System.Exception("config not init");
        return GetValue<string>(IE_GetIp(), true);
    }

	public static IPAddress GetAddress()
	{
		string ip = GetIp();
		IPAddress ipAddress = null;
		try
		{
			if(IPAddress.TryParse(ip, out ipAddress))
			{
				return ipAddress;
			}
			else
			{
				
				IPAddress[] addresses = Dns.GetHostEntry(ip).AddressList;
				foreach (var item in addresses)
				{
					if (item.AddressFamily == AddressFamily.InterNetwork)
					{
						ipAddress = item;
						break;
					}
				}
				//return ipAddress;
			}
			return ipAddress;
		}
		catch(Exception e)
		{
			Debug.LogError(e);
		}
		finally
		{
			//return ipAddress;
		}
		return ipAddress;
	}	

    static IEnumerator IE_GetIp()
    {
        yield return m_ip;
    }

    public static string GetUrl()
    {
        if (!m_isInit) throw new System.Exception("config not init");
        return GetValue<string>(IE_GetUrl(), true);
    }

    static IEnumerator IE_GetUrl()
    {
        yield return m_url;
    }

    public static string ParseVersion(string content)
    {
        return GetValue<string>(IE_ParseVersion(content), true);
    }

    static IEnumerator IE_ParseVersion(string content)
    {
        string private_key = Adjust_String_3(m_config.Key_3);
        string aes_key = RSA_Decrypt(private_key, m_config.Key_4);
        string aes_iv = RSA_Decrypt(private_key, m_config.Key_5);
        //UnityEngine.Debug.Log("content: " + content);
        //UnityEngine.Debug.Log("aes_key: " + aes_key);
        //UnityEngine.Debug.Log("aes_iv: " + aes_iv);
        string ret = AES_Transform(aes_key, aes_iv, content);
        yield return ret;
    }

    static IEnumerator IE_ReadIp()
    {
        string private_key = m_config.Key_2;
        if (private_key == null || private_key.Length == 0)
            throw new System.Exception("config.Key_2 is empty");
        private_key = SafeTools.Adjust_String_3(private_key);
        //Debug.Log("private_key= " + private_key);
        string strIp = SafeTools.Adjust_String_3(m_config.IP);
        string clearIp = SafeTools.RSA_Decrypt(private_key, strIp);
        //Debug.Log("ip:" + clearIp);
        yield return clearIp;
    }

    static IEnumerator IE_ReadUrl()
    {
        string private_key = m_config.Key_2;
        if (private_key == null || private_key.Length == 0)
            throw new System.Exception("config.Key_2 is empty");
        private_key = SafeTools.Adjust_String_3(private_key);
        //Debug.Log("private_key= " + private_key);
        if (string.IsNullOrEmpty(m_config.Key_8))
        {
            yield return "";
            yield break;
        }
        string strUrl = SafeTools.Adjust_String_3(m_config.Key_8);
        string clearUrl = SafeTools.RSA_Decrypt(private_key, strUrl);
        //Debug.Log("url:" + clearUrl);
        yield return clearUrl;
    }

    public static int GetPort()
    {
        return m_config.Port;
    }

    public static string GetVersionKey()
    {
        return "";
    }

    static IEnumerator IE_GetVersionKey()
    {
        string ret = "";
        string private_key = m_config.Key_3;
        if (private_key == null || private_key.Length == 0)
            throw new System.Exception("config.Key_3 is empty");
        private_key = SafeTools.Adjust_String_3(private_key);
        ret = SafeTools.RSA_Decrypt(private_key, m_config.Key_4);
        yield return ret;
    }

    public static void TestCollect()
    {
        Debug.Log("TestCollect");

        string platform = Application.platform.ToString();
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("serverIp", GetIp());
        dic.Add("version", Application.version);
        dic.Add("package", Application.identifier);
        //string jsonStr = NGUIJsonTool.jso
        string jsonStr = NGUIJsonTool.jsonEncode(dic);
        Debug.Log(string.Format("jsonStr: {0}", jsonStr));
    }

    static IEnumerator Post(string url, string data)
    {
        WWW www = new WWW(url, Encoding.UTF8.GetBytes(data));
        yield return www;
        if(www.error!=null)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.text);

        }
        yield break;
    }

    

}

/*
 * 
 * RSA不对称加密，公钥加密私钥解密，私钥加密公钥解密 - flyingdream123的专栏 - 博客频道 - CSDN.NET
 * http://blog.csdn.net/flyingdream123/article/details/9532077
 * C#RSA私钥加密，公钥解密出错的原因_百度经验
 * http://jingyan.baidu.com/article/359911f57e701557fe030686.html
 * RSA算法原理（一） - 阮一峰的网络日志
http://www.ruanyifeng.com/blog/2013/06/rsa_algorithm_part_one.html
 * RSACryptoServiceProvider Class (System.Security.Cryptography)
https://msdn.microsoft.com/en-us/library/system.security.cryptography.rsacryptoserviceprovider(v=vs.110).aspx
 * .NET_RSA加密全接触（重、难点解析） - lubiaopan的专栏 - 博客频道 - CSDN.NET
http://blog.csdn.net/lubiaopan/article/details/6231490
 * RSA的公钥和私钥到底哪个才是用来加密和哪个用来解密？ - 算法 - 知乎
http://www.zhihu.com/question/25912483
 * 
 * 
 * 
 * 
 */




// Source: UIToolkit -- https://github.com/prime31/UIToolkit/blob/master/Assets/Plugins/MiniJSON.cs

// Based on the JSON parser from 
// http://techblog.procurios.nl/k/618/news/view/14605/14863/How-do-I-write-my-own-parser-for-JSON.html

/// <summary>
/// This class encodes and decodes JSON strings.
/// Spec. details, see http://www.json.org/
/// 
/// JSON uses Arrays and Objects. These correspond here to the datatypes ArrayList and Hashtable.
/// All numbers are parsed to doubles.
/// </summary>

public class NGUIJsonTool
{
	private const int TOKEN_NONE = 0;
	private const int TOKEN_CURLY_OPEN = 1;
	private const int TOKEN_CURLY_CLOSE = 2;
	private const int TOKEN_SQUARED_OPEN = 3;
	private const int TOKEN_SQUARED_CLOSE = 4;
	private const int TOKEN_COLON = 5;
	private const int TOKEN_COMMA = 6;
	private const int TOKEN_STRING = 7;
	private const int TOKEN_NUMBER = 8;
	private const int TOKEN_TRUE = 9;
	private const int TOKEN_FALSE = 10;
	private const int TOKEN_NULL = 11;
	private const int BUILDER_CAPACITY = 2000;

	/// <summary>
	/// On decoding, this value holds the position at which the parse failed (-1 = no error).
	/// </summary>
	protected static int lastErrorIndex = -1;
	protected static string lastDecode = "";

	/// <summary>
	/// Parse the specified JSon file, loading sprite information for the specified atlas.
	/// </summary>
    /*
	static public void LoadSpriteData (UIAtlas atlas, TextAsset asset)
	{
		if (asset == null || atlas == null) return;

		string jsonString = asset.text;

		Hashtable decodedHash = jsonDecode(jsonString) as Hashtable;

		if (decodedHash == null)
		{
			Debug.LogWarning("Unable to parse Json file: " + asset.name);
		}
		else LoadSpriteData(atlas, decodedHash);

		asset = null;
		Resources.UnloadUnusedAssets();
	}

	/// <summary>
	/// Parse the specified JSon file, loading sprite information for the specified atlas.
	/// </summary>

	static public void LoadSpriteData (UIAtlas atlas, string jsonData)
	{
		if (string.IsNullOrEmpty(jsonData) || atlas == null) return;

		Hashtable decodedHash = jsonDecode(jsonData) as Hashtable;

		if (decodedHash == null)
		{
			Debug.LogWarning("Unable to parse the provided Json string");
		}
		else LoadSpriteData(atlas, decodedHash);
	}

	/// <summary>
	/// Parse the specified JSon file, loading sprite information for the specified atlas.
	/// </summary>

	static void LoadSpriteData (UIAtlas atlas, Hashtable decodedHash)
	{
		if (decodedHash == null || atlas == null) return;
		List<UISpriteData> oldSprites = atlas.spriteList;
		atlas.spriteList = new List<UISpriteData>();

		Hashtable frames = (Hashtable)decodedHash["frames"];

		foreach (System.Collections.DictionaryEntry item in frames)
		{
			UISpriteData newSprite = new UISpriteData();
			newSprite.name = item.Key.ToString();

			bool exists = false;

			// Check to see if this sprite exists
			foreach (UISpriteData oldSprite in oldSprites)
			{
				if (oldSprite.name.Equals(newSprite.name, StringComparison.OrdinalIgnoreCase))
				{
					exists = true;
					break;
				}
			}

			// Get rid of the extension if the sprite doesn't exist
			// The extension is kept for backwards compatibility so it's still possible to update older atlases.
			if (!exists)
			{
				newSprite.name = newSprite.name.Replace(".png", "");
				newSprite.name = newSprite.name.Replace(".tga", "");
			}

			// Extract the info we need from the TexturePacker json file, mainly uvRect and size
			Hashtable table = (Hashtable)item.Value;
			Hashtable frame = (Hashtable)table["frame"];

			int frameX = int.Parse(frame["x"].ToString());
			int frameY = int.Parse(frame["y"].ToString());
			int frameW = int.Parse(frame["w"].ToString());
			int frameH = int.Parse(frame["h"].ToString());

			// Read the rotation value
			//newSprite.rotated = (bool)table["rotated"];

			newSprite.x = frameX;
			newSprite.y = frameY;
			newSprite.width = frameW;
			newSprite.height = frameH;

			// Support for trimmed sprites
			Hashtable sourceSize = (Hashtable)table["sourceSize"];
			Hashtable spriteSize = (Hashtable)table["spriteSourceSize"];

			if (spriteSize != null && sourceSize != null)
			{
				// TODO: Account for rotated sprites
				if (frameW > 0)
				{
					int spriteX = int.Parse(spriteSize["x"].ToString());
					int spriteW = int.Parse(spriteSize["w"].ToString());
					int sourceW = int.Parse(sourceSize["w"].ToString());

					newSprite.paddingLeft = spriteX;
					newSprite.paddingRight = sourceW - (spriteX + spriteW);
				}

				if (frameH > 0)
				{
					int spriteY = int.Parse(spriteSize["y"].ToString());
					int spriteH = int.Parse(spriteSize["h"].ToString());
					int sourceH = int.Parse(sourceSize["h"].ToString());

					newSprite.paddingTop = spriteY;
					newSprite.paddingBottom = sourceH - (spriteY + spriteH);
				}
			}

			// If the sprite was present before, see if we can copy its inner rect
			foreach (UISpriteData oldSprite in oldSprites)
			{
				if (oldSprite.name.Equals(newSprite.name, StringComparison.OrdinalIgnoreCase))
				{
					newSprite.borderLeft = oldSprite.borderLeft;
					newSprite.borderRight = oldSprite.borderRight;
					newSprite.borderBottom = oldSprite.borderBottom;
					newSprite.borderTop = oldSprite.borderTop;
				}
			}

			// Add this new sprite
			atlas.spriteList.Add(newSprite);
		}

		// Sort imported sprites alphabetically
		atlas.spriteList.Sort(CompareSprites);
		Debug.Log("Imported " + atlas.spriteList.Count + " sprites");
	}

	/// <summary>
	/// Sprite comparison function for sorting.
	/// </summary>

	static int CompareSprites (UISpriteData a, UISpriteData b) { return a.name.CompareTo(b.name); }
    */
	/// <summary>
	/// Parses the string json into a value
	/// </summary>
	/// <param name="json">A JSON string.</param>
	/// <returns>An ArrayList, a Hashtable, a double, a string, null, true, or false</returns>
	public static object jsonDecode( string json )
	{
		// save the string for debug information
        NGUIJsonTool.lastDecode = json;

		if( json != null )
		{
			char[] charArray = json.ToCharArray();
			int index = 0;
			bool success = true;
            object value = NGUIJsonTool.parseValue(charArray, ref index, ref success);

			if( success )
                NGUIJsonTool.lastErrorIndex = -1;
			else
                NGUIJsonTool.lastErrorIndex = index;

			return value;
		}
		else
		{
			return null;
		}
	}


	/// <summary>
	/// Converts a Hashtable / ArrayList / Dictionary(string,string) object into a JSON string
	/// </summary>
	/// <param name="json">A Hashtable / ArrayList</param>
	/// <returns>A JSON encoded string, or null if object 'json' is not serializable</returns>
	public static string jsonEncode( object json )
	{
		var builder = new StringBuilder( BUILDER_CAPACITY );
		var success = NGUIJsonTool.serializeValue( json, builder );
		
		return ( success ? builder.ToString() : null );
	}


	/// <summary>
	/// On decoding, this function returns the position at which the parse failed (-1 = no error).
	/// </summary>
	/// <returns></returns>
	public static bool lastDecodeSuccessful()
	{
		return ( NGUIJsonTool.lastErrorIndex == -1 );
	}


	/// <summary>
	/// On decoding, this function returns the position at which the parse failed (-1 = no error).
	/// </summary>
	/// <returns></returns>
	public static int getLastErrorIndex()
	{
		return NGUIJsonTool.lastErrorIndex;
	}


	/// <summary>
	/// If a decoding error occurred, this function returns a piece of the JSON string 
	/// at which the error took place. To ease debugging.
	/// </summary>
	/// <returns></returns>
	public static string getLastErrorSnippet()
	{
		if( NGUIJsonTool.lastErrorIndex == -1 )
		{
			return "";
		}
		else
		{
			int startIndex = NGUIJsonTool.lastErrorIndex - 5;
			int endIndex = NGUIJsonTool.lastErrorIndex + 15;
			if( startIndex < 0 )
				startIndex = 0;

			if( endIndex >= NGUIJsonTool.lastDecode.Length )
				endIndex = NGUIJsonTool.lastDecode.Length - 1;

			return NGUIJsonTool.lastDecode.Substring( startIndex, endIndex - startIndex + 1 );
		}
	}

	
	#region Parsing
	
	protected static Hashtable parseObject( char[] json, ref int index )
	{
		Hashtable table = new Hashtable();
		int token;

		// {
		nextToken( json, ref index );

		bool done = false;
		while( !done )
		{
			token = lookAhead( json, index );
			if( token == NGUIJsonTool.TOKEN_NONE )
			{
				return null;
			}
			else if( token == NGUIJsonTool.TOKEN_COMMA )
			{
				nextToken( json, ref index );
			}
			else if( token == NGUIJsonTool.TOKEN_CURLY_CLOSE )
			{
				nextToken( json, ref index );
				return table;
			}
			else
			{
				// name
				string name = parseString( json, ref index );
				if( name == null )
				{
					return null;
				}

				// :
				token = nextToken( json, ref index );
				if( token != NGUIJsonTool.TOKEN_COLON )
					return null;

				// value
				bool success = true;
				object value = parseValue( json, ref index, ref success );
				if( !success )
					return null;

				table[name] = value;
			}
		}

		return table;
	}

	
	protected static ArrayList parseArray( char[] json, ref int index )
	{
		ArrayList array = new ArrayList();

		// [
		nextToken( json, ref index );

		bool done = false;
		while( !done )
		{
			int token = lookAhead( json, index );
			if( token == NGUIJsonTool.TOKEN_NONE )
			{
				return null;
			}
			else if( token == NGUIJsonTool.TOKEN_COMMA )
			{
				nextToken( json, ref index );
			}
			else if( token == NGUIJsonTool.TOKEN_SQUARED_CLOSE )
			{
				nextToken( json, ref index );
				break;
			}
			else
			{
				bool success = true;
				object value = parseValue( json, ref index, ref success );
				if( !success )
					return null;

				array.Add( value );
			}
		}

		return array;
	}

	
	protected static object parseValue( char[] json, ref int index, ref bool success )
	{
		switch( lookAhead( json, index ) )
		{
			case NGUIJsonTool.TOKEN_STRING:
				return parseString( json, ref index );
			case NGUIJsonTool.TOKEN_NUMBER:
				return parseNumber( json, ref index );
			case NGUIJsonTool.TOKEN_CURLY_OPEN:
				return parseObject( json, ref index );
			case NGUIJsonTool.TOKEN_SQUARED_OPEN:
				return parseArray( json, ref index );
			case NGUIJsonTool.TOKEN_TRUE:
				nextToken( json, ref index );
				return Boolean.Parse( "TRUE" );
			case NGUIJsonTool.TOKEN_FALSE:
				nextToken( json, ref index );
				return Boolean.Parse( "FALSE" );
			case NGUIJsonTool.TOKEN_NULL:
				nextToken( json, ref index );
				return null;
			case NGUIJsonTool.TOKEN_NONE:
				break;
		}

		success = false;
		return null;
	}

	
	protected static string parseString( char[] json, ref int index )
	{
		string s = "";
		char c;

		eatWhitespace( json, ref index );
		
		// "
		c = json[index++];

		bool complete = false;
		while( !complete )
		{
			if( index == json.Length )
				break;

			c = json[index++];
			if( c == '"' )
			{
				complete = true;
				break;
			}
			else if( c == '\\' )
			{
				if( index == json.Length )
					break;

				c = json[index++];
				if( c == '"' )
				{
					s += '"';
				}
				else if( c == '\\' )
				{
					s += '\\';
				}
				else if( c == '/' )
				{
					s += '/';
				}
				else if( c == 'b' )
				{
					s += '\b';
				}
				else if( c == 'f' )
				{
					s += '\f';
				}
				else if( c == 'n' )
				{
					s += '\n';
				}
				else if( c == 'r' )
				{
					s += '\r';
				}
				else if( c == 't' )
				{
					s += '\t';
				}
				else if( c == 'u' )
				{
					int remainingLength = json.Length - index;
					if( remainingLength >= 4 )
					{
						char[] unicodeCharArray = new char[4];
						Array.Copy( json, index, unicodeCharArray, 0, 4 );

						// Drop in the HTML markup for the unicode character
						s += "&#x" + new string( unicodeCharArray ) + ";";

						/*
uint codePoint = UInt32.Parse(new string(unicodeCharArray), NumberStyles.HexNumber);
// convert the integer codepoint to a unicode char and add to string
s += Char.ConvertFromUtf32((int)codePoint);
*/

						// skip 4 chars
						index += 4;
					}
					else
					{
						break;
					}

				}
			}
			else
			{
				s += c;
			}

		}

		if( !complete )
			return null;

		return s;
	}
	
	
	protected static double parseNumber( char[] json, ref int index )
	{
		eatWhitespace( json, ref index );

		int lastIndex = getLastIndexOfNumber( json, index );
		int charLength = ( lastIndex - index ) + 1;
		char[] numberCharArray = new char[charLength];

		Array.Copy( json, index, numberCharArray, 0, charLength );
		index = lastIndex + 1;
		return Double.Parse( new string( numberCharArray ) ); // , CultureInfo.InvariantCulture);
	}
	
	
	protected static int getLastIndexOfNumber( char[] json, int index )
	{
		int lastIndex;
		for( lastIndex = index; lastIndex < json.Length; lastIndex++ )
			if( "0123456789+-.eE".IndexOf( json[lastIndex] ) == -1 )
			{
				break;
			}
		return lastIndex - 1;
	}
	
	
	protected static void eatWhitespace( char[] json, ref int index )
	{
		for( ; index < json.Length; index++ )
			if( " \t\n\r".IndexOf( json[index] ) == -1 )
			{
				break;
			}
	}
	
	
	protected static int lookAhead( char[] json, int index )
	{
		int saveIndex = index;
		return nextToken( json, ref saveIndex );
	}

	
	protected static int nextToken( char[] json, ref int index )
	{
		eatWhitespace( json, ref index );

		if( index == json.Length )
		{
			return NGUIJsonTool.TOKEN_NONE;
		}
		
		char c = json[index];
		index++;
		switch( c )
		{
			case '{':
				return NGUIJsonTool.TOKEN_CURLY_OPEN;
			case '}':
				return NGUIJsonTool.TOKEN_CURLY_CLOSE;
			case '[':
				return NGUIJsonTool.TOKEN_SQUARED_OPEN;
			case ']':
				return NGUIJsonTool.TOKEN_SQUARED_CLOSE;
			case ',':
				return NGUIJsonTool.TOKEN_COMMA;
			case '"':
				return NGUIJsonTool.TOKEN_STRING;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4': 
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
			case '-': 
				return NGUIJsonTool.TOKEN_NUMBER;
			case ':':
				return NGUIJsonTool.TOKEN_COLON;
		}
		index--;

		int remainingLength = json.Length - index;

		// false
		if( remainingLength >= 5 )
		{
			if( json[index] == 'f' &&
				json[index + 1] == 'a' &&
				json[index + 2] == 'l' &&
				json[index + 3] == 's' &&
				json[index + 4] == 'e' )
			{
				index += 5;
				return NGUIJsonTool.TOKEN_FALSE;
			}
		}

		// true
		if( remainingLength >= 4 )
		{
			if( json[index] == 't' &&
				json[index + 1] == 'r' &&
				json[index + 2] == 'u' &&
				json[index + 3] == 'e' )
			{
				index += 4;
				return NGUIJsonTool.TOKEN_TRUE;
			}
		}

		// null
		if( remainingLength >= 4 )
		{
			if( json[index] == 'n' &&
				json[index + 1] == 'u' &&
				json[index + 2] == 'l' &&
				json[index + 3] == 'l' )
			{
				index += 4;
				return NGUIJsonTool.TOKEN_NULL;
			}
		}

		return NGUIJsonTool.TOKEN_NONE;
	}

	#endregion
	
	
	#region Serialization
	
	protected static bool serializeObjectOrArray( object objectOrArray, StringBuilder builder )
	{
		if( objectOrArray is Hashtable )
		{
			return serializeObject( (Hashtable)objectOrArray, builder );
		}
		else if( objectOrArray is ArrayList )
			{
				return serializeArray( (ArrayList)objectOrArray, builder );
			}
			else
			{
				return false;
			}
	}

	
	protected static bool serializeObject( Hashtable anObject, StringBuilder builder )
	{
		builder.Append( "{" );

		IDictionaryEnumerator e = anObject.GetEnumerator();
		bool first = true;
		while( e.MoveNext() )
		{
			string key = e.Key.ToString();
			object value = e.Value;

			if( !first )
			{
				builder.Append( ", " );
			}

			serializeString( key, builder );
			builder.Append( ":" );
			if( !serializeValue( value, builder ) )
			{
				return false;
			}

			first = false;
		}

		builder.Append( "}" );
		return true;
	}
	
	
	protected static bool serializeDictionary( Dictionary<string,string> dict, StringBuilder builder )
	{
		builder.Append( "{" );
		
		bool first = true;
		foreach( var kv in dict )
		{
			if( !first )
				builder.Append( ", " );
			
			serializeString( kv.Key, builder );
			builder.Append( ":" );
			serializeString( kv.Value, builder );

			first = false;
		}

		builder.Append( "}" );
		return true;
	}
	
	
	protected static bool serializeArray( ArrayList anArray, StringBuilder builder )
	{
		builder.Append( "[" );

		bool first = true;
		for( int i = 0; i < anArray.Count; i++ )
		{
			object value = anArray[i];

			if( !first )
			{
				builder.Append( ", " );
			}

			if( !serializeValue( value, builder ) )
			{
				return false;
			}

			first = false;
		}

		builder.Append( "]" );
		return true;
	}

	
	protected static bool serializeValue( object value, StringBuilder builder )
	{
		// Type t = value.GetType();
		// Debug.Log("type: " + t.ToString() + " isArray: " + t.IsArray);

		if( value == null )
		{
			builder.Append( "null" );
		}
		else if( value.GetType().IsArray )
		{
			serializeArray( new ArrayList( (ICollection)value ), builder );
		}
		else if( value is string )
		{
			serializeString( (string)value, builder );
		}
		else if( value is Char )
		{
			serializeString( Convert.ToString( (char)value ), builder );
		}
		else if( value is Hashtable )
		{
			serializeObject( (Hashtable)value, builder );
		}
		else if( value is Dictionary<string,string> )
		{
			serializeDictionary( (Dictionary<string,string>)value, builder );
		}
		else if( value is ArrayList )
		{
			serializeArray( (ArrayList)value, builder );
		}
		else if( ( value is Boolean ) && ( (Boolean)value == true ) )
		{
			builder.Append( "true" );
		}
		else if( ( value is Boolean ) && ( (Boolean)value == false ) )
		{
			builder.Append( "false" );
		}
		else if( value.GetType().IsPrimitive )
		{
			serializeNumber( Convert.ToDouble( value ), builder );
		}
		else
		{
			return false;
		}

		return true;
	}

	
	protected static void serializeString( string aString, StringBuilder builder )
	{
		builder.Append( "\"" );

		char[] charArray = aString.ToCharArray();
		for( int i = 0; i < charArray.Length; i++ )
		{
			char c = charArray[i];
			if( c == '"' )
			{
				builder.Append( "\\\"" );
			}
			else if( c == '\\' )
			{
				builder.Append( "\\\\" );
			}
			else if( c == '\b' )
			{
				builder.Append( "\\b" );
			}
			else if( c == '\f' )
			{
				builder.Append( "\\f" );
			}
			else if( c == '\n' )
			{
				builder.Append( "\\n" );
			}
			else if( c == '\r' )
			{
				builder.Append( "\\r" );
			}
			else if( c == '\t' )
			{
				builder.Append( "\\t" );
			}
			else
			{
				int codepoint = Convert.ToInt32( c );
				if( ( codepoint >= 32 ) && ( codepoint <= 126 ) )
				{
					builder.Append( c );
				}
				else
				{
					builder.Append( "\\u" + Convert.ToString( codepoint, 16 ).PadLeft( 4, '0' ) );
				}
			}
		}

		builder.Append( "\"" );
	}

	
	protected static void serializeNumber( double number, StringBuilder builder )
	{
		builder.Append( Convert.ToString( number ) ); // , CultureInfo.InvariantCulture));
	}
	
	#endregion
	
}

