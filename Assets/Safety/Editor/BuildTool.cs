﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

public class BuildTool : Editor
{
    static string configPath = "Assets/Safety/New Data Config.asset";
    static void Test1()
    {
        Debug.Log("BuildTool.Test1");
    }

    //[MenuItem("BuildTool/Test2")]
    static void Test2()
    {
        Debug.Log("BuildTool.Test2");
        DataConfig config = AssetDatabase.LoadAssetAtPath<DataConfig>(configPath);
        Debug.Log(config.IP);
        //System.Environment.GetCommandLineArgs();
        string ip = "123";
        string args = string.Join(" ", System.Environment.GetCommandLineArgs());
        Debug.Log(Application.dataPath + "/Safety/log.txt");
        var streamFile = File.CreateText(Application.dataPath + "/Safety/log.txt");
        streamFile.Write(args);
        streamFile.Close();
        foreach (string arg in System.Environment.GetCommandLineArgs())
        {
            if (arg.StartsWith("ip"))
            {
                ip = arg.Split('-')[1];
            }
        }
        if (ip.Length > 0)
        {
            Debug.Log(ip);
            config.IP = ip;
            AssetDatabase.SaveAssets();
        }
        else
        {
            //EditorApplication.Exit(0);
            throw new System.Exception("wrong");
        }
        //return "Test2";
    }
    //E:\pj\Hall_Update\GameHall_5_3_2\Assets\Scripts\Safety
    //[MenuItem("BuildTool/Test3")]
    static void Test3()
    {
        Debug.Log("BuildTool.Test3");
        DataConfig config = AssetDatabase.LoadAssetAtPath<DataConfig>(configPath);
        Debug.Log(config.IP);
        //System.Environment.GetCommandLineArgs();
        string ip = "";
        string args = string.Join(" ", System.Environment.GetCommandLineArgs());
        //Debug.Log(Application.dataPath + "/Scripts/Safety/log.txt");
        //var streamFile = File.CreateText(@"E:\pj\Hall_Update\GameHall_5_3_2\Assets\Scripts\Safety\log3.txt");
        string logPath = Application.dataPath + "/Safety/log3.txt";
        var streamFile = File.CreateText(logPath);
        streamFile.Write(args);
        streamFile.Flush();
        //streamFile.Close();
        foreach (string arg in System.Environment.GetCommandLineArgs())
        {
            streamFile.Write("@" + arg);
            if (arg.StartsWith("ip"))
            {
                ip = arg.Split('-')[1];
                streamFile.Write("@@" + ip);
            }
        }

        if (ip.Length > 0)
        {
            Debug.Log(ip);
            config.IP = ip;
            streamFile.Write("@@@success" + ip);
            streamFile.Flush();
            streamFile.Close();
            SaveConfig(config);
        }
        else
        {
            streamFile.Flush();
            streamFile.Close();
            //EditorApplication.Exit(0);
            throw new System.Exception("wrong");
        }
        //return "Test2";
    }

    static void SaveIp(DataConfig config,string ip)
    {
        Debug.Log("BuildTool.SaveIp");
        string public_key = config.Key_1;
        if (public_key == null || public_key.Length == 0)
            throw new System.Exception("config.Key_1 is empty");
        public_key = SafeTools._Adjust_String_3(public_key);
        string confusedIp = SafeTools.RSA_Encrypt(public_key, ip);
        confusedIp = SafeTools.Adjust_String_2(confusedIp);
        config.IP = confusedIp;
    }

    static void SaveUrl(DataConfig config, string url)
    {
        Debug.Log("BuildTool.SaveUrl");
        string public_key = config.Key_1;
        if (public_key == null || public_key.Length == 0)
            throw new System.Exception("config.Key_1 is empty");
        public_key = SafeTools._Adjust_String_3(public_key);
        string confusedStr = SafeTools.RSA_Encrypt(public_key, url);
        confusedStr = SafeTools.Adjust_String_2(confusedStr);
        config.Key_8 = confusedStr;
    }

    static string ReadIp(DataConfig config,string ip)
    {
        Debug.Log("BuildTool.ReadIp");
        string private_key = config.Key_2;
        if (private_key == null || private_key.Length == 0)
            throw new System.Exception("config.Key_1 is empty");
        private_key = SafeTools._Adjust_String_3(private_key);
        Debug.Log("private_key= "+private_key);
        string strIp = SafeTools._Adjust_String_3(ip);
        string clearIp = SafeTools.RSA_Decrypt(private_key, strIp);
        Debug.Log("ip:" + clearIp);
        return clearIp;
    }

    static string ReadUrl(DataConfig config, string url)
    {
        Debug.Log("BuildTool.ReadIp");
        string private_key = config.Key_2;
        if (private_key == null || private_key.Length == 0)
            throw new System.Exception("config.Key_1 is empty");
        private_key = SafeTools._Adjust_String_3(private_key);
        Debug.Log("private_key= " + private_key);
        string str = SafeTools._Adjust_String_3(url);
        string clearStr = SafeTools.RSA_Decrypt(private_key, str);
        Debug.Log("url:" + clearStr);
        return clearStr;
    }

    [MenuItem("BuildTool/Gen IP RSA")]
    static void GenIpRSA()
    {
        Debug.Log("BuildTool.GenIpRSA");
        DataConfig config = AssetDatabase.LoadAssetAtPath<DataConfig>(configPath);
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        string public_key = rsa.ToXmlString(false);
        string private_key = rsa.ToXmlString(true);
        config.Key_1 = SafeTools.Adjust_String_2(public_key);
        config.Key_2 = SafeTools.Adjust_String_2(private_key);
        SaveConfig(config);
    }

    [MenuItem("BuildTool/Test IP")]
    static void TestIP()
    {
        Debug.Log("BuildTool.TestIP");
        GenIpRSA();
        DataConfig config = AssetDatabase.LoadAssetAtPath<DataConfig>(configPath);
        string ip = "12345";
        SaveIp(config, ip);
        ReadIp(config, config.IP);
        SaveConfig(config);
    }

    static void ShellSetupIP()
    {
        Debug.Log("BuildTool.ShellSetupIP");
        DataConfig config = AssetDatabase.LoadAssetAtPath<DataConfig>(configPath);
        Debug.Log(config.IP);
        string ip = "";
        string args = string.Join(" ", System.Environment.GetCommandLineArgs());
        string logPath = Application.dataPath + "/Safety/sh_log.txt";
        var streamFile = File.CreateText(logPath);
        streamFile.Write(args);
        streamFile.Flush();
        //streamFile.Close();
        foreach (string arg in System.Environment.GetCommandLineArgs())
        {
            streamFile.Write("\n@" + arg);
            if (arg.StartsWith("ip"))
            {
                ip = arg.Split('-')[1];
                streamFile.Write("\n@@" + ip);
            }
        }

        if (ip.Length > 0)
        {
            Debug.Log(ip);
            SaveIp(config, ip);
            streamFile.Write("\n@@@success" + ip);
            streamFile.Flush();
            streamFile.Close();
            config.IP2 = ip;
            SaveConfig(config);
        }
        else
        {
            streamFile.Write("\n@@@failed");
            config.IP2 = "failed: " + ip;
            streamFile.Flush();
            streamFile.Close();
            //EditorApplication.Exit(0);
            throw new System.Exception("wrong");
        }
    }

    [MenuItem("BuildTool/Setup IP")]
    static void SetupIP()
    {
        Debug.Log("BuildTool.SetupIP");
        GenIpRSA();
        DataConfig config = AssetDatabase.LoadAssetAtPath<DataConfig>(configPath);
        string ip = config.IP2;
        SaveIp(config, ip);
        ReadIp(config, config.IP);
        string url = config.Key_9;
        if(!string.IsNullOrEmpty(url))
        {
            SaveUrl(config, url);
            ReadUrl(config, config.Key_8);
        }
        //config.IP2 = "";
        SaveConfig(config);
    }

    [MenuItem("BuildTool/Gen one RSA")]
    static void GenOneRSA()
    {
        Debug.Log("BuildTool.GenOneRSA");
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        string public_key = rsa.ToXmlString(false);
        string private_key = rsa.ToXmlString(true);
        Debug.Log("public_key:\n" + public_key);
        Debug.Log("private_key:\n" + private_key);
    }

    [MenuItem("BuildTool/Write Version Key")]
    static void WriteVersionKey()
    {
        Debug.Log("BuildTool.WriteVersionKey");
        DataConfig config = AssetDatabase.LoadAssetAtPath<DataConfig>(configPath);
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        string public_key = rsa.ToXmlString(false);
        string private_key = rsa.ToXmlString(true);
        config.Key_3 = SafeTools.Adjust_String_2(private_key);
        config.Key_4 = SafeTools.RSA_Encrypt(private_key, ver_aes_key);
        config.Key_5 = SafeTools.RSA_Encrypt(private_key, ver_aes_iv);
        SaveConfig(config);
    }

    static void VersionConvert(DataConfig config)
    {
        Debug.Log("BuildTool.VersionConvert");
        string versionPath = Application.dataPath+"/Version.txt";
        string versionSavePath = Application.dataPath + "/Version_output.txt";
        string content = File.ReadAllText(versionPath,Encoding.UTF8);
        string private_key = SafeTools._Adjust_String_3(config.Key_3);
        Debug.Log("content: " + content);
        Debug.Log("content.length: " + content.Length);
        Debug.Log("private_key: " + private_key);
        string aes_key = SafeTools.RSA_Decrypt(private_key, config.Key_4);
        string aes_iv = SafeTools.RSA_Decrypt(private_key, config.Key_5);
        Debug.Log("aes_key: " + aes_key);
        Debug.Log("aes_iv: " + aes_iv);
        string confusedStr = SafeTools.AES_Transform(aes_key, aes_iv, content, true);
        File.WriteAllText(versionSavePath, confusedStr, Encoding.UTF8);
    }

    [MenuItem("BuildTool/Test Version")]
    static void TestVersionProcess()
    {
        Debug.Log("BuildTool.TestVersionProcess");
        DataConfig config = AssetDatabase.LoadAssetAtPath<DataConfig>(configPath);
        VersionConvert(config);
        string versionPath = Application.dataPath + "/Version_output.txt";
        string content = File.ReadAllText(versionPath, Encoding.UTF8);
        ReadVersion(config,content);
    }

    static string ReadVersion(DataConfig config, string confusedContent)
    {
        Debug.Log("BuildTool.ReadVersion");
        Debug.Log("confusedContent: " + confusedContent);
        string private_key = SafeTools._Adjust_String_3(config.Key_3);
        string aes_key = SafeTools.RSA_Decrypt(private_key, config.Key_4);
        string aes_iv = SafeTools.RSA_Decrypt(private_key, config.Key_5);
        string clearStr = SafeTools.AES_Transform(aes_key, aes_iv, confusedContent);
        Debug.Log("clearStr: " + clearStr);
        return clearStr;
    }

    static void SaveVersionKey(DataConfig config, string key)
    {

    }

    static void SaveConfig(DataConfig config)
    {
        EditorUtility.SetDirty(config);
        AssetDatabase.SaveAssets();
        AssetDatabase.SaveAssets();
    }

    //当config文件不可用时，重新初始化，重置数据
    [MenuItem("BuildTool/Init Local(本地初始化)")]
    static void InitLocal()
    {
        DataConfig config = AssetDatabase.LoadAssetAtPath<DataConfig>(configPath);
        config.Key_a = new string(SafeTools.GetMapA());
        config.Key_b = new string(SafeTools.GetMapB());
        SaveConfig(config);
    }

    static void InitPlayerSettings()
    {
        //打包之前先设置一下 预定义标签， 我建议大家最好 做一些  91 同步推 快用 PP助手一类的标签。 这样在代码中可以灵活的开启 或者关闭 一些代码。
        //因为 这里我是承接 上一篇文章， 我就以sharesdk做例子 ，这样方便大家学习 ，
		PlayerSettings.applicationIdentifier ="com.xingli.hall_haidihuanleyuan";
		PlayerSettings.bundleVersion="9.1.2";
		PlayerSettings.productName="海底欢乐园";
		PlayerSettings.Android.keystorePass = "123456";
		PlayerSettings.Android.keyaliasName = "gamehall";
        PlayerSettings.Android.keyaliasPass = "123456";
    }

    static void BuildForIPhone()
    {
        ShellSetupIP();
        InitPlayerSettings();
        //PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iPhone, "USE_SHARE");
        //这里就是构建xcode工程的核心方法了， 
        //参数1 需要打包的所有场景
        //参数2 需要打包的名子， 这里取到的就是 shell传进来的字符串 91
        //参数3 打包平台projectName
        string[] levels = { "Assets/GameHall/Scenes/MainScene.unity" };
		BuildPipeline.BuildPlayer(levels, "Hall", BuildTarget.iOS, BuildOptions.None);
    }

    static void BuildForAndroid()
    {
        ShellSetupIP();
        InitPlayerSettings();
		string[] levels = { "Assets/GameHall/Scenes/MainScene.unity" };
        //string path = Application.dataPath +"/" + Function.projectName+".apk";
		BuildPipeline.BuildPlayer(levels, "Hall.apk", BuildTarget.Android, BuildOptions.None);
    }

    //Version.txt RSA 公钥私钥 弃用
    //static string ver_public_key = "<RSAKeyValue><Modulus>jlNNmo1L339rg5hWwcSxLV+pgBfsGxnbFslAoIO5meXaLV3GC4+RE+Lp56h2hvrcJultSZCtwvjxJMtSHMVpAPJM4HwLZoSSzhXtZcbwOqSkc0PkdIl1Rwlxn84vTmoCiy13heCuiCVfbWkj8u/FW3WD23D4yXtWrMn+QRLECLE=</Modulus><Exponent>EQ==</Exponent></RSAKeyValue>";
    //static string ver_private_key ="<RSAKeyValue><Modulus>jlNNmo1L339rg5hWwcSxLV+pgBfsGxnbFslAoIO5meXaLV3GC4+RE+Lp56h2hvrcJultSZCtwvjxJMtSHMVpAPJM4HwLZoSSzhXtZcbwOqSkc0PkdIl1Rwlxn84vTmoCiy13heCuiCVfbWkj8u/FW3WD23D4yXtWrMn+QRLECLE=</Modulus><Exponent>EQ==</Exponent><P>rFPp+xjReKWHeQZ+YgBBV8NJKZmOxn6UgdDvFb5nkeNNraxILqHM/Si5zJXuMJvI+zvuNaXE2Zi/oj4bX90WFw==</P><Q>024jM19uowmH4h6dYUFJ4ZHKMmLZS1gjChNmalA1gQc3Ag8cnlrYT+dufmgFXYLkg2byqwyx0IYPzvLnoPXcdw==</Q><DP>ZV6JosNOCru5GgPR30txuyeUctLMdMLt8gJuhULTgv4tsXRmsgTS70Uh/9+5ScUM0AUitiVGnh2d5vdbZZEcDQ==</DP><DQ>Vw87q73iQyIKxoUToIRLmR3p2IMOLhU7mryxs040JhIHl2+iX1KVThQAUirVCGMSzLHrc5vQv0ZCvqBBQkceiw==</DQ><InverseQ>HWdV6rEPf+BwvvEWBvowCkeiKEzqmDOt0b1nFW9Md2n5x0lsX/r4JBQNEvP+20s7HkZrjjPw/7DicFMaUIGt7w==</InverseQ><D>fZTMANcGtiUioUoubrycVTZKQ9jfYzTufYRmM0cNLXB1NxaBkbrpa+ZV6oWVwmTgXpG6yHCZUa56a8J1oOprtDfj557NIeQUPuELFCFd5hNkJ3JFOC1Pq8kiUJ9KBeNvjUHEHS9+1zdVAMA/pRntbFJOCkMQ36mLpTA8oC3y100=</D></RSAKeyValue>";
    static string ver_aes_key = "ktznqmxlgzppazss";
    static string ver_aes_iv = "1234abcdefgh6780";
}

/*
 * RSA密钥长度、明文长度和密文长度 - 天缘博客
 * http://www.metsky.com/archives/657.html
 * 
 */
