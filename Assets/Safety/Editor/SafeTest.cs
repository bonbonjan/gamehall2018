﻿using UnityEngine;
using System;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using UnityEditor;
//using System.Diagnostics;

//public class SafeTest : MonoBehaviour
public class SafeTest //: MonoBehaviour
{
#if UNITY_EDITOR
    public DataConfig config;
    void Start()
    {
        //Test1();
        //Test2();
        //Test3();
        //Test4();
        //Test5();
        //Test6();
        Test7();
    }

    void Update()
    {

    }

    [MenuItem("Tools/OpenUnity")]
    public static void OpenUnity()
    {
        System.Diagnostics.Process proc = new System.Diagnostics.Process();
        proc.StartInfo.FileName = "/bin/bash";
        proc.StartInfo.Arguments = "-c \" " + "open -na unity" + " \"";
        proc.StartInfo.UseShellExecute = false;
        proc.StartInfo.RedirectStandardOutput = true;
        proc.Start();
    }

    void Test1()
    {
        Debug.Log("SafeTest.Test1");
        string key;
        string content = "中国abc";
        var watch = new TimeWatch().Start();
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        RSACryptoServiceProvider rsa2 = new RSACryptoServiceProvider();
        Debug.Log(watch.Watch());
        //rsa.FromXmlString(key);
        byte[] cipherBytes = rsa.Encrypt(Encoding.UTF8.GetBytes(content), false);
        byte[] cipherBytes2 = rsa.Encrypt(Encoding.UTF8.GetBytes(content), false);
        byte[] cipherBytes3 = rsa.EncryptValue(Encoding.UTF8.GetBytes(content));
        string cipherStr = Convert.ToBase64String(cipherBytes);
        string cipherStr2 = Convert.ToBase64String(cipherBytes2);
        string cipherStr3 = Convert.ToBase64String(cipherBytes3);
        Debug.Log(watch.Watch());
        Debug.Log("cipherStr: " + cipherStr);
        Debug.Log("cipherStr2: " + cipherStr2);
        Debug.Log("cipherStr3: " + cipherStr3);

        byte[] clearBytes = rsa.Decrypt(cipherBytes, false);
        byte[] clearBytes2 = rsa.Decrypt(cipherBytes, false);
        byte[] clearBytes22 = rsa.DecryptValue(cipherBytes);
        byte[] clearBytes3 = rsa.DecryptValue(cipherBytes3);
        string clearStr = Encoding.UTF8.GetString(clearBytes);
        string clearStr2 = Encoding.UTF8.GetString(clearBytes2);
        string clearStr3 = Encoding.UTF8.GetString(clearBytes3);
        Debug.Log("clearStr: " + clearStr);
        Debug.Log("clearStr2: " + clearStr2);
        Debug.Log("clearStr3: " + clearStr3);
        Debug.Log(watch.Watch());
        //Debug.Log(rsa.);
    }

    void Test2()
    {
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        string publicKey = rsa.ToXmlString(false);
        string privateKey = rsa.ToXmlString(true);
        Debug.Log("publicKey: " + publicKey);
        Debug.Log("privateKey: " + privateKey);
        //公钥加密
        string configIP = SafeTools.RSA_Encrypt(publicKey, "192.168.1.2");
        Debug.Log(configIP);
        //私钥解密
        string ip = SafeTools.RSA_Decrypt(privateKey, configIP);
        Debug.Log(ip);

        //
        string public_key = "<RSAKeyValue><Modulus>3CmT7HXEMj4We0F2lMVmSvQnCn85ZnXszclqN3P67h+qg4+pxjUQB3czCye4R6uT8MsjipeYC8VjDXgyjcUQ0i/31Tu6PqiIir8nv3QAVyJ/QJChnTIGuSh94q0Vv9sL8VRHdHjh1z5XuDKcBKnqslTzgPcxRecnvEwMXTKbnlE=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        //尝试剔除public part
        string private_key = "<RSAKeyValue><P>+WcLFw7reN4PsayTalZqPzx5SuprmqkyP6JY9fcpOMJm5g2gpdVkW8YSpXFlENd+uQZMz/IBSsQThhXBQJnPRw==</P><Q>4fyFLOZr3jKiOdWoimyeIvKtR8yptQ9zBYCnJNgPT5yZXDp98hhk+JS07cJ48vNm8Z4nE6rsEK2JNVE1pCjhpw==</Q><DP>OkQvnBh5PMisY/cMjahYtCNdtvnjX8OtoJ4+KGCw+bi5L3/5iyS6iJJS4uIGGZQu3+0v3tkMIjqC0S2d84i7mw==</DP><DQ>Y+fMRG5Vr7S4zVKsoQ2l15NrkbtkJ1x+ICehPQObuTlk/0YImfe448ByQE5iRB3hG94sLmC43iKp7v1I9prwLw==</DQ><InverseQ>gr1X+8gnX7GaIfr+WuWahLPvpjUQaHHrQDsNJjrIXd2HI6oxb7KlRW0x+J4Rn7EFuWp3iwKOgzD0CaDpnrX0gA==</InverseQ><D>MDTybN5EfXPW3FozKtQmV9cqDURaPzMnDNBDb6z3jthkL7ZvSDUqM1hIVvL1iMvq8tioCZqz8i/gRnzBJQW3bsZ2SLuv466U/WcqYKalt+6oG2jbx7Enw3cO2XmJ8+uIbqiwLg+5oKpRdiaRWZUadWf9MzHMU68Ty8mSTdpR23k=</D></RSAKeyValue>";
        configIP = SafeTools.RSA_Encrypt(public_key, "192.168.1.2");
        Debug.Log(configIP);
        ip = SafeTools.RSA_Decrypt(private_key, configIP);
        Debug.Log(ip);

    }

    void Test3()
    {
        Debug.Log(SafeTools.Adjust_String_1("123456"));
        Debug.Log(SafeTools.Adjust_String_1("1234567"));
        Debug.Log(SafeTools.Adjust_String_1("12345678"));
    }

    void Test4()
    {
        var watch = new TimeWatch().Start();
        string private_Key =
@"MIICXQIBAAKBgQC7PyjMEuniN6BPn8oqzIZ6AO1NjSTO9R3adCCIwKfKIEoWXXM+
tHDpktdPKSaAsWJPTNAGvEvtxOfzXib/EMXKqD0eUy5MatfpRjRdf1hJVimmfrb0
9Qx2j7CsKLy7nD23m4xubdYBwvkjMwt/L3JxB5D6qryW1wei/j1c+/OCxQIDAQAB
AoGAT7vGYJgRNf4f6qgNS4pKHTu10RcwPFyOOM7IZ9M5380+HyXuBB6MEjowKwpH
1fcy+LepwaR+5KG7b5uBGY4H2ticMtdysBd9gLwnY4Eh4j7LCWE54HvELpeWXkWp
FQdb/NQhcqMAGwYsTnRPdBqkrUmJBTYqEGkIlqCQ5vUJOCECQQDhe0KGmbq1RWp6
TDvgpA2dUmlt2fdP8oNW8O7MvbDaQRduoZnVRTPYCDKfzFqpNXL1hAYgth1N0vzD
nv3VoLcpAkEA1JcY+rLv5js1g5Luv8LaI5/3uOg0CW7fmh/LfGuz8k/OxASN+cAO
UjPHrxtc5xn1zat4/bnV5GEdlOp/DhquPQJBAIV2Fsdi4M+AueiPjPWHRQO0jvDV
jfwFOFZSn5YSRUa6NmtmPY6tumUJXSWWqKb1GwlVTuc3xBqXYsNLLUWwLhkCQQDJ
UJCiD0LohhdGEqUuSKnj5H9kxddJO4pZXFSI7UEJbJQDwcBkyn+FTm2BH+tZGZdQ
fVnlA89OJr0poOpSg+eNAkAKY85SR9KASaTiDBoPpJ8N805XEhd0Kq+ghzSThxL3
fVtKUQLiCh7Yd8oMd/G5S3xWJHUXSioATT8uPRH2bOb/";
        private_Key = private_Key.Replace("\n", "");
        //Debug.Log("private_Key:" + private_Key);
        RSACryptoService rsaService = new RSACryptoService(private_Key);
        string public_Key =
@"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC7PyjMEuniN6BPn8oqzIZ6AO1N
jSTO9R3adCCIwKfKIEoWXXM+tHDpktdPKSaAsWJPTNAGvEvtxOfzXib/EMXKqD0e
Uy5MatfpRjRdf1hJVimmfrb09Qx2j7CsKLy7nD23m4xubdYBwvkjMwt/L3JxB5D6
qryW1wei/j1c+/OCxQIDAQAB";
        public_Key = public_Key.Replace("\n", "");
        RSACryptoService rsaService2 = new RSACryptoService(null, public_Key);
        Debug.Log(watch.Watch());
    }

    void Test5()
    {
        string private_key = "<RSAKeyValue><Modulus>3CmT7HXEMj4We0F2lMVmSvQnCn85ZnXszclqN3P67h+qg4+pxjUQB3czCye4R6uT8MsjipeYC8VjDXgyjcUQ0i/31Tu6PqiIir8nv3QAVyJ/QJChnTIGuSh94q0Vv9sL8VRHdHjh1z5XuDKcBKnqslTzgPcxRecnvEwMXTKbnlE=</Modulus><Exponent>AQAB</Exponent><P>+WcLFw7reN4PsayTalZqPzx5SuprmqkyP6JY9fcpOMJm5g2gpdVkW8YSpXFlENd+uQZMz/IBSsQThhXBQJnPRw==</P><Q>4fyFLOZr3jKiOdWoimyeIvKtR8yptQ9zBYCnJNgPT5yZXDp98hhk+JS07cJ48vNm8Z4nE6rsEK2JNVE1pCjhpw==</Q><DP>OkQvnBh5PMisY/cMjahYtCNdtvnjX8OtoJ4+KGCw+bi5L3/5iyS6iJJS4uIGGZQu3+0v3tkMIjqC0S2d84i7mw==</DP><DQ>Y+fMRG5Vr7S4zVKsoQ2l15NrkbtkJ1x+ICehPQObuTlk/0YImfe448ByQE5iRB3hG94sLmC43iKp7v1I9prwLw==</DQ><InverseQ>gr1X+8gnX7GaIfr+WuWahLPvpjUQaHHrQDsNJjrIXd2HI6oxb7KlRW0x+J4Rn7EFuWp3iwKOgzD0CaDpnrX0gA==</InverseQ><D>MDTybN5EfXPW3FozKtQmV9cqDURaPzMnDNBDb6z3jthkL7ZvSDUqM1hIVvL1iMvq8tioCZqz8i/gRnzBJQW3bsZ2SLuv466U/WcqYKalt+6oG2jbx7Enw3cO2XmJ8+uIbqiwLg+5oKpRdiaRWZUadWf9MzHMU68Ty8mSTdpR23k=</D></RSAKeyValue>";

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(private_key);
        XmlNodeList xmlNodeList = xmlDoc.SelectNodes("/RSAKeyValue");
        Debug.Log(xmlDoc.ChildNodes.Count);
        Debug.Log(xmlNodeList.Count);
        Debug.Log(xmlDoc.SelectSingleNode("/RSAKeyValue").InnerXml);
        Debug.Log(xmlDoc.SelectSingleNode("RSAKeyValue").InnerXml);
        Debug.Log(xmlNodeList[0].InnerXml);
        Debug.Log(xmlNodeList[0].SelectSingleNode("Modulus").InnerXml);
        string RSAKeyModulus = (xmlNodeList[0].SelectSingleNode("//Modulus")).InnerText;
        string RSAKeyExponent = (xmlNodeList[0].SelectSingleNode("//Exponent")).InnerText;
    }

    void Test6()
    {
        string str1 = "01234567890";
        string private_key = "<RSAKeyValue><Modulus>3CmT7HXEMj4We0F2lMVmSvQnCn85ZnXszclqN3P67h+qg4+pxjUQB3czCye4R6uT8MsjipeYC8VjDXgyjcUQ0i/31Tu6PqiIir8nv3QAVyJ/QJChnTIGuSh94q0Vv9sL8VRHdHjh1z5XuDKcBKnqslTzgPcxRecnvEwMXTKbnlE=</Modulus><Exponent>AQAB</Exponent><P>+WcLFw7reN4PsayTalZqPzx5SuprmqkyP6JY9fcpOMJm5g2gpdVkW8YSpXFlENd+uQZMz/IBSsQThhXBQJnPRw==</P><Q>4fyFLOZr3jKiOdWoimyeIvKtR8yptQ9zBYCnJNgPT5yZXDp98hhk+JS07cJ48vNm8Z4nE6rsEK2JNVE1pCjhpw==</Q><DP>OkQvnBh5PMisY/cMjahYtCNdtvnjX8OtoJ4+KGCw+bi5L3/5iyS6iJJS4uIGGZQu3+0v3tkMIjqC0S2d84i7mw==</DP><DQ>Y+fMRG5Vr7S4zVKsoQ2l15NrkbtkJ1x+ICehPQObuTlk/0YImfe448ByQE5iRB3hG94sLmC43iKp7v1I9prwLw==</DQ><InverseQ>gr1X+8gnX7GaIfr+WuWahLPvpjUQaHHrQDsNJjrIXd2HI6oxb7KlRW0x+J4Rn7EFuWp3iwKOgzD0CaDpnrX0gA==</InverseQ><D>MDTybN5EfXPW3FozKtQmV9cqDURaPzMnDNBDb6z3jthkL7ZvSDUqM1hIVvL1iMvq8tioCZqz8i/gRnzBJQW3bsZ2SLuv466U/WcqYKalt+6oG2jbx7Enw3cO2XmJ8+uIbqiwLg+5oKpRdiaRWZUadWf9MzHMU68Ty8mSTdpR23k=</D></RSAKeyValue>";

        //SafeTools.Adjust_String_2("01234567890");
        //Debug.Log(SafeTools.Adjust_String_2("01234567890中文"));
        string confuseStr1 = SafeTools.Adjust_String_2(private_key);
        Debug.Log(confuseStr1);
        string restoreStr1 = SafeTools.Adjust_String_3(confuseStr1);
        Debug.Log(restoreStr1);
        Debug.Log(private_key.Equals(restoreStr1));
        Debug.Log(SafeTools.CompareStr(private_key, restoreStr1));

    }

    void Test7()
    {
        IEnumerator ie = SafeTools.IE_Demo("abc", "123");
        Debug.Log(ie.Current);
        Debug.Log(ie.MoveNext());
        Debug.Log(ie.Current);
        Debug.Log(ie.MoveNext());
        Debug.Log(ie.MoveNext());
        Debug.Log(ie.Current);
        string ret1 = SafeTools.GetValue<string>(ie);
        Debug.Log("ret1: " + ret1);

        IEnumerator ie2 = SafeTools.IE_Demo("xyz", "123");
        string ret2 = (string)SafeTools.GetValue(ie2, true);
        Debug.Log("ret2: " + ret2);

        IEnumerator ie3 = SafeTools.IE_Demo("xyz", "123");
        int ret3 = (int)SafeTools.GetValue(ie3, false);
        Debug.Log("ret3: " + ret3);
        Debug.Log(SafeTools.GetValue(ie3, false));


    }

#endif
}

class TimeWatch
{

    float startTime;
    float lastWatchTime;
    long lastElapsedTicks;
    long startTicks;
    bool useTicks;
    public TimeWatch(bool useTicks = true)
    {
        //Start();
    }
    public TimeWatch Start()
    {
        startTime = Time.time;
        lastWatchTime = startTime;
        startTicks = DateTime.UtcNow.Ticks;
        lastElapsedTicks = startTicks;
        Debug.Log("start: " + startTime);
        return this;
    }

    public float Watch()
    {
        float watchTime = Time.time - lastWatchTime;
        lastWatchTime = Time.time;
        long elapsedTicks = DateTime.UtcNow.Ticks - lastElapsedTicks;
        lastElapsedTicks = DateTime.UtcNow.Ticks;
        if (useTicks)
        {
            return elapsedTicks / 1000000f;//毫秒,1秒=1000,000,000ticks
        }
        else
        {
            return watchTime;
        }
    }

    public float Stop()
    {
        return Time.time - startTime;
    }

    public float GetTime()
    {
        return Time.time - startTime;
    }
}

