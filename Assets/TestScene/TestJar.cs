﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Linq;

public class TestJar : MonoBehaviour
{

    //#if UNITY_ANDROID && !UNITY_EDITOR
    public AndroidJavaClass jc;//调用java工程
    public AndroidJavaObject jo;
    //#endif
    [SerializeField]
    InputField m_inputField1;
    [SerializeField]
    InputField m_inputField2;
    [SerializeField]
    InputField m_inputField3;
    [SerializeField]
    InputField m_inputField4;
    [SerializeField]
    Button m_btnInvoke1;
    [SerializeField]
    Button m_btnInvoke2;
    [SerializeField]
    Button m_btnInvoke3;
    [SerializeField]
    Button m_btnInvoke4;
    [SerializeField]
    Dropdown m_ddGame;
    [SerializeField]
    Button m_btnTest1;
    [SerializeField]
    Button m_btnCollect;//收集游戏信息
    [SerializeField]
    InputField m_inputField5;//展示信息

    List<GameActionItem> m_gameActions;

    void Start()
    {
        InitJava();
        //m_btnInvoke4.onClick.AddListener(OnBtnClick_StartApp);

        m_gameActions = new List<GameActionItem>() { 
            new GameActionItem { name = "幸运六师", action = "com.xingli.lion_lion", package = "com.xingli.lion", schema = "LuckyLion://www.xingli.com?" }, 
            new GameActionItem { name = "摇钱树", action = "com.xingli.fish_fish", package = "com.xingli.fish", schema = "MoneyFish://www.xingli.com?" }, 
            new GameActionItem { name = "单挑", action = "com.xingli.dantiao.MainActivity", package = "com.xingli.dantiao", schema = "DanTiao://www.xingli.com?" }, 
            new GameActionItem { name = "万炮捕鱼", action = "com.xingli.overfish_fish", package = "com.xingli.overfish", schema = "WanFish://www.xingli.com?" }, 
            new GameActionItem { name = "QQ美人鱼", action = "com.xingli.beautyfish_fish", package = "com.xingli.beautyfish", schema = "BeautyFish://www.xingli.com?" }, 
            new GameActionItem { name = "缺一门", action = "android.intent.action.queyimen", package = "com.xingli.queyimen", schema = "QueYiMen://www.xingli.com?" }, 
            new GameActionItem { name = "欢乐牛牛", action = "com.xingli.joybullaction", package = "com.xingli.joybull", schema = "Joy://www.xingli.com?" },
            new GameActionItem { name = "水浒传", action = "com.xingli.waterlaction", package = "com.xingli.water", schema = "WaterMargin://www.xingli.com?" }
        };
        m_ddGame.ClearOptions();
        m_ddGame.AddOptions(new List<string>(from item in m_gameActions select item.GetOptionTitle()));
        Test();
    }

    void Test()
    {
        var o1 = new GameObject();
        var o2 = o1;
        DestroyImmediate(o1);
        Debug.Log(o2 == null);
    }

    void InitJava()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        jo = jc.GetStatic<AndroidJavaObject>("currentActivity");

        m_btnInvoke1.onClick.AddListener(OnBtnClick_VoidInvokeVoid);
        m_btnInvoke2.onClick.AddListener(OnBtnClick_StringInvokeVoid);
        m_btnInvoke3.onClick.AddListener(OnBtnClick_StringInvokeString);
#endif
        m_btnInvoke4.onClick.AddListener(OnBtnClick_StartApp);
        m_btnTest1.onClick.AddListener(OnBtnClick_Test1);
        m_btnCollect.onClick.AddListener(OnBtnClick_Collect);
        for(int count = 10;count>0;count--)
        {
            m_inputField5.text += string.Format("{0}.\n",count);
        }
    }


    void Update()
    {

    }


    public void OnBtnClick_VoidInvokeVoid()
    {
        //jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
        //Debug.Log(jo.Call<string>("Hello") + ", TestJar");
        jo.Call(m_inputField1.text);
    }

    public void OnBtnClick_StringInvokeVoid()
    {
        string ret = jo.Call<string>(m_inputField2.text);
        Debug.Log(ret);
    }
    public void OnBtnClick_StringInvokeString()
    {
        string ret = jo.Call<string>(m_inputField3.text, m_inputField4.text);
        Debug.Log(ret);
    }

    public void OnBtnClick_StartApp()
    {
        Debug.Log("OnBtnClick_StartApp");
        string method = "startFish";
        method = "startApp";
        string userName = "wanmei001";
        string pwd = "qwerty1";
        string language = "0";
        string ip = "114.215.206.19";
        string action = "com.xingli.waterlaction";
        action = m_gameActions[m_ddGame.value].action;
        string key = "userMessage";
        string hallPackageName = Application.identifier;
        string value = GenerateStartAppValue(userName, pwd, language, ip, hallPackageName);
        Debug.Log(string.Format("value: [{0}]", value));
#if UNITY_ANDROID && !UNITY_EDITOR
        jo.Call(method, action, key, value, true);
#endif
    }

    public void OnBtnClick_Test1()
    {
        bool isFromGame = jo.Call<Boolean>("isFromGame");
        Debug.Log("isFromGame: " + isFromGame);
        if (isFromGame)
        {
            string startParams = jo.Call<string>("getStartParams");
            Debug.Log("startParams: " + startParams);
        }
    }

    public void OnBtnClick_Collect()
    {
        Debug.Log("OnBtnClick_Collect");
        string info = "";
        foreach (GameActionItem item in m_gameActions)
        {
            Debug.Log(item.package);
            string versionName = "";
            try
            {
                AndroidJavaObject _jo = jo.Call<AndroidJavaObject>("getTargetPackageInfo", item.package);
                Debug.Log(_jo);
                versionName = jo.Call<string>("getPackageVersionName", _jo);
                Debug.Log(versionName);

            }
            catch (Exception e)
            {

            }
            //versionName = jo.Call<string>("getPackageVersionName", _jo.GetRawObject());
            info += string.Format("[{0}]-[{1}] ({2})\n", item.name, item.package, versionName.Equals("") ? "未安装" : versionName);
        }
        m_inputField5.text = info;
    }

    //生成启动子游戏时用的参数
    string GenerateStartAppValue(string userName, string pwd, string language, string ip, string hallPackageName, string port = "", bool newVersion = false)
    {
        string ret = "";
        long timeMIlls = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
        string timeStr = (timeMIlls / 1000).ToString();
        if (newVersion)
        {
            ret = string.Join(" ", new string[] { userName, pwd, hallPackageName, ip, port, language, timeStr });
        }
        else
        {

            ret = string.Join(" ", new string[] { userName, pwd, hallPackageName, ip, language, timeStr });
        }
        return ret;
    }


}

public class GameActionItem
{
    public int id = 0;
    public string name = "";//应用名称
    public string action = "";//android 启动 action
    public string package = "";//包名，表示名
    public string schema = "";//ios URL schema

    public GameActionItem()
    {

    }

    public GameActionItem(string name, string action, string package)
    {
        this.name = name;
        this.action = action;
        this.package = package;
    }

    public string GetOptionTitle()
    {
        string ret = string.Format("[{0}]--[{1}]", name, package);
        return ret;
    }
}