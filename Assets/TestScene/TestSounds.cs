﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;


//using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using WMCY.GameHall;

public class TestSounds : MonoBehaviour
{

    void Awake()
    {

    }

    void Start()
    {

    }

    void Update()
    {

    }

    public void OnBtnClick_TestClick()
    {
        Debug.Log("OnBtnClick_TestClick");
        SoundManager.Get().PlaySound(SoundType.Common_Click);
    }

    public void OnBtnClick_TestBG()
    {
        Debug.Log("OnBtnClick_TestBG");
        SoundManager.Get().PlaySound(SoundType.BG_First);
    }

    public void OnToggle_SoundMute(bool value)
    {
        SoundManager.Get().SetSilenceSound(value);
    }

    public void OnToggle_BGMute(bool value)
    {
        SoundManager.Get().SetSilenceBG(value);
    }
}

#endif
