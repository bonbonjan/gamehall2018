//
//  Hall_UnityAppController.m
//  Unity-iPhone
//
//  Created by 光徽电子 on 16/8/9.
//
//

#import <Foundation/Foundation.h>
#import "Hall_UnityAppController.h"


@implementation Hall_UnityAppController

NSString *_startParameter = @"";
NSString *_startOpenUrl = @"";
NSString *_startQuery = @"";
NSString *_startScheme = @"";
NSString *_startSourceApplication = @"";

 - (BOOL) application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    _startQuery = [url query];
    _startScheme = [url scheme];
    //_startOpenUrl = [url ];
    _startSourceApplication = sourceApplication;
    return [super application: application openURL: url sourceApplication: sourceApplication annotation: annotation];
}

@end

// Converts C style string to NSString
NSString* CreateNSString (const char* string)
{
    if (string)
        return [NSString stringWithUTF8String: string];
    else
        return [NSString stringWithUTF8String: ""];
}

// Helper method to create C string copy
char* MakeStringCopy (const char* string)
{
    if (string == NULL)
        return NULL;
    
    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);
    return res;
}


// When native code plugin is implemented in .mm / .cpp file, then functions
// should be surrounded with extern "C" block to conform C function naming rules
extern "C" {
    
    void _DoCopy(const char *str)
    {
        if(str)
        {
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =[NSString stringWithUTF8String:str];
        }
    }
    
    BOOL _CanOpenUrl(const char *str)
    {
        NSURL *url = [NSURL URLWithString:CreateNSString(str)];
        return [[UIApplication sharedApplication] canOpenURL:url];
    }
    
    const char* _TestString(const char *str)
    {
        return  MakeStringCopy([[[NSString stringWithUTF8String: str] stringByAppendingString:@"中文"] UTF8String]);
    }
    
    void _Send_UnityMessage(const char *gameObject, const char *methodName, const char *msg)
    {
        UnitySendMessage(gameObject, methodName, msg);
    }
    
    const char* _GetVersion()
    {
        NSString *ver = [NSString stringWithFormat:@"%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey]];
        return MakeStringCopy([ver UTF8String]);
    }
    
    const char* _GetPhoneSerials()
    {
        NSString* identifierNumber = [[UIDevice currentDevice].identifierForVendor UUIDString ];
        return MakeStringCopy([identifierNumber UTF8String]);
    }
    
    const char* _GetphoneModel()
    {
        NSString* phoneModel = [[UIDevice currentDevice] model];
        return MakeStringCopy([phoneModel UTF8String]);
    }
    
    const char* _GetStartOpenUrl()
    {
        return MakeStringCopy([_startOpenUrl UTF8String]);
    }
    
    const char* _GetStartQuery()
    {
        return MakeStringCopy([_startQuery UTF8String]);
    }
    
    const char* _GetStartScheme()
    {
        return MakeStringCopy([_startScheme UTF8String]);
    }
    
    const char* _GetSourceApplication()
    {
        return MakeStringCopy([_startSourceApplication UTF8String]);
    }
    
 

}




