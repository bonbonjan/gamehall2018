﻿
using UnityEngine;
using System.Collections;
using WMCY.GameHall.Common;
using System.Collections.Generic;


namespace WMCY.GameHall
{


    public class SoundManager : MB_Singleton<SoundManager>
    {

        [SerializeField]
        AudioSource m_sourceTemplate;//
        [SerializeField]
        List<AudioSource> m_sourcePool;//只增不减池
        [SerializeField]
        AudioSource m_sourceBG;//唯一的背景音源

        [Header("背景音乐")]
        [SerializeField]
        GameSound[] m_clipsBG;//背景
        [Header("通用点击声")]
        [SerializeField]
        GameSound m_clipCommonClick;//通用点击音效，使用与点击和弹框
        [Header("弹框提示声")]
        [SerializeField]
        GameSound m_clipAlertHint;//
        [Header("加减按钮点击声")]
        [SerializeField]
        GameSound m_clipClickPlusMinus;
        [Header("删除邮件")]
        [SerializeField]
        GameSound m_clipMailDelete;
        [Header("阅读邮件")]
        [SerializeField]
        GameSound m_clipMailRead;
        [Header("收到新邮件")]
        [SerializeField]
        GameSound m_clipMailReceive;
        [Header("选择邮件")]
        [SerializeField]
        GameSound m_clipMailSelect;
        [Header("获得金币")]
        [SerializeField]
        GameSound m_clipGoldCoinGain;
        //[SerializeField]
        //GameSound m_clipTest;
        //Mail_Delete,//删除邮件
        //Mail_Read,//阅读邮件
        //Mail_Receive,//收到新邮件
        //Mail_Select,//选择邮件
        //GoldCoin_Gain,//获得金币

        bool m_silenceSound = false;//是否静音音效
        bool m_silenceBG = false;//是否静音背景声
        string m_strSoundKey = "MuteSound";
        string m_strBGKey = "MuteBG";

        void Awake()
        {
            if (_instance == null)
            {
                SetInstance(this);
                m_sourcePool = new List<AudioSource>();
            }
        }

        void Start()
        {
            Init();
        }

        public void PlaySound(SoundType soundType)
        {
            switch (soundType)
            {
                case SoundType.None:
                    break;
                case SoundType.Common_Click:
                    SourcePlayClip(GetSource(), m_clipCommonClick.clip, m_silenceSound ? 0 : m_clipCommonClick.volume);
                    break;
                case SoundType.Click_PlusMinus:
                    SourcePlayClip(GetSource(), m_clipClickPlusMinus.clip, m_silenceSound ? 0 : m_clipClickPlusMinus.volume);
                    break;
                case SoundType.Alert_Hint:
                    SourcePlayClip(GetSource(), m_clipAlertHint.clip, m_silenceSound ? 0 : m_clipAlertHint.volume);
                    break;
                case SoundType.GoldCoin_Gain:
                    SourcePlayClip(GetSource(), m_clipGoldCoinGain.clip, m_silenceSound ? 0 : m_clipGoldCoinGain.volume);
                    break;
                case SoundType.Mail_Delete:
                    SourcePlayClip(GetSource(), m_clipMailDelete.clip, m_silenceSound ? 0 : m_clipMailDelete.volume);
                    break;
                case SoundType.Mail_Read:
                    SourcePlayClip(GetSource(), m_clipMailRead.clip, m_silenceSound ? 0 : m_clipMailRead.volume);
                    break;
                case SoundType.Mail_Receive:
                    SourcePlayClip(GetSource(), m_clipMailReceive.clip, m_silenceSound ? 0 : m_clipMailReceive.volume);
                    break;
                case SoundType.Mail_Select:
                    SourcePlayClip(GetSource(), m_clipMailSelect.clip, m_silenceSound ? 0 : m_clipMailSelect.volume);
                    break;
                default:
                    PlaySoundBG(soundType);
                    break;
            }
        }

        void PlayClip(AudioClip clip, bool loop = false)
        {

        }

        AudioSource GetSource()
        {
            foreach (var source in m_sourcePool)
            {
                if (!source.isPlaying)
                {
                    return source;
                }
            }
            return CreateSource();

        }

        AudioSource CreateSource()
        {
            AudioSource one = null;
            //one = gameObject.AddComponent<AudioSource>();
            one = Instantiate<AudioSource>(m_sourceTemplate);
            one.transform.SetParent(m_sourceTemplate.transform.parent);
            one.name = "Source";
            m_sourcePool.Add(one);
            return one;
        }

        //专门用于播放背景音
        void PlaySoundBG(SoundType soundType)
        {
            GameSound sound = null;
            switch (soundType)
            {
                case SoundType.BG_First:
                    sound = m_clipsBG[0];
                    break;
                case SoundType.BG_Second:
                    sound = m_clipsBG[1];
                    break;
                case SoundType.BG_Third:
                    sound = m_clipsBG[2];
                    break;
                case SoundType.BG_Four:
                    sound = m_clipsBG[3];
                    break;
                case SoundType.BG_Random:
                    sound = m_clipsBG[Random.Range(0,4)];
                    break;
            }

            if (sound == null)
            {
                return;
            }

            SourcePlayClip(m_sourceBG, sound.clip,  sound.volume, true);
        }

        void SourcePlayClip(AudioSource source, AudioClip clip, float volume, bool loop = false)
        {
            source.clip = clip;
            source.loop = loop;
            source.volume = volume;
            source.Play();

        }

        public void SetSilenceSound(bool value)
        {
            m_silenceSound = value;
            if (value)
            {
                PlayerPrefs.SetInt(m_strSoundKey, 1);
            }
            else
            {
                PlayerPrefs.DeleteKey(m_strSoundKey);
            }
            PlayerPrefs.Save();
        }

        public bool IsSilenceSound()
        {
            return m_silenceSound;
        }

        public void SetSilenceBG(bool value)
        {
            m_silenceBG = value;
            m_sourceBG.mute = value;
            if (value)
            {
                PlayerPrefs.SetInt(m_strBGKey, 1);
                
            }
            else
            {
                PlayerPrefs.DeleteKey(m_strBGKey);
            }
            PlayerPrefs.Save();
           
        }

        public bool IsSilenceBG()
        {
            return m_silenceBG;
        }

        public void Init()
        {
            if (PlayerPrefs.HasKey(m_strSoundKey))
            {
                m_silenceSound = true;
            }
            if (PlayerPrefs.HasKey(m_strBGKey))
            {
                m_silenceBG = true;
                m_sourceBG.mute = true;
            }
        }


    }

    public enum SoundType
    {
        None,
        BG_First,
        BG_Second,
        BG_Third,
        BG_Four,
        BG_Random,//随机背景声
        Common_Click,//通用点击声
        Alert_Hint,//弹框提示
        Click_PlusMinus,//加减按钮
        Mail_Delete,//删除邮件
        Mail_Read,//阅读邮件
        Mail_Receive,//收到新邮件
        Mail_Select,//选择邮件
        GoldCoin_Gain,//获得金币


    }

    [System.Serializable]
    public class GameSound
    {
        public AudioClip clip;

        [Range(0, 1f)]
        public float volume = 1f;
    }

}
