﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;

namespace WMCY.GameHall
{
    public class LotteryCityPanelController : MonoBehaviour
    {
        public Transform _listItemPrefab;
        public Transform _receiveAddressPrefab;
        public Transform _tipExchangeGifts;
        private Transform m_listContent;
        private int m_curTabIndex = 0;
        void Awake()
        {
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Fresh_GoldAndLottery, this, FreshGoldAndLottery);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Open_ReveiveAddressPanel, this, OpenReveiveAddressPanel);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Open_BuyLotteryItems, this, OpenBuyLotteryItems);
            m_listContent = transform.Find("listPanel/listScrollView/Viewport/Content");
            
        }

        void OnEnable()
        {
            transform.Find("topBg/lotteryItem/num").GetComponent<Text>().text = Convert.ToString(GVars.user.lottery);
            FocusTabByIndex(1);
            //SwitchIntoEnglish();
        }

        void OnDisable()
        {
            m_curTabIndex = 0;
            UserCompleteController.UserTempNoPrompt = false;
        }

        /*
         * 清除列表
         */
        void DestroyList()
        {
            for (int i = 0; i < m_listContent.childCount; i++)
            {
                DestroyObject(m_listContent.GetChild(i).gameObject);
            }
        }

        /*
         * 更新列表
         */
        void UpdateList()
        {
            DestroyList();
            m_listContent.localPosition = new Vector3(-574, 146, 0);
            int itemNum = 3;
            for (int i = 0; i < itemNum; i++)
            {
                Transform listItem = (Transform)Instantiate(_listItemPrefab);
                listItem.parent = m_listContent;
                listItem.localPosition = new Vector3(200+370*i,-146,0);
                listItem.localScale = Vector3.one;
                listItem.name = ""+i;
                listItem.GetComponent<LotteryListItem>().ShowUI(i + 1 + (m_curTabIndex - 1) * 3);
            }
            float rectW = itemNum * 400;
            float rectH = m_listContent.GetComponent<RectTransform>().rect.height;
            m_listContent.GetComponent<RectTransform>().sizeDelta = new Vector2(rectW, rectH);
        }

        void Update()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
                return;
            transform.Find("TitleBg/Title").GetComponent<Text>().text = "Lottery Mall";
            transform.Find("TitleBg/Title").GetComponent<Text>().fontSize = 30;
            transform.Find("tab/tab1/Text").GetComponent<Text>().text = "Accessories";
            transform.Find("tab/tab1/tabHighlight/Text").GetComponent<Text>().text = "Accessories";
            transform.Find("tab/tab2/Text").GetComponent<Text>().text = "Toy";
            transform.Find("tab/tab2/tabHighlight/Text").GetComponent<Text>().text = "Toy";
            transform.Find("tab/tab3/Text").GetComponent<Text>().text = "Apple Series";
            transform.Find("tab/tab3/tabHighlight/Text").GetComponent<Text>().text = "Apple Series";


            transform.Find("TitleBg/Title").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("tab/tab1/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("tab/tab1/tabHighlight/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("tab/tab2/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("tab/tab2/tabHighlight/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("tab/tab3/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("tab/tab3/tabHighlight/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
        }

        public void OnBtnClick_Close()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            gameObject.SetActive(false);
        }

        public void OnBtnClick_Tab1()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            FocusTabByIndex(1);
        }
        public void OnBtnClick_Tab2()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            FocusTabByIndex(2);

        }
        public void OnBtnClick_Tab3()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            FocusTabByIndex(3);
        }

        void FocusTabByIndex(int index)
        {
            if (m_curTabIndex == index)
            {
                return;
            }
            m_curTabIndex = index;
            UpdateList();
            for (int i = 1; i < 4; i ++ )
            {
                transform.Find(string.Format("tab/tab{0}/tabHighlight", i)).gameObject.SetActive(i == index ? true : false);
            }
        }

        private void FreshGoldAndLottery(object obj)
        {
            transform.Find("topBg/lotteryItem/num").GetComponent<Text>().text = Convert.ToString(GVars.user.lottery);
        }

        private void OpenReveiveAddressPanel(object obj)
        {
            Transform listItem = (Transform)Instantiate(_receiveAddressPrefab);
            listItem.parent = transform;
            listItem.localPosition = new Vector3(0, 216, 0);
            listItem.localScale = Vector3.one;
            listItem.GetComponent<ReceiveAddressController>().buySendData = (object[])obj;
        }

        private void OpenBuyLotteryItems(object obj)
        {
            Transform tipPanel = (Transform)Instantiate(_tipExchangeGifts);
            tipPanel.parent = transform;
            tipPanel.localPosition = new Vector3(0, 216, 0);
            tipPanel.localScale = Vector3.one;
            tipPanel.GetComponent<TipExchangeGifts>().ShowUI((int)obj);
        }
    }
}