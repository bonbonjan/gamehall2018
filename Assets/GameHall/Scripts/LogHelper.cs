﻿using UnityEngine;
using System.Collections;

public enum RichColor
{
    aqua=0,//水色、水蓝色 =cyan
    black,
    blue,
    brown,
    cyan,//青色=aqua
    darkblue,
    fuchsia,
    green,
    grey,
    lightblue,
    lime,
    magenta,//品红、洋红
    maroon,
    navy,
    olive,
    orange,
    purple,
    red,
    silver,
    teal,
    white,
    yellow

}
/**
 * default Debug Color
 * Debug.Log ------------ White
 * Debug.Warnning ------- Yellow
 * Debug.Error ---------- Red
 * 推荐自定义
 * aqua
 * green
 * magenta
 * orange
 * blue
 **/

/**常见log颜色用法
 * 表示游戏开始或退出 推荐magenta
 * 表示出现罕见情形  推荐 orange
 * 表示成对出现（函数的开始和结束）brown
 * 表示特别值得关注的信息 aqua
 * 表示正在调试的信息（调试完一定要换颜色或注释掉） green
 * 其它 blue
 * 
 **/


public static class LogHelper 
{
    


    public static string ColorStr(string inner, RichColor color)
    {
        
        string ret;
#if UNITY_EDITOR
        ret = "<color="+color.ToString()+">"+inner+"</color>";
#else
        ret = inner;
#endif
        //Debug.Log(color.ToString());
        return ret;
        
    }

    public static string ColorStr(string inner, string color)
    {

        string ret;
#if UNITY_EDITOR
        ret = "<color=" + color + ">" + inner + "</color>";
#else
        ret = inner;
#endif
        //Debug.Log(color.ToString());
        return ret;

    }

    //public static string Format(string format, params object[] args);
    public static string Key(object o, RichColor color = RichColor.aqua)
    {
        return ColorStr("" + o.ToString(), color);
    }
    public static string Key(object o, string color)
    {
        return ColorStr("" + o.ToString(), color);
    }

    public static string Color_NetHandle = "#00C0F0ff";
    public static string NetHandle(string str)
    {
        return ColorStr(str, Color_NetHandle);
    }

    public static string Aqua(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.aqua);
    }


    public static string Aqua(string inner)
    {
        return ColorStr(inner, RichColor.aqua);
    }

    public static string Black(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.black);
    }


    public static string Black(string inner)
    {
        return ColorStr(inner, RichColor.black);
    }

    public static string Blue(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.blue);
    }


    public static string Blue(string inner)
    {
        return ColorStr(inner, RichColor.blue);
    }

    public static string Brown(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.brown);
    }

    public static string Brown(string inner)
    {
        return ColorStr(inner, RichColor.brown);
    }

    public static string Cyan(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.cyan);
    }

    public static string Cyan(string inner)
    {
        return ColorStr(inner, RichColor.cyan);
    }

    public static string Green(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.green);
    }

    public static string Green(string inner)
    {
        return ColorStr(inner, RichColor.green);
    }
    //lightblue
    public static string Lightblue(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.lightblue);
    }

    public static string Lightblue(string inner)
    {
        return ColorStr(inner, RichColor.lightblue);
    }
    //lime
    public static string Lime(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.lime);
    }

    public static string Lime(string inner)
    {
        return ColorStr(inner, RichColor.lime);
    }

    public static string Magenta(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.magenta);
    }

    public static string Magenta(string inner)
    {
        return ColorStr(inner, RichColor.magenta);
    }
    //olive
    public static string Olive(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.olive);
    }

    public static string Olive(string inner)
    {
        return ColorStr(inner, RichColor.olive);
    }

    public static string Orange(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.orange);
    }

    public static string Orange(string inner)
    {
        return ColorStr(inner, RichColor.orange);
    }

    public static string Red(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.red);
    }

    public static string Red(string inner)
    {
        return ColorStr(inner, RichColor.red);
    }

    public static string Silver(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.silver);
    }

    public static string Silver(string inner)
    {
        return ColorStr(inner, RichColor.silver);
    }

    public static string Teal(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.teal);
    }

    public static string Teal(string inner)
    {
        return ColorStr(inner, RichColor.teal);
    }

    public static string White(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.white);
    }

    public static string White(string inner)
    {
        return ColorStr(inner, RichColor.white);
    }

    public static string Yellow(string format, params object[] args)
    {
        return ColorStr(string.Format(format, args), RichColor.yellow);
    }

    public static string Yellow(string inner)
    {
        return ColorStr(inner, RichColor.yellow);
    }






}
