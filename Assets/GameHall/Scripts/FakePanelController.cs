﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;

namespace WMCY.GameHall
{


    public class FakePanelController : MonoBehaviour
    {
        private int m_rechargeNum = 0;

        public GameObject rankButton;
        public static int rankButtonCount =10 ;
        public Transform grid;
        void Awake()
        {
            grid = this.transform.Find("Rank_bg/GameTypeButton/GameTypeButton_bg/Grid");
            
        }

        void Start()
        {
            for (int i = 0; i < rankButtonCount; i++)
            {
                //GameObject go = Instantiate(rankButton, grid.localPosition, grid.rotation) as GameObject;
                GameObject go = Instantiate(rankButton, Vector3.zero, Quaternion.identity) as GameObject;
                go.transform.parent = grid;
                go.transform.localScale = new Vector3(1,1,1);
                go.transform.localPosition = new Vector3(0,0,0);
                if (i == 0)
                {

                    System.Action act = () =>
                    {
                        Debug.Log("localScale: " + go.transform.localScale);
                        Debug.Log("position: " + go.transform.position);
                        Debug.Log("localPosition: " + go.transform.localPosition);
                    };
                    System.Action<int> act2 = (a) =>
                    {
                        Debug.Log(a);
                    };
                    act2(3);
                    act();
                    StartCoroutine(DelayCall(0.1f, () => { act2(4); }));
                }
            }

            Transform xformTitle = transform.Find("title");
            //Debug.Assert(xformTitle);
            if (xformTitle != null) xformTitle.GetComponent<Text>().text = gameObject.name;
        }

        IEnumerator DelayCall(float delay, System.Action action)
        {
            yield return  new  WaitForSeconds(delay);
            action();
        }

        void Update()
        {

        }


        public void OnBtnClick_Close()
        {
            Debug.Log(gameObject.name+">>OnBtnClick_Close:");
            gameObject.SetActive(false);
        }

        public void OnBtnClick_Sure()
        {
            gameObject.SetActive(false);

        }
    }
}