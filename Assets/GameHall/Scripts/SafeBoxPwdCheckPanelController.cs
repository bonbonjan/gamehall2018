﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;

namespace WMCY.GameHall
{
    public class SafeBoxPwdCheckPanelController : MonoBehaviour
    {
        void Awake()
        {

        }

        void Start()
        {
            NetManager.GetInstance().RegisterHandler("checkSafeBoxPwd", HandleNetMsg_PwdCheck);
        }

        void OnEnable()
        {
            //SwitchIntoEnglish();
            transform.Find("pwdItem/input").GetComponent<InputField>().text = "";
        }

        void OnDisable()
        {
            
        }

        void Update()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
                return;
            transform.Find("titleBg/title").GetComponent<Text>().text = "Order";
            transform.Find("titleBg/title").GetComponent<Text>().fontSize = 30;
            transform.Find("pwdItem/input/Placeholder").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("pwdItem/input/Placeholder").GetComponent<Text>().text = "please enter order";
            transform.Find("sureBtn/Text").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("sureBtn/Text").GetComponent<Text>().text = "Confirm";

            transform.Find("titleBg/title").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("pwdItem/input/Placeholder").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("sureBtn/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
        }

        public void OnBtnClick_Close()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            gameObject.SetActive(false);
        }

        public void OnBtnClick_Sure()
        {            
            string pwd = transform.Find("pwdItem/input").GetComponent<InputField>().text;
            if (pwd == "")
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "口令不能为空" : "Please enter the safe password");
                return;
            }
            NetManager.GetInstance().Send("gcsecurityService/checkSafeBoxPwd", new object[] { pwd });
            //SoundManager.Get().PlaySound(SoundType.Common_Click);
        }

        void HandleNetMsg_PwdCheck(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                Dictionary<string, object> temp = obj["safeBox"] as Dictionary<string, object>;
                GVars.savedGameGold = (int)temp["gameGold"];
                GVars.savedLottery = (int)temp["lottery"];
                gameObject.SetActive(false);
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_SafeBoxPanel); 
            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                Debug.Log(msgcode);
                switch (msgcode)
                {
                    case 2:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "用户名不存在" : "The account does not exist");
                        break;
                    case 3:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "口令输入错误，请联系上级找回" : "Order input error, please contact the superior to get it back");
                        break;
                    case 6:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被冻结" : "This account has been frozen");
                        break;
                    case 7:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "系统维护" : "Server maintenance");
                        break;
                    case 11:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请勿重复申请" : "Please do not repeat the application");
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "输入失败" : "input failed");
                        break;
                }
            }
        }
        
    }
}