﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ExtendMethod
{
    public static bool GetBoolValue(this Dictionary<string, object> dic, string key, bool defaultValue = false)
    {
        if (dic.ContainsKey(key))
        {
            return (bool)dic[key];
        }
        else
        {
            return defaultValue;
        }
    }

    public static T GetValue<T>(this Dictionary<string, object> dic, string key, T defaultValue)
    {
        T ret = defaultValue;
        if (dic.ContainsKey(key))
        {
            ret = (T)dic[key];
        }
        return ret;
    }

    public static int[] Multiply(this int[] array, int x)
    {
        int length = array.Length;
        int[] ret = new int[length];
        for (int i = 0; i < length; i++)
        {
            ret[i] = array[i] * x;
        }
        return ret;
    }

    public static int[] Divide(this int[] array, int x)
    {
        x = x == 0 ? 1 : x;
        int length = array.Length;
        int[] ret = new int[length];
        for (int i = 0; i < length; i++)
        {
            ret[i] = array[i] / x;
        }
        return ret;
    }

    public static string Repeat(this string str, int n)
    {
        string ret = "";
        for (int i = 0; i < n; i++)
        {
            ret += str;
        }
        return ret;
    }
}

