﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using DG.Tweening;

namespace WMCY.GameHall
{


    public class LotteryListItem : MonoBehaviour
    {
        private int m_itemId;
        //public Transform _tipExchangeGifts;
        [SerializeField]
        LotteryItemList _lotteryItemList;
        void Awake()
        {

        }

        void Start()
        {

        }

        void OnEnable()
        {
            SwitchIntoEnglish();
        }

        void OnDisable()
        {
            
        }

        void Update()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
                return;
            transform.Find("ItemNum").GetComponent<Text>().fontSize = 23;
            transform.Find("buyBtn/Text").GetComponent<Text>().fontSize = 30;

            transform.Find("ItemNum").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("buyBtn/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
        }

        /*
         * itemId : 1-9
         * */
        public void ShowUI(int itemId)
        {
            m_itemId = itemId;
            string priceName = GVars.language == "zh" ? "彩票" : "Lottery";
            transform.Find("buyBtn/Text").GetComponent<Text>().text = Convert.ToString(_lotteryItemList.list[m_itemId - 1].price) + priceName;
            transform.Find("Item").GetComponent<RawImage>().texture = Resources.Load("Icon/" + _lotteryItemList.list[m_itemId - 1].iconName) as Texture;
            transform.Find("ItemNum").GetComponent<Text>().text = GVars.language == "zh" ? _lotteryItemList.list[m_itemId - 1].zh_name : _lotteryItemList.list[m_itemId - 1].en_name;
        }

        public void OnBtnClick_Buy()
        {
            if (GVars.user.card == "-1" && UserCompleteController.UserCompleteNoPrompt == false && UserCompleteController.UserTempNoPrompt == false)
            {
                //用户尚未完善资料
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_UserCompletePanel, new ArrayList { "LotteryCity",m_itemId });
                return;
            }
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_BuyLotteryItems, m_itemId);
            //Transform tipPanel = (Transform)Instantiate(_tipExchangeGifts);
            //tipPanel.parent = transform.parent.parent.parent.parent.parent.parent;
            //tipPanel.localPosition = new Vector3(0, 0, 0);
            //tipPanel.localScale = Vector3.one;
            //tipPanel.GetComponent<TipExchangeGifts>().ShowUI(m_itemId);
        }

    }
}