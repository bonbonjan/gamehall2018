﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;

namespace WMCY.GameHall
{


    public class ReceiveAddressController : MonoBehaviour
    {
        public object[] buySendData;
        void Awake()
        {
            
        }

        void Start()
        { 
            NetManager.GetInstance().RegisterHandler("bugByLottery", HandleNetMsg_Buy);
        }

        void OnEnable()
        {
            ResetView();
            SwitchIntoEnglish();
        }

        void Update()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
                return;
            transform.Find("TelItem/itemName").GetComponent<Text>().text = "Recipient number:";
            transform.Find("TelItem/itemName").GetComponent<Text>().fontSize = 33;
            transform.Find("TelItem/itemName").localPosition = new Vector3(-177.95f, 145, 0);
            transform.Find("TelItem/input").GetComponent<RectTransform>().sizeDelta = new Vector2(500,80);
            transform.Find("TelItem/input").localPosition = new Vector3(409.4f, 145, 0);
            transform.Find("TelItem/input/Placeholder").GetComponent<Text>().text = "5-20 numbers";
            transform.Find("addressItem/itemName").GetComponent<Text>().text = "Shipping address:";
            transform.Find("addressItem/itemName").GetComponent<Text>().fontSize = 33;
            transform.Find("addressItem/itemName").localPosition = new Vector3(-177.95f, 145, 0);
            transform.Find("addressItem/input").GetComponent<RectTransform>().sizeDelta = new Vector2(500, 184.7f);
            transform.Find("addressItem/input").localPosition = new Vector3(409.4f, 185, 0);
            transform.Find("addressItem/input/Placeholder").GetComponent<Text>().text = "10-100 characters";
            transform.Find("sureBtn/Text").GetComponent<Text>().text = "Submit";

            transform.Find("TelItem/itemName").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("TelItem/input").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("TelItem/Placeholder").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("addressItem/itemName").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("addressItem/input").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("addressItem/input/Placeholder").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("sureBtn/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
        }

        void ResetView()
        {
            transform.Find("TelItem/input").GetComponent<InputField>().text = "";
            transform.Find("addressItem/input").GetComponent<InputField>().text = "";
        }

        public void OnBtnClick_Close()
        {
            gameObject.SetActive(false);
        }

        public void OnBtnClick_Sure()
        {
            string telNum = transform.Find("TelItem/input").GetComponent<InputField>().text;
            string address = transform.Find("addressItem/input").GetComponent<InputField>().text;
            if (telNum.Length <5)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入5-15手机号码" : "Wrong phone number");
                return;
            }
            if (address.Length < 10)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "收货地址输入错误" : "Wrong shipping address");
                return;
            }
            Debug.Log(""+buySendData[0] + buySendData[1] + buySendData[2]);
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            NetManager.GetInstance().Send("gcshopService/bugByLottery", new object[] {buySendData[0],buySendData[1],buySendData[2], telNum, address });
        }

        void HandleNetMsg_Buy(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            Debug.Log(obj);
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                Destroy(transform.gameObject);
                GVars.user.lottery = (int)obj["lottery"];
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_GoldAndLottery);
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "发送成功（兑奖申请正在处理，稍后将提示您处理结果）" : "Send Successful！Your exchange application is being processed,later we will prompt you the result");
            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                Debug.Log(msgcode);
                switch (msgcode)
                {
                    case 0:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "程序异常" : "Program exception");
                        break;
                    case 2:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号或密码错误" : "Account name or password error, please enter again");
                        break;
                    case 3:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码不正确" : "password error, please enter again");
                        break;
                    case 6:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被冻结" : "This account has been frozen");
                        break;
                    case 7:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "系统维护" : "Server maintenance");
                        break;
                    case 11:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请勿重复申请" : "Please do not repeat the application");
                        break;
                    case 12:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "余额不足" : "Coin shortage");
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "购买失败" : "Purchase failed");
                        break;
                }
            }
        }
    }
}