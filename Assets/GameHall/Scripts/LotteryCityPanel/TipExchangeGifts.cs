﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using DG.Tweening;

namespace WMCY.GameHall
{


    public class TipExchangeGifts : MonoBehaviour
    {
        private int m_itemId;
        private int m_buyCount = 0;
        private string m_pwd = "";
        [SerializeField]
        LotteryItemList _lotteryItemList;
        void Awake()
        {
        }

        void OnEnable()
        {
            SwitchIntoEnglish();
        }

        void Start()
        {
            NetManager.GetInstance().RegisterHandler("checkUserPwd", HandleNetMsg_CheckUserPwd);
        }

        void Update()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
                return;
            transform.Find("btnSure/Text").GetComponent<Text>().text = "Buy";
            transform.Find("tip").GetComponent<Text>().fontSize = 27;
            transform.Find("InputPassword/Placeholder").GetComponent<Text>().text = "Enter password";

            transform.Find("btnSure/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("tip").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("InputPassword/Placeholder").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
        }

        /*
         * itemId: 1-9
         * */
        public void ShowUI(int itemId)
        {
            m_itemId = itemId;
            m_buyCount = _lotteryItemList.list[m_itemId - 1].price > GVars.user.lottery ? 0 : 1;
            UpdateBuycount();
            UpdateTopTip();
        }

        public void OnBtnClick_Sure()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            if (_lotteryItemList.list[m_itemId - 1].price > GVars.user.lottery)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "彩票不足" : "Insufficient number of lottery tickets");
                return;
            }
            m_pwd = transform.Find("InputPassword").GetComponent<InputField>().text;
            if (m_buyCount == 0)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入正确的兑换数量" : "Illegal number");
                return;
            }
            else if (m_pwd == "")
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入密码" : "Please enter password");
                return;
            }
            
            NetManager.GetInstance().Send("gcshopService/checkUserPwd", new object[] { m_pwd });
        }

        public void OnBtnClick_Cancel()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            Destroy(transform.gameObject);

        }

        public void OnBtnClick_Add()
        {
            SoundManager.Get().PlaySound(SoundType.Click_PlusMinus);
            if ((m_buyCount + 1) * _lotteryItemList.list[m_itemId - 1].price > GVars.user.lottery)
                m_buyCount = _lotteryItemList.list[m_itemId - 1].price > GVars.user.lottery ? 0 : 1;
            else
                m_buyCount += 1;
            UpdateBuycount();
        }

        public void OnBtnClick_Reduce()
        {
            if (m_buyCount <= 0)
            {
                m_buyCount = 0;
            }
            else if (m_buyCount == 1 )
            {
                m_buyCount = Convert.ToInt32(GVars.user.lottery / _lotteryItemList.list[m_itemId - 1].price);
            }
            else
            {
                m_buyCount -= 1;
            }
            SoundManager.Get().PlaySound(SoundType.Click_PlusMinus);
            UpdateBuycount();
        }

        void UpdateBuycount()
        {
            transform.Find("InputBuyCount").GetComponent<InputField>().text = m_buyCount.ToString();
        }

        public void UpdateTopTip()
        {
            string num = transform.Find("InputBuyCount").GetComponent<InputField>().text;
            if (num.Contains("-"))
            {
                //AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "非法数字" : "Illegal number");
                m_buyCount = Convert.ToInt32(GVars.user.lottery / _lotteryItemList.list[m_itemId - 1].price);
            }
            else
            {
                m_buyCount = Convert.ToInt32(num);
            }
            if (m_buyCount * _lotteryItemList.list[m_itemId - 1].price > GVars.user.lottery)
            {
                m_buyCount = Convert.ToInt32(GVars.user.lottery / _lotteryItemList.list[m_itemId - 1].price);
            }
            else if (m_buyCount < 0)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "非法数字" : "Illegal number");
                m_buyCount = Convert.ToInt32(GVars.user.lottery / _lotteryItemList.list[m_itemId - 1].price);
            }
            UpdateBuycount();
            transform.Find("tip").GetComponent<Text>().text = string.Format(GVars.language == "zh" ? "确认花费<size=38><color=#E4C751>{0}彩票</color></size>购买{1}么？" : "Confirm pay {0} lotteries for {1}?", m_buyCount * _lotteryItemList.list[m_itemId - 1].price, GVars.language == "zh" ? _lotteryItemList.list[m_itemId - 1].zh_name : _lotteryItemList.list[m_itemId - 1].en_name);
        }

        void HandleNetMsg_CheckUserPwd(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            Debug.Log(obj);
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                bool isCorrect = (bool)obj["isCorrect"];
                if (isCorrect)
                {
                    Destroy(transform.gameObject);
                    AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_ReveiveAddressPanel, new object[] { m_itemId+11, m_buyCount, m_pwd });
                }
                else
                {
                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码不正确" : "Incorrect password");
                }
            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                switch (msgcode)
                {
                    case 2:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号或密码错误" : "Account name or password error, please enter again");
                        break;
                    case 3:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码不正确" : "Password error, please enter again");
                        break;
                    case 6:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被冻结" : "This account has been frozen");
                        break;
                    case 7:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "系统维护" : "Server maintenance");
                        break;
                    case 11:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请勿重复申请" : "Please do not repeat the application");
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "失败" : "failed");
                        break;
                }
            }
        }

    }
}