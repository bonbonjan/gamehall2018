﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class LotteryItem {
    public string zh_name;
    public string en_name;
    public int price;
    public string iconName;
}

[CreateAssetMenu()]
public class LotteryItemList : ScriptableObject
{
    public List<LotteryItem> list;
}