﻿using UnityEngine;
using System.Collections;

public class RotateAni : MonoBehaviour {

    public float speed = 1f;
    public bool disperse = false;

    void Start () {
	
	}
	
	void Update () {
        transform.Rotate(Vector3.forward * speed * Time.deltaTime);
	}
}
