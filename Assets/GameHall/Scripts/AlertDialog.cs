﻿using UnityEngine;
using System.Collections;

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;

namespace WMCY.GameHall
{
    public enum AlertType
    {
        Confirm,
        OkCancel
    }

    public class AlertDialog : MB_Singleton<AlertDialog>
    {
        [SerializeField]
        private GameObject m_goContainer;
        [SerializeField]
        private GameObject m_goBtnOkCancelPanel;
        [SerializeField]
        private GameObject m_goBtnConfirm;
        [SerializeField]
        private Text m_textContent;
        private System.Action _onOkCallback;

        public bool isTouchForbidden = false;
        void Awake()
        {

            if (_instance == null)
            {
                SetInstance(this);
                PreInit();
            }
        }

        void Start()
        {
            Init();
        }

        public void PreInit()
        {
            if (m_goContainer == null)
            {
                m_goContainer = gameObject;
            }
        }

        public void Init()
        {
        }

        public void ShowDialog(string content, bool showOkCancel = false, System.Action callback = null)
        {
            SoundManager.Get().PlaySound(SoundType.Alert_Hint);
            Debug.Log("ShowDialog: " + content);
            float delay = 0f;
            if (m_goContainer.activeSelf)//如果当前存在弹窗
            {
                delay = 0.1f;
                Hide();
                if (m_goBtnConfirm.activeSelf)//确认按钮的响应事件必然执行
                {
                    if (_onOkCallback != null) _onOkCallback();
                }

            }
            AppRoot.GetInstance().StartCoroutine(Utils.DelayCall(delay, () =>
            {
                _showDialog(content, showOkCancel, callback);
            }));

        }

        private void _showDialog(string content, bool showOkCancel = false, System.Action callback = null)
        {
            m_textContent.font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;

            m_textContent.text = content;
            _onOkCallback = callback;
            m_goBtnOkCancelPanel.SetActive(showOkCancel);
            m_goBtnConfirm.SetActive(!showOkCancel);
            m_goContainer.SetActive(true);
        }

        public void Show()
        {
            m_goContainer.SetActive(true);
        }

        public void Hide()
        {
            m_goContainer.SetActive(false);
        }

        public void OnBtnOK_Click()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            Hide();

            if (_onOkCallback != null)
                _onOkCallback();
        }

        public void OnBtnCancel_Click()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            Hide();
        }

        public void OnBtnConfirm_Click()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            Hide();
            if (_onOkCallback != null)
                _onOkCallback();
        }

        public void OnBtnClose_Click()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            Hide();
        }

        public bool IsShow()
        {
            return m_goContainer.activeSelf;
        }
    }
}

