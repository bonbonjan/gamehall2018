﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;

namespace WMCY.GameHall
{


    public class CashOperatePanelController : MonoBehaviour
    {
        private float m_remaintime;//初始2分钟
        private Text m_textMinute;
        private Text m_textSecond;
        void Awake()
        {

        }

        void Start()
        {
            NetManager.GetInstance().RegisterHandler("excharge", HandleNetMsg_Excharge);
            m_textMinute = transform.Find("remaintimeItem/minute/num").GetComponent<Text>();
            m_textSecond = transform.Find("remaintimeItem/second/num").GetComponent<Text>();
        }

        void OnEnable()
        {
            ResetView();
            //SwitchIntoEnglish();
        }

        void Update()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
                return;
            transform.Find("titleBg/title").GetComponent<Text>().text = "Exchange";
            transform.Find("remaintimeItem/itemName").GetComponent<Text>().text = "countdown:";
            transform.Find("remaintimeItem/itemName").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("remaintimeItem/itemName").GetComponent<Text>().fontSize = 45;
            transform.Find("cashItem/itemName").GetComponent<Text>().text = "Amount:";
            transform.Find("cashItem/itemName").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("cashItem/itemName").GetComponent<Text>().fontSize = 45;
            transform.Find("password/itemName").GetComponent<Text>().text = "Enter password:";
            transform.Find("password/itemName").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("password/itemName").GetComponent<Text>().fontSize = 40;
            transform.Find("password/input/Placeholder").GetComponent<Text>().text = "Enter password";
            transform.Find("password/input/Placeholder").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("password/input/Placeholder").GetComponent<Text>().fontSize = 30;
            transform.Find("sureBtn/Text").GetComponent<Text>().text = "Confirm";

        }

        void FixedUpdate()
        {
            if (m_remaintime > 0)
            {
                m_remaintime -= Time.deltaTime;
                if (m_remaintime < 0)
                {
                    m_remaintime = 0;
                }
                m_textMinute.text = string.Format("{0:D2}", Mathf.FloorToInt(m_remaintime / 60));
                m_textSecond.text = string.Format("{0:D2}", Mathf.FloorToInt(m_remaintime % 60));
            }
            
        }

        void ResetView()
        {
            transform.Find("password/input").GetComponent<InputField>().text = "";
            m_remaintime = 120;
        }

        public void OnBtnClick_Close()
        {
            gameObject.SetActive(false);
        }

        public void OnBtnClick_Sure()
        {
            string num = transform.Find("cashItem/num").GetComponent<InputField>().text;
            string pwd = transform.Find("password/input").GetComponent<InputField>().text;
            if (pwd == "")
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入密码" : "Please enter password");
                return;
            }
            if (m_remaintime <= 0)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "操作超时" : "time out");
                return;
            }
            NetManager.GetInstance().Send("gcuserService/excharge", new object[] { Convert.ToInt32(num), pwd });
        }

        void HandleNetMsg_Excharge(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            Debug.Log(obj);
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "兑换成功" : "Cash success");
            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                Debug.Log(msgcode);
                switch (msgcode)
                {
                    case 2:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号或密码错误" : "Account name or password error, please enter again");
                        break;
                    case 3:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码错误" : "password error, please enter again");
                        break;
                    case 6:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被冻结" : "This account has been frozen");
                        break;
                    case 7:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "系统维护" : "Server maintenance");
                        break;
                    case 11:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请勿重复申请" : "Please do not repeat the application");
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "兑换失败" : "Cash failed");
                        break;
                }
            }
        }
    }
}