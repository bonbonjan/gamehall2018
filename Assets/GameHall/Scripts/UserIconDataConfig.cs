﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace WMCY.GameHall
{
    [System.Serializable]
    public class UserIconData
    {
        public Sprite sprite;

    }



    [CreateAssetMenu()]
    public class UserIconDataConfig : ScriptableObject
    {
        public List<UserIconData> list;
    }
}

