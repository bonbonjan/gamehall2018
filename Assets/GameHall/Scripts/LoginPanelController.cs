﻿/*
 *  author: jiangwen
 *  creation date: 2016/3/28
 *  modified by: jiangwen
 *  modified date: 2016/4/28
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using System;

namespace WMCY.GameHall
{
    public class LoginPanelController : MonoBehaviour
    {
        [SerializeField]
        private InputField m_inputName;
        [SerializeField]
        private InputField m_inputPwd;
        [SerializeField]
        private Toggle m_toggleRemeberUserName;
        private bool m_hasInited = false;

#if UNITY_ANDROID
        public AndroidJavaClass jc;//调用java工程
        public AndroidJavaObject jo;
#endif
        void Start()
        {
            Init();
        }

        public void Init()
        {
            if (PlayerPrefs.GetInt("isON") == 0)
            {
                m_toggleRemeberUserName.isOn = true;
            }
            else
            {
                m_toggleRemeberUserName.isOn = false;
            }

            if (m_toggleRemeberUserName.isOn)
            {
                m_inputName.text = PlayerPrefs.GetString("userName");
            }
            AppManager.Get().RefreshLocalization();
            if (m_hasInited) return;
            m_hasInited = true;
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_FreshDealer, -1);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Fresh_LoginUserName, this, SetUserName);
#if UNITY_ANDROID && !UNITY_EDITOR
		jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
#endif
            NetManager.GetInstance().RegisterHandler("login", HandleNetMsg_Login);
            NetManager.GetInstance().RegisterHandler("visitorLogin", HandleNetMsg_Visitor);
            NetManager.GetInstance().RegisterHandler("bonusRainStart", HandleNetMsg_BonusRainStart);
            NetManager.GetInstance().RegisterHandler("bonusRainStop", HandleNetMsg_BonusRainStop);

            

            if (GVars.isStartedFromGame)
            {
                //AppManager.Get().Notify(UIGameMsgType.UINotify_Allow_LoadingFinish);
            }
            else if (GVars.isStartedFromLuaGame)
            {

            }
            else
            {
                Debug.Log("UINotify_Allow_LoadingFinish");
                AppManager.Get().StartCoroutine(Utils.DelayCall(0.1f, () =>
                {
                    AppManager.Get().Notify(UIGameMsgType.UINotify_Allow_LoadingFinish);
                }));

            }
        }

        void OnEnable()
        {
            AppManager.Get().RefreshLocalization();
        }
        public void ToggleRemberUserName(Toggle t)
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            Debug.Log("Toggle t.isON: " + t.isOn);
            PlayerPrefs.SetInt("isON", t.isOn ? 0 : 1);
            PlayerPrefs.Save();
        }

        public void OnBtnClick_Login()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            //NetManager.GetInstance().Send("gcuserService/login", new object[] { "jiangwen003", "qqq111" });//测试
            //return;

            if (InputCheck.CheckLoginUserName(m_inputName.text))
            {
                if (InputCheck.CheckLoginPassWord(m_inputPwd.text))
                {
                    GVars.username = m_inputName.text.Trim();
                    GVars.pwd = m_inputPwd.text.Trim();
                    if (AppManager.Get().CheckStartConnectAndLogin(true))
                    {
                        return;
                    }
                    //NetManager.GetInstance().Send("gcuserService/login", new object[] { m_inputName.text, m_inputPwd.text });
                    AppManager.Get().Send_Login();
                }
            }
        }

        public void OnBtnClick_ForgetPassword()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            AppManager.GetInstance().GetPanel("RecoverPasswordPanel").SetActive(true);
            this.gameObject.SetActive(false);
            //m_recoverPanel.SetActive(true);
        }

        //游客登录
        public void OnBtnClick_VisitorLogin()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            Debug.Log("游客登录按钮点击");
            string username = "";
            string nickname = "";
#if UNITY_ANDROID && !UNITY_EDITOR       
            username = jo.Call<string>("GetPhoneSerial");
#endif
#if UNITY_IOS && !UNITY_EDITOR
            //username =IOSGameStart.GetSingleton().Mserials;
            username = IOS_Agent.GetPhoneSerials();
#endif

            YouKePlayer.YouKePlayerName youke = new YouKePlayer.YouKePlayerName();
            username = youke.GetYouKeName(username);

            Debug.Log("GetPhoneSerial: " + username);
            char[] c = username.ToCharArray();
            string user = "";
            for (int i = 0; i < username.Length; i++)
            {
                if (c[i] >= '0' && c[i] <= '9')
                {
                    //Debug.Log("c: " + i + " " + c[i]);
                    user += c[i].ToString();
                }
            }



#if UNITY_ANDROID && !UNITY_EDITOR
             nickname = jo.Call<string>("GetPhoneModel");
#endif
#if UNITY_IOS && !UNITY_EDITOR
            nickname = IOS_Agent.GetPhoneModel();
#endif
			Debug.Log("nickname: "+nickname);
            if (nickname.Length > 10)
            {
                nickname = nickname.Substring(0, 9);
            }

            char sex = UnityEngine.Random.Range(0, 1) == 0 ? '男' : '女';
            string password = "";

            int photoId = UnityEngine.Random.Range(1, 2);
            if (sex == '男')
                photoId = UnityEngine.Random.Range(3, 4);


            User vistor = new User();
            string prefix = GVars.language.Equals("zh") ? GVars.touristAccountPrefix_cn : GVars.touristAccountPrefix_en;
            //vistor.username = prefix + user;
            vistor.username = user;
            vistor.nickname = nickname;
            vistor.phone = "游客电话号码";
            vistor.password = "";
            vistor.password = GVars.touristPwd;
            // vistor.sex = "男";
            // vistor.photoId = 2;
            vistor.type = 1;
            vistor.promoterUsername = "admin";
#if UNITY_EDITOR
            vistor.username = "01234567";
            vistor.nickname = "电脑游客";
#endif
            GVars.username = vistor.username;
            GVars.pwd = vistor.password;
            GVars.nickname = vistor.nickname;
            GVars.isTourist = true;
            if (AppManager.Get().CheckStartConnectAndLogin(true))
            {
                return;
            }
            //NetManager.GetInstance().Send("gcuserService/visitorLogin", new object[] { vistor.username, vistor.nickname });
            AppManager.Get().Send_Login(true);
        }

        //注册成功后，添加用户名
        public void SetUserName(object obj)
        {
            m_inputName.text = obj.ToString();
            m_inputPwd.text = "";
        }

        //点击注册按钮
        public void OnBtnClick_Register()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            AppManager.GetInstance().GetPanel("RegisterPanel").SetActive(true);
            this.gameObject.SetActive(false);
        }

        public void OnBtnClick_SwitchLanguage()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            if (GVars.language == "zh")
            {
                GVars.language = "en";
            }
            else
            {
                GVars.language = "zh";
            }
            Debug.Log("lan: " + GVars.language);
            SwitchLang(GVars.language);
        }

        private void SwitchLang(string lang)
        {
            //if (lan == "en")
            //{
            //    this.transform.Find("bg2/Text").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            //    this.transform.Find("bg2/Text").GetComponent<Text>().text = "UserID:";
            //}
            //else
            //{
            //    this.transform.Find("bg2/Text").GetComponent<Text>().text = "账号：";
            GVars.language = lang;
            I2Localization.SetCurLanguage(lang);
            AppManager.Get().RefreshLocalization();
        }

        public void ShowAnnouce()
        {
            NetManager.GetInstance().Send("gcuserService/getNotice", new object[] { });
            AppManager.GetInstance().GetPanel("AnnouncementPanel").SetActive(true);
        }

        public void HandleNetMsg_Login(object[] objs)
        {
            AppManager.Get().RefreshLocalization();
#if UNITY_EDITOR
            Debug.Log(LogHelper.Magenta("HandleNetMsg_Login@: " + Time.time));
#endif
#if !UNITY_EDITOR
            Debug.Log(LogHelper.NetHandle("HandleNetMsg_Login"));
#endif
            AppManager.Get().Handle_RecLoginOrVisitorLogin();
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            bool isLogin = (bool)obj["isLogin"];

            if (isLogin)
            {
                if (m_toggleRemeberUserName.isOn)
                {
                    PlayerPrefs.SetString("userName", m_inputName.text.Trim());
                }
                else
                {
                    PlayerPrefs.SetString("userName", "");
                    m_inputName.text = "";
                }
                PlayerPrefs.Save();

                m_inputPwd.text = "";//清空密码
                GVars.user = User.CreateWithDic(obj["user"] as Dictionary<string, object>);
                GVars.payMode = (int)obj["payMode"];

                gameObject.SetActive(false);
                AppManager.Get().ShowMainPanel();
                AppManager.Get().Notify(UIGameMsgType.UINotify_Allow_LoadingFinish);
                AppManager.Get().Notify(UIGameMsgType.UINotify_Fresh_UseMailSign, 0);//隐藏邮件标志

                //ShowAnnouce();
                AppManager.Get().isShowAnnouce = false;

                UserCompleteController.UserCompleteNoPrompt = false;//重置完善信息跳过的标志位

                object gameRunStatus = (object)obj["gameRunStatus"];
                InnerGameManager.Get().UpdateGameRunStatus(gameRunStatus);

                //接收道具信息
                GVars.ownedProps.Clear();
                object[] propObjs;
                propObjs = obj.ContainsKey("prop") ? (object[])obj["prop"] : new object[] { };
                for (int i = 0; i < propObjs.Length; i++)
                {
                    Dictionary<string, object> propObj = propObjs[i] as Dictionary<string, object>;
                    GVars.ownedProps.Add(Convert.ToInt32(propObj["propId"]), new ownShopProp(Convert.ToInt32(propObj["propId"]), Convert.ToInt64(propObj["remainTime"]), (bool)propObj["show"]));
                }

                for (int i = 9; i < 12; i++)
                {
                    if (GVars.ownedProps.ContainsKey(i) == true)
                    {
                        AppManager.GetInstance().Notify(UIGameMsgType.UINotify_FreshDealer, i - 8);
                        break;
                    }
                    else if (i == 11)
                    {
                        AppManager.GetInstance().Notify(UIGameMsgType.UINotify_FreshDealer, 0);
                    }
                }
                //Debug.Log("GVars.user.username: " + GVars.user.username);
                //Debug.Log("GVars.user.nickname: " + GVars.user.nickname);
                //Debug.Log("GVars.user.photoId: " + GVars.user.photoId);
                //Debug.Log("GVars.user.phone: " + GVars.user.phone);
                //Debug.Log("GVars.user.password: " + GVars.user.password);
                //Debug.Log("GVars.user.card: " + GVars.user.card);
                //Debug.Log("GVars.user.registDate: " + GVars.user.registDate);
                //Debug.Log("GVars.user.status: " + GVars.user.status);
                //Debug.Log("GVars.user.overflow: " + GVars.user.overflow);
                //Debug.Log("GVars.user.gameGold: " + GVars.user.gameGold);
                //Debug.Log("GVars.user.lottery: " + GVars.user.lottery);

                NetManager.GetInstance().isLogined = true;

                int accountProtectionState = (int)obj["accountProtectionState"];
                if (accountProtectionState == 0)//已填写号码 
                {
                    AppManager.Get().ShowNotice();
                }
                else if (accountProtectionState == 1)
                {
                    AppManager.Get().ShowProperSecurity();
                    AppManager.Get().isShowAnnouce = true;
                }
                else
                {
                    AppManager.Get().ShowNotice();
                }
                bool isShow = !(accountProtectionState == 0);
                AppManager.Get().Notify(UIGameMsgType.UINotify_Fresh_ProperSecuritySign, isShow);

                //if (_coKeepHeart != null) AppManager.Get().StopCoroutine(_coKeepHeart);
                //_coKeepHeart = AppManager.Get().StartCoroutine(NetManager.GetInstance().KeepHeart());
                //NetManager.GetInstance().connectCount = 0;
            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                switch (msgcode)
                {
                    case 2:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号或密码错误" : "Account name or password error, please enter again");
                        break;
                    case 3:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号或密码错误" : "Account name or password error, please enter again");
                        break;
                    case 6:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被冻结" : "This account has been frozen");
                        break;
                    case 7:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "系统维护" : "Server maintenance", false, () => { this.gameObject.SetActive(true); });
                        break;
                    case 16:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "网络不稳定" : "Network instability");
                        break;
                    //case 21:
                    //    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号暴机" : "");
                    //    break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "登录失败" : "Login failed");
                        break;
                }
                AppManager.Get().Handle_LoginFailed();
            }

        }

        public void HandleNetMsg_BonusRainStart(object[] objs)
        {
            transform.parent.Find("RedBonusRain").gameObject.SetActive(true);
            RedBonusControl._redBonusControl.redBonusStart();
        }

        public void HandleNetMsg_BonusRainStop(object[] objs)
        {
            if (transform.parent.Find("RedBonusRain").gameObject.activeSelf)
            {
                RedBonusControl._redBonusControl.redBonusStop();
            }

        }

        public void HandleNetMsg_Visitor(object[] objs)
        {
#if UNITY_EDITOR
            Debug.Log(LogHelper.Magenta("HandleNetMsg_Visitor@: " + Time.time));
#endif
#if !UNITY_EDITOR
            Debug.Log(LogHelper.NetHandle("HandleNetMsg_Visitor"));
#endif
            AppManager.Get().Handle_RecLoginOrVisitorLogin();
            Debug.Log("游客登录了11111");
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            bool isLogin = (bool)obj["isLogin"];
            if (isLogin)
            {
                Debug.Log("游客登录了");
                gameObject.SetActive(false);
                GVars.user = User.CreateWithDic(obj["user"] as Dictionary<string, object>);
                GVars.payMode = (int)obj["payMode"];

                m_inputPwd.text = "";//清空密码

                AppManager.GetInstance().ShowMainPanel();
                AppManager.Get().Notify(UIGameMsgType.UINotify_Allow_LoadingFinish);
                ShowAnnouce();

                object gameRunStatus = (object)obj["gameRunStatus"];
                InnerGameManager.Get().UpdateGameRunStatus(gameRunStatus);

                GVars.ownedProps.Clear();

                for (int i = 9; i < 12; i++)
                {
                    if (GVars.ownedProps.ContainsKey(i) == true)
                    {
                        AppManager.GetInstance().Notify(UIGameMsgType.UINotify_FreshDealer, i - 8);
                        break;
                    }
                    else if (i == 11)
                    {
                        AppManager.GetInstance().Notify(UIGameMsgType.UINotify_FreshDealer, 0);
                    }
                }

                NetManager.GetInstance().isLogined = true;
                //if (_coKeepHeart != null) AppManager.Get().StopCoroutine(_coKeepHeart);
                //_coKeepHeart = AppManager.Get().StartCoroutine(NetManager.GetInstance().KeepHeart());
                //NetManager.GetInstance().connectCount = 0;

                Debug.Log("GVars.user.username: " + GVars.user.username);
                Debug.Log("GVars.user.nickname: " + GVars.user.nickname);
                Debug.Log("GVars.user.photoId: " + GVars.user.photoId);
                Debug.Log("GVars.user.phone: " + GVars.user.phone);
                Debug.Log("GVars.user.password: " + GVars.user.password);
                Debug.Log("GVars.user.card: " + GVars.user.card);
                Debug.Log("GVars.user.registDate: " + GVars.user.registDate);
                Debug.Log("GVars.user.status: " + GVars.user.status);
                Debug.Log("GVars.user.overflow: " + GVars.user.overflow);
                Debug.Log("GVars.user.gameGold: " + GVars.user.gameGold);
            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                switch (msgcode)
                {
                    case 2:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号或密码错误" : "Account name or password error, please enter again");
                        break;
                    case 3:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号或密码错误" : "Account name or password error, please enter again");
                        break;
                    case 6:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被冻结" : "This account has been frozen");
                        break;
                    case 7:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "系统维护" : "Server maintenance");
                        break;
                    case 16:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "网络不稳定" : "Network instability");
                        break;
                    case -2:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游客登录功能未开启" : "Tourists landing function yet open");
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "登录失败" : "Login failed");
                        break;
                }
                AppManager.Get().Handle_LoginFailed();
            }

        }

        //子游戏返回大厅游戏界面时，执行
        public IEnumerator DirectLogin()
        {
            Debug.Log("DirectLogin");
            yield return null;
            while (!NetManager.Get().isReady)
            {
                yield return null;
            }

            bool isTourist = false;//是否游客
            if (GVars.pwd.Equals(GVars.touristPwd) && GVars.username.StartsWith(GVars.language.Equals("zh") ? GVars.touristAccountPrefix_cn : GVars.touristAccountPrefix_en))
            {
                isTourist = true;
            }

            //if (isTourist)
            //{
            //    NetManager.Get().Send("gcuserService/visitorLogin", new object[] { GVars.username, GVars.pwd });
            //}
            //else
            //{
            //    NetManager.Get().Send("gcuserService/login", new object[] { GVars.username, GVars.pwd });
            //}

            AppManager.Get().Send_Login(isTourist);

            yield break;
        }
    }

}
