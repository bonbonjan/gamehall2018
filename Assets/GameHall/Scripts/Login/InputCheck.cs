﻿using UnityEngine;
using System.Collections;
using System;
using System.Text.RegularExpressions;

///输入检查类

namespace WMCY.GameHall
{
    public static class InputCheck  {

        public static bool CheckUserName(string UserName)
        {
            if(UserName.CompareTo("")==0)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游戏账号必须是5-15个字符" : "Account must be 5-15 characters");
                return false;
            }
            else if(_calculateCharCount(UserName) > 15|| _calculateCharCount(UserName)<5)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游戏账号必须是5-15个字符" : "Account must be 5-15 characters");
                return false;
            }
            else
                {
                    for(int i=0;i<UserName.Length;i++)
                    {
                        if (_isDigits(UserName[i]) || _isCharacter(UserName[i]) || _isUnderline(UserName[i]))
                        {                           
                            continue;
                        }
                        else
                        {
                            AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游戏账号必须使用字母、数字或下划线" : "The User ID must use letters,numbers or underline");
                            return false;
                        }
                    }
                }
            return true;
        }
        public static bool CheckPresentUserName(string UserName)
        {
            if (_calculateCharCount(UserName) > 15 || _calculateCharCount(UserName) < 1)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游戏账号必须是5-15个字符" : "Account must be 5-15 characters");
                return false;
            }
            else
            {
                for (int i = 0; i < UserName.Length; i++)
                {
                    if (_isDigits(UserName[i]) || _isCharacter(UserName[i]) || _isUnderline(UserName[i]))
                    {
                        continue;
                    }
                    else
                    {
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游戏账号必须使用字母、数字或下划线" : "The User ID must use letters,numbers or underline");
                        return false;
                    }
                }
            }
            return true;
        }

        public static bool CheckLoginUserName(string UserName)
        {
            if (UserName.CompareTo("") == 0)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "用户名不可为空" : "Please enter your user ID");
                return false;
            }
            
            return true;
        }

        public static bool CheckPassWord(string PassWord)
        {
            if (PassWord == "")
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入游戏密码" : "Please enter the password");
                return false;
            }
            else if (_calculateCharCount(PassWord) > 16 || _calculateCharCount(PassWord) < 6)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游戏密码长度要求6-16位" : "Password length requirements 6-16");
                return false;
            }
            else
            {
                for (int i = 0; i < PassWord.Length; i++)
                {
                    if (_isDigits(PassWord[i]) || _isCharacter(PassWord[i]))
                    {
                        continue;
                    }
                    else
                    {
                        Debug.Log("PassWord: " + PassWord);
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游戏密码只能输入字母和数字" : "Password can only enter letters and numbers");
                        return false;
                    }
                }
                int Digits = 0;
                int Character = 0;
                for (int i = 0; i < PassWord.Length; i++)
                {
                    if (_isDigits(PassWord[i]))
                    {
                        Digits++;
                    }
                    if (_isCharacter(PassWord[i]))
                    {
                        Character++;
                    }
                    if (Digits == PassWord.Length)
                    {
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码不可全为数字" : "The password can not be all numbers");
                        return false;
                    }
                    if (Character == PassWord.Length)
                    {
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码不可全为字母" : "The password can not be all letters");
                        return false;
                    }
                }
            }
            return true;
        }


        public static bool CheckReceivePassWord(string PassWord)
        {
            if (PassWord == "")
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码为空" : "Please enter the password");
                return false;
            }
            else if (_calculateCharCount(PassWord) > 16 || _calculateCharCount(PassWord) < 6)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码格式错误" : "Password malformed");
                return false;
            }
            else
            {
                for (int i = 0; i < PassWord.Length; i++)
                {
                    if (_isDigits(PassWord[i]) || _isCharacter(PassWord[i]))
                    {
                        continue;
                    }
                    else
                    {
                        Debug.Log("PassWord: " + PassWord);
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码格式输入错误" : "Password malformed");
                        return false;
                    }
                }
                int Digits = 0;
                int Character = 0;
                for (int i = 0; i < PassWord.Length; i++)
                {
                    if (_isDigits(PassWord[i]))
                    {
                        Digits++;
                    }
                    if (_isCharacter(PassWord[i]))
                    {
                        Character++;
                    }
                    if (Digits == PassWord.Length || Character == PassWord.Length)
                    {
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码不可全为数字或字母" : "The password cannot be composed of numbers all/nThe password cannot be composed of letters all");
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// 20160808 修改在登录时的密码判断（登录时只检测密码是否为空，以及是否是异常符号）
        /// </summary>
        /// <param name="PassWord"></param>
        /// <returns></returns>
        public static bool CheckLoginPassWord(string PassWord)
        {
            if (PassWord == "")
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码不可为空" : "Please enter the password");
                return false;
            }
            //else if (_calculateCharCount(PassWord) > 16 || _calculateCharCount(PassWord) < 6)
            //{
            //    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码错误" : "");
            //    return false;
            //}
            else
            {
                for (int i = 0; i < PassWord.Length; i++)
                {
                    if (_isDigits(PassWord[i]) || _isCharacter(PassWord[i]))
                    {
                        continue;
                    }
                    else
                    {
                        Debug.Log("PassWord: " + PassWord);
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码错误" : "password error");
                        return false;
                    }
                }
                //int Digits = 0;
                //int Character = 0;
                //for (int i = 0; i < PassWord.Length; i++)
                //{
                //    if (_isDigits(PassWord[i]))
                //    {
                //        Digits++;
                //    }
                //    if (_isCharacter(PassWord[i]))
                //    {
                //        Character++;
                //    }
                //    if (Digits == PassWord.Length || Character == PassWord.Length)
                //    {
                //        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码错误" : "");
                //        return false;
                //    }
                //}
            }
            return true;
        }

        public static bool CheckChangeNickName(string NickName,Action a)
        {
            if (NickName == "")
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "昵称不可为空" : "Please enter the NickName", false, a);
                return false;
            }
            else if (_calculateCharCount(NickName) > 10 || _calculateCharCount(NickName) < 1)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入正确长度的昵称" : "Please enter a NickName of the correct length",false,a);
                return false;
            }
            //else
            //{
            //    for (int i = 0; i < NickName.Length; i++)
            //    {
            //        bool RightInput = Regex.IsMatch(NickName, @"^[a-zA-Z0-9\u4e00-\u9fa5]+$");
            //        if (RightInput)
            //        {
            //            continue;
            //        }
            //        else
            //        {
            //            AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "昵称必须使用字母、数字" : "");
            //            return false;
            //        }
            //    }
            //}
            return true;
        }

        public static bool CheckNickName(string NickName)
        {
            if (NickName == "")
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入游戏昵称" : "Please enter a nickname");
                return false;
            }
            else if (_calculateCharCount(NickName) > 10 || _calculateCharCount(NickName) < 1)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游戏昵称必须在10字符或5汉字以内" : "Nickname must be within 10 characters or 5 chinese characters");
                return false;
            }
            //else
            //{
            //    for (int i = 0; i < NickName.Length; i++)
            //    {
            //        bool RightInput = Regex.IsMatch(NickName, @"^[a-zA-Z0-9\u4e00-\u9fa5]+$");
            //        if (RightInput)
            //        {
            //            continue;
            //        }
            //        else
            //        {
            //            AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "昵称必须使用字母、数字" : "");
            //            return false;
            //        }
            //    }
            //}
            return true;
        }


        public static bool CheckPhoneNumber(string PhoneNumber)
        {
            if (PhoneNumber.CompareTo("") == 0)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入电话号码" : "Please enter a phone number");
                return false;
            }
            else if (_calculateCharCount(PhoneNumber) > 20 || _calculateCharCount(PhoneNumber) < 5)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入正确的电话号码" : "Please enter the correct phone number");
                return false;
            }
            else
            {
                for (int i = 0; i < PhoneNumber.Length; i++)
                {
                    bool RightInput = Regex.IsMatch(PhoneNumber, @"^[a-zA-Z0-9\u4e00-\u9fa5]+$");
                    if (RightInput)
                    {
                        continue;
                    }
                    else
                    {
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "电话必须使用字母、数字、汉字" : "PhoneNumber must use letters,numbers or Chinese");
                        return false;
                    }
                }
            }
            return true;
        }




            //判断是否是数字
        public static bool _isDigits(char c)
        {
            bool bIsDigits = false;
            if (c >= '0' && c <= '9')
            {
                bIsDigits = true;
            }
            return bIsDigits;
        }

            //判断是否是字母
        public static bool _isCharacter(char c)
        {
            bool bIsCharacter = false;
            if (c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z')
            {
                bIsCharacter = true;
            }
            return bIsCharacter;
        }

            //判断是否是下划线
        public static bool _isUnderline(char c)
        {
            bool bIsUnderline = false;
            if (c == '_')
            {
                bIsUnderline = true;
            }
            return bIsUnderline;
        }


            /*统计字符串字符长度*/
        public static int _calculateCharCount(string str)
        {
            int digitCount = 0;//数字
            int leterCount = 0;//英文字母
            int spaceCount = 0;//空格
            int chineseLeterCount = 0;//中文字
            int otherLeterCount = 0;//其他字符
            for (int i = 0; i < str.Length; i++)
            {
                if (Char.IsDigit(str, i))
                {
                    digitCount++; //统计数字
                }
                else if (Char.IsWhiteSpace(str.Trim(), i))
                {
                    spaceCount++;//统计空格	
                }
                else if (Char.ConvertToUtf32(str, i) >= Convert.ToInt32("4e00", 16) && Char.ConvertToUtf32(str, i) <= Convert.ToInt32("9fff", 16))
                {
                    chineseLeterCount++;//统计汉字占两个字符				
                }
                else if (Char.IsLetter(str, i))
                {
                    leterCount++;//统计字母,
                }
                else
                {
                    otherLeterCount++;//其他字符
                }
            }
            return digitCount + leterCount + spaceCount + chineseLeterCount * 2 + otherLeterCount;
        }
    }
}