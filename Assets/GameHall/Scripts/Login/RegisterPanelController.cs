﻿/*
 *  author: jiangwen
 *  creation date: 2016/3/1
 *  modified by: shenhao
 *  modified date: 2016/7/11
 *  
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;


namespace WMCY.GameHall
{
    public class RegisterPanelController : MonoBehaviour
    {
        public InputField[] m_inputRegister;
        [SerializeField]
        private GameObject registerInvoke;

        void Start()
        {
            if (registerInvoke==null)
                registerInvoke = this.transform.parent.Find("loginBg").gameObject;
            //m_inputName.text = "玩美创游";
            NetManager.GetInstance().RegisterHandler("register", HandleNetMsg_Register);
            NetManager.GetInstance().RegisterHandler("isRegistOK", IsRisterOK);

            m_inputRegister[5].transform.parent.gameObject.SetActive(GVars.isInternational);
        }

        void Update()
        {

        }

        public void OnBtnClick_Register()
        {
            if (InputCheck.CheckUserName(m_inputRegister[0].text.Trim()))
            {
                if (InputCheck.CheckNickName(m_inputRegister[1].text.Trim()))
                {
                    //if (InputCheck.CheckPhoneNumber(m_inputRegister[2].text))
                    //{
                    if (InputCheck.CheckPassWord(m_inputRegister[3].text.Trim()))
                        {
                            if (m_inputRegister[4].text.Trim() == m_inputRegister[3].text.Trim())
                            {            
                                if(!GVars.isInternational)
                                {
                                    m_inputRegister[5].text = "admin";
                                }
                                if(m_inputRegister[5].text == "")
                                {
                                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入推荐人账号" : "Recommended by account number for an 5-15 letters,numbers");
                                } 
                                else
                                {
                                    User user = new User();
                                    user.username = m_inputRegister[0].text.Trim();//用户名
                                    user.nickname = m_inputRegister[1].text.Trim();//昵称
                                    user.phone = "987654321";//m_inputRegister[2].text;//电话（验证信息）
                                    user.password = m_inputRegister[3].text.Trim();//密码
                                    //user.photoId = UnityEngine.Random.Range(3, 4);
                                    user.type = 0;//玩家类型（0 普通玩家；1 游客）
                                    user.promoterUsername = m_inputRegister[5].text.Trim().ToLower();//推广员
                                    NetManager.GetInstance().Send("gcuserService/register", new object[] { user });

                                    SoundManager.Get().PlaySound(SoundType.Common_Click);
                                    AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Register_Invoke, user.username);
                                }
            
                            }
                            else
                            {
                                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "两次密码不一致，请重新输入" : "The two passwords do not match,please re-enter");
                            }
                        }
                    //}
                }
            }                           
        }



        //点击注册按钮
        public void OnBtnClick_Close()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            //AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_LoginUserName, m_inputRegister[0].text);
            foreach (InputField input in m_inputRegister)
            {
                input.text = "";
            }
            if (!this.transform.parent.Find("MainPanel").gameObject.activeSelf)
            {
                AppManager.GetInstance().GetPanel("LoginPanel").SetActive(true);
            }         
            
            this.gameObject.SetActive(false);
        }

        public void HandleNetMsg_Register(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            bool isRegister = (bool) obj["isRegister"];

            if (isRegister)
            {
                Debug.Log("zhucesssss");
                registerInvoke.GetComponent<RegisterInvoke>().CancelRepeat();
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "注册成功" : "Registration Successful", false, () =>
                {
                    AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_LoginUserName, m_inputRegister[0].text);
                    OnBtnClick_Close();
                });
            }
            else
            {
                //string msg = (string)obj["msg"];
                //AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? msg : "");

                int msgcode = (int)obj["msgCode"];
                
                switch (msgcode)
                {
                    case -3:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "注册已达到上限" : "The registration has reached the limit");
                        break;
                    case -4:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "等待后台验证" : "Please wait for the system process");
                        break;
                    case 4:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "用户名重复" : "This User ID has already been taken,please choose another User ID");
                        break;
                    case 5:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "推广员不存在" : "This extension is not exist");
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "注册失败" : "Registration failed");
                        break;
                }

                if (msgcode ==-4) return;
                registerInvoke.GetComponent<RegisterInvoke>().CancelRepeat();
            }
            
        }

        private void IsRisterOK(object[] objs)
        {
            Debug.Log("IsRisterOK");
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            bool isRegistOK = (bool)obj["success"];
            int messageStatus = (int)obj["messageStatus"];
            if(messageStatus==0)
            {
                return;
            }         
            if (messageStatus == 1)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "注册成功" : "Registration Successful", false, () => {
                    AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_LoginUserName, m_inputRegister[0].text);
                    OnBtnClick_Close();
                });
            }
            else if(messageStatus ==2)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "申请未通过" : "Application failed", false, OnBtnClick_Close);
            }
            registerInvoke.GetComponent<RegisterInvoke>().CancelRepeat();
            
        }
    }

}
