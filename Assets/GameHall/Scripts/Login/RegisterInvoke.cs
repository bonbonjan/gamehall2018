﻿/*
 *  author: jiangwen
 *  creation date: 2016/8/11
 *  
 */

using UnityEngine;
using System.Collections;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using System.Collections.Generic;

namespace WMCY.GameHall
{
    public class RegisterInvoke : MonoBehaviour
    {
        private string username = "";
        // Use this for initialization
        void Start()
        {
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Register_Invoke, this, Repeat);
            NetManager.GetInstance().RegisterHandler("dingFen", HandleNetMsg_DingFen);
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void HandleNetMsg_DingFen(object[] objs)
        {
            GVars.ScoreOverflow = true;
        }

        //private void HandleNetMsg_LastGameScore(object[] objs)
        //{
        //    Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
        //    int score = (int)obj["score"];

        //    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? string.Format("上局的得分为{0}", score) : string.Format("Previous round scores{0}", score));
        //    GVars.ScoreOverflow = false;
        //}

        public void CancelRepeat()
        {
            Debug.Log("CancelRepeat");
            CancelInvoke();
        }

        private void Repeat(object obj)
        {
            Debug.Log("Repeat");
            username = (string)obj;
            InvokeRepeating("SendRegister", 10, 10);
        }

        private void  SendRegister()
        {
            Debug.Log("SendRegister");
            NetManager.GetInstance().Send("gcuserService/isRegistOK", new object[] { username });
        }

        


    }
}