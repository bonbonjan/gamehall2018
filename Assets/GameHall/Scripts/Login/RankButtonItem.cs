﻿using System;
using UnityEngine;
using System.Collections;
using WMCY.GameHall;

public class RankButtonItem : MonoBehaviour
{
    public Action<int> OnSendSelect;
    //private RankPanelController rpc;

    void Start ()
	{
	    //rpc = this.transform.parent.parent.parent.parent.parent.GetComponent<RankPanelController>();
	}
	
	void Update () {
	
	}

    public void RankButtonClick()
    {
        int index = int.Parse(this.name.Substring(this.name.Length-1, 1));
        Debug.Log("index: "+index);
        //rpc.InitRankItem(index);
        OnSendSelect(index);
    }
}
