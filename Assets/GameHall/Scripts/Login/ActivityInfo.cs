﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ActivityInfo
{
    public int id;
    public string startDate;
    public string endDate;
    public int activityStatus;//0开启 1活动中 2过期
 

    public static ActivityInfo CreateWithDic(Dictionary<string,object> data)
    {
        ActivityInfo ret = new ActivityInfo();

        try
        {
            ret.startDate = (string)data["startDate"];
            ret.endDate = (string)data["endDate"];
            ret.activityStatus = (int)data["activityStatus"];
        }
        catch (Exception e)
        {
            Debug.LogError("ActivityInfo excpetion: " + e.Message);
            string keys = "";
            foreach (string key in data.Keys)
            {
                keys += key + ", ";
            }
            Debug.Log("keys: " + keys);
        }
        
        return ret;
    }
}
