﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace WMCY.GameHall
{
    public class QuestionItem : MonoBehaviour, IPointerExitHandler, IPointerClickHandler
    {
        [SerializeField]
        private ChooseController chooseController;
        [SerializeField]
        private bool isChange;
        [SerializeField]
        private int questionIndex;

        void Start()
        {
            isChange = true;
            questionIndex = int.Parse(this.name.Substring(this.name.Length - 1, 1));
        }

        void Update()
        {

        }
        //public void OnPointerEnter(PointerEventData eventData)
        //{
        //    this.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        //    this.transform.Find("Text").GetComponent<Text>().color = new Color(0.89f, 0.71f, 0.35f, 1.0f);
        //}
        public void OnPointerExit(PointerEventData eventData)
        {
            if (isChange)
            {
                this.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
                this.transform.Find("Text").GetComponent<Text>().color = new Color(0.98f, 0.91f, 0.57f, 1.0f);
            }
        }

        public void Init()
        {
            isChange = true;
            this.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
            this.transform.Find("Text").GetComponent<Text>().color = new Color(0.98f, 0.91f, 0.57f, 1.0f);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            chooseController.InitSelf(false);
            isChange = false;
            this.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            this.transform.Find("Text").GetComponent<Text>().color = new Color(0.89f, 0.71f, 0.35f, 1.0f);
            chooseController.SetQuestionStr(this);
        }


    }
}