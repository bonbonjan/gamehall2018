﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace WMCY.GameHall
{
    public class ChooseController : MonoBehaviour
    {
        [SerializeField]
        public Text Placeholder;
        [SerializeField]
        public Text questionText;
        [SerializeField]
        public InputField answerText;
        [SerializeField]
        private QuestionItem[] current_item;
        [SerializeField]
        private QuestionItem[] other_item;
        [SerializeField]
        private string QuestionStr = "";
        [SerializeField]
        public int currentIndex = 0;

        public void InitSelf(bool initself)
        {
            foreach (QuestionItem q in current_item)
            {
                if (initself)
                {
                    q.Init();
                    q.enabled = true;
                }
                else
                {
                    if(q.enabled)
                    {
                        q.Init();
                    }
                }
            }
        }

        public void ResetQuestionObject()
        {
            InitSelf(true);
            Placeholder.gameObject.SetActive(true);
            questionText.text = "";
            questionText.gameObject.SetActive(false);

            answerText.text = "";
            QuestionStr = "";
        }


        public void ChooseQuestion()
        {
            if (QuestionStr != "")
            {
                for (int i = 0; i < current_item.Length;i++)
                {
                    if(current_item[i].transform.Find("Text").GetComponent<Text>().text==QuestionStr)
                    {
                        currentIndex = i;
                    }
                }
                    
                questionText.text = QuestionStr;

                if (GVars.language == "zh")
                {
                    questionText.fontSize = 30;
                }
                else
                {
                    questionText.fontSize = 23;
                }
                questionText.font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
                foreach (QuestionItem q in other_item)
                {
                    q.Init();
                    q.enabled = true;
                    //if (q.transform.Find("Text").GetComponent<Text>().text == QuestionStr)
                    //{
                    //    q.enabled = false;
                    //    q.GetComponentInChildren<Text>().color = new Color(0.74f, 0.74f, 0.74f, 0.63f);
                    //}
                }
                other_item[currentIndex].enabled = false;
                other_item[currentIndex].GetComponentInChildren<Text>().color = new Color(0.74f, 0.74f, 0.74f, 0.63f);

                Placeholder.gameObject.SetActive(false);
                //this.gameObject.SetActive(false);
            }
            else
            {
                Placeholder.gameObject.SetActive(true);
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请选择密保问题" : "Please choose the security question");
            }
        }

        public void SetQuestionStr(QuestionItem qi)
        {
            QuestionStr = qi.transform.Find("Text").GetComponent<Text>().text;
            
        }

    }
}