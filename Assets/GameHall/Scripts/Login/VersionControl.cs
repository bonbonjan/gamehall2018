﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VersionControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
        this.GetComponent<Text>().text = "V" + Application.version;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
