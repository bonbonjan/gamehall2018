﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
namespace WMCY.GameHall
{
    public class RewardAlertController : MonoBehaviour
    {

        [SerializeField]
        private Image rewardType;
        [SerializeField]
        private Text rewardText;
        [SerializeField]
        private Image[] rewardImage;

        public void ShowReward(int type, int count)
        {
            this.gameObject.SetActive(true);
            rewardText.text = GVars.language == "zh" ? string.Format("游戏币 X{0}", count) : string.Format("Gold X{0}", count);
            StartCoroutine(Diappear(2));
        }

        public void BeClick()
        {
            this.gameObject.SetActive(false);
        }

        IEnumerator Diappear(float t)
        {
            yield return new WaitForSeconds(t);
            this.gameObject.SetActive(false);
        }
    }
}
