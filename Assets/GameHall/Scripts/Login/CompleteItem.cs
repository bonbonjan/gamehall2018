﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace WMCY.GameHall
{
    public class CompleteItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        [SerializeField]
        private CompleteController CompleteController;
        [SerializeField]
        private bool isChange;
        [SerializeField]
        private int questionIndex;

        void Start()
        {
            isChange = true;
            questionIndex = int.Parse(this.name.Substring(this.name.Length - 1, 1));
        }

        void Update()
        {

        }
        public void OnPointerEnter(PointerEventData eventData)
        {
            Debug.Log("OnPointerEnter");
            this.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            this.transform.Find("Text").GetComponent<Text>().color = new Color(0.89f, 0.71f, 0.35f, 1.0f);
        }
        public void OnPointerExit(PointerEventData eventData)
        {
            Debug.Log("OnPointerExit");
            if (isChange)
            {
                this.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
                this.transform.Find("Text").GetComponent<Text>().color = new Color(0.98f, 0.91f, 0.57f, 1.0f);
            }
            Debug.Log("isChange: " + isChange);
        }

        public void Init()
        {
            isChange = true;
            this.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
            this.transform.Find("Text").GetComponent<Text>().color = new Color(0.98f, 0.91f, 0.57f, 1.0f);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Debug.Log("OnPointerClick");
            CompleteController.InitSelf(false);
            isChange = false;
            this.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            this.transform.Find("Text").GetComponent<Text>().color = new Color(0.89f, 0.71f, 0.35f, 1.0f);
            CompleteController.SetQuestionStr(this);
        }
    }
}