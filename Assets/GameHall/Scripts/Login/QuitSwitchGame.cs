﻿using UnityEngine;
using System.Collections;
using WMCY.GameHall.Net;

namespace WMCY.GameHall
{
    public class QuitSwitchGame : MonoBehaviour
    {

        public void OnBtnClick_QuitGame()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            Application.Quit();
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        }

        public void OnBtnClick_Switch()
        {
            Debug.Log("OnBtnClick_Switch");
            GVars.isStartedFromLuaGame = false;
            GVars.isStartedFromGame = false;
            GVars.lockRelogin = true;//禁止自动登陆
            GVars.shareMode = 2;
            GVars.ScoreOverflow = false;
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Reset_UserInfoPanel, false);
            NetManager.GetInstance().Disconnect();
            AppManager.Get().HideAllPanels();
            AppManager.GetInstance().ShowLoginPanel();
            AppManager.Get().InitNet(true);
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_FreshDealer, -1);
            AppManager.Get().hasLogined = false;
        }
    }
}