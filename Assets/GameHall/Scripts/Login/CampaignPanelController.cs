﻿/*
 *  author: jiangwen
 *  creation date: 2016/3/30
 *  modified by: jiangwen
 *  modified date: 2016/5/10
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;

namespace WMCY.GameHall
{
    public class CampaignPanelController : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_campButton;
        [SerializeField]
        private Transform m_campGrid;
        private int m_campIndex=0;
        [SerializeField]
        private GameObject campaignItem;
        [SerializeField]
        private GameObject campaignItemGrid;
        public Sprite[] imageButtonStatus;

        public static int campCount = 6;
        [SerializeField]
        private GameObject[] campButtonGroup;
        [SerializeField]
        private Campaign currentCampaignItem;
        [SerializeField]
        private int currentCampaignType = 0;
        [SerializeField]
        private Text MembersText;

        string[] ActivityName = { "充值奖励", "累计游玩奖励", "等级奖励", "排行奖励", "单局奖励", "万元红包来袭" };
        string[] ActivityNameEn = { "Recharge bonus", "Cumulative reward", "Level reward", "Ranking reward", "Single reward", "Red envelopes" };

        //int[] activityMark = new int[7] { 0,0,0,0,0,0,0};
        //Campaign[] cpa = new Campaign[10];

        void Awake()
        {
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Fresh_UseCampaignPanel, this, RefreshCampaignPanel);
        }

        // Use this for initialization
        void Start()
        {
            NetManager.GetInstance().RegisterHandler("getActivity", HandleNetMsg_Campagin); //注册请求活动名单
            NetManager.GetInstance().RegisterHandler("getActivityAward", HandleNetMsg_CampaignComplete); //注册完成活动
            NetManager.GetInstance().RegisterHandler("activityClickableNotice", HandleNetMsg_ActivityClickableNotice);
        }

        void OnEnable()
        {
            foreach (var cb in campButtonGroup)
            {
                Destroy(cb);
            }
            currentCampaignType = 1;
            Init();
        }

        void Init()
        {
            campButtonGroup = new GameObject[campCount];
            for (int i = 0; i < campCount; i++)
            {
                //GameObject go = Instantiate(rankButton, grid.localPosition, grid.rotation) as GameObject;
                GameObject go = Instantiate(m_campButton, Vector3.zero, Quaternion.identity) as GameObject;
                campButtonGroup.SetValue(go, i);
                go.name = "CampButton" + i;
                go.transform.Find("Text").GetComponent<Text>().text = GVars.language == "zh" ? ActivityName[i] : ActivityNameEn[i];
                go.transform.Find("Image/Text").GetComponent<Text>().text = GVars.language == "zh" ? ActivityName[i] : ActivityNameEn[i];
                go.transform.Find("Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
                go.transform.Find("Image/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;

                go.transform.SetParent(m_campGrid);
                go.transform.localScale = new Vector3(1, 1, 1);
                go.transform.localPosition = new Vector3(0, 0, 0);
                //go.GetComponent<RankButtonItem>().OnSendSelect = InitRankItem;
                //给生成的按钮添加点击事件
                go.GetComponent<Button>().onClick.AddListener(() =>
                {
                    Debug.Log("go.name: " + go.name);
                    m_campIndex = int.Parse(go.name.Substring(go.name.Length - 1, 1));
                    campButtonToggle(m_campIndex);
                    //InitRankItem(m_campIndex);
                    OnBtnClick_CampaignType(m_campIndex);
                });

            }
            campButtonToggle(0);
        }

        public void campButtonToggle(int i)
        {
            foreach (GameObject go in campButtonGroup)
            {
                go.transform.Find("Image").gameObject.SetActive(false);
            }

            campButtonGroup[i].transform.Find("Image").gameObject.SetActive(true);
        }

        //请求奖项类型（充值奖励 活动奖励 等级奖励 单局奖励 红包奖励）
        public void OnBtnClick_CampaignType(int type)
        {
            //GameObject[] go = GameObject.FindGameObjectsWithTag("campagin");
            //foreach (GameObject g in go)
            //{
            //    Destroy(g);
            //}
            currentCampaignType = type + 1;
            Debug.Log("getActivitytype: " + currentCampaignType);
            NetManager.GetInstance().Send("gcuserService/getActivity", new object[] { currentCampaignType });
        }

        private void RefreshCampaignPanel(object objs)
        {
            int activityType = (int)objs;
            Debug.Log(LogHelper.Red("activityType: " + activityType));
            //刷新活动界面
            if (currentCampaignType == activityType)
                NetManager.GetInstance().Send("gcuserService/getActivity", new object[] { activityType });
        }

        private void HandleNetMsg_Campagin( object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            bool success = (bool)obj["success"];
            if (!success) return;

            Debug.Log(LogHelper.Red("promoterId(0直属 1非直属): " + GVars.user.promoterId));
            MembersText.gameObject.SetActive(GVars.user.promoterId == 0?false:true);

            GameObject[] gotmp = GameObject.FindGameObjectsWithTag("campagin");
            foreach (GameObject g in gotmp)
            {
                Destroy(g);
            }

            object[] activityList = (object[])obj["activityList"];
            int length = activityList.Length;

            Debug.Log("length1: " + length);

            if (length == 0) return;
            //int type = (int)obj["type"];//活动ID
            Campaign[] camp = new Campaign[length];
            for (int i = 0; i < length; i++)
            {
                camp[i] = Campaign.CreateWithDic((Dictionary<string, object>)activityList[i]);
                Debug.Log("camp[0].targetSchedule: " + camp[0].targetSchedule);
            }

            //List<Campaign> capList = obj["campagin"] as List<Campaign>;
            //Campaign[] cap = obj["campagin"] as Campaign[];

           // int length = capList.Count;
            if (length <= 5)
            {
                campaignItemGrid.GetComponent<RectTransform>().sizeDelta = new Vector2(955, 528);
            }
            if (length > 5)
            {
                campaignItemGrid.GetComponent<RectTransform>().sizeDelta = new Vector2(955, 528 + (length - 5)*105+10);
                campaignItemGrid.GetComponent<RectTransform>().localPosition = new Vector3(0, -40*(length - 3), 0);
                    //更新位置
            }
            for (int i = 0; i < length; i++)
            {
                GameObject go = Instantiate(campaignItem);
                go.transform.SetParent(this.transform.Find("bg/Gridbg/Grid"));
                go.transform.localPosition = new Vector3(0, 0, 0);
                go.transform.localScale = new Vector3(1, 1, 1);
                go.tag = "campagin";
                go.GetComponent<CampaignItem>().Init(camp[i], imageButtonStatus); //初始化活动，并将完成任务传递过去
                go.transform.Find("Button").GetComponent<Button>().onClick.AddListener(() => {                    
                    CompleteTask(go.GetComponent<CampaignItem>().cap);
                    go.GetComponent<CampaignItem>().ChangeButtonStatus();
                });
            }
        }

        private void HandleNetMsg_ActivityClickableNotice(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            object[] ob = (object[])obj["activityMark"];
            int[] activityMark = { 0, 0, 0, 0, 0, 0, 0 };
            for (int i = 1; i < ob.Length;i++ )
            {
                activityMark[i] = (int)ob[i];
            }
            ReFreshRedSign(activityMark);
        }

        private void ReFreshRedSign(int[] redSignStatus)
        {
            for (int i = 1; i < redSignStatus.Length; i++)
            {
                campButtonGroup[i-1].transform.Find("newUserCamBtn").gameObject.SetActive(redSignStatus[i] == 1);
            }
        }

        public void CompleteTask(Campaign cap)
        {
            //currentCampaignItem = c;

            NetManager.GetInstance().Send("gcuserService/getActivityAward", new object[] { cap.Id }); //
        }

        public void HandleNetMsg_CampaignComplete(object[] objs)
        {
            //已完成任务，刷新金币数
            Dictionary<string,object> obj=objs[0] as Dictionary<string,object>;
            bool success = (bool)obj["success"];
            if(success)
            {
                int gameGold = (int)obj["gameGold"];
                Debug.Log("gameGold: " + gameGold);
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? string.Format("恭喜您获得{0}游戏币", gameGold - GVars.user.gameGold) : string.Format("Congratulations on getting {0} coins", gameGold - GVars.user.gameGold));
                //this.transform.Find("RewardAlert").GetComponent<RewardAlertController>().ShowReward(1, gameGold - GVars.user.gameGold);
                GVars.user.gameGold = gameGold;
                AppManager.Get().Notify(UIGameMsgType.UINotify_Fresh_GoldAndLottery);
            }
            else
            {
                int msgCode = (int)obj["msgCode"];
                switch (msgCode) 
                {
                    case 18:
                        AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "未完成" : "Unfinished");
                        break;
                    case 19:
                        AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "已领取过奖励" : "It has already received over award");
                        break;
                    default:
                        AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "领取奖励失败" : "Failure to receive awards");
                        break;
                }
            }

        }

        public void OnBtnClick_Close()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            campButtonToggle(0);
            //transform.Find("CampType/GameTypeButton_bg/Grid/CampButton0").GetComponent<Toggle>().isOn = true;
            GameObject[] go = GameObject.FindGameObjectsWithTag("campagin");
            foreach(GameObject g in go)
            {
                Destroy(g);
            }

            this.gameObject.SetActive(false);
        }
    }
}