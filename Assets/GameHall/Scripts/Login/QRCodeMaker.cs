﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Drawing;
using ZXing;
using ZXing.QrCode;
using ZXing.Common;
using ZXing.Common;  
using ZXing.Rendering;
using ZXing.QrCode.Internal;

public class QRCodeMaker : MonoBehaviour
{
	public static Texture2D encoded=new Texture2D(512, 512);   //二维码贴图
 
	/// <summary>
	///根据二维码包含的信息以及宽高，对文本信息进行转码
	/// </summary>
	/// <param name="textForEncoding"></param>
	/// <param name="width"></param>
	/// <param name="height"></param>
	/// <returns></returns>
	private static Color32[] Encode(string textForEncoding, int width, int height)
	{
		BarcodeWriter writer = new BarcodeWriter
		{
			Format = BarcodeFormat.QR_CODE,
			Options = new QrCodeEncodingOptions
			{
//				DisableECI = true,  
//				CharacterSet = "UTF-8",
				Height = height,
				Width = width,
				Margin =0,
				ErrorCorrection = ErrorCorrectionLevel.H
			}

		};
		return writer.Write(textForEncoding);
	}


	public static void QRCodeCreate(string url)
	{
		if (url != null)
		{
			Color32[] color32 = Encode(url, 256, 256);
			Color32[] color=new Color32[512*512];
			for(int i=0;i<256;i++)
			{
				for(int j=0;j<256;j++)
				{
					color[i*1024+2*j]=color32[i*256+j];
					color[i*1024+2*j+1]=color32[i*256+j];
					color[i*1024+2*j+512]=color32[i*256+j];
					color[i*1024+2*j+513]=color32[i*256+j];
				}
			}
			encoded.SetPixels32(color);   //根据转换来的32位颜色值来计算二维码的像素
			encoded.Apply();    //生成二维码
		}
	}
}

  
 
