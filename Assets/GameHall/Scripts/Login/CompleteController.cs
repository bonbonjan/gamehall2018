﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace WMCY.GameHall
{
    public class CompleteController : MonoBehaviour
    {

        [SerializeField]
        public Text Placeholder;
        [SerializeField]
        public Text questionText;
        [SerializeField]
        public InputField answerText;
        [SerializeField]
        private CompleteItem[] current_item;
        [SerializeField]
        private string QuestionStr = "";

        public void InitSelf(bool initself)
        {
            foreach (CompleteItem q in current_item)
            {
                if (initself)
                {
                    q.Init();
                    q.enabled = true;
                }
                else
                {
                    if (q.enabled)
                    {
                        q.Init();
                    }
                }
            }
        }

        public void ResetQuestionObject()
        {
            InitSelf(true);
            Placeholder.gameObject.SetActive(true);
            questionText.text = "";
            questionText.gameObject.SetActive(false);

            answerText.text = "";
            QuestionStr = "";
        }


        public void ChooseQuestion()
        {
            if (QuestionStr != "")
            {
                questionText.text = QuestionStr;

                if (GVars.language == "zh")
                {
                    questionText.fontSize = 30;
                }
                else
                {
                    questionText.fontSize = 23;
                }
                questionText.font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
                
                Placeholder.gameObject.SetActive(false);
                //ResetQuestionObject();
                //this.gameObject.SetActive(false);
            }
            else
            {
                Placeholder.gameObject.SetActive(true);
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请选择安全问题" : "Please choose the security question");
            }
        }

        public void SetQuestionStr(CompleteItem qi)
        {
            QuestionStr = qi.transform.Find("Text").GetComponent<Text>().text;
        }
    }
}
