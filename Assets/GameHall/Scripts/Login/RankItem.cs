﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace WMCY.GameHall
{
    public class RankItem : MonoBehaviour
    {
        [SerializeField]
        private Image m_imageHead;
        [SerializeField]
        private Text m_textName;
        [SerializeField]
        private Text m_textCoin;
        [SerializeField]
        private GameObject m_imageCoin;

        //public RankPanelController rpc;
        void Start()
        {

        }

        public void Init(UserIconDataConfig uIdc, WealthLevel tr, int type)
        {
            //imageNumber.sprite = ;
            //Debug.Log(tr.nickname);
            //Debug.Log(rpc.spriteHead[tr.photoId]);
            m_imageHead.sprite = uIdc.list[tr.photoId].sprite;
            m_textName.text = tr.nickname;
            if (type == 1)
            {
                m_textCoin.text = /*"游戏币:" + */tr.gameGoldOrLevel.ToString();
                m_imageCoin.SetActive(true);
            }
            if (type == 2)
            {
                m_textCoin.text = /*"等级:  " + */tr.gameGoldOrLevel+"级";
                m_imageCoin.SetActive(false);
            }              
        }
    }

}