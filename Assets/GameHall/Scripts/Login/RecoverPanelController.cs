﻿/*
 *  author: jiangwen
 *  creation date: 2016/3/28
 *  modified by: jiangwen
 *  modified date: 2016/4/16
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;

namespace WMCY.GameHall
{
    public class RecoverPanelController : MonoBehaviour
    {
        public GameObject firstStep;//第一步
        public GameObject secondStep;//第二步
        public GameObject thirdStep;//第三步

        public InputField m_account;

        //public Dropdown drop1;
        //public Dropdown drop2;
        public InputField text_answer1;
        public InputField text_answer2;
        public Text text_Q1;
        public Text text_Q2;

        public InputField passwordText;//第一次输入密码
        public InputField repeatPasswordText;//验证密码
        [SerializeField]
        private GameObject alert;
        [SerializeField]
        private Text textAlert;

        QuestionInfo info1 = new QuestionInfo();
        QuestionInfo info2 = new QuestionInfo();

        private string m_userName;

        void Start()
        {
           
            NetManager.GetInstance().RegisterHandler("usernameExist", HandleNetMsg_ConfirmUserName);
            NetManager.GetInstance().RegisterHandler("securityCheck", HandleNetMsg_ConfirmSecurity);
            NetManager.GetInstance().RegisterHandler("setPwd", HandleNetMsg_ConfirmPassword);
        }

        void Update()
        {

        }

        //输入账号
        public void OnBtnClick_Next()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            if (m_account.text == "")
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入要找回的账号" : "Please enter account to get back");
            }
            else if (InputCheck.CheckUserName(m_account.text))
            {
                NetManager.GetInstance().Send("gcuserService/usernameExist", new object[] { m_account.text }); //  
            }              
        }

        //输入密保答案
        public void OnBtnClick_Next2()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);

            //info1.ID = drop1.value;
            info1.Question = text_Q1.text;
            info1.Answer = text_answer1.text;

            //info2.ID = drop2.value;
            info2.Question = text_Q2.text;
            info2.Answer = text_answer2.text;
            Dictionary<int, object> dic = new Dictionary<int, object>();
            dic.Add(1, info1);
            dic.Add(2, info2);
            //输入内容检测
            //if (info1.ID == info2.ID)
            //{
            //    AlertDialog.GetInstance().ShowDialog("两个问题不能相同");
            //}else
             if (info1.Answer == "" || info2.Answer == "")
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入密保问题答案" : "Please enter the security question answers");
            }
            else
            {
                NetManager.GetInstance().Send("gcsecurityService/securityCheck", new object[] { m_userName,dic }); //
                //Send_Questions(dic);
                Debug.Log("username：" + m_userName + "问题ID：" + info1.ID + "问题：" + info1.Question + " 答案： " + info1.Answer);
                Debug.Log("username：" + m_userName + "问题ID：" + info2.ID + "问题：" + info2.Question + " 答案： " + info2.Answer);
            }
            
        }

        //输入新密码
        public void OnBtnClick_Sure()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            Debug.Log("passwordText: " + passwordText.text);
            if (InputCheck.CheckReceivePassWord(passwordText.text))
            {
                if (passwordText.text == repeatPasswordText.text)
                {
                    NetManager.GetInstance().Send("gcuserService/setPwd", new object[] { passwordText.text }); //
                }
                else
                {
                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "两次输入不一致" : "Two input is inconsistent");
                }
            }
        }


        public void OnBtnClick_Cancel()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            AppManager.GetInstance().GetPanel("LoginPanel").SetActive(true);

            m_account.text = "";

            text_answer1.text = "";
            text_answer2.text = "";

            passwordText.text = "";
            repeatPasswordText.text = "";

            firstStep.SetActive(true);
            secondStep.SetActive(false);
            thirdStep.SetActive(false);

            this.gameObject.SetActive(false);
        }

        private void HandleNetMsg_ConfirmUserName(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            bool isExit = (bool)obj["success"];
            
            if (isExit)
            {
                m_userName = m_account.text;

                bool haveSecurity = (bool) obj["haveSecurity"];
                if (!haveSecurity)
                {
                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "该账号未设置密保问题" : "The account security question is not set");
                }
                else
                {
                    Dictionary<string, object> security = (Dictionary<string, object>)obj["security"];
                    Dictionary<string, object> que1 = (Dictionary<string, object>)security["1"];
                    Dictionary<string, object> que2 = (Dictionary<string, object>)security["2"];
                    firstStep.SetActive(false);
                    secondStep.SetActive(true);

                    info1.ID = (int)que1["questionId"];
                    info2.ID = (int)que2["questionId"];

                    Debug.Log("info1.ID: " + info1.ID);
                    Debug.Log("info2.ID: " + info2.ID);

                    text_Q1.text = GVars.language == "zh" ? SecurityQuestion.securityQuestion_zh[info1.ID] : SecurityQuestion.securityQuestion_en[info1.ID];//(string)que1["question"];
                    text_Q2.text = GVars.language == "zh" ? SecurityQuestion.securityQuestion_zh[info2.ID] : SecurityQuestion.securityQuestion_en[info2.ID];//(string)que2["question"];

                    text_Q1.fontSize = GVars.language == "zh" ? 30 : 25;
                    text_Q2.fontSize = GVars.language == "zh" ? 30 : 25;

                    text_Q1.font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
                    text_Q2.font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
                }               
            }
            else
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号不存在" : "The account does not exist");
            }
        }
        private void HandleNetMsg_ConfirmSecurity(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                secondStep.SetActive(false);
                thirdStep.SetActive(true);
            }
            else
            {               
                int inputCount = (int)obj["errorCount"];
                Debug.Log("密保问题测试" + inputCount);
                if(inputCount<5)
                {                   
                    if (inputCount >= 3)
                    {
                        alert.SetActive(true);
                        textAlert.text = GVars.language == "zh" ? string.Format("您已连续输入{0}次密码错误,还剩{1}次操作机会",inputCount, 5 - inputCount) : string.Format("You enter the password for {0} times ,the rest of {1} chance", inputCount, 5 - inputCount);
                        //AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? string.Format("您已连续输入3次密码错误,还剩{0}次操作机会", 5 - inputCount) : string.Format("You enter the password for {0} times ,the rest of {1} chance", inputCount, 5 - inputCount));
                        return;
                    }
                    alert.SetActive(true);
                    textAlert.text = GVars.language == "zh" ? "密保问题答案输入错误" : "Security question answer enter wrong";
                    //AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密保问题答案输入错误" : "Security question answer enter wrong");
                }
                else 
                {
                    int remainTime = (int)obj["remainTime"];
                    int hour = remainTime/1000 /3600;
                    int min = remainTime / 1000 / 60 - hour * 60;
                    int seco = remainTime / 1000 - hour * 3600 - min * 60;
                    Debug.Log("hour: " + hour);
                    Debug.Log("min: " + min);
                    Debug.Log("seco: " + seco);
                    if (min == 0)
                    {
                        alert.SetActive(true);
                        textAlert.text = GVars.language == "zh" ? string.Format("密保问题答案多次错误，请3小时后再试") : "you answer the wrong questions to many times  , please try it again after 3 hours";
                        //AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? string.Format("连续回答5次错误,请在3小时后重试") : "you answer the wrong questions to many times  , please try it again after 3 hours");
                    }
                    else
                    {
                        alert.SetActive(true);
                        textAlert.text = GVars.language == "zh" ? string.Format("密保问题答案多次错误，请{0}小时{1}分钟{2}秒后重试", hour, min, seco) : string.Format("you answer the wrong questions to many times, pls try it again at {0} hours {1} seconds {2} points", hour, min, seco);
                        //AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? string.Format("连续回答5次错误,请在{0}小时{1}分钟{2}秒后重试", hour, min, seco) : string.Format("you answer the wrong questions to many times, pls try it again at {0} hours {1} seconds {2} points", hour, min, seco));
                    }
                }
            }           
        }
        private void HandleNetMsg_ConfirmPassword(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码设置成功" : "Password set successfully", false, OnBtnClick_Cancel);
                //this.gameObject.SetActive(false);
            }
            else
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码设置失败" : "Password set failed");
            }
        }


    }
}
