﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using System.Collections.Generic;

namespace WMCY.GameHall
{
    public class UserCompleteController : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_alert;
        [SerializeField]
        private GameObject m_complete;
        [SerializeField]
        private GameObject m_chooseBox;
        [SerializeField]
        private Toggle m_toggleNoPrompt;
        [SerializeField]
        private InputField m_textIDNumber;
        [SerializeField]
        private Text m_textQuestion;
        [SerializeField]
        private InputField m_textAnswer;
        [SerializeField]
        public ArrayList FromArgs; //"LotteryCity" : 彩票城;  "Store"商城

        public static bool UserCompleteNoPrompt = false;
        public static bool UserTempNoPrompt = false;    //当前某个模块的玩家信息完善不提示标志

        void Start()
        {
            m_alert.SetActive(true);
            m_complete.SetActive(false);
            NetManager.GetInstance().RegisterHandler("addUserInfo", HandleNetMsg_addUserInfo);
            NetManager.GetInstance().RegisterHandler("notShowUserInfo", HandleNetMsg_notShowUserInfo);
        }

        public void ToggleNoPrompt()
        {
            Debug.Log(LogHelper.Yellow("m_toggleNoPrompt.isOn: " + m_toggleNoPrompt.isOn));
            UserCompleteNoPrompt = m_toggleNoPrompt.isOn;
        }

        private void HandleNetMsg_notShowUserInfo(object[] obj)
        {
            Dictionary<string, object> objs = obj[0] as Dictionary<string, object>;
            bool success = (bool)objs["success"];
            if (success)
            {
                GVars.user.card = "1";
                //gameObject.SetActive(false);

            }
            else
            {

            }
        }

        private void HandleNetMsg_addUserInfo(object[] obj)
        {
            Dictionary<string, object> objs = obj[0] as Dictionary<string, object>;
            bool isComplete = (bool)objs["success"];

            if (isComplete)
            {
                GVars.user.card = "1";
                AlertDialog.GetInstance().ShowDialog("资料完善成功！",false, OnBtnClick_Close);
                gameObject.SetActive(false);
              
            }
            else
            {

            }
        }

        public void OnBtnClick_QuitComplete()
        {
            this.gameObject.SetActive(false);
            if ((string)FromArgs[0] == "Store")
            {
                if (GVars.payMode == 1)
                {
                    UserTempNoPrompt = true;
                    AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_BuyShopItems, (int)FromArgs[1]);
                }
                else
                    AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_RechargePanel);
            }
            else if ((string)FromArgs[0] == "LotteryCity")
            {
                UserTempNoPrompt = true;
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_BuyLotteryItems, (int)FromArgs[1]);
            }
            else if ((string)FromArgs[0] == "cash")
            {
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_CashPanel);
            }
        }

        public void  OnBtnClick_SureComplete()
        {
            if(UserCompleteNoPrompt)
            {
                //this.gameObject.SetActive(false);

                NetManager.GetInstance().Send("gcuserService/notShowUserInfo", new object[] {"1"});//      

                //if ((string)FromArgs[0] == "Store")
                //{
                //    if (GVars.payMode == 1)
                //    {
                //        //AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_BuyShopItems, (int)FromArgs[1]);
                //        AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_StorePanel, 1);
                //    }
                //    else
                //        AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_RechargePanel);
                //}
                //else if ((string)FromArgs[0] == "LotteryCity")
                //{
                //    AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_LotteryCityPanel);
                //}
                //else if ((string)FromArgs[0] == "cash")
                //{
                //    AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_CashPanel);
                //}
                //return;
            }

            m_alert.SetActive(false);
            m_complete.SetActive(true);
        }

        public void OnBtnClick_Choose()
        {
            m_chooseBox.SetActive(true);
        }

        public void OnBtnClick_Skip()
        {
            UserCompleteNoPrompt = true;
            OnBtnClick_Close();
        }

        public void OnBtnClick_Sure()
        {
            if (m_textIDNumber.text=="")
            {
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "请输入身份证号" : "Please enter the ID number");
                return;
            }
            if (m_textQuestion.text == "")
            {
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "请选择安全问题" : "Please choose the security question");
                return;
            }
            if (m_textAnswer.text == "")
            {
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "答案不能为空" : "The answer cannot be empty");
                return;
            }
            NetManager.GetInstance().Send("gcuserService/addUserInfo", new object[] { m_textIDNumber.text, m_textQuestion.text, m_textAnswer.text });//      
        }

        public void OnBtnClick_Close()
        {
            m_alert.SetActive(true);
            m_complete.SetActive(false);
            this.gameObject.SetActive(false);
            m_textIDNumber.text ="";
            m_chooseBox.GetComponentInChildren<CompleteController>().ResetQuestionObject();
            if ((string)FromArgs[0] == "Store")
            {
                if (GVars.payMode == 1)
                {
                    UserTempNoPrompt = true;
                    AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_BuyShopItems, (int)FromArgs[1]);
                }
                else
                    AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_RechargePanel);
            }
            else if ((string)FromArgs[0] == "LotteryCity")
            {
                UserTempNoPrompt = true;
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_BuyLotteryItems, (int)FromArgs[1]);
            }
            else if ((string)FromArgs[0] == "cash")
            {
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_CashPanel);
            }
        }

    }
}