﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using WMCY.GameHall;

public class GameRankItem : MonoBehaviour {

    //private Image imageNumber;
    public Text textName;
    public Text textCoin;
    public Text textType;
    public Text texTime;

    void Start()
    {
        //imageNumber = this.transform.Find("number").GetComponent<Image>();
        //textName = this.transform.Find("name").GetComponent<Text>();
        //textCoin = this.transform.Find("coin").GetComponent<Text>();
        //textType = this.transform.Find("type").GetComponent<Text>();
        //texTime = this.transform.Find("time").GetComponent<Text>();
    }

    public void Init(TopRank tr)
    {
        //imageNumber.sprite = ;
        //Debug.Log("tr.nickname: " + tr.nickname);
        textName.text = tr.nickname;
        textCoin.text = GVars.language == "zh" ? string.Format("游戏币:   {0}", tr.gold) : string.Format("Gold:   {0}", tr.gold);//tr.gold
        textType.text = tr.awardName;
        texTime.text = tr.datetime;
    }
}
