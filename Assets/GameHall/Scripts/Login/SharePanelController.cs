﻿/*
 *  author: jiangwen
 *  creation date: 2016/3/25
 *  modified by: jiangwen
 *  modified date: 2016/4/28
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using ZXing.QrCode.Internal;

namespace WMCY.GameHall
{
    public class SharePanelController : MonoBehaviour
    {
        //[SerializeField]
        //private string QRCodeText = "";
        [SerializeField]
        private Image QRBigImage;
        [SerializeField]
        private string QRString = "";//"http://ttn2ery6.s01th.space:1828/luckyfish/html/Hall.html";
        [SerializeField]
        private GameObject shareItem;
        [SerializeField]
        private GameObject SpecialShareItem;
        [SerializeField]
        private Text textLink;
        [SerializeField]
        private GameObject twoTab;
        [SerializeField]
        private GameObject singleTab;
        [SerializeField]
        private GameObject open;
        [SerializeField]
        private GameObject close;
        [SerializeField]
        private GameObject defau;
        [SerializeField]
        private GameObject AwardInfo;

        public Image m_QRCode;

        private GameObject[] Items;
        private int currentpage=0;

        List<ShareLog> sList = new List<ShareLog>();

#if UNITY_ANDROID
        public AndroidJavaClass jc;//调用java工程
        public AndroidJavaObject jo;
#endif

        void Awake()
        {

            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Fresh_SharePanel, this, Fresh_SharePanel);

#if UNITY_ANDROID && !UNITY_EDITOR
		jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
#endif
        }

        void Start()
        {
            QRString = GVars.downloadStr;
            textLink.text = QRString;
            this.transform.Find("TwoTab/Tab1Content/QRCodebg/QRCode/Android_iOS/Text").GetComponent<Text>().text = Application.productName;
            
            this.transform.Find("SingleTab/QRCodebg/QRCode/Android_iOS/Text").GetComponent<Text>().text = Application.productName;
            this.transform.Find("SingleTab/bg-2/bg/Text").GetComponent<Text>().text = QRString;
            ShowQRCode(QRString, this.transform.Find("SingleTab/QRCodebg/QRCode").GetComponent<Image>());

            ShowQRCode(QRString, m_QRCode);
            twoTab.SetActive(true);

            //twoTab.SetActive(GVars.shareMode == 0);
            //singleTab.SetActive(!twoTab.activeSelf);
            //this.transform.Find("AwardInfo").gameObject.SetActive(GVars.shareMode == 0);

            NetManager.GetInstance().RegisterHandler("getShareList", HandleNetMsg_getShareList);
        }

        void OnEnable()
        {
            //twoTab.SetActive(GVars.shareMode == 0);
            //singleTab.SetActive(!twoTab.activeSelf);

            ChangeTab(1);
            currentpage = 1;            
        }

        public void EnlargeQRCode()
        {
            ShowQRCode(QRString, QRBigImage.transform.Find("Image").GetComponent<Image>());
            QRBigImage.transform.Find("Image/Android_iOS/Text").GetComponent<Text>().text = Application.productName;
            QRBigImage.gameObject.SetActive(true);
        }

        public void HideQRCode()
        {
            QRBigImage.gameObject.SetActive(false);
        }

        public void OnBtnClick_Close()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            ClearItem();
            sList.Clear();
            ShowShareContent(2);
            this.gameObject.SetActive(false);
        }

        private void ClearItem()
        {
            if (Items != null && Items.Length != 0)
            {
                foreach (var v in Items)
                {
                    Destroy(v.gameObject);
                }
            }
        }

        private void HandleNetMsg_getShareList(object[] obj)
        {
            Dictionary<string, object> objs = obj[0] as Dictionary<string, object>;
            int countAward = (int)objs["countAward"];
            int bestAward = (int)objs["bestAward"];
            object[] o = (object[])objs["shareList"];
            int length = o.Length;
            //List<ShareLog> sList = new List<ShareLog>();
            sList.Clear();
            //ShareLog[] sl = new ShareLog[length];
            for (int i = 0; i < length;i++ )
            {
                sList.Add(ShareLog.CreateWithDic((Dictionary<string, object>)o[i]));
            }

            sList.Sort((x, y) =>
            {
                int value = x.countGameGold.CompareTo(y.countGameGold);
                if (value == 0)
                    value = x.bindTime.CompareTo(y.bindTime);
                return -value;
            });

            ShowShareItem(length, sList);

            this.transform.Find("TwoTab/Tab2Content/open/awardcount").GetComponent<Text>().text = countAward.ToString();
            this.transform.Find("TwoTab/Tab2Content/open/peoplecount").GetComponent<Text>().text = length.ToString();
            this.transform.Find("AwardInfo/Awardcount").GetComponent<Text>().text = bestAward.ToString();
            ShowShareContent(GVars.shareMode);
        }

        //private void ShowCopyTip()
        //{
        //    //m_copyTip.SetActive(true);
        //    //StartCoroutine(HideTip());
        //}

        //IEnumerator HideTip()
        //{
        //    yield return new WaitForSeconds(2);
        //    //m_copyTip.SetActive(false);
        //}

        public void OnBtnClick_Copy()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            //ShowCopyTip();
#if UNITY_IOS
		//string str = "";
		//UnityToIOS.GetSingleton().Copy(textLink.text);
            IOS_Agent.DoCopy(textLink.text);
#endif
            //TextEditor te = new TextEditor();//很强大的文本工具  
            //te.text = textLink.text;
            //te.OnFocus();
            //te.Copy();

#if UNITY_ANDROID && !UNITY_EDITOR
		string str = "";
		str = jo.Call<string>("Docopy", textLink.text);
		if (str.CompareTo(textLink.text) != 0)
		{
//			NGUIDebug.Log("COPY=" + str);
		}
		
#endif
            AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "已复制" : "Copy Done");
        }

        public void ShowQRCode(string str,Image QRimage)
        {
            QRCodeMaker.QRCodeCreate(str);
            QRimage.sprite = Sprite.Create(QRCodeMaker.encoded, new Rect(0, 0, 512, 512), new Vector2(0, 0));


//#if UNITY_ANDROID && !UNITY_EDITOR
//        AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
//        AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
//        jo.Call("setScreenMode",0);
//        jo.Call("saveScreenBrightness",254);
//        jo.Call("setScreenBrightness",254);
//#endif

        }

        private void Fresh_SharePanel(object obj)
        {
            int sMode = (int)obj;
            ShowShareContent(sMode);
            if(sList !=null)
                ShowShareItem(sList.Count, sList);
        }

        private void ShowShareContent(int sMode)
        {
            if(sMode==0)
            {
                open.SetActive(true);
                close.SetActive(false);
                defau.SetActive(false);
                AwardInfo.SetActive(true);
            }
            else if(sMode==1)
            {
                open.SetActive(false);
                close.SetActive(true);
                defau.SetActive(false);
                AwardInfo.SetActive(false);
            }
            else
            {
                open.SetActive(false);
                close.SetActive(false);
                defau.SetActive(true);
                AwardInfo.SetActive(false);
            }
        }

        public void ChangeTab(int tabIndex)
        {            
            if (tabIndex == currentpage)
            {
                return;
            }
            currentpage = tabIndex;
            if(tabIndex==2)
            {
                if (GVars.user.type == 1) //游客用户止步
                {
                    currentpage = 1;
                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游客账号无法操作此功能" : "Guest accounts can't operate this function");
                    return;
                }
            }

            this.transform.Find("TwoTab/Tab1Content").gameObject.SetActive(tabIndex == 1);
            this.transform.Find("TwoTab/tab1/tabHighlight").gameObject.SetActive(tabIndex == 1);

            this.transform.Find("TwoTab/tab2/tabHighlight").gameObject.SetActive(tabIndex == 2);
            this.transform.Find("TwoTab/Tab2Content").gameObject.SetActive(tabIndex == 2);

            if (currentpage==1)
                AwardInfo.SetActive(false);

            if (currentpage==2)
            {
                //ShowShareContent(GVars.shareMode);
                NetManager.GetInstance().Send("gcuserService/getShareList", new object[] {GVars.user.id });
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_ShareRedSign,false);
            }         
        }

        private void ShowShareItem(int length,List<ShareLog> sha)
        {
            if (GVars.specialMark == 2) return;
            ClearItem();

            Items = new GameObject[length];

            if(length<=4)
            {
                this.transform.Find("TwoTab/Tab2Content/open/ShareScroll/Grid").GetComponent<RectTransform>().sizeDelta = new Vector2(1016, 253);
                this.transform.Find("TwoTab/Tab2Content/open/ShareScroll/Grid").GetComponent<RectTransform>().localPosition = Vector3.zero;
            }
            if(length>4)
            {
                this.transform.Find("TwoTab/Tab2Content/open/ShareScroll/Grid").GetComponent<RectTransform>().sizeDelta = new Vector2(1016, 280 + (length - 4) * 68);
                this.transform.Find("TwoTab/Tab2Content/open/ShareScroll/Grid").GetComponent<RectTransform>().localPosition = new Vector3(0, (4 - length) * 68, 0);
            }


            this.transform.Find("TwoTab/Tab2Content/open/Catalog").gameObject.SetActive(GVars.specialMark == 0);
            this.transform.Find("TwoTab/Tab2Content/open/SpecialCatalog").gameObject.SetActive(GVars.specialMark == 1);

            GameObject items = GVars.specialMark==1?SpecialShareItem : shareItem;

            for(int i=0;i<length;i++)
            {
                GameObject item = Instantiate(items);              
                Items.SetValue(item, i);
                item.transform.SetParent(this.transform.Find("TwoTab/Tab2Content/open/ShareScroll/Grid"));
                item.transform.localPosition = Vector3.zero;
                item.transform.localScale = Vector3.one;

                item.transform.Find("account").GetComponent<Text>().text = sha[i].Username.Substring(0, sha[i].Username.Length - 3) + "***";//sha[i].Username;
                item.transform.Find("time").GetComponent<Text>().text = sha[i].bindTime;
                item.transform.Find("expend").GetComponent<Text>().text = sha[i].countGameGold.ToString();
                item.transform.Find("award").GetComponent<Text>().text = sha[i].wardGameGold.ToString();

                if(GVars.specialMark==1)
                {
                    item.transform.Find("weeklyRecharge").GetComponent<Text>().text = sha[i].weeklyRecharge.ToString();
                    item.transform.Find("weeklyExpiry").GetComponent<Text>().text = sha[i].weeklyExpiry.ToString();
                }
            }
        }

    }

}
