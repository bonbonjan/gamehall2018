﻿/*
 *  author: jiangwen
 *  creation date: 2016/4/1
 *  modified by: jiangwen
 *  modified date: 2016/5/9
 */

using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using System.Text.RegularExpressions;

namespace WMCY.GameHall
{
    public class MailPanelController : MonoBehaviour
    {
        public GameObject m_mailContent;
        public GameObject mailItem;
        public GameObject awardItem;

        private Text mailSend;
        private Text mailTime;
        private Text mailContent;
        private GameObject mailContent2;//显示分享奖励
        private GameObject onlyDeletBtn;
        private GameObject GainADelbtn;

        private GameObject grid;
        private GameObject[] awardItemList;

        private Mail currentMail;//当前选中的mail
 
        private bool isAllSelect = false;
        private string m_selectMail;
       // private Toggle[] toggleGroup;

        void Start()
        {
            grid = this.transform.Find("bg/ItemBg/Grid").gameObject;

            mailSend = this.transform.Find("Mask/MailContent/mailContent/sender").GetComponent<Text>();
            mailTime = this.transform.Find("Mask/MailContent/mailContent/sendTime").GetComponent<Text>();
            mailContent = this.transform.Find("Mask/MailContent/mailContent/scrollContent/content").GetComponent<Text>();
            mailContent2 = this.transform.Find("Mask/MailContent/mailContent/scrollContent2").gameObject;
            onlyDeletBtn = this.transform.Find("Mask/MailContent/DeleteButton").gameObject;
            GainADelbtn = this.transform.Find("Mask/MailContent/ReAndDe").gameObject;

            NetManager.GetInstance().RegisterHandler("getEmailList", HandleNetMsg_Mail);
            NetManager.GetInstance().RegisterHandler("updateEmail", HandleNetMsg_updateEmail);
            NetManager.GetInstance().RegisterHandler("refreshEmailList", HandleNetMsg_refreshEmailList);
        }

        private void HandleNetMsg_refreshEmailList(object[] obj)
        {
            ClearItem();
            OnBtnClick_MailContentBack();
            OnBtnClick_Back();
            AlertDialog.GetInstance().ShowDialog("后台修改邮件");
            //HandleNetMsg_Mail(obj);
        }

        private void HandleNetMsg_updateEmail(object[] obj)
        {
            Dictionary<string, object> objs = obj[0] as Dictionary<string, object>;
            bool success = (bool)objs["success"];
            if(success)
            {
                if (objs.ContainsKey("gameGold"))
                {
                    int gameGold = (int)objs["gameGold"];
                    AlertDialog.Get().ShowDialog(GVars.language == "zh" ? string.Format("恭喜您通过绑定帐号获得分享奖励{0}币！", gameGold - GVars.user.gameGold)
                        : string.Format("Congratulations,you get {0} coins from sharing rewards by binding account", gameGold - GVars.user.gameGold));
                    GVars.user.gameGold = gameGold;
                    AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_GoldAndLottery);

                    if (GainADelbtn.transform.Find("ReceiveButton").gameObject.activeSelf)
                    {
                        GainADelbtn.transform.Find("ReceiveButton").GetComponent<Button>().interactable = false;
                        GainADelbtn.transform.Find("DeleteButton").GetComponent<Button>().interactable = true;
                    }
                }
            }
            else
            {
                int msgCode = (int)objs["msgCode"];
                switch(msgCode)
                {
                    case 0:
                        AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "程序异常" : "");
                        break;
                    case 14:
                        AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "无效的参数" : "");
                        break;
                    case 26:
                        AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "已领取" : "");
                        break;
                    default:
                        string msg = (string)objs["msg"];
                        AlertDialog.Get().ShowDialog(msg);
                        break;
                }
            }
        }

        private void HandleNetMsg_Mail(object[] obj)
        {
            Dictionary<string,object> mail = obj[0] as Dictionary<string,object>;

            bool getResult = (bool)mail["success"];
            Debug.Log("getResult: " + getResult);
            if (!getResult) return;

            object[] emailList = (object[])mail["mailList"];
            int length = emailList.Length;
            if (length == 0) return;
            Mail[] mm = new Mail[length];

            for (int i = 0; i < length; i++)
            {
                mm[i] = Mail.CreateWithDic((Dictionary<string, object>)emailList[i]);
            }
            Debug.Log("mm: " + mm[0].mailName);
                                                      
            if (length <= 3)
            {
                grid.GetComponent<RectTransform>().sizeDelta = new Vector2(992, 400);
            }
            if (length > 3)
            {
                grid.GetComponent<RectTransform>().sizeDelta = new Vector2(992, 350 + (length - 3) * 100);
                grid.GetComponent<RectTransform>().localPosition = new Vector3(0, (length - 3) * -52, 0);
            }

            for(int i=0;i<length;i++)
            {
                //toggleGroup = new Toggle[length];
                //GameObject[] item = new GameObject[length];
                GameObject go = Instantiate(mailItem);
               // item[i] = Instantiate(mailItem);
                go.name = "MailItem" + i;
                go.transform.SetParent(this.transform.Find("bg/ItemBg/Grid"));
                go.transform.localScale = new Vector3(1, 1, 1);
                go.transform.localPosition = new Vector3(0, 0, 0);
                go.tag = "MailItem";
                go.GetComponent<MailItem>().Init(mm[i]);
                go.GetComponent<Button>().onClick.AddListener(() =>
                {
                    Debug.Log("afsdf");
                    m_selectMail = go.name;
                    Debug.Log(m_selectMail);
                    go.GetComponent<MailItem>().Read();
                    //mailList[i].mailIsReadStatus = 1;
                    ShowMailContent(go.GetComponent<MailItem>().mail);                  
                });
            }

        }

        public void OnBtnClick_SelectAll()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            isAllSelect = true;
            GameObject[] go = GameObject.FindGameObjectsWithTag("MailItem");
            foreach (GameObject g in go)
            {
                if(g.GetComponent<MailItem>().toggleMail.isOn ==false)
                {
                    isAllSelect = false;
                }
            }
            foreach (GameObject g in go)
            {
                g.GetComponent<MailItem>().CheckToggle(!isAllSelect);
            }
        }

        public void OnBtnClick_Delete()
        {
            SoundManager.Get().PlaySound(SoundType.Mail_Delete);
            string del = "";
            int allAward = 0;
            GameObject[] go = GameObject.FindGameObjectsWithTag("MailItem");
            if (go.Length == 0) return;
            foreach (var g in go)
            {
                //g.transform.GetComponent<MailItem>().mail.mailId;
                if (g.transform.GetComponent<MailItem>().toggleMail.isOn)
                {
                    del = del+g.transform.GetComponent<MailItem>().mail.mailId+",";
                    if(g.transform.GetComponent<MailItem>().mail.gainStatus==1)
                    {
                        string content = g.transform.GetComponent<MailItem>().mail.mailContent;
                        string[] strContent =Regex.Split(content, ",");
                        int awar = int.Parse(strContent[0]);
                        allAward += awar;
                    }
                }
                g.transform.GetComponent<MailItem>().DeleteItem(false);
            }

            if (del=="")
            {
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "请选择要删除的邮件" : "Please select the message");
                return;
            }

            if (allAward == 0)
            {
                SendUpdateMail(del);
            }
            else
            {
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? string.Format("邮件含有未领取的奖励，删除后您将直接获得{0}游戏币!", allAward) 
                    :string.Format("The message contains rewards,after deletion,you will receive {0} coins directly", allAward)
                    , true
                    , () => { SendUpdateMail(del); }
                    );
            }                               
        }

        private void SendUpdateMail(string strDel)
        {
            Debug.Log("strDel1: " + strDel);
            if (strDel.Substring(strDel.Length - 1, 1) == ",")
            {
                strDel = strDel.Substring(0, strDel.Length - 1);
            }
            Debug.Log("strDel2: " + strDel);
            NetManager.GetInstance().Send("gcuserService/updateEmail", new object[] { strDel, 2 });//      
        }

        public void OnBtnClick_Back()
        {
            int mailNoReadCount = 0;
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            GameObject[] go = GameObject.FindGameObjectsWithTag("MailItem");
            if (go.Length == 0) Debug.Log("nullnullnullnullnull");
            //List<Mail> tr = new List<Mail>();
            foreach (GameObject g in go)
            {
                //tr.Add(g.GetComponent<MailItem>().mail);
                if (g.transform.GetComponent<MailItem>().mail.mailIsReadStatus == 0)
                {
                    mailNoReadCount++;
                }
                g.transform.GetComponent<MailItem>().DeleteItem(true);
            }

            //Dictionary<string, object> table = new Dictionary<string, object>();
            //table.Add("newmail", tr);
            //NetManager.GetInstance().Send("gcuserService/newmail", new object[] { table });//      

            Debug.Log("mailNoReadCount: " + mailNoReadCount);

            AppManager.Get().Notify(UIGameMsgType.UINotify_Fresh_UseMailSign, mailNoReadCount);

            this.gameObject.SetActive(false);
        }

        public void OnBtnClick_MailContentBack()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            Debug.Log("close");
            ClearItem();
            m_mailContent.SetActive(false);
        }

        public void ReceiveShareAward()
        {
            NetManager.GetInstance().Send("gcuserService/updateEmail", new object[] { currentMail.mailId + "", 3 });//      
        }

        public void ShowMailContent(Mail mail)
        {
            this.currentMail = mail;
            SoundManager.Get().PlaySound(SoundType.Mail_Read);
            GameObject del = this.transform.Find("bg/ItemBg/Grid").Find(m_selectMail).gameObject;

            NetManager.GetInstance().Send("gcuserService/updateEmail", new object[] { del.GetComponent<MailItem>().mail.mailId + "", 1 });//      

            Debug.Log("ShowMailContent"+name);

            m_mailContent.SetActive(true);
            mailSend.text = GVars.language == "zh" ? string.Format("发件人：{0}", mail.mailSendPeople) : string.Format("From：{0}", mail.mailSendPeople);
            mailTime.text = GVars.language == "zh" ? string.Format("发件时间：{0}", mail.mailTime) : string.Format("Date：{0}", mail.mailTime);

            onlyDeletBtn.SetActive(mail.gainStatus == 0);
            GainADelbtn.SetActive(!(mail.gainStatus == 0));

            GainADelbtn.transform.Find("ReceiveButton").GetComponent<Button>().interactable=mail.gainStatus == 1;
            GainADelbtn.transform.Find("DeleteButton").GetComponent<Button>().interactable = mail.gainStatus == 2;

            Debug.Log("mail.gainStatus: " + mail.gainStatus);

            mailContent.transform.parent.gameObject.SetActive(mail.gainStatus == 0);
            mailContent2.SetActive(!(mail.gainStatus == 0));

            if (mail.gainStatus == 0)//0-不需要领取
            {
                mailContent.text = mail.mailContent;
                
            }
            else// 1-未领取金币 2-已领取金币
            {
                string[] str =Regex.Split(mail.mailContent, ",");
                int length = str.Length;
                int count = (length - 1) / 2;

                awardItemList = new GameObject[count];

                mailContent2.transform.Find("content").GetComponent<Text>().text = GVars.language == "zh" ? string.Format("恭喜您通过绑定账号获得分享奖励{0}币", str[0]) : string.Format("Congratulations,you get {0} coins from sharing rewards by binding account", str[0]);
                mailContent2.transform.Find("content").GetComponent<Text>().fontSize = GVars.language == "zh" ? 30 : 20;
                for (int i = 0; i < count; i++)
                {
                    GameObject go = Instantiate(awardItem);
                    awardItemList.SetValue(go, i);
                    go.transform.SetParent(mailContent2.transform.Find("scroll/Grid"));
                    go.transform.localPosition = Vector3.zero;
                    go.transform.localScale = Vector3.one;
                    go.transform.Find("account").GetComponent<Text>().text = str[i * 2 + 1];
                    go.transform.Find("award").GetComponent<Text>().text = str[i *2+ 2];
                }

                if(count<4)
                {
                    mailContent2.transform.Find("scroll/Grid").GetComponent<RectTransform>().sizeDelta = new Vector2(797, 160);
                }
                if(count>4)
                {
                    mailContent2.transform.Find("scroll/Grid").GetComponent<RectTransform>().sizeDelta = new Vector2(797, 160 + (count - 3) * 50);
                    mailContent2.transform.Find("scroll/Grid").GetComponent<RectTransform>().localPosition = new Vector3(0, -50 * (count - 3), 0);
                }
            }
            
        }

        public void OnBtnClick_MailContentDelete()
        {
            SoundManager.Get().PlaySound(SoundType.Mail_Delete);
            GameObject del = this.transform.Find("bg/ItemBg/Grid").Find(m_selectMail).gameObject;

            NetManager.GetInstance().Send("gcuserService/updateEmail", new object[] { del.GetComponent<MailItem>().mail.mailId + "", 2 });//      
          
            Destroy(del);
            ClearItem();
            m_mailContent.SetActive(false);
        }

        private void ClearItem()
        {
            if(awardItemList!=null)
            {
                foreach(var v in awardItemList)
                {
                    Destroy(v);
                }
            }
        }
    }
}