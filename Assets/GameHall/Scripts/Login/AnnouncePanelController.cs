﻿/*
 *  author: jiangwen
 *  creation date: 2016/3/31
 *  modified by: jiangwen
 *  modified date: 
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using System.Collections.Generic;

namespace WMCY.GameHall
{
    public class AnnouncePanelController : MonoBehaviour
    {
        public Text m_textAnnounceTitle;
        public Text m_textAnnounceContent;

        void Start()
        {
            
            //m_textAnnounceContent = this.GetComponentInChildren<Text>();
        }

        void Update()
        {

        }

        public void HandleNetMsg_Announce(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            Dictionary<string, object> notice = obj["notice"] as Dictionary<string, object>;
            string title = (string)notice["title"];
            string content = (string)notice["content"];
            m_textAnnounceTitle.text = title;
            m_textAnnounceContent.text = content;
			m_textAnnounceContent.GetComponent<RectTransform>().sizeDelta = new Vector2(600, m_textAnnounceContent.preferredHeight);
            m_textAnnounceContent.transform.localPosition = new Vector3(0,-2000,0);
        }

        public void OnBtnClick_Close()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            gameObject.SetActive(false);
        }
    }
}
