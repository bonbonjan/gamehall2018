﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace WMCY.GameHall
{
    public class MailItem : MonoBehaviour
    {
        public Sprite[] photo;

        public Image readImage;
        public Text textMailName;
        public Text textMailSender;
        public Text textMailSendTime;
        public Toggle toggleMail;

        public Mail mail;
        //System.Action<Mail> act;

        void Start()
        {

        }

        public void Init(Mail mail)
        {
            //act = a;
            this.mail = mail;
            readImage.sprite = mail.mailIsReadStatus == 0 ? photo[0] : photo[1];
            textMailName.text = mail.mailName;
            textMailName.fontSize = 30;
            if (mail.mailName == "本周分享奖励结算" || mail.mailName == "本周分享奖励")
            {
                textMailName.text = GVars.language == "zh" ? mail.mailName : "Invitation Award";
                textMailName.fontSize = GVars.language == "zh" ? 30 : 22;
            }
            textMailSender.text = mail.mailSendPeople;
            textMailSendTime.text = mail.mailTime;

            if (mail.mailIsReadStatus==1)
            {
                textMailName.color = new Color(0.74f, 0.74f, 0.74f, 0.63f);
                textMailSender.color = new Color(0.74f, 0.74f, 0.74f, 0.63f);
                textMailSendTime.color = new Color(0.74f, 0.74f, 0.74f, 0.63f);

                this.transform.Find("send").GetComponent<Text>().color = new Color(0.74f, 0.74f, 0.74f, 0.63f);
                this.transform.Find("sendtime").GetComponent<Text>().color = new Color(0.74f, 0.74f, 0.74f, 0.63f);
            }

            toggleMail.isOn = false;
        }

        public void CheckToggle(bool b)
        {
            toggleMail.isOn = b;
        }

        public void Read()
        {
            mail.mailIsReadStatus = 1;
            readImage.sprite = photo[1];

            textMailName.color = new Color(0.74f, 0.74f, 0.74f, 0.63f);
            textMailSender.color = new Color(0.74f, 0.74f, 0.74f, 0.63f);
            textMailSendTime.color = new Color(0.74f, 0.74f, 0.74f, 0.63f);

            this.transform.Find("send").GetComponent<Text>().color = new Color(0.74f, 0.74f, 0.74f, 0.63f);
            this.transform.Find("sendtime").GetComponent<Text>().color = new Color(0.74f, 0.74f, 0.74f, 0.63f);
        }

        public void DeleteItem(bool check)
        {
            if (check)
            {
                Destroy(this.gameObject);
            }
            else
            {
                if (toggleMail.isOn)
                {
                    Destroy(this.gameObject);
                }
            }          
        }
    }
}