﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
namespace WMCY.GameHall
{
    public class CampaignItem : MonoBehaviour
    {
        //public int campaignID;
        public Image imageCampaginSign;
        public Text textCampaignContent;
        public int campaignStatus = 0;
        public Text textCampaginSchedule;
        public GameObject buttonComplete;
        public Text awardGold;


        public Campaign cap;

        private Color[] colorText = new Color[3]{
        new Color(0.96f,0.92f,0.69f),
        new Color(0.45f,0.25f,0),
        new Color(0.74f,0.74f,0.74f)
    };

        // private Action<CampaignItem> onSendComplete;

        Sprite[] s;

        void Start()
        {

        }

        void Update()
        {

        }

        public void Init(Campaign c, Sprite[] s)
        {
            this.cap = c;
            this.s = s;
            //campaignID = c.Id;       
            textCampaignContent.text = c.content;// GVars.language == "zh" ? c.content : c.content_en;

            if (c.type == 6)
            {
                textCampaignContent.GetComponent<RectTransform>().sizeDelta = new Vector2(870,66);
                textCampaignContent.GetComponent<RectTransform>().localPosition = new Vector3(60, 0, 0);
                textCampaginSchedule.gameObject.SetActive(false);
                buttonComplete.SetActive(false);
                return;
            }
            campaignStatus = c.status;

            if (c.userSchedule > c.targetSchedule)
            {
                c.userSchedule = c.targetSchedule;
            }

            textCampaginSchedule.text = c.userSchedule + "/" + c.targetSchedule;

            if(c.type==4 || c.type==5)
            {
                textCampaginSchedule.gameObject.SetActive(false);
            }

            buttonComplete.GetComponent<Image>().sprite = s[campaignStatus];
            if (campaignStatus != 1)
            {
                buttonComplete.GetComponent<Button>().enabled = false;
            }
            awardGold.text = c.awardGold + "";
            awardGold.color = colorText[campaignStatus];
            if (campaignStatus == 2)
            {
                awardGold.text = GVars.language == "zh" ? "已完成" : "Finished";
                if(GVars.language == "zh")
                {
                    awardGold.fontSize = 36;
                }
                if (GVars.language == "en")
                {
                    awardGold.fontSize = 28;
                }
            }
            awardGold.font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            //onSendComplete = action;//
        }

        public void ChangeButtonStatus()
        {
            cap.status = 2;
            campaignStatus = cap.status;
            buttonComplete.GetComponent<Image>().sprite = s[campaignStatus];

            buttonComplete.GetComponent<Button>().enabled = false;

            awardGold.text = GVars.language == "zh" ? "已完成" : "Finished";
            if (GVars.language == "zh")
            {
                awardGold.fontSize = 36;
            }
            if (GVars.language == "en")
            {
                awardGold.fontSize = 28;
            }
            awardGold.font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            awardGold.color = colorText[campaignStatus];
        }
    }
}