﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using System.Collections.Generic;

namespace WMCY.GameHall
{
    public class BackgroundControl : MonoBehaviour
    {
        [SerializeField]
        private Text title;
        [SerializeField]
        private GameObject ExchargeNum;
        [SerializeField]
        private GameObject PwdInput;
        [SerializeField]
        private Text CountdownText;
        [SerializeField]
        private GameObject cancelButton;
        [SerializeField]
        private GameObject sureNutton;

        private float runTime = 0;
        private int totalTime = 0;
        private bool isStartCountDown = false;
        private int nType = 0;

        // Use this for initialization
        void Start()
        {
            
            NetManager.GetInstance().RegisterHandler("tellServerUserPassword", HandleNetMsg_TellServerUserPassword);
            NetManager.GetInstance().RegisterHandler("cancelExpiryPwd", HandleNetMsg_CancelExpiryPwd);
            
        }

        // Update is called once per frame
        void Update()
        {
            if (isStartCountDown)
            {
                runTime += Time.deltaTime;
                if (runTime >= 1)
                {
                    runTime = 0;
                    totalTime -= 1;
                }
                Debug.Log("totalTime: " + totalTime);
                if (totalTime == -1)
                {
                    TimeOut();
                }
                else
                {
                    CountdownText.text = GVars.language == "zh" ? string.Format("剩余{0}分钟{1}秒", totalTime / 60, totalTime % 60) : string.Format("{0} minutes {1} seconds remaining", totalTime / 60, totalTime % 60);//minutes seconds remaining
                }
            }
        }

        public void TimeOut()
        {
            CountdownText.text = GVars.language == "zh" ? "超时" : "Timeout";
            runTime = 0;
            isStartCountDown = false;
            OnBtnClick_Canel();
            ResetPanel();
        }

        public void Init(int type,int gameScore)
        {
            nType = type;
            this.gameObject.SetActive(true);
            if (type == 0)
            {
                title.text = GVars.language == "zh" ? "兑奖" : "Exchange";
                title.font = GVars.language == "zh" ? AppManager.Get().zh_font_title : AppManager.Get().en_font_title;
                ExchargeNum.SetActive(true);
                ExchargeNum.transform.Find("Goldbg/Gold").GetComponent<Text>().text=gameScore+"";
            }
            if(type==1)
            {
                title.text = GVars.language == "zh" ? "平板租借" : "Rent tablet";
                title.font = GVars.language == "zh" ? AppManager.Get().zh_font_title : AppManager.Get().en_font_title;
                ExchargeNum.SetActive(false);
                //PwdInput.SetActive(true);
            }
            if (type == 2)
            {
                title.text = GVars.language == "zh" ? "平板归还" : "Return tablet";
                title.font = GVars.language == "zh" ? AppManager.Get().zh_font_title : AppManager.Get().en_font_title;
                ExchargeNum.SetActive(false);
            }
            if(type==3)
            {
                title.text = GVars.language == "zh" ? "推广员兑换" : "Extension exchange";
                title.font = GVars.language == "zh" ? AppManager.Get().zh_font_title : AppManager.Get().en_font_title;
                ExchargeNum.SetActive(false);
            }

            CountdownControl();
        }

        //public void ResetPanel()
        //{
        //    this.gameObject.SetActive(false);
        //}
        private void ResetPanel()
        {
            PwdInput.transform.Find("PwdInputField").GetComponent<InputField>().text = "";
            this.gameObject.SetActive(false);
        }

        public void OnBtnClick_Sure()
        {
            string pwd = PwdInput.transform.Find("PwdInputField").GetComponent<InputField>().text;
            NetManager.GetInstance().Send("gcuserService/tellServerUserPassword", new object[] { nType,pwd });//       cancelExpiryPwd
            ResetPanel();
        }


        public void OnBtnClick_Canel()
        {
            NetManager.GetInstance().Send("gcuserService/cancelExpiryPwd", new object[] { GVars.user.id});
            //this.gameObject.SetActive(false);
        }

        public void CountdownControl()
        {
            CountdownText.gameObject.SetActive(true);
            isStartCountDown = true;
            totalTime = 120;
            runTime = 0;
            CountdownText.text = GVars.language == "zh" ? string.Format("剩余{0}分钟{1}秒", totalTime / 60, totalTime % 60) : string.Format("{0} minutes {1} seconds remaining", totalTime / 60, totalTime % 60);//minutes seconds remaining
            CountdownText.font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
        }


        private void HandleNetMsg_TellServerUserPassword(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            int nType = (int)obj["nType"];
            bool bFlag = (bool)obj["bFlag"];

            if (bFlag)
            {
                switch (nType)
                {
                    case 0:
                        //AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "兑奖成功" : "");//后面的chargenotice会弹框
                        break;
                    case 1:
                        AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "平板租借成功" : "");
                        break;
                    case 2:
                        AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "平板归还成功" : "");
                        break;
                    case 3:
                        AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "推广员成功" : "");
                        break;
                }
            }
            else
            {
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "密码错误" : "password error");
            }

        }

 

        private void HandleNetMsg_CancelExpiryPwd(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            bool success = (bool)obj["success"];
            string msg = (string)obj["msg"];
            if (success)
            {
                ResetPanel();
            }
            else
            {
                AlertDialog.Get().ShowDialog(msg);
            }

        }



    }
}
