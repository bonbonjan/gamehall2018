﻿using UnityEngine;
using System.Collections;

namespace WMCY.GameHall
{
    public class ToggleController : MonoBehaviour
    {

        public GameObject toggleOn;
        public GameObject toggleOff;
        public GameObject imagebg;
        public System.Action<bool> onToggle = null;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnBtnClick_Toogle()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            toggleOn.SetActive(!toggleOn.activeSelf);
            toggleOff.SetActive(!toggleOff.activeSelf);
            imagebg.SetActive(toggleOn.activeSelf);
            if (onToggle != null)
                onToggle(!toggleOn.activeSelf);
        }

        public void SetValue(bool value)
        {
            toggleOn.SetActive(value);
            toggleOff.SetActive(!value);
            imagebg.SetActive(value);
        }
    }
}