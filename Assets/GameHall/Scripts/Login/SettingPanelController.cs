﻿/*
 *  author: jiangwen
 *  creation date: 2016/3/31
 *  modified by: jiangwen
 *  modified date: 2016/5/17
 */

using UnityEngine;
using System.Collections;

namespace WMCY.GameHall
{


    public class SettingPanelController : MonoBehaviour
    {

        public GameObject about;
        [SerializeField]
        ToggleController m_toggleCtrl_Sound;
        [SerializeField]
        ToggleController m_toggleCtrl_BG;


        void Start()
        {
            m_toggleCtrl_Sound.onToggle = OnToggle_Sound;
            m_toggleCtrl_BG.onToggle = OnToggle_BG;

            Init();
        }

        void Update()
        {

        }

        public void OnBtnClick_ShowAbout()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            about.SetActive(true);
        }

        public void OnBtnClick_CloseAbout()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            about.SetActive(false);
        }

        public void OnBtnClick_CloseSetting()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            this.gameObject.SetActive(false);
        }

        public void OnToggle_Sound(bool newValue)
        {
            Debug.Log("OnToggle_Sound: "+ newValue);
            SoundManager.Get().SetSilenceSound(newValue);
            SoundManager.Get().PlaySound(SoundType.Common_Click);
        }

        public void OnToggle_BG(bool newValue)
        {
            Debug.Log("OnToggle_BG: " + newValue);
            SoundManager.Get().SetSilenceBG(newValue);
            SoundManager.Get().PlaySound(SoundType.Common_Click);
        }

        public void OnToggle_TestUninstall(bool newValue)
        {
            Debug.Log("OnToggle_TestUninstall: " + newValue);
            InnerGameManager.Get().debug_Uninstall = newValue;
            InnerGameManager.Get().Refresh_UninstallBtnState();
        }

        public void Init()
        {
            m_toggleCtrl_Sound.SetValue(!SoundManager.Get().IsSilenceSound());
            m_toggleCtrl_BG.SetValue(!SoundManager.Get().IsSilenceBG());
        }
    }
}