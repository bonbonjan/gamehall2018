﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;

namespace WMCY.GameHall
{
    public class SafeBoxPanelController : MonoBehaviour
    {
        private int m_inputNum;
        private int m_curTabIndex = 0;
        void Awake()
        {
            FocusTabByIndex(1);
        }

        void Start()
        {
            NetManager.GetInstance().RegisterHandler("deposit", HandleNetMsg_Deposit);
            NetManager.GetInstance().RegisterHandler("extract", HandleNetMsg_Extract);
        }

        void OnEnable()
        {
            FocusTabByIndex(1);
            //SwitchIntoEnglish();
            transform.Find("neikuang/input").GetComponent<InputField>().text = "";
            transform.Find("tab/tab2").gameObject.SetActive(GVars.payMode == 1 ? true : false);
            if (GVars.payMode == 1)
            {
                transform.Find("tab/tab1").localPosition = new Vector3(-190f, 152.7f,0);
                transform.Find("tab/tab1/tabHighlight/Image1").localPosition = new Vector3(311.6f, -98.26f, 0);
            }
            else
            {
                 transform.Find("tab/tab1").localPosition = new Vector3(0, 152.7f, 0);
                 transform.Find("tab/tab1/tabHighlight/Image1").localPosition = new Vector3(121f, -98.26f, 0);
            }
        }

        void OnDisable()
        {
            
        }

        /*
         * 清除列表
         */
        void DestroyList()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
                return;
            transform.Find("titleBg/title").GetComponent<Text>().text = "Safe case";
            transform.Find("titleBg/title").GetComponent<Text>().fontSize = 30;
            transform.Find("tab/tab1/Text").GetComponent<Text>().text = "Access coin";
            transform.Find("tab/tab1/tabHighlight/Text").GetComponent<Text>().text = "Access coin";
            transform.Find("tab/tab2/Text").GetComponent<Text>().text = "Access lottery";
            transform.Find("tab/tab2/tabHighlight/Text").GetComponent<Text>().text = "Access lottery";
            transform.Find("neikuang/Text").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("neikuang/Text").GetComponent<Text>().text = "Currently stored:";
            transform.Find("neikuang/Text1").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("neikuang/Text1").GetComponent<Text>().text = "Access Number:";
            transform.Find("neikuang/input/Placeholder").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("neikuang/input/Placeholder").GetComponent<Text>().text = "input Access Number:";
            transform.Find("inBtn/Text").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial",25);
            transform.Find("inBtn/Text").GetComponent<Text>().text = "Deposit";
            transform.Find("outBtn/Text").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("outBtn/Text").GetComponent<Text>().text = "Take out";
            transform.Find("inBtn/Image").localPosition = new Vector3(66.25f, 4, 0);
            transform.Find("outBtn/Image").localPosition = new Vector3(66.25f, 4, 0);
            transform.Find("neikuang/Text").localPosition += new Vector3(40,0,0);
            transform.Find("neikuang/savedNum").localPosition += new Vector3(40, 0, 0);
            transform.Find("tab/tab1/tabHighlight/Image1").localPosition += new Vector3(40, 0, 0);
            transform.Find("tab/tab2/tabHighlight/Image2").localPosition += new Vector3(40, 0, 0);
            transform.Find("neikuang/Text1").localPosition += new Vector3(40, 0, 0);
            transform.Find("neikuang/input").localPosition += new Vector3(40, 0, 0);

            transform.Find("titleBg/title").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("tab/tab1/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("tab/tab1/tabHighlight/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("tab/tab2/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("tab/tab2/tabHighlight/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("neikuang/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("neikuang/Text1").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("neikuang/input/Placeholder").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("inBtn/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("outBtn/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
        }

        /*
         * 更新内容
         */
        void UpdateContent()
        {
            transform.Find("neikuang/savedNum").GetComponent<Text>().text = "" + (m_curTabIndex == 1 ? GVars.savedGameGold : GVars.savedLottery);
        }

        void Update()
        {

        }

        public void OnBtnClick_Close()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            gameObject.SetActive(false);
        }

        public void OnBtnClick_In()
        {
            string num = transform.Find("neikuang/input").GetComponent<InputField>().text;
            if (num == "")
            {
                string hint = GVars.payMode == 1 ? string.Format(m_curTabIndex == 1 ? "请输入存入游戏币的数量" : "请输入存入彩票的数量") : "请输入存入游戏币的数量";
                string hint_en = GVars.payMode == 1 ? string.Format(m_curTabIndex == 1 ? "Please enter the number of coins that need to be saved" : "Please enter the number of lottery that need to be saved") : "Please enter the number of coins that need to be saved";
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? hint : hint_en);
                return;
            }
            m_inputNum = Convert.ToInt32(num);
            if (m_inputNum <= 0)
            {
                string hint = GVars.payMode == 1 ? string.Format(m_curTabIndex == 1 ? "请输入存入游戏币的数量" : "请输入存入彩票的数量") : "请输入存入游戏币的数量";
                string hint_en = GVars.payMode == 1 ? string.Format(m_curTabIndex == 1 ? "Please enter the number of coins that need to be saved" : "Please enter the number of lottery that need to be saved") : "Please enter the number of coins that need to be saved";
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? hint : hint_en);
                return;
            }
            NetManager.GetInstance().Send("gcsecurityService/deposit", new object[] { m_inputNum, m_curTabIndex });
            SoundManager.Get().PlaySound(SoundType.Common_Click);
        }

        public void OnBtnClick_Out()
        {
            string num = transform.Find("neikuang/input").GetComponent<InputField>().text;
            if (num == "")
            {
                string hint = GVars.payMode == 1 ? string.Format(m_curTabIndex == 1 ? "请输入取出游戏币的数量" : "请输入取出彩票的数量") : "请输入取出游戏币的数量";
                string hint_en = GVars.payMode == 1 ? string.Format(m_curTabIndex == 1 ? "Please enter the number of coins that need to be taken out" : "Please enter the number of lottery that need to be taken out") : "Please enter the number of coins that need to be taken out";
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? hint : hint_en);
                return;
            }
            m_inputNum = Convert.ToInt32(num);
            if (m_inputNum <= 0)
            {
                string hint = GVars.payMode == 1 ? string.Format(m_curTabIndex == 1 ? "请输入存入游戏币的数量" : "请输入存入彩票的数量") : "请输入存入游戏币的数量";
                string hint_en = GVars.payMode == 1 ? string.Format(m_curTabIndex == 1 ? "Please enter the number of coins that need to be saved" : "Please enter the number of lottery that need to be saved") : "Please enter the number of coins that need to be saved";
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? hint : hint_en);
                return;
            }
            else if (m_curTabIndex == 1 && m_inputNum > GVars.savedGameGold)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游戏币不足" : "Coin shortage");
                return;
            }
            else if (m_curTabIndex == 2 && m_inputNum > GVars.savedLottery)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "彩票不足" : "Lottery shortage");
                return;
            }
            NetManager.GetInstance().Send("gcsecurityService/extract", new object[] { m_inputNum, m_curTabIndex });
            SoundManager.Get().PlaySound(SoundType.Common_Click);
        }

        public void OnBtnClick_Tab1()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            FocusTabByIndex(1);
        }
        public void OnBtnClick_Tab2()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            FocusTabByIndex(2);

        }

        void FocusTabByIndex(int index)
        {
            //if (m_curTabIndex == index)
            //{
            //    return;
            //}
            m_curTabIndex = index;
            UpdateContent();
            for (int i = 1; i < 3; i ++ )
            {
                transform.Find(string.Format("tab/tab{0}/tabHighlight", i)).gameObject.SetActive(i == index ? true : false);
            }
        }

        void HandleNetMsg_Deposit(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            Debug.Log(obj);
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                string zhTip = string.Format(GVars.language == "zh" ? "您已经成功存入{0}{1}" : "You have successfully deposited {0}{1}", m_inputNum, m_curTabIndex == 1 ? (GVars.language == "zh" ? "游戏币" : " coins") : (GVars.language == "zh" ? "彩票" : " lottery"));
                AlertDialog.GetInstance().ShowDialog(zhTip);
                GVars.savedGameGold = (int)obj["boxGameGold"];
                GVars.savedLottery = (int)obj["boxLottery"];
                GVars.user.gameGold = (int)obj["gameGold"];
                GVars.user.lottery = (int)obj["lottery"];
                transform.Find("neikuang/input").GetComponent<InputField>().text = "";
                transform.Find("neikuang/savedNum").GetComponent<Text>().text = "" + (m_curTabIndex == 1 ? GVars.savedGameGold : GVars.savedLottery);
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_GoldAndLottery);
            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                Debug.Log(msgcode);
                switch (msgcode)
                {
                    case 2:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号或密码错误" : "Account name or password error, please enter again");
                        break;
                    case 3:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码错误" : "password error, please enter again");
                        break;
                    case 6:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被冻结" : "This account has been frozen");
                        break;
                    case 7:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "系统维护" : "Server maintenance");
                        break;
                    case 11:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请勿重复申请" : "Please do not repeat the application");
                        break;
                    case 12:
                        transform.Find("neikuang/input").GetComponent<InputField>().text = "";
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "余额不足" : "Your balance is not enough");
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "存取失败" : "deposited failed");
                        break;
                }
            }
        }

        void HandleNetMsg_Extract(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                string zhTip = string.Format(GVars.language == "zh" ? "您已经成功取出{0}{1}" : "You have successfully take out {0}{1}", m_inputNum, m_curTabIndex == 1 ? (GVars.language == "zh" ? "游戏币" : " coins") : (GVars.language == "zh" ? "彩票" : " lottery"));
                AlertDialog.GetInstance().ShowDialog(zhTip);
                GVars.savedGameGold = (int)obj["boxGameGold"];
                GVars.savedLottery = (int)obj["boxLottery"];
                GVars.user.gameGold = (int)obj["gameGold"];
                GVars.user.lottery = (int)obj["lottery"];
                transform.Find("neikuang/input").GetComponent<InputField>().text = "";
                transform.Find("neikuang/savedNum").GetComponent<Text>().text = "" + (m_curTabIndex == 1 ? GVars.savedGameGold : GVars.savedLottery);
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_GoldAndLottery);
            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                Debug.Log(msgcode);
                switch (msgcode)
                {

                    case 2:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号或密码错误" : "Account name or password error, please enter again");
                        break;
                    case 3:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码错误" : "password error, please enter again");
                        break;
                    case 6:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被冻结" : "This account has been frozen");
                        break;
                    case 7:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "系统维护" : "Server maintenance");
                        break;
                    case 11:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请勿重复申请" : "Please do not repeat the application");
                        break;
                    case 12:
                        transform.Find("neikuang/input").GetComponent<InputField>().text = "";
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "余额不足" : "Your balance is not enough");
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "存取失败" : "take out failed");
                        break;
                }
            }
        }
    }
}