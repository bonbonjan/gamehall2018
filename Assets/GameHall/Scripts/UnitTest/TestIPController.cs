﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace WMCY.GameHall
{
    public class TestIPController : MonoBehaviour
    {
        [SerializeField]
        private InputField index;
        [SerializeField]
        private Text show;

        private string IP = "";
        private bool isCorrectIP = true;
        private bool isErrorIP = false;

        private string[] IP_whileList = { 
                                           "47.89.21.34"//海外封测场地IP
                                           ,"47.89.44.225"
                                           ,"118.178.24.158"//内部测试IP 平台组
                                           ,"47.90.46.40"//星力动漫
                                           ,"47.89.43.14"
                                           ,"121.40.187.208"
                                           ,"114.55.179.109"//开发测试服
                                           ,"118.178.138.253"
                                           ,"192.168.1.63"
										   ,"58.82.242.30"//AWS testing
                                       };

        private string[] domainName_whileList = { 
                                                   ".s01th.space"//第一
                                                  ,".oi8t5y.site"//第二
                                                  ,".lwchfkm.xyz"//第三
                                                  ,".10tfmud.club"//第四
                                                  ,".zirttte.com"//第五
                                                  ,".89uter.pub"//第六
                                                  ,".swncd88.vip"//第七
                                                  ,".hlwsccl.top"//低防
                                                  ,".cmgplgg.vip"//第八
                                                  };

        public void ChangeSelectCorrect()
        {
            isCorrectIP = !isCorrectIP;
            Debug.Log("isCorrectIP: " + isCorrectIP);
        }

        public void ChangeSelectError()
        {
            isErrorIP = !isErrorIP;
            Debug.Log("isErrorIP: " + isErrorIP);
        }


        public void GenerateIP()
        {
            if(index.text=="")
            {
                AlertDialog.GetInstance().ShowDialog("请输入数字!");
                return;
            }
            int currentIndex =int.Parse(index.text);

            if (currentIndex > 17)
            {
                AlertDialog.GetInstance().ShowDialog("请输入正确的数字!");
                return;
            }

            Debug.Log("currentIndex: " + currentIndex);
            if(isCorrectIP)
            {
                if(currentIndex<9)
                {
                    IP = IP_whileList[currentIndex];
                }
                else
                {
                    IP =GenerateFakeStr(4,62)+ domainName_whileList[currentIndex - 9];
                }
            }
            else
            {
                if (currentIndex < 9)
                {
                    IP = GenerateFakeStr(Random.Range(2, 4), 9) + "." + GenerateFakeStr(Random.Range(2, 4), 9) + "." + GenerateFakeStr(Random.Range(2, 4), 9);
                }
                else
                {
                    IP = GenerateFakeStr(4, 62) + "." + GenerateFakeStr(Random.Range(5, 8), 62) + "." + GenerateFakeStr(Random.Range(3, 6), 62);
                }
                
            }


            show.text = IP;
            GVars.IPAddress_Game = show.text;
        }

        public void AssignmentIP()
        {
            GVars.IPAddress_Game = show.text;
        }

        private string GenerateFakeStr(int len,int typelen)
        {
            string str = "";
            string chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            for (int i = 0; i < len;i++ )
            {
                str += chars[Random.Range(0, typelen)];
            }

            return str;
        }
    }
}