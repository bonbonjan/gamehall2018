﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

public class ButtonScale : MonoBehaviour,IPointerDownHandler, IPointerUpHandler
{
    public float scale = 1.1f;
    public float pressDuration = 0.1f;
    public float releaseDuration = 0.1f;
    public RectTransform target = null;

    void Awake()
    {
        if (target==null)
        {
            target = GetComponent<RectTransform>();
        }
    }

	void Start () {
	
	}


    public void OnPointerDown(PointerEventData eventData)
    {
        target.DOScale(new Vector3(scale, scale, 1f), pressDuration);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        target.DOScale(new Vector3(1f, 1f, 1f), releaseDuration);

    }
}
