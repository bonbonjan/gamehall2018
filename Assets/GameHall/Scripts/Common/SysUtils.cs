﻿using UnityEngine;
using System.Collections;

public class SysUtils {

    public static void PrintSystemInfo()
    {
        Debug.LogFormat("deviceModel: [{0}]", SystemInfo.deviceModel);
        Debug.LogFormat("deviceName: [{0}]", SystemInfo.deviceName);
        Debug.LogFormat("deviceType: [{0}]", SystemInfo.deviceType);
        Debug.LogFormat("deviceUniqueIdentifier: [{0}]", SystemInfo.deviceUniqueIdentifier);
        Debug.LogFormat("operatingSystem: [{0}]", SystemInfo.operatingSystem);
        Debug.LogFormat("processorCount: [{0}]", SystemInfo.processorCount);
        Debug.LogFormat("processorFrequency: [{0}]", SystemInfo.processorFrequency);
        Debug.LogFormat("processorType: [{0}]", SystemInfo.processorType);
        Debug.LogFormat("systemMemorySize: [{0}]", SystemInfo.systemMemorySize);
    }

    public static void PrintApplicationInfo()
    {
        Debug.LogFormat("platform: [{0}]", Application.platform);
        Debug.LogFormat("bundleIdentifier: [{0}]", Application.identifier);
        Debug.LogFormat("dataPath: [{0}]", Application.dataPath);
        Debug.LogFormat("streamingAssetsPath: [{0}]", Application.streamingAssetsPath);
        Debug.LogFormat("persistentDataPath: [{0}]", Application.persistentDataPath);
        Debug.LogFormat("systemLanguage: [{0}]", Application.systemLanguage);
    }


}
