﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
[RequireComponent(typeof(Image))]
public class AniStar : MonoBehaviour
{
    [SerializeField]
    Image thisImage;
    //[SerializeField]
    float duration = 1f;
    Vector3 maxScale;

    Coroutine coAnim;


    void Awake()
    {
        thisImage = GetComponent<Image>();
        maxScale = transform.localScale;
        transform.localScale = Vector3.zero;
        transform.DOScale(maxScale, duration).SetLoops(-1, LoopType.Yoyo);
        transform.DORotate(new Vector3(0, 0, -45), duration, RotateMode.Fast).SetLoops(-1, LoopType.Yoyo);
    }

    void OnEnable()
    {
        //if (coAnim != null) StopCoroutine(coAnim);
        //coAnim = StartCoroutine(Anim());
        //transform.doro


    }

    IEnumerator Anim()
    {
        while(true)
        {
            yield return null;
        }

        yield break;
    }

}
