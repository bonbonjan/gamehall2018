﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[AddComponentMenu("I2/I2LocalizeImage")]
public class I2LocalizeImage : MonoBehaviour
{


    Dictionary<string, string> _dics;
    [HideInInspector]
    [SerializeField]
    List<string> _keys;
    [HideInInspector]
    [SerializeField]
    List<Sprite> _values;

    Image _image;

    void OnEnable()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying) return;
#endif
        Refresh();
    }

    void Awake()
    {


        PreInit();
    }

    void Start()
    {
        Refresh();

    }

    void PreInit()
    {

        _image = GetComponent<Image>();

    }

    public void InitKeysAndValues()
    {
        Debug.Log("create keys and values");
        _keys = new List<string>();
        _keys.AddRange(I2Localization.GetAllLanguages());
        _values = new List<Sprite>();
        for (int i = 0, count = _keys.Count; i < count; i++)
        {
            _values.Add(null);
        }
    }


    public List<string> GetKeys()
    {
        return _keys;
    }

    public void Refresh()
    {
        //Debug.Log("UpdateText");
        _image = GetComponent<Image>();
        _image.sprite = _getValue();
    }

    Sprite _getValue()
    {
        int index = 0;
        for (int i = 0, count = _keys.Count; i < count; i++)
        {
            if (I2Localization.GetCurLanguage() == _keys[i])
            {
                index = i;
                break;
            }
        }
        return _values[index];
    }

}
