﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[AddComponentMenu("I2/I2LocalizeText")]
public class I2LocalizeText : MonoBehaviour
{

    bool _started = false;


    Dictionary<string, string> _dics;
    [HideInInspector]
    [SerializeField]
    List<string> _keys;
    [HideInInspector]
    [SerializeField]
    List<string> _values;
    [SerializeField]
    int _zh_fontSize;
    [SerializeField]
    int _en_fontSize;
    [SerializeField]
    Font _zh_font;
    [SerializeField]
    Font _en_font;
    void OnEnable()
    {
        //Debug.Log("[I2LocalizeText] OnEnable");

#if UNITY_EDITOR
        if (!Application.isPlaying) return;
#endif
        if (_started) OnLocalize();
        Refresh();
    }

    void Awake()
    {
        //Debug.Log("[I2LocalizeText] Awake");

        PreInit();
    }

    void Start()
    {
        Refresh();
        //Debug.Log("[I2LocalizeText] Start");

#if UNITY_EDITOR
        if (!Application.isPlaying) return;
#endif
        _started = true;
        OnLocalize();
    }

    void PreInit()
    {
        _dics = new Dictionary<string, string>();

        if (_keys == null)
        {

        }

        if (_keys != null && _keys.Count > 0)
        {
            if (_values != null && _values.Count == _keys.Count)
            {
                for (int count = _keys.Count, i = 0; i < count; i++)
                {
                    _dics[_keys[i]] = _values[i];
                }
            }
            else
            {
                Debug.LogError("keys and values do not match");
            }
        }
        else
        {
            Debug.LogError("_keys have no value");
        }

    }

    public void InitKeysAndValues()
    {
        Debug.Log("create keys and values");
        _keys = new List<string>();
        _keys.AddRange(I2Localization.GetAllLanguages());
        _values = new List<string>();
        for (int i = 0, count = _keys.Count; i < count; i++)
        {
            _values.Add("");
        }
    }

    public void OnLocalize()
    {

    }

    public List<string> GetKeys()
    {
        return _keys;
    }

    public void Refresh()
    {
        //Debug.Log("UpdateText");
        Text text = GetComponent<Text>();
        text.text = _getValue();
        if (_zh_fontSize > 0 && I2Localization.GetCurLanguage() == "zh")
        {
            text.fontSize = _zh_fontSize;
        }

        if (_en_fontSize > 0 && I2Localization.GetCurLanguage() == "en")
        {
            text.fontSize = _en_fontSize;
        }
        if(_zh_font != null && I2Localization.GetCurLanguage() == "zh")
        {
            text.font = _zh_font;
        }
        if (_en_font != null && I2Localization.GetCurLanguage() == "en")
        {
            text.font = _en_font;
        }

    }

    string _getValue()
    {
        int index = 0;
        for (int i = 0, count = _keys.Count; i < count; i++)
        {
            if (I2Localization.GetCurLanguage() == _keys[i])
            {
                index = i;
                break;
            }
        }
        return _values[index];
    }

    public void SetValues(string zh, string en)
    {
        _values[0] = zh;
        _values[1] = en;
        Refresh();
    }

}
