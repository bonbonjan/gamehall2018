﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class I2Localization
{
    static string[] _languages = null;
    static HashSet<string> _languageSet;
    static string _defaultLanguage;
    static string _curLanguage;

    static I2Localization()
    {
        _languages = new string[] { "zh", "en" };
        _languageSet = new HashSet<string>();
        foreach(string lang in _languages)
        {
        _languageSet.Add(lang);
        }
        _defaultLanguage = "zh";
        _curLanguage = _defaultLanguage;
    }

    public static bool ChangeLanguage(string language)
    {
        bool success = false;
        foreach (string lang in _languages)
        {
            if (lang == language)
            {
                success = true;
                _curLanguage = language;
            }
        }
        return success;
    }

    public static string[] GetAllLanguages()
    {
        return _languages;
    }

    public static string GetCurLanguage()
    {
        return _curLanguage;
    }

    public static void  SetCurLanguage(string lang)
    {
        if (_languageSet.Contains(lang))
        {
            _curLanguage = lang;
        }
        else
        {
            Debug.LogError(string.Format("unknown language: [{0}]", lang));
        }
    } 
}
