﻿/*
 * author:shen
 * company:wanmeichuangyou
 * 
 */

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;


[CanEditMultipleObjects]
#if UNITY_3_5
[CustomEditor(typeof(I2LocalizeText))]
#else
[CustomEditor(typeof(I2LocalizeText), true)]
#endif
public class I2LocalizeTextEditor : Editor
{
    List<string> _keys;
    I2LocalizeText _text;
    void Awake()
    {
        //Debug.Log("[I2LocalizeTextEditor] Awake");
    }

    void OnEnable()
    {
        //Debug.Log("[I2LocalizeTextEditor] OnEnable");

        _keys = new List<string>();
        //_keys.Add("zh");
        //_keys.Add("en");
        //foreach(string lang in I2Localization.GetAllLanguages())
        //{
        //    _keys.Add(lang);
        //}
        _keys.AddRange(I2Localization.GetAllLanguages());
        //Debug.Log(string.Format("_keys.Count: {0}", _keys.Count));
        if (_text == null)
            _text = target as I2LocalizeText;
        if (_text.GetKeys() == null || _text.GetKeys().Count == 0)
        {
            _text.InitKeysAndValues();
        }

    }

    public override void OnInspectorGUI()
    {
        //Debug.Log("[I2LocalizeTextEditor] OnInspectorGUI");
        serializedObject.Update();
        SerializedProperty spKeys = serializedObject.FindProperty("_keys");
        SerializedProperty spValues = serializedObject.FindProperty("_values");

        //Debug.Log(string.Format("spKeys.arraySize: {0}", spKeys.arraySize));
        //Debug.Log(string.Format("spValues.arraySize: {0}", spValues.arraySize));

        EditorGUI.BeginChangeCheck();
        for (int i = 0, count = spKeys.arraySize; i < count; i++)
        {

            GUILayout.Space(6f);
            EditorTools.SetLabelWidth(80f);

            GUILayout.BeginHorizontal();
            // Key not found in the localization file -- draw it as a text field
            //SerializedProperty sp = EditorTools.DrawProperty(key.stringValue, serializedObject, key.stringValue);
             
            SerializedProperty sp = EditorTools.DrawProperty(spKeys.GetArrayElementAtIndex(i).stringValue, spValues.GetArrayElementAtIndex(i), false);
            if (sp == null)
            {
                Debug.Log("sp is null");
                continue;
            }

            //string myKey = sp.stringValue;
            //bool isPresent = (lang != null) && lang.Contains(myKey);
            // GUI.color = isPresent ? Color.green : Color.red;
            GUILayout.BeginVertical(GUILayout.Width(22f));
            GUILayout.Space(2f);
            //#if UNITY_3_5
            //        GUILayout.Label(isPresent? "ok" : "!!", GUILayout.Height(20f));
            //#else
            //            GUILayout.Label(isPresent ? "\u2714" : "\u2718", "TL SelectionButtonNew", GUILayout.Height(20f));
            //#endif
            GUILayout.EndVertical();
            GUI.color = Color.white;
            GUILayout.EndHorizontal();
        }
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_zh_fontSize"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_en_fontSize"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_zh_font"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_en_font"));
        serializedObject.ApplyModifiedProperties();
        if (EditorGUI.EndChangeCheck())
        {
            _text.Refresh();
        }
    }

    public void UpdateIfDirtyOrScript()
    {
        Debug.Log("[UpdateIfDirtyOrScript]");
    }
}
