﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

public class AniButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public float pressDuration = 0.07f;
    public float releaseDuration = 0.05f;
    public RectTransform target = null;
    public bool m_override;
    public Vector3 m_posOffset = new Vector3(0f,-5f,0f);
    private Vector3 m_posOrigin;
    public Button btn;

    void OnValidate()
    {
        //Debug.Log("OnValidate");
    }
    void Awake()
    {
        if (target == null)
        {
            target = GetComponent<RectTransform>();
        }
        btn = GetComponent<Button>();
        m_posOrigin = transform.localPosition;
    }



    public void OnPointerDown(PointerEventData eventData)
    {
        if (btn !=null && btn.interactable==false)
        {
            return;
        }
        //target.DOScale(new Vector3(scale, scale, 1f), pressDuration);
        target.DOLocalMove(m_posOrigin + m_posOffset, pressDuration);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (btn != null && btn.interactable == false)
        {
            return;
        }
        //target.DOScale(new Vector3(1f, 1f, 1f), releaseDuration);
        target.DOLocalMove(m_posOrigin, releaseDuration);

    }
}
