﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class FollowRTSize : MonoBehaviour {

    public RectTransform target;
    public Vector2 offset = new Vector2(0,0);
    Vector2 oldSize;
    RectTransform thisRT;

    void Awake()
    {
        thisRT = transform as RectTransform;
        if (target)
        {
            oldSize = target.sizeDelta;
        }
    }

	void Start () {
	
	}
	
    void OnValidate()
    {
        if (!target || !thisRT) return;
        Refresh();
    }

	void Update ()
    {
        if (!target) return;
        if (oldSize != target.sizeDelta)
        {
            Refresh();
        }
    }

    void Refresh()
    {
        thisRT.sizeDelta = target.sizeDelta + offset;
        oldSize = target.sizeDelta;
    }
}
