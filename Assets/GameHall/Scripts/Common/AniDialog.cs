﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using DG.Tweening.Core;
using UnityEngine.EventSystems;
using DG.Tweening.CustomPlugins;
using DG.Tweening.Plugins;
[RequireComponent(typeof(CanvasGroup))]
public class AniDialog : MonoBehaviour {

    CanvasGroup m_canvasGroup;
    float m_fade = 0.1f;
    float m_scale = 0.7f;
    float m_duration = 0.2f;

    void Awake()
    {
        if (m_canvasGroup == null)
        {
            m_canvasGroup = GetComponent<CanvasGroup>();
        }
    }

    void OnEnable()
    {
        AniOpen();
    }

    public void AniOpen()
    {
        m_canvasGroup.alpha = m_fade;
        m_canvasGroup.transform.localScale = Vector3.one * m_scale;
        //m_canvasGroup.DOFade(1f, m_duration).SetEase(Ease.OutCubic);
        m_canvasGroup.DOFade(1f, m_duration).SetEase(Ease.OutQuad);
        m_canvasGroup.transform.DOScale(1f, m_duration).SetEase(Ease.OutBack);
    }

    public void AniClose()
    {
        m_canvasGroup.alpha = m_fade;
        m_canvasGroup.DOFade(1f, m_duration);
    }
}
