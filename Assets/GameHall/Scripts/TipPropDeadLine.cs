﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using DG.Tweening;

namespace WMCY.GameHall
{

    public class TipPropDeadLine : MonoBehaviour
    {
        private int m_itemId;
        [SerializeField]
        ShopPropList _shopProplist;
        void Awake()
        {
            
        }

        void Start()
        {
            SwitchIntoEnglish();
            NetManager.GetInstance().RegisterHandler("bugByGameGold", HandleNetMsg_Buy);
        }

        void Update()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
                return;
            transform.Find("btnSure/Text").GetComponent<Text>().text = "Buy";
        }

        /*
         * itemIndex : 5-11
         * */
        public void ShowUI(int itemId)
        {
            m_itemId = itemId;
            transform.Find("tip").GetComponent<Text>().text = string.Format(GVars.language == "zh" ? "您的{0}有效期仅剩一天，是否继续购买?" : "You have only one day validity of {0} , whether to buy again?", GVars.language == "zh" ? _shopProplist.list[m_itemId - 1].zh_name : _shopProplist.list[m_itemId - 1].en_name);
        }

        public void OnBtnClick_Sure()
        {
            Destroy(transform.gameObject);
            int tabIndex = m_itemId <= 8 ? 2 : 3;
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_StorePanel, tabIndex);
        }

        public void OnBtnClick_Cancel()
        {
            Destroy(transform.gameObject);

        }

        public void OnBtnClick_Fader()
        {

        }

        void HandleNetMsg_Buy(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            Debug.Log(obj);
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                Destroy(transform.gameObject);
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请等候系统进行处理" : "Please wait for the system process");

            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                Debug.Log(msgcode);
                switch (msgcode)
                {
                    case 2:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号或密码错误" : "Account name or password error, please enter again");
                        break;
                    case 3:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码错误" : "password error, please enter again");
                        break;
                    case 6:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被冻结" : "This account has been frozen");
                        break;
                    case 7:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "系统维护" : "Server maintenance");
                        break;
                    case 11:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请勿重复申请" : "Please do not repeat the application");
                        break;
                    case 12:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "余额不足" : "Your balance is not enough");
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "处理失败" : "process failed");
                        break;
                }
            }
        }
    }
}