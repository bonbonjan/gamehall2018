﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Mail
{
    public int mailId;//邮件ID
    public string mailName;//邮件标题
    public string mailSendPeople;//邮件发送者
    public string mailTime;//邮件发送时间
    public string mailContent;//邮件内容
    public int mailIsReadStatus;//邮件状态（0 未读 1 已读）
    public int gainStatus;//0-不需要领取 1-未领取金币 2-已领取金币
    //public bool mailIsDelete;//邮件是否被删除

    public static Mail CreateWithDic(Dictionary<string, object> data)
    {
        Mail ret = new Mail();       

        try
        {
            ret.mailName = (string)data["title"];
            ret.mailContent = (string)data["content"];
            ret.mailTime = (string)data["datetime"];
            ret.mailSendPeople = (string)data["sender"];
            //int a = (int)data["receiver"];
            //int b = (int)data["rangeType"];
            ret.mailIsReadStatus = (int)data["status"];
            ret.mailId = (int)data["id"];
            ret.gainStatus = (int)data["gainStatus"];         
        }
        catch (Exception e)
        {
            Debug.LogError("Mail excpetion: " + e.Message);
            string keys = "";
            foreach (string key in data.Keys)
            {
                keys += key + ", ";
            }
            Debug.Log("keys: " + keys);
        }
        return ret;
    }
}
