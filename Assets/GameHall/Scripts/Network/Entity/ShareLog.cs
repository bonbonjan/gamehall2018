﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ShareLog {

    public int UserId;//子账号id
    public string Username;//显示子绑定账号
    public string bindingName;//母账号
    public string bindTime;//绑定时间
    public int countGameGold;//本周游玩
    public int weeklyRecharge;//每周充值金额
    public int weeklyExpiry;// 每周兑奖金额
    public int wardGameGold;//本周奖励

    public static ShareLog CreateWithDic(Dictionary<string, object> data)
    {
        ShareLog ret = new ShareLog();
        try 
        {
            ret.UserId = (int)data["userId"];
            ret.Username = (string)data["username"];
            ret.bindingName = (string)data["bindingName"];
            ret.bindTime = (string)data["bindingTime"];
            ret.countGameGold = (int)data["weekendGameGold"];
            if (data.ContainsKey("weeklyRecharge"))
            {
                ret.weeklyRecharge = (int)data["weeklyRecharge"];
            }
            if (data.ContainsKey("weeklyExpiry"))
            {
                ret.weeklyExpiry = (int)data["weeklyExpiry"];
            }
            ret.wardGameGold = (int)data["weekendAwardGold"];
        }
        catch (Exception e)
        {
            Debug.LogError("ShareLog excpetion: " + e.Message);
            string keys = "";
            foreach (string key in data.Keys)
            {
                keys += key + ", ";
            }
            Debug.Log("keys: " + keys);
        }
        

        return ret;
    }
}
