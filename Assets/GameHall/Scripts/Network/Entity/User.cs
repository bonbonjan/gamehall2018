﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User
{
    public string username;
    public string nickname;
    public string phone;
    public string password;
    public string sex;
    public string card;// 身份证
    public string question;// 安全问题
    public string answer;// 问题答案
    public string registDate;// 注册时间
    public string loginDate;// 登录时间
    public int status = 0;// 0-正常 1-封号
    public int overflow = 0;// 是否爆机 0-否 1-是
    public int gameGold = 0;// 游戏币
    public int lottery = 0; //彩票
    public int expeGold = 0;// 体验币
    public double levelScore = 0;// 成长积分
    public int gameScore = 0;// 游戏分值
    public int expeScore = 0;// 体验分值
    public int level = 1;// 等级
    public int photoId = 1;// 头像编号
    //public int lastDeskId;// 最后进入的桌子ID
    //public int shutupStatus = 0;// 0 不禁言 1 20分钟 2 6小时 3 1天
    //public int lastGame;// 最后进入的游戏 0-六狮 1-捕鱼 2-单挑 3-万炮捕鱼 4-QQ美人鱼5-缺一门 6-欢乐牛牛
    public int type;// 账号类型 0-正常用户 1-游客用户 2-特殊账号
    //public int expiryNum = 0;// 申请兑奖额度
    public int payMoney;// 申请充值金额
    public int id;
    public int security; //0没设置密保， 1设置了密保
    public int promoterId;//推广员Id 0为admin 
    public string promoterUsername;// 推广员账号
    public int safeBox; //0没有设置过保险柜口令 1设置过保险柜口令
    //public bool ScoreOverflow=false;//玩家顶分状态

    //public string username;
    //public string nickname;
    //public string phone;
    //public string password;
    //public string sex;
    //public int id;
    //public int level;
    //public int gameGold;//游戏币
    ////public int addGameGold;
    //public int expeGold;//体验币
    //public int photoId;//头像1-8
    //public int overflow;//爆机：否0，是1
    //public int type;//玩家类型：0：普通玩家，1：游客
    //public string promoterUsername;//推广员

    public static User CreateWithDic(Dictionary<string, object> data)
    {
        User ret = new User();
        try
        {
            //ret.id = (int)data["id"];
            ret.username = (string)data["username"];
            ret.nickname = (string)data["nickname"];
            ret.phone = TryGetValue<string>(data, "phone", "");
            ret.photoId = (int)data["photoId"]-1;
            Debug.Log(LogHelper.Red("ret.photoId: " + ret.photoId));
            //ret.password = (string)data["password"];
            ret.sex = (string)data["sex"];
            ret.level = (int)data["level"];
            ret.gameGold = (int)data["gameGold"];
            ret.lottery = (int)data["lottery"];
            ret.card = (string)data["card"];
            ret.expeGold = (int)data["expeGold"];           
            ret.overflow = (int)data["overflow"];
            ret.type = (int)data["type"];
            ret.security = (int)data["security"];
            ret.safeBox = (int)data["safeBox"];
            ret.id = (int)data["id"];
            ret.promoterId = (int)data["promoterId"];
            ret.promoterUsername = (string)data["promoterName"];
            //ret.addGameGold = 0;
            //ret.photoId = ret.photoId > 4 ? 4 : ret.photoId < 1 ? 1 : ret.photoId;
        }
        catch (Exception e)
        {
            Debug.LogError("User excpetion: " + e.Message);
            string keys = "";
            foreach (string key in data.Keys)
            {
                keys += key + ", ";
            }
            Debug.Log("keys: " + keys);
        }
        return ret;
    }

    static T TryGetValue<T>(Dictionary<string, object> data, string key, T defValue)
    {
        if(data.ContainsKey(key))
        {
            return (T)data[key];
        }
        else
        {
            return defValue;
        }
    }
}