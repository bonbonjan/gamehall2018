﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TopRank {
    public int userId;              //用户的ID
    public string nickname;         //昵称
    public int gold;                //中奖游戏币
    public string awardName;        //奖项名称
    public string datetime;         //中奖时间
    //public int photoId;             //玩家头像编号
    //public int gameGold;        //财富榜财富
    //public int level;                  //等级

    public static TopRank CreateWithDic(Dictionary<string, object> data)
    {
        TopRank ret = new TopRank();

        try
        {
            ret.userId = (int)data["userId"];
            ret.nickname = (string)data["nickname"];
            ret.gold = (int)data["gold"];
            ret.awardName = (string)data["awardName"];
            ret.datetime = (string)data["datetime"];
        }
        catch (Exception e)
        {
            Debug.LogError("Mail excpetion: " + e.Message);
            string keys = "";
            foreach (string key in data.Keys)
            {
                keys += key + ", ";
            }
            Debug.Log("keys: " + keys);
        }
        return ret;
    }

}

public class WealthLevel
{
    public int userId;
    public string nickname;         //玩家昵称
    public int photoId;             //玩家头像    
    public int gameGoldOrLevel;            //游戏币数量或等级

    public static WealthLevel CreateWithDic(Dictionary<string, object> data)
    {
        WealthLevel ret = new WealthLevel();

        try
        {
            ret.userId = (int)data["userId"];
            ret.nickname = (string)data["nickname"];
            ret.photoId = (int)data["photoId"]-1;
            ret.gameGoldOrLevel = (int)data["gameGoldOrLevel"];
            
        }
        catch (Exception e)
        {
            Debug.LogError("WealthLevel excpetion: " + e.Message);
            string keys = "";
            foreach (string key in data.Keys)
            {
                keys += key + ", ";
            }
            Debug.Log("keys: " + keys);
        }
        return ret;
    }

}

/*大厅等级榜的自定义类*/
//public class LevelList
//{
//    public int photoId;             //玩家头像
//    public string nickname;         //玩家昵称
//    public int level;            //等级
//}
