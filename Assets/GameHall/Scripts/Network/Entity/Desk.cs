﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Desk : IComparable
{
    public int id;
    public int roomId;
    public string name;
    public int minGold;
    public int exchange;
    public int onceExchangeValue;
    //public int autoKickOut;
    public int maxSinglelineBet;
    public int minSinglelineBet;
    public int singlechangeScore;
    public int diceSwitch;//0,1
    public int diceDirectSwitch;//0,1
    public int diceOverflow;
    //public bool full;
    public int userId;
    public int userPhotoId;
    public string nickname;
    

    public static Desk CreateWithDic(Dictionary<string, object> data)
    {
        Desk ret = new Desk();

        try
        {
            ret.id = (int)data["id"];
            ret.roomId = (int)data["roomId"];
            ret.name = (string)data["name"];
            ret.minGold = (int)data["minGold"];
            ret.exchange = (int)data["exchange"];
            ret.onceExchangeValue = (int)data["onceExchangeValue"];

            ret.maxSinglelineBet = (int)data["maxSinglelineBet"];
            ret.minSinglelineBet = (int)data["minSinglelineBet"];
            ret.singlechangeScore = (int)data["singlechangeScore"];
            ret.diceSwitch = (int)data["diceSwitch"];
            ret.diceDirectSwitch = (int)data["diceDirectSwitch"];
            ret.diceOverflow = (int)data["diceOverflow"];
            //ret.full = (bool)data["full"];
            ret.userId = (int)data["userId"];
            ret.userPhotoId = (int)data["userPhotoId"];
            ret.nickname = (string)data["nickname"];
            //ret.nickname = data.GetValue<string>("nickname","");
           
        }
        catch (Exception e)
        {
            Debug.LogError("Desk excpetion: " + e.Message);
            string keys = "";
            foreach (string key in data.Keys)
            {
                keys += key + ", ";
            }
            Debug.Log("keys: " + keys);
        }


        return ret;
    }

    public int CompareTo(object obj)
    {
        if(obj is Desk)
        {
            Desk other = obj as Desk;
            return this.id - other.id;
        }
        return 0;
    }
}

                //"roomId": 2,
                //"name": "aaa",
                //"minGold": 10,
                //"autoKickOut": 0,
                //"maxSinglelineBet": 10,
                //"minSinglelineBet": 10,
                //"exchange": 10,
                //"onceExchangeValue": 100,
                //"orderBy": 1,
                //"state": 1,
                //"diceSwitch": 0,
                //"singlechangeScore": 10,
                //"diceDirectSwitch": 100,
                //"diceOverflow": 50000,
                //"mainGameWinRate": 10,
                //"diceGameWinRate": 10,
                //"sumYaFen": 0,
                //"sumDeFen": 0,
                //"mainGameSumYaFen": 0,
                //"mainGameSumDeFen": 0,
                //"diceGameSumYaFen": 0,
                //"diceGameSumDeFen": 0,
                ////"full": false,
                //"userId": -1,
                //"userPhotoId": -1,
                //"playMary": 0,
                //"allSamePhoto": -1,
                //"id": 1