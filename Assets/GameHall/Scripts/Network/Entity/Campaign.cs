﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Campaign {

    public int Id;                  //活动ID
    //public int userId;              //用户的ID
    public int type;             //活动类型 1-充值反馈 2-累计游玩 3-等级奖励 4-排行奖励 5-单局奖励 6-万元红包来袭
    public string content;             //活动说明
    public string content_en;             //活动说明english
    public int awardGold;        //活动对应赠送币值
    public int status;           //玩家是否领取了奖励 0-未完成 1-完成未领取 2-已领取
    public int userSchedule;           //玩家目前的活动进度
    public int targetSchedule;         //累计游玩的目标
    //public int condition2;         //充值奖励的目标


    public static Campaign CreateWithDic(Dictionary<string, object> data)
    {
        Campaign ret = new Campaign();

        try
        {
            ret.Id = (int)data["id"];
            ret.type = (int)data["type"];
            ret.content = (string)data["content"];
            ret.content_en = (string)data["contentEn"];
            ret.awardGold = (int)data["award"];
            ret.status = (int)data["status"];
            ret.userSchedule = (int)data["userSchedule"];
            ret.targetSchedule = (int)data["targetSchedule"];
        }
        catch (Exception e)
        {
            Debug.LogError("Campaign excpetion: " + e.Message);
            string keys = "";
            foreach (string key in data.Keys)
            {
                keys += key + ", ";
            }
            Debug.Log("keys: " + keys);
        }
        return ret;
    }

}
