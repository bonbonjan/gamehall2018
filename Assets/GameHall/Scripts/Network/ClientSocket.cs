﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.IO;
using wmcyou.mobile.arcade.network;
using System.Threading;

public class ClientSocket
{
    /// Events
    public Action connectHandler;
    public Action<Exception> exceptionHandler;
    public Action<byte[], int, int> dataHandler;
    public Action<int, int, ClientSocket> checkTimeoutHandler;
    public List<bool> doneList;//记录任务（连接或发送）的任务状态是否完成

    private TcpClient _pTcpClient;

    private object __lock;

    private NetworkStream _pNetwrokStream;

    private byte[] _receiveBuffer;

    private int _nLastMsgBodySize;
    private MemoryStream _pLastMsgBodyStream;   //方便处理消息跨包

    private const int HEAD_SIZE = 4;
    //private const int SEND_BUFFER_SIZE = 64*1024;
    private const int MAX_MESSAGE_SIZE = 16 * 1024;
    private int _packetId = 0;
    public ClientSocket(bool useTcpClient)
    {
        _pTcpClient = new TcpClient();
        _receiveBuffer = new byte[MAX_MESSAGE_SIZE * 4];
        _pTcpClient.NoDelay = true;
        _pTcpClient.Client.NoDelay = true;
        //Debug.Log(string.Format("SendTimeout: {0}, ReceiveTimeout: {1}", _pTcpClient.SendTimeout, _pTcpClient.ReceiveTimeout));
        //_pTcpClient.ReceiveTimeout = _pTcpClient.ReceiveTimeout == 0 ? 2000 : Mathf.Min(_pTcpClient.ReceiveTimeout, 2000);
        //_pTcpClient.SendTimeout = _pTcpClient.SendTimeout == 0 ? 2000 : Mathf.Min(_pTcpClient.SendTimeout, 2000);
        //Debug.Log(string.Format("SendTimeout: {0}, ReceiveTimeout: {1}", _pTcpClient.SendTimeout, _pTcpClient.ReceiveTimeout));
        doneList = new List<bool>();
        _nLastMsgBodySize = 0;
        _pLastMsgBodyStream = new MemoryStream();

        dataHandler = _defaultDataHandler;
        exceptionHandler = _defaultExceptionHandler;

        __lock = new object();
    }

    /*
        /// 这个方法会被系统从其他线程调用
        ~ClientSocket()
        {
            Close();
        }
    */

    public TcpClient GetTcpClient() { return _pTcpClient; }

    public void Close()
    {
        if (_pTcpClient != null)
        {
            lock (__lock)
            {
                _pTcpClient.Close();

                _pLastMsgBodyStream.Close();

                if (_pNetwrokStream != null)
                {
                    _pNetwrokStream.Close();
                }
            }
        }

        Debug.Log("socket closed");
    }

    public void Connect(string host, int port)
    {
        try
        {
            _pTcpClient.BeginConnect(host, port, new AsyncCallback(_onConnected), this);
            //_pTcpClient.BeginConnect(host, port, AsyncTimeoutWrapper(_onConnected, 2000), this);

        }
        catch (Exception ex)
        {
            Debug.LogError(ex);
            exceptionHandler(ex);
        }
    }

    /*
     * 测试超时的封装，目前无法检测发送超时，只能检测连接超时
     */
    AsyncCallback AsyncTimeoutWrapper(Action<IAsyncResult,int> func,int timeout)
    {
        doneList.Add(false);
        int checkId = doneList.Count - 1;
        checkTimeoutHandler(checkId, timeout, this);

        return new AsyncCallback((IAsyncResult ar)=>{
            func(ar, checkId);
        });
    }

    private void _onConnected(IAsyncResult ar)
    {
        //Debug.Log("_onConnected");
        lock (__lock)
        {
            try
            {
                if (!ar.IsCompleted) throw new Exception("_onConnected is not completed");

                _pTcpClient.EndConnect(ar);

                _pTcpClient.NoDelay = true;

                _pNetwrokStream = _pTcpClient.GetStream();
                //Debug.Log(string.Format("ReadTimeout: {0}, WriteTimeout: {1}", _pNetwrokStream.ReadTimeout, _pNetwrokStream.WriteTimeout));//异步不起作用，同步时的意义和理解不一样
                //_pNetwrokStream.ReadTimeout = 2000;
                //_pNetwrokStream.WriteTimeout = 2000;
                //Debug.Log(string.Format("ReadTimeout: {0}, WriteTimeout: {1}, CanTimeout: {2}", _pNetwrokStream.ReadTimeout, _pNetwrokStream.WriteTimeout, _pNetwrokStream.CanTimeout));

                //if (_pNetwrokStream == null) Debug.LogError("_pNetwrokStream is empty");
                //Debug.Log("_pNetwrokStream assigned");

                _beginReceive();
                if (connectHandler != null) connectHandler();
            }
            catch (Exception e)
            {
                //Debug.Log("Exception happened in _onConnected " + e.Message);
                //Debug.Log("Exception happened in _onConnected " + e.StackTrace);
                exceptionHandler(e);
            }
        }
    }

    private void _onConnected(IAsyncResult ar,int checkId)
    {
        //Debug.Log("_onConnected");
        lock (__lock)
        {
            try
            {
                if (!ar.IsCompleted) throw new Exception("_onConnected is not completed");

                _pTcpClient.EndConnect(ar);

                _pTcpClient.NoDelay = true;

                _pNetwrokStream = _pTcpClient.GetStream();
                Debug.Log(string.Format("ReadTimeout: {0}, WriteTimeout: {1}", _pNetwrokStream.ReadTimeout, _pNetwrokStream.WriteTimeout));//异步不起作用，同步时的意义和理解不一样
                _pNetwrokStream.ReadTimeout = 2000;
                _pNetwrokStream.WriteTimeout = 2000;
                Debug.Log(string.Format("ReadTimeout: {0}, WriteTimeout: {1}, CanTimeout: {2}", _pNetwrokStream.ReadTimeout, _pNetwrokStream.WriteTimeout, _pNetwrokStream.CanTimeout));

                //if (_pNetwrokStream == null) Debug.LogError("_pNetwrokStream is empty");
                //Debug.Log("_pNetwrokStream assigned");

                _beginReceive();
                if (connectHandler != null) connectHandler();
                doneList[checkId] = true;

            }
            catch (Exception e)
            {
                //Debug.Log("Exception happened in _onConnected " + e.Message);
                //Debug.Log("Exception happened in _onConnected " + e.StackTrace);
                exceptionHandler(e);
            }
        }
    }

    public bool Connected
    {
        get { return _pTcpClient.Connected; }
    }

    protected void _beginReceive()
    {
        //Debug.Log("_beginReceive");
        lock (__lock)
        {
            try
            {
                if (_pNetwrokStream == null)
                {
                    _pNetwrokStream = _pTcpClient.GetStream();
                    //Debug.Log("_pNetwrokStream assigned");
                }

                _pNetwrokStream.BeginRead(_receiveBuffer, 0, MAX_MESSAGE_SIZE, new AsyncCallback(EndReceiveHandle), null);
            }
            catch (Exception ex)
            {
                //Debug.Log(string.Format("_receiveBuffer.length: {0}, ReceiveBufferSize: {1}", _receiveBuffer.Length, _pTcpClient.ReceiveBufferSize));
                //Debug.LogError(ex);
                exceptionHandler(ex);
            }
        }
    }

    public void EndReceiveHandle(IAsyncResult ar)
    {
        //Debug.Log("EndReceiveHandle");
        lock (__lock)
        {
            try
            {
                if (!ar.IsCompleted) throw new Exception("EndReceiveHandle is not completed");

                if (!_pTcpClient.Connected)
                {
                    //exceptionHandler(null);
                    exceptionHandler(new Exception("EndReceiveHandle> Connected is false"));
                    return;
                }

                int bytesRead = _pNetwrokStream.EndRead(ar);
                if (bytesRead == 0)
                {
                    /// bytesRead == 0表示服务器关闭socket
                    //exceptionHandler(null);
                    exceptionHandler(new Exception("EndReceiveHandle> bytesRead == 0"));
                    return;
                }

                int position = 0;
                MemoryStream stream = new MemoryStream(_receiveBuffer, 0, bytesRead);
                BinaryReader reader = new BinaryReader(stream);

                while (position < bytesRead)
                {
                    if (_nLastMsgBodySize != 0 && _nLastMsgBodySize > _pLastMsgBodyStream.Length)
                    {
                        int gap = (int)(_nLastMsgBodySize - _pLastMsgBodyStream.Length);
                        int remain = (int)(stream.Length - stream.Position);
                        int size = 0;
                        size = gap > remain ? remain : gap;
                        _pLastMsgBodyStream.Write(reader.ReadBytes(size), 0, size);
                        position += size;

                        if (_pLastMsgBodyStream.Length == _nLastMsgBodySize)
                        {
                            byte[] msgBuffer = _pLastMsgBodyStream.ToArray();
                            //handle msgBuffer
                            if (dataHandler != null) dataHandler(msgBuffer, _packetId, position);
                            _nLastMsgBodySize = 0;

                            _pLastMsgBodyStream.Close();
                            _pLastMsgBodyStream = new MemoryStream();
                        }
                        else
                        {
                            continue;
                        }
                    }

                    if (position >= bytesRead)
                    {
                        break;
                    }

                    int bodySize = 0;
                    //bodySize = reader.ReadInt32();
                    byte[] sizeBytes = reader.ReadBytes(HEAD_SIZE);
                    Array.Reverse(sizeBytes);
                    bodySize = BitConverter.ToInt32(sizeBytes, 0);
                    //Debug.Log("bodySize: " + bodySize);
                    position += HEAD_SIZE;

                    if (bodySize <= 0)
                    {
                        //todo:handle error
                        exceptionHandler(new Exception("wrong bodysize: " + bodySize));
                        Debug.LogError("wrong bodysize: " + bodySize);
                        break;
                    }
                    else if (bodySize + position > bytesRead)
                    {
                        _pLastMsgBodyStream = new MemoryStream();
                        _nLastMsgBodySize = bodySize;
                        continue;
                    }
                    else
                    {
                        byte[] msgBuffer = reader.ReadBytes(bodySize);
                        position += bodySize;
                        //_pLastMsgBodyStream.Close();
                        //_nLastMsgBodySize = 0;
                        //handle msgBuffer
                        if (dataHandler != null) dataHandler(msgBuffer, _packetId, position);
                    }
                }

                reader.Close();
                stream.Close();
                _beginReceive();

            }
            catch (ObjectDisposedException ex)
            {
                exceptionHandler(ex);
                LogInfo("Receive Closed");
            }
            catch (Exception ex)
            {
                exceptionHandler(ex);
                LogError(ex.Message + "\n " + ex.StackTrace + "\n" + ex.Source);
            }
            _packetId++;
        }
    }

    protected void _defaultDataHandler(byte[] msgBuffer, int packetId, int endIndex)
    {
        if (msgBuffer == null || msgBuffer.Length == 0)
        {
            Debug.LogError("HandleRecvMsgAsString>msgBuffer is empty");
            return;
        }
        string message = System.Text.Encoding.UTF8.GetString(msgBuffer);
        Debug.Log(string.Format("recevie>size: {0}, message: {1}", msgBuffer.Length, message));
    }

    public void Send(byte[] msgBytes, bool useHead = true)
    {
        lock (__lock)
        {
            try
            {
                int size = msgBytes.Length;
                //Debug.Log("size: " + size);
                MemoryStream stream = new MemoryStream();
                BinaryWriter writer = new BinaryWriter(stream);
                if (useHead)
                {
                    byte[] sizeBytes = BitConverter.GetBytes(size);
                    Array.Reverse(sizeBytes);
                    writer.Write(sizeBytes);
                }
                writer.Write(msgBytes);

                byte[] streamBytes = stream.ToArray();
                PrintBin(streamBytes, "send");
                //Debug.Log("BeginWrite begin @"+Time.realtimeSinceStartup);
                _pNetwrokStream.BeginWrite(streamBytes, 0, streamBytes.Length, new AsyncCallback(_onEndWrite), _pNetwrokStream);
                //Debug.Log("BeginWrite end @" + Time.realtimeSinceStartup);

                //_pNetwrokStream.BeginWrite(streamBytes, 0, streamBytes.Length, AsyncTimeoutWrapper(_onEndWrite, 2000), _pNetwrokStream);
                //_pTcpClient.Client.BeginSend(streamBytes, 0, streamBytes.Length, 0, AsyncTimeoutWrapper(_onEndWrite2, 2000), _pTcpClient.Client);
                //float time = Time.unscaledTime;
                //_pTcpClient.Client.Send(streamBytes);
                //doneList.Add(false);
                //int checkId = doneList.Count - 1;
                //doneList[checkId] = true;
                //Debug.Log(LogHelper.Magenta("send id:{0}, time: {1}",checkId, Time.unscaledTime - time));
                //Debug.Log("BeginSend success");
                stream.Close();
            }
            catch (ObjectDisposedException ex)
            {
                Debug.Log("Send Closed");
                exceptionHandler(ex);
            }
            catch (Exception ex)
            {
                Debug.Log("BeginSend: " + ex);
                Debug.Log("BeginSend: " + ex.StackTrace);
                //Debug.Log(string.Format("_pNetwrokStream: {0}", _pNetwrokStream == null));
                exceptionHandler(ex);
            }
        }
    }

    protected void _onEndWrite(IAsyncResult ar)
    {
        //Debug.Log("_onEndWrite");
        lock (__lock)
        {
            try
            {
                if (!ar.IsCompleted) throw new Exception("_onEndWrite is not completed");
                NetworkStream myNetworkStream = (NetworkStream)ar.AsyncState;
                myNetworkStream.EndWrite(ar);
                //_pNetwrokStream.EndWrite(ar);
            }
            catch (Exception ex)
            {
                LogError(ex.Message);
                exceptionHandler(ex);
            }
        }
    }

    /*
     * 测试超时用
    protected void _onEndWrite(IAsyncResult ar, int checkId)
    {
        lock (__lock)
        {
            try
            {
                if (!ar.IsCompleted) throw new Exception("_onEndWrite is not completed");
                NetworkStream myNetworkStream = (NetworkStream)ar.AsyncState;
                myNetworkStream.EndWrite(ar);
                //_pNetwrokStream.EndWrite(ar);
                
                Debug.Log(LogHelper.Aqua("_onEndWrite id: {0}", checkId));

                doneList[checkId] = true;

            }
            catch (Exception ex)
            {
                LogError(ex.Message);
                exceptionHandler(ex);
            }
        }
    }

    protected void _onEndWrite2(IAsyncResult ar, int checkId)
    {
        lock (__lock)
        {
            try
            {
                if (!ar.IsCompleted) throw new Exception("_onEndWrite is not completed");
                //NetworkStream myNetworkStream = (NetworkStream)ar.AsyncState;
                //myNetworkStream.EndWrite(ar);
                //_pNetwrokStream.EndWrite(ar);
                Socket client = (Socket)ar.AsyncState;
                int bytesSent = client.EndSend(ar);
                Debug.Log(LogHelper.Aqua("_onEndWrite id: {0}, bytesSent: {1}", checkId, bytesSent));

                doneList[checkId] = true;

            }
            catch (Exception ex)
            {
                LogError(ex.Message);
                exceptionHandler(ex);
            }
        }
    }
    */

    private void _defaultExceptionHandler(System.Exception ex)
    {

    }

    #region Logging related

    /// !!!! 存在线程安全问题 !!!!!
    public delegate void LogDelegate(string msg);
    private LogDelegate _logInfoFunc;
    private LogDelegate _logErrorFunc;

    public void RegisterLog(LogDelegate logInfoFunc, LogDelegate logErrorFunc)
    {
        _logInfoFunc = logInfoFunc;
        _logErrorFunc = logErrorFunc;
    }

    private void LogInfo(string msg)
    {
        if (_logInfoFunc != null)
        {
            _logInfoFunc(msg);
        }
    }

    private void LogError(string msg)
    {
        if (_logErrorFunc != null)
        {
            _logErrorFunc(msg);
        }
    }

    void PrintBin(byte[] bytes, string head = "")
    {
        return;
        Debug.Log(head + " bytes.length: " + bytes.Length);
        string tempStr = "";
        int count = Math.Min(bytes.Length, 256);
        for (int i = 0; i < bytes.Length; i++)
        {
            tempStr += Convert.ToString(bytes[i], 16) + " ";
            if ((i > 0 && i % 16 == 0) || (i == bytes.Length - 1 && i % 16 != 0))
            {
                Debug.Log(tempStr);
                tempStr = "";
            }
        }
    }
    #endregion
}
