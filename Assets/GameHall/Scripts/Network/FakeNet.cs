﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using wmcyou.mobile.arcade.network;
using JsonFx.Json;
using System;

namespace WMCY.GameHall.Net
{


public class FakeNet
{
    NetManager _net;
    Queue<NetMessage> _netMsgQueue;

    public delegate void ResponseHandler(object[] args);
    Dictionary<string, string> _reqToRespMap = new Dictionary<string, string>();
    Dictionary<string, ResponseHandler> _handerMap = new Dictionary<string, ResponseHandler>();

    public FakeNet(NetManager net, Queue<NetMessage> netMsgQueue)
    {
        this._net = net;
        this._netMsgQueue = netMsgQueue;

    }

    public void RegisterHandler(string reqMethod, string respMethod, ResponseHandler hander)
    {
        _reqToRespMap.Add(reqMethod, respMethod);
        if (!_handerMap.ContainsKey(respMethod))
        {
            _handerMap.Add(respMethod, hander);
        }

    }

    public void Init()
    {

        RegisterHandler("gcuserService/login", "login", (args) =>
        {
            Dictionary<string, object> table = new Dictionary<string, object>();
            table.Add("isLogin", true);
            //table.Add("messageStatus", 0);
            Dictionary<string, object> user = new Dictionary<string, object>();
            //user.Add("id", 12345);
            user.Add("username", "FakeNet");
            user.Add("nickname", "FakeNet nick");
            user.Add("phone","ddddddd");
            user.Add("sex", "dd");
            user.Add("level", 10);
            user.Add("gameGold", 19999);
            user.Add("lottery",5623);
            user.Add("expeGold", 999);
            user.Add("security", 1);
            user.Add("safeBox", 1);
            user.Add("photoId", 3);
            user.Add("overflow", 4);
            user.Add("type", 0);
            //user.Add("promoterName", "FakeNet推广员");

            table.Add("user", user);
            //table.Add("special", false);

            _netMsgQueue.Enqueue(new NetMessage("login", new object[] { table }));
        });

        RegisterHandler("userService/heart", "empty", (args) =>
        {
            //_netMsgQueue.Enqueue(new NetMessage("empty", args));
        });


        RegisterHandler("gcuserService/gameranktype", "gameranktype", (args) =>
        {
            int gametype = (int)args[0]; 

            Dictionary<string, object> table = new Dictionary<string, object>();
            table.Add("ranktype", gametype);
            
            if(gametype <2)
            {
                List<TopRank> wl = new List<TopRank>();
                for (int i = 0; i < 4; i++)
                {
                    TopRank rk = new TopRank();
                    //rk.photoId = UnityEngine.Random.Range(0, 4);
                    //rk.nickname = "jiangwen" + i;
                    //rk.gameGold = i * 10;

                    wl.Add(rk);
                    //tp[i] = new TopRank();
                    //io.Add(rk);
                }
                table.Add("TrList", wl);
            }
            else
            {
                List<TopRank> tr = new List<TopRank>();
                for(int i = 0;i<6;i++)
                {
                    TopRank rr = new TopRank();
                    rr.nickname = "zhanghe" + i;
                    rr.gold = i * 10;
                    rr.awardName = "quanpanjiang";
                    rr.datetime = "2016.05.0"+i;

                    tr.Add(rr);
                }
                table.Add("TrList", tr);
            }
            _netMsgQueue.Enqueue(new NetMessage("gameranktype", new object[] { table }));
        });

        List<Campaign> trCamp = new List<Campaign>();
        //int type = 0;

        for(int i=0;i<8;i++)
        {
            Campaign cap = new Campaign();
            cap.Id = i;
            cap.content = "neirong"+i+"type";
            cap.status = 0;
            cap.userSchedule = 50;
            cap.targetSchedule = 100;
            cap.awardGold = 50;
            trCamp.Add(cap);
        }
        //Campaign cap0 = new Campaign();
        //cap0.content = "neirong" + i + "type";
        //cap0.status = 0;
        //cap0.userSchedule = 50;
        //cap0.targetSchedule = 100;
        //cap0.awardGold = "50";

        //Campaign cap1 = new Campaign();
        //cap1.content = "neirong" + i + "type";
        //cap1.status = 1;
        //cap1.userSchedule = 50;
        //cap1.targetSchedule = 100;
        //cap1.awardGold = "100";

        //Campaign cap2 = new Campaign();
        //cap2.content = "neirong2type";
        //cap2.status = 2;
        //cap2.userSchedule = 50;
        //cap2.targetSchedule = 100;
        //cap2.awardGold = "已完成";
        //trCamp.Add(cap0);
        //trCamp.Add(cap1);
        //trCamp.Add(cap2);

        //for (int i = 0; i < 3; i++)
        //{
        //    Campaign rk = new Campaign();
        //    rk.content = "neirong" + i;
        //    rk.status = i;
        //    rk.userSchedule = 50;
        //    rk.targetSchedule = 100;
        //    rk.awardGold = i * 100 + "";

        //    trCamp.Add(rk);
        //    //tp[i] = new TopRank();
        //    //io.Add(rk);
        //}

        RegisterHandler("gcuserService/campagin", "campagin", (args) =>
        {
            //Dictionary<string, object> obj = args[0] as Dictionary<string, object>;
            int type = (int)args[0];
            Debug.Log("type: " + type);

            Dictionary<string, object> table = new Dictionary<string, object>();
            //table.Add("ranktype", 0);
            //table.Add("messageStatus", 0);
            for (int i = 0; i < 8;i++ )
            {
                trCamp[i].content = "neirong"+i+"type" + type;
            }
            trCamp[2].status = 1;
            trCamp[3].status = 1;   

            table.Add("campagin", trCamp);

            _netMsgQueue.Enqueue(new NetMessage("campagin", new object[] { table }));
        });

        RegisterHandler("gcuserService/campaigncomplete", "campaigncomplete", (args) =>
        {
            Campaign cc = args[0] as Campaign;
            Debug.Log("cc.awardGold: " + cc.awardGold);
            // List<Campaign> capList = obj["campagin"] as List<Campaign>; 

            Debug.Log("campaigncomplete");
            trCamp[cc.Id].status = 2;
            trCamp[cc.Id].awardGold = 1;

        });

        RegisterHandler("gcuserService/newmail", "newmail", (args) =>
        {
            Debug.Log("gcuserService/newmail");
        });

        RegisterHandler("gcuserService/dwmtype", "dwmtype", (args) =>
        {
            int dwm = (int)args[0];
            Dictionary<string, object> table = new Dictionary<string, object>();
            //table.Add("dwmtype", 0);
            //table.Add("messageStatus", 0);
            List<TopRank> tr = new List<TopRank>();
            for (int i = 0; i < 6; i++)
            {
                TopRank rr = new TopRank();
                rr.nickname = "dwty" + i + "dwm" + dwm;
                rr.gold = i * 10;
                rr.awardName = "quanpanjiang1111";
                rr.datetime = "2016.06.0" + i;

                tr.Add(rr);
            }
            table.Add("TrList", tr);

            _netMsgQueue.Enqueue(new NetMessage("dwmtype", new object[] { table }));
        });

        RegisterHandler("gcuserService/mail", "mail", (args) =>
        {
            Dictionary<string, object> table = new Dictionary<string, object>();
            //table.Add("Mail", 0);
            //table.Add("messageStatus", 0);
            List<Mail> tr = new List<Mail>();
            for (int i = 0; i < 6; i++)
            {
                Mail ma = new Mail();
                ma.mailId = i;
                ma.mailName = "jiangwen" + i;
                ma.mailSendPeople = "admin"+i;
                ma.mailTime = "2016.05.0"+i;

                tr.Add(ma);
                //tp[i] = new TopRank();
                //io.Add(rk);
            }
            table.Add("mail", tr);

            _netMsgQueue.Enqueue(new NetMessage("mail", new object[] { table }));
        });

        /*gcuserService/ranktype
        RegisterHandler("userService/userLogin", "userLogin", (args) =>
        {
            Dictionary<string, object> table = new Dictionary<string, object>();
            table.Add("isLogin", true);
            //table.Add("messageStatus", 0);
            Dictionary<string, object> user = new Dictionary<string, object>();
            user.Add("id", 12345);
            user.Add("username", "FakeNet");
            user.Add("nickname", "FakeNet nick");
            user.Add("sex", "shemale");
            user.Add("level", 10);
            user.Add("gameGold", 19999);
            user.Add("expeGold", 999);
            user.Add("photoId", 3);
            user.Add("overflow", 0);
            user.Add("type", 0);
            user.Add("promoterName", "FakeNet推广员");

            table.Add("user", user);
            table.Add("special", false);

            _netMsgQueue.Enqueue(new NetMessage("userLogin", new object[] { table }));
        });

        RegisterHandler("userService/enterRoom", "roomInfo", (args) =>
        {
            //"roomId": 2,
            //"name": "aaa",
            //"minGold": 10,
            //"autoKickOut": 0,
            //"maxSinglelineBet": 10,
            //"minSinglelineBet": 10,
            //"exchange": 10,
            //"onceExchangeValue": 100,
            //"orderBy": 1,
            //"state": 1,
            //"diceSwitch": 0,
            //"singlechangeScore": 10,
            //"diceDirectSwitch": 100,
            //"diceOverflow": 50000,
            //"mainGameWinRate": 10,
            //"diceGameWinRate": 10,
            //"sumYaFen": 0,
            //"sumDeFen": 0,
            //"mainGameSumYaFen": 0,
            //"mainGameSumDeFen": 0,
            //"diceGameSumYaFen": 0,
            //"diceGameSumDeFen": 0,
            //"full": false,
            //"userId": -1,
            //"userPhotoId": -1,
            //"playMary": 0,
            //"allSamePhoto": -1,
            //"id": 1
            List<Dictionary<string, object>> deskList = new List<Dictionary<string, object>>();
            for (int i = 1; i <= 11; i++)
            {
                Dictionary<string, object> desk = new Dictionary<string, object>();
                desk.Add("id", i);
                desk.Add("name", "net桌子" + i);
                desk.Add("roomId", 2);
                desk.Add("minGold", 10);
                desk.Add("maxSinglelineBet", 80);
                desk.Add("minSinglelineBet", 10);
                desk.Add("singlechangeScore", 30);
                desk.Add("exchange", 10);
                desk.Add("onceExchangeValue", 100);
                desk.Add("diceSwitch", 1);
                desk.Add("diceDirectSwitch", 1);
                desk.Add("diceOverflow", 50000);
                bool full = i > 3 ? false : true;
                desk.Add("full", full);
                if (full)
                {
                    desk.Add("userId", i + 20000);
                    desk.Add("userPhotoId", i);
                    desk.Add("nickname", "fake占位"+i);
                }
                else
                {
                    desk.Add("userId", -1);
                    desk.Add("userPhotoId", -1);
                    desk.Add("nickname", "");

                }


                deskList.Add(desk);
            }

            _netMsgQueue.Enqueue(new NetMessage("roomInfo", new object[] { deskList.ToArray() }));
        });

        RegisterHandler("userService/leaveRoom", "empty", (args) =>
        {
            //_netMsgQueue.Enqueue(new NetMessage("empty", args));
        });

        RegisterHandler("userService/leaveDesk", "empty", (args) =>
        {
            //_netMsgQueue.Enqueue(new NetMessage("empty", args));
        });



        RegisterHandler("userService/enterDesk", "enterDesk", (args) =>
        {
            Dictionary<string, object> table = new Dictionary<string, object>();
            table.Add("success", true);

            //table.Add("lastResult", Utils.GetRandomMatrix3x5());
            table.Add("lastResult", new int[3][] { new int[5] { 1, 2, 3, 4, 5 }, new int[5] { 1, 2, 3, 4, 5 }, new int[5] { 1, 2, 3, 4, 5 } });
            table.Add("betRecord", new int[] { 1, 0, -1 });
            Debug.Log(JsonWriter.Serialize(table));
            table = JsonReader.Deserialize(JsonWriter.Serialize(table)) as Dictionary<string, object>;
            _netMsgQueue.Enqueue(new NetMessage("enterDesk", new object[] { table }));
        });

        RegisterHandler("userService/gameStart", "gameResult", (args) =>
        {
            Debug.Log("gameResult+++=");
            Dictionary<string, object> table = new Dictionary<string, object>();

            int[,] gameContent;
            //gameContent = new int[3, 5] {{1,1,1,1,1},{2,2,2,2,4},{3,3,3,5,6} };
            gameContent = Utils.GetRandomMatrix3x5();
            //int[][] gameContent2 = new int[3][];
            //for (int row = 0; row < 3;row++ )
            //{
            //    gameContent2[row] = new int[5];
            //    for (int col = 0; col < 5;col++ )
            //    {
            //        gameContent2[row][col] = gameContent[row,col];
            //    }
            //}
            //int[][] gameContent3 = new int[3][] { new int[5] { 1,2,3,4,5}, new int[5] {1,2,3,4,5 }, new int[5] {1,2,3,4,5 } };
            table.Add("gameContent", gameContent);
            table.Add("totalWin", 100);
            table.Add("multipGame", true);
            bool maryGame = false;
            table.Add("maryGame", maryGame);
            table.Add("times", 3);
            table.Add("specialAward", 0);
            Debug.Log(JsonWriter.Serialize(table));

            //{"args":[{"totalWin":3770,"times":9,"gameContent":[[2,1,9,5,4],[9,9,9,9,4],[9,1,9,5,4]],"multipGame":true,"maryGame":true,"specialAward":0}],"method":"gameResult"}
            _netMsgQueue.Enqueue(new NetMessage("gameResult", new object[] { table }));
            if (maryGame)
            {
                Dictionary<string, object> table2 = new Dictionary<string, object>();
                int[] photoNumber = new int[] { 1, 5, 12 , 10, 18,20,0};
                //int[][] photos = new int[3][] { new int[4] { 1, 2, 3, 4 }, new int[4] { 5, 6, 7, 8 }, new int[4] { 1, 1, 1, 2 } };
                int[,] photos = new int[,] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 1, 2, 3, 4 } };
                int[] totalWin = new int[3] { 0, 4000, 5000 };
                int credit = 12000;
                int totalBet = 10000;
                table2.Add("photoNumber", photoNumber);
                table2.Add("photos", photos);

                table2.Add("totalWin", totalWin);
                table2.Add("credit", credit);
                table2.Add("totalBet", totalBet);
                _netMsgQueue.Enqueue(new NetMessage("maryResult", new object[] { table2 }));
            }
        });

        RegisterHandler("userService/multipleInfo", "multipResult", (args) =>
        {
            Dictionary<string, object> table = new Dictionary<string, object>();
            int[] number = new int[2] { 1, 6 };
            table.Add("number", number);
            table.Add("totalWin", 2000);
            table.Add("overflow", false);
            table = JsonReader.Deserialize(JsonWriter.Serialize(table)) as Dictionary<string, object>;
            _netMsgQueue.Enqueue(new NetMessage("multipResult", new object[] { table }));
        });

        RegisterHandler("userService/maryStart", "maryResult", (args) =>
        {
            Dictionary<string, object> table = new Dictionary<string, object>();
            int[] photoNumber = new int[3] { 1, 5, 12 };
            int[][] photos = new int[3][] { new int[4] { 1, 2, 3, 4 }, new int[4] { 5, 6, 7, 8 }, new int[4] { 1, 1, 1, 2 } };
            int[] totalWin = new int[3] { 0, 4000, 5000 };
            int credit = 12000;
            int totalBet = 10000;
            table.Add("photoNumber", photoNumber);
            table.Add("photos", photos);

            table.Add("totalWin", totalWin);
            table.Add("credit", credit);
            table.Add("totalBet", totalBet);
            _netMsgQueue.Enqueue(new NetMessage("maryResult", new object[] { table }));
        });

        RegisterHandler("userService/userCoinIn", "updateGoldAndScore1", (args) =>
        {
            int coin = (int)args[0];
            Dictionary<string, object> table = new Dictionary<string, object>();
            table.Add("gold", 2000);
            table.Add("gameScore", 20000);
            _netMsgQueue.Enqueue(new NetMessage("updateGoldAndScore", new object[] { table }));
        });

        RegisterHandler("userService/userCoinOut", "updateGoldAndScore2", (args) =>
        {
            int score = (int)args[0];
            Dictionary<string, object> table = new Dictionary<string, object>();
            table.Add("gold", 2100);
            table.Add("gameScore", 21000);
            _netMsgQueue.Enqueue(new NetMessage("updateGoldAndScore", new object[] { table }));
        });
        */


    }

    public IEnumerator MakeResponse(string reqMethod, object[] args)
    {
        yield return new WaitForSeconds(0.1f);

        string respMethod;
        if (_reqToRespMap.ContainsKey(reqMethod))
        {
            respMethod = _reqToRespMap[reqMethod];
            if (respMethod != "empty")
            {
                ResponseHandler hander = _handerMap[respMethod];
                hander.Invoke(args);
            }
        }
        else
        {
            Debug.LogError("unregister method: " + reqMethod);
        }



    }

}

}