using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using wmcyou.mobile.arcade.network;
using JsonFx.Json;
using System;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;


namespace WMCY.GameHall.Net
{
    public class NetManager : MB_Singleton<NetManager>
    {
        private ClientSocket _pClient;
        private DataEncrypt _pCrypto;
        public bool isConnected = false;//建立连接成功
        public bool isReady = false;//交换加密成功，允许登陆,注册
        public bool isLogined = false;//登陆成功
        private FakeNet _fake;
        public bool useFake = true;
        public int connectCount = 0;//统计异常后重连次数，连上重置为0
        public float connectTimeCount = 0f;//统计异常后重连时间，连上重置为0
        public int connectMaxTimes = 0;//最大重连次数
        public float connectMaxTimeout = 0f;//最大重连时间，在游戏启动和登录过程中严格，在游戏中过程中不严格
        public bool dropSend = false;//调试-丢弃发送
        public bool dropDispatch = false;//调试-丢弃转发报文
        public bool isHeartPong = true;//是否收到返回心跳
        public bool printSendLog = false;//打印发送报文
        public bool printRecvLog = false;//打印接收报文
        public bool autoReady = false;//自动发送public key
        //public System.Action on
        void Awake()
        {
            if (_instance == null)
            {
                SetInstance(this);
            }
            RegisterHandler("netConnected", _onConnected);
        }

        void Start()
        {

        }

        private void _initClientSocket()
        {
            Debug.Log("_initClientSocket");
            isHeartPong = true;
            if (useFake)
            {
                Debug.Log("use <color=magenta>fake</color> net");
                isConnected = true;
                isReady = true;
                isLogined = true;
                _fake = new FakeNet(this, _netMsgQueue);
                _fake.Init();
            }
            else
            {

                isConnected = false;
                isReady = false;
                isLogined = false;

                if (_pClient != null)
                {
                    _pClient.Close();
                }
                if (_pCrypto != null)
                {
                    _pCrypto = null;
                }

                _pClient = new ClientSocket(true);
                _pCrypto = new DataEncrypt();

                /// 注意：这三个事件都是从网络层线程传递上来，在这三个处理函数里不能调用UnityEngine里的方法！！！
                _pClient.dataHandler = _handleData;
                _pClient.exceptionHandler = _handleException;
                _pClient.connectHandler = _handleConnected;
                _pClient.checkTimeoutHandler = _checkTimeoutStart;
            }


            //for (int i = 0; i < 10;i++ )
            //{
            //    _netMsgQueue.Enqueue(new NetMessage("test MsgQueue "+i,new object[]{null}));
            //}

        }

        private Coroutine _coKeepHeart;
        private void _onConnected(object[] args)
        {
            Debug.Log("Connect success!!!");
            if (autoReady) SendPublicKey();
            //SendPublicKey();
            if (_coKeepHeart != null) AppManager.Get().StopCoroutine(_coKeepHeart);
            _coKeepHeart = AppManager.Get().StartCoroutine(NetManager.GetInstance().KeepHeart());
            NetManager.GetInstance().connectCount = 0;
        }

        private void _handleException(Exception ex)
        {
            //Debug.Log("_handleException@" + System.DateTime.Now.Ticks);
            Debug.Log("_handleException");
            isConnected = false;
            isReady = false;
            isLogined = false;
            lock (_netMsgQueue)
            {
                _netMsgQueue.Enqueue(new NetMessage("netDown", new object[] { ex }));
            }
        }

        public void Connect(string ip, int port)
        {
            Debug.Log("Connect");
            connectCount++;
            //_pClient.BeginConnect(ip, port);
            //_pClient.BeginConnect("192.168.1.80", 10016, FinishConnection, _pClient);//office_self

            _initClientSocket();
            if (useFake) return;
#if UNITY_EDITOR
            Debug.Log(LogHelper.Magenta("IP:[{0}], PORT:[{1}]", ip, port));
#endif
            _pClient.Connect(ip, port);
        }

        private void _handleConnected()
        {
            //Debug.Log("_handleConnected");
            try
            {
                isConnected = true;

                lock (_netMsgQueue)
                {
                    _netMsgQueue.Enqueue(new NetMessage("netConnected", new object[] { }));
                }
            }
            catch (Exception ex)
            {
                Debug.LogError("_onConnected> exception: " + ex.Message);
                _handleException(ex);
            }
        }

        public void SendPublicKey()
        {
            string[] publicKeys = new string[2];
            publicKeys = _pCrypto.NetConnectGetRsaKey();
            //string strMethod = "userService/publicKey";
            System.Object[] args = new object[] { publicKeys[0], publicKeys[1] };
            Send("gcuserService/publicKey", args);
        }

        public IEnumerator KeepHeart()
        {
            while (true)
            {
                //if (isConnected && isLogined)
                if (isConnected && isReady)
                {
                    //Debug.Log(LogHelper.Aqua("heart ping"));
                    Send("gcuserService/heart", new object[] { });
                    isHeartPong = false;
                }
                yield return new WaitForSeconds(5f);
            }
        }

        public void Handle_sendServerTime(object[] args)
        {
            Debug.Log("Handle_sendServerTime");
            //客户端和服务器建立链接时，接收到服务器当前的时间和加密密钥
            long serverTime = 0;          //服务器时间
            string strKey = null;           //加密密钥

            Dictionary<string, object> myDictionary = new Dictionary<string, object>();
            myDictionary = args[0] as Dictionary<string, object>;

            serverTime = Convert.ToInt64(myDictionary["time"]);
            strKey = (string)myDictionary["key"];

            //设置获取到的服务器的时间
            _pCrypto.setServerTime(serverTime);

            //设置获取到的AES加密密钥 
            _pCrypto.DecryptKey(strKey);
            isReady = true;
        }

        protected void _handleData(byte[] msgBuffer, int packetId, int endIndex)
        {
            _pCrypto.GetKey();
            if (_pCrypto.GetKey() != "none")
            {
                //如果有了加密密钥，需要先进行解密
                msgBuffer = _pCrypto.Decrypt(msgBuffer);
            }

            //使用Json进行反序列化
            Hashtable table = new Hashtable();
            string jsonStr = System.Text.Encoding.UTF8.GetString(msgBuffer);
            table = JsonReader.Deserialize<Hashtable>(jsonStr);

            string method = table["method"].ToString();
            object[] args = table["args"] as object[];
            connectTimeCount = 0f;
#if UNITY_EDITOR
            if (printRecvLog)
            {
                Debug.Log("receiceMsg>>>Net:" + method);
                UnityEngine.Debug.Log("recv<<json: " + jsonStr);
            }
#endif
            if (true || method != "heart")
            {
                /// 这里是在网络线程，不能用Debug.Log或任何其他UnityEngine下的东西！！！
                //UnityEngine.Debug.Log("recv<<method: " + method);
            }

            if (method == "publicKey")
            {
                Handle_sendServerTime(args);
                return;
            }
            else if (method == "heart")
            {
                //接收到网络心跳
                //_recriveHeartTime = _getCurTime();
                Debug.Log(LogHelper.Aqua("heart pong"));
                isHeartPong = true;
                return;
            }

            /// 这里是在网络线程，不能用Debug.Log或任何其他UnityEngine下的东西！！！
            //UnityEngine.Debug.Log("recv<<json: " + jsonStr);

            lock (_netMsgQueue)
            {
                var message = new NetMessage(method, args);
                message.jsonString = jsonStr;
                message.packetId = packetId;
                message.endIndex = endIndex;
                _netMsgQueue.Enqueue(message);

            }
            //PrintBin(msgBuffer);
        }

        private IEnumerator _checkTimeout(int id, int timeout, ClientSocket cs)
        {
            Debug.Log(LogHelper.Aqua("_checkTimeout start id: {0}", id));
            yield return new WaitForSeconds(timeout / 1000f);
            bool isTimeout = false;
            if (cs != null && cs.doneList.Count > id)
            {
                //Debug.Log(Application.internetReachability.ToString());
                if (cs.doneList[id] == false)
                {
                    lock (_netMsgQueue)
                    {
                        _netMsgQueue.Enqueue(new NetMessage("timeout", new object[] { id }));
                        isTimeout = true;
                    }
                }
            }

            if (isTimeout) Debug.Log(LogHelper.Aqua("_checkTimeout start id: {0} timeout: {1}", id, isTimeout));

        }

        private IEnumerator _checkHeartTimeout(int timeout)
        {
            //yield break;
            //Debug.Log("_checkHeartTimeout@"+Time.realtimeSinceStartup);
            yield return new WaitForSeconds(timeout / 1000f);
            bool isTimeout = false;
            if (isReady && !isHeartPong)
            {
                lock (_netMsgQueue)
                {
                    isConnected = false;
                    isReady = false;
                    isLogined = false;
                    _netMsgQueue.Enqueue(new NetMessage("heart timeout", new object[] { }));
                    isTimeout = true;
                }
            }
            if (isTimeout) Debug.Log(LogHelper.Aqua("_checkHeartTimeout is timeout"));
        }


        public void _checkTimeoutStart(int id, int timeout, ClientSocket cs)
        {
            StartCoroutine(_checkTimeout(id, timeout, cs));
        }

        public delegate void NetMessageHandler(object[] args);
        private Queue<NetMessage> _netMsgQueue = new Queue<NetMessage>();
        private Dictionary<string, NetMessageHandler> _netMsgHandlerDic = new Dictionary<string, NetMessageHandler>();
        public void RegisterHandler(string method, NetMessageHandler hander)
        {
            if (!_netMsgHandlerDic.ContainsKey(method))
            {
                NetMessageHandler msgHander = new NetMessageHandler(hander);
                _netMsgHandlerDic.Add(method, msgHander);
            }
            else
            {
                _netMsgHandlerDic[method] = hander;
                //
                Debug.Log(method + " has multicast hander");
            }

        }

        public bool IsMethodRegistered(string method)
        {
            if (_netMsgHandlerDic.ContainsKey(method))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Send(string method, object[] args)
        {
            //Debug.Log("send begin:@"+Time.realtimeSinceStartup);
            if (method != "gcuserService/heart")
                Debug.Log(LogHelper.Olive("send>>method: ") + LogHelper.Key(method, "#AAffffff"));
            else
                StartCoroutine(_checkHeartTimeout(3000));
            //if (!isConnected) return;
            if (dropSend && method != "gcuserService/heart")
            {
                Debug.Log(LogHelper.Magenta("drop send>>method: {0}", method));
                return;
            }
            if (useFake)
            {
                StartCoroutine(_fake.MakeResponse(method, args));
                return;
            }

            Hashtable msgContext = new Hashtable();
            msgContext.Add("method", method);
            msgContext.Add("args", args);
            msgContext.Add("version", GVars.netVersion);

            if (isReady)
                msgContext.Add("time", _pCrypto.GetUnixTime());

            string jsonString = JsonWriter.Serialize(msgContext);
            byte[] buf = System.Text.Encoding.UTF8.GetBytes(jsonString);

            if (method != "gcuserService/heart")
            {

                if (method == "userService/publicKey")
                {
                    Debug.Log("send>>json: " + "*".Repeat(10));
                }
                else
                {
                    Debug.Log("send>>json: " + jsonString);

                }
            }

            if (_pCrypto.GetKey() != "none")
            {
                buf = _pCrypto.Encrypt(buf);
            }
            //Debug.Log("_pCrypto.key: "+_pCrypto.GetKey());
            _pClient.Send(buf);
            //Debug.Log("send end:@" + Time.realtimeSinceStartup);

        }

        void Update()
        {
            Dispatching();
        }

        void Dispatching()
        {
            NetMessage netMsg;
            //GetNetMessage
            while (GetNetMessage(out netMsg))
            {
                DispatchNetMessage(netMsg);
            }

        }

        bool GetNetMessage(out NetMessage netMsg)
        {
            lock (_netMsgQueue)
            {
                if (_netMsgQueue.Count == 0)
                {
                    netMsg = null;
                    return false;
                }
                netMsg = _netMsgQueue.Dequeue();
                return true;
            }

        }

        void DispatchNetMessage(NetMessage netMsg)
        {
            Debug.Log(string.Format("{0}> packetId: {1}, endIndex: {2}", netMsg.method, netMsg.packetId, netMsg.endIndex));
            if (netMsg.jsonString != null && netMsg.jsonString.Length > 0) UnityEngine.Debug.Log("recv<<json: " + netMsg.jsonString);
            if (dropDispatch)
            {
                Debug.Log(LogHelper.Magenta("drop send>>method: {0}", netMsg.method));
                return;
            }
            if (_netMsgHandlerDic.ContainsKey(netMsg.method))
            {
                _netMsgHandlerDic[netMsg.method].Invoke(netMsg.args);
            }
            else
            {
                Debug.LogError("DispatchNetMessage>unknown msg: " + netMsg.method);
            }
        }

        void OnApplicationPause(bool pauseStatus)
        {
            //Debug.Log("OnApplicationPause:> " + pauseStatus);
        }

        public void OnDestroy()
        {
            Debug.Log("NetworkManager OnDestory");
            if (_pClient != null)
            {
                _pClient.Close();
            }

        }

        public void Disconnect()
        {
            if (_pClient != null)
            {
                _pClient.Close();
            }
        }

    }

    public class NetMessage
    {
        public string method;
        public object[] args;
        public string jsonString;
        public int packetId;
        public int endIndex;

        public NetMessage(string method, object[] args)
        {
            this.method = method;
            this.args = args;
            //this.jsonString = json;
        }
    }

}