﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
namespace WMCY.GameHall
{
    public class GiftRemindPanelController : MonoBehaviour
    {
        [SerializeField]
        private Text gift;
        [SerializeField]
        private Text giftTime;
        [SerializeField]
        private Text giftRemark;

        public void Init(string uname,int gold,string time,string note)
        {
            gift.text = GVars.language == "zh" ? string.Format("恭喜您收到好友{0}赠送的{1}游戏币", uname, gold) : string.Format("Congratulations on receiving {0} coins from your friends {1}", gold, uname);
            giftTime.text = time;
            giftRemark.text = note;
        }

        public void OnBtnClick_Sure()
        {
            //this.gameObject.SetActive(false);
            Destroy(this.gameObject);
        }
    }
}