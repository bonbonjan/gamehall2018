﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using System.Text.RegularExpressions;

namespace WMCY.GameHall
{
    public class SafeBoxPwdSetPanelController : MonoBehaviour
    {
        void Awake()
        {

        }

        void Start()
        {
            NetManager.GetInstance().RegisterHandler("setSafeBoxPwd", HandleNetMsg_PwdSet);
        }

        void OnEnable()
        {
            //SwitchIntoEnglish();
            transform.Find("pwdItem/input").GetComponent<InputField>().text = "";
            transform.Find("repwdItem/input").GetComponent<InputField>().text = "";
        }

        void OnDisable()
        {
            
        }

        void Update()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
                return;
            transform.Find("titleBg/title").GetComponent<Text>().text = "Setting order";
            transform.Find("titleBg/title").GetComponent<Text>().fontSize = 30;
            transform.Find("pwdItem/input/Placeholder").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("pwdItem/input/Placeholder").GetComponent<Text>().text = "Can Only enter numbers";
            transform.Find("repwdItem/input/Placeholder").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("repwdItem/input/Placeholder").GetComponent<Text>().text = "Can Only enter numbers";
            transform.Find("pwdItem/itemName").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("pwdItem/itemName").GetComponent<Text>().text = "Enter order:";
            transform.Find("repwdItem/itemName").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("repwdItem/itemName").GetComponent<Text>().text = "Re-enter:";
            transform.Find("sureBtn/Text").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("sureBtn/Text").GetComponent<Text>().text = "Confirm";
        }

        public void OnBtnClick_Close()
        {
            gameObject.SetActive(false);
        }

        public void OnBtnClick_Sure()
        {
            string pwd = transform.Find("pwdItem/input").GetComponent<InputField>().text;
            string repwd = transform.Find("repwdItem/input").GetComponent<InputField>().text;
            if (Regex.IsMatch(pwd, "^[0-9]+$") == false)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码格式错误" : "Password format error");
                return;
            }
            if (pwd.Length != 6)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码格式错误" : "Password format error");
            }
            else if (pwd == repwd)
            {
                NetManager.GetInstance().Send("gcsecurityService/setSafeBoxPwd", new object[] { pwd });
            }
            else
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "两次输入不一致" : "Entered passwords differ!");
            }
        }

        void HandleNetMsg_PwdSet(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            Debug.Log(obj);
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                gameObject.SetActive(false);
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "设置成功" : "Set success!", false, () => { AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_SafeBoxPwdCheckPanel); });
                GVars.user.safeBox = 1;
            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                Debug.Log(msgcode);
                switch (msgcode)
                {
                    case 13:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "用户名不存在" : "The account does not exist");
                        break;
                    case 14:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入6位密码" : "Please enter the 6-digit password!");
                        break;
                    case 6:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被冻结" : "This account has been frozen");
                        break;
                    case 7:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "系统维护" : "Server maintenance");
                        break;
                    case 11:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请勿重复申请" : "Please do not repeat the application");
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "设置失败" : "set failed");
                        break;
                }
            }
        }
        
    }
}