﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using WMCY.GameHall.Net;

namespace WMCY.GameHall
{

    public class ProperSecurityPanelController : MonoBehaviour
    {
        public InputField input_phoneNum;
        public Text text_lastSubmit;
        public Button btn_submit;
        public Button btn_close;

        private string currentPhone = string.Empty;

        void Awake()
        {
            InitUiEvent();
            InitNetEvent();
        }

        void OnEnable()
        {
            input_phoneNum.text = string.Empty;
        }

        void InitUiEvent()
        {
            btn_submit.onClick.AddListener(OnBtnSubmit_Click);
            btn_close.onClick.AddListener(OnBtnClose_Click);
            //input_phoneNum.onEndEdit.AddListener(CheckInput);
            input_phoneNum.onValueChanged.AddListener(CheckInput);
        }

        void InitNetEvent()
        {
            NetManager.GetInstance().RegisterHandler("getAccountProtectionStatus", HandleNetMsg_GetAccountProtectionStatus);
            NetManager.GetInstance().RegisterHandler("updateAccountProtectionNumber", HandleNetMsg_UpdateAccountProtectionNumber);
        }

        void OnBtnClose_Click()
        {
            gameObject.SetActive(false);
            if(AppManager.Get().isShowAnnouce)
            {
                AppManager.Get().ShowNotice();
                AppManager.Get().isShowAnnouce = false;
            }
        }

        void OnBtnSubmit_Click()
        {
            string phone = input_phoneNum.text;
            if (phone=="")
            {
                AlertDialog.Get().ShowDialog("请输入您的手机号码");
                return;
            }
            NetManager.GetInstance().Send("gcuserService/updateAccountProtectionNumber", new object[] { phone });
            currentPhone = phone;
        }

        void CheckInput(string phone)
        {
            if (phone.IndexOf('-') !=-1)
            {
                phone=phone.Trim('-');
                input_phoneNum.text = phone;
            }
        }

        void HandleNetMsg_GetAccountProtectionStatus(object[] args)
        {
            IDictionary dic = (IDictionary)args[0];
            int state = (int)dic["state"];
            if(state==0)
            {
                string phone = (string)dic["phone"];
                text_lastSubmit.text = string.Format("您上次提交的手机号码为:{0}", phone);
            }
            else
            {
                text_lastSubmit.text = "您暂未提交过手机号码~";
            }
            
        }

        void HandleNetMsg_UpdateAccountProtectionNumber(object[] args)
        {
            IDictionary dic = (IDictionary)args[0];
            bool success = (bool)dic["success"];
            if(success)
            {
                AlertDialog.Get().ShowDialog("提交成功，感谢您的参与！", false,() => { gameObject.SetActive(false); });
                //text_lastSubmit.text = string.Format("您上次提交的手机号码为:{0}", currentPhone);
                AppManager.Get().Notify(UIGameMsgType.UINotify_Fresh_ProperSecuritySign, false);
            }
            else
            {
                int msgCode = (int)dic["msgCode"];
                AlertDialog.Get().ShowDialog("请输入正确的手机号码");
            }
        }

    }
}
