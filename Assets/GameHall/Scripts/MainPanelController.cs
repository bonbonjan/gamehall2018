﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using UnityEngine.SceneManagement;


namespace WMCY.GameHall
{

    public class MainPanelController : MonoBehaviour
    {
        public Transform _tipPropDeadLinePrefab;
        [SerializeField]
        Text m_textGold;//游戏币
        [SerializeField]
        Text m_textExpeGold;//体验币
        [SerializeField]
        Text m_textUserNickName;//用户名
        [SerializeField]
        Text m_textLevel;//等级
        [SerializeField]
        Text m_textLevelName;//称号
        [SerializeField]
        Image m_imageUserIcon;//用户头像
        [SerializeField]
        Image m_newUserMail;//新邮件提醒
        [SerializeField]
        UserIconDataConfig m_userIconDataConfig;
        [SerializeField]
        private GameObject GiftRemindPanel;

        private int layerOrder = 15;

        void Awake()
        {
            Debug.Log("MainPanelController.Awake()");
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Open_CashPanel, this, OpenCashPanel);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Open_RechargePanel, this, OpenRechargePanel);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Open_StorePanel, this, OpenStorePanel);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Close_StorePanel, this, CloseStorePanel);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Open_LotteryCityPanel, this, OpenLotteryCityPanel);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Close_LotteryCityPanel, this, CloseLotteryCityPanel);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Open_SafeBoxPwdCheckPanel, this, OpenSafeBoxPwdCheckPanel);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Open_SafeBoxPanel, this, OpenSafeBoxPanel);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Fresh_GoldAndLottery, this, FreshGoldAndLottery);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Open_UserCompletePanel, this, OpenUserCompletePanel);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Fresh_UserInfo, this, (obj) => { FreshUserInfo(); });

            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Fresh_UseMailSign, this, RefreshNewMailSign);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Fresh_ShareRedSign, this, RefreshShareRedSign);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Fresh_CampRedSign, this, RefreshCampRedSign);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Fresh_ProperSecuritySign, this, RefreshProperSecurityRedsign);
        }

        void Start()
        {
            Debug.Log("MainPanelController.Start()");
            NetManager.GetInstance().RegisterHandler("chargeNotice", HandleNetMsg_ChargeNotice);
            NetManager.GetInstance().RegisterHandler("exChargeGiveUp", HandleNetMsg_exchargeFalseNotice);
            NetManager.GetInstance().RegisterHandler("rechargeGiveUp", HandleNetMsg_rechargeFalseNotice);
            NetManager.GetInstance().RegisterHandler("newGameGold", HandleNetMsg_newGameGoldNotice);
            NetManager.GetInstance().RegisterHandler("newLottery", HandleNetMsg_newLotteryNotice);
            NetManager.GetInstance().RegisterHandler("userAward", HandleNetMsg_userAwardNotice);
            NetManager.GetInstance().RegisterHandler("unOverflow", HandleNetMsg_userUnOverflow);
            NetManager.GetInstance().RegisterHandler("overflow", HandleNetMsg_userOverflow);
            //NetManager.GetInstance().RegisterHandler("quitToLogin", HandleNetMsg_QuitToLogin);
            NetManager.GetInstance().RegisterHandler("newUserMail", HandleNetMsg_NewUserMail);
            NetManager.GetInstance().RegisterHandler("getExpeGold", HandleNetMsg_AddExpeGold);
            NetManager.GetInstance().RegisterHandler("inputPassword", HandleNetMsg_InputPassword);
            NetManager.GetInstance().RegisterHandler("cancelWaitingExpiryUser", HandleNetMsg_CancelWaitingExpiryUser);
            NetManager.GetInstance().RegisterHandler("getActivityInfo", HandleNetMsg_GetActivityInfo);
            NetManager.GetInstance().RegisterHandler("updateActivityInfo", HandleNetMsg_UpdateActivityInfo);
            NetManager.GetInstance().RegisterHandler("newUserLevel", HandleNetMsg_NewUserLevel);
            NetManager.GetInstance().RegisterHandler("newExpeGold", HandleNetMsg_NewExpeGold);
            NetManager.GetInstance().RegisterHandler("newFinishActivity", HandleNetMsg_NewFinishActivity);
            NetManager.GetInstance().RegisterHandler("newUserActivity", HandleNetMsg_NewUserActivity);

            //NetManager.GetInstance().RegisterHandler("dingFen", HandleNetMsg_DingFen);
            NetManager.GetInstance().RegisterHandler("lastGameScore", HandleNetMsg_LastGameScore);
            NetManager.GetInstance().RegisterHandler("replyLastGameScore", HandleNetMsg_ReplyLastGameScore);
            NetManager.GetInstance().RegisterHandler("modifySafeBoxPwd", HandleNetMsg_ModifySafeBoxPwd);

            NetManager.GetInstance().RegisterHandler("updateRunStatus", HandleNetMsg_UpdateRunStatus);

            NetManager.GetInstance().RegisterHandler("giftMode", HandleNetMsg_UpdategiftMode);
            NetManager.GetInstance().RegisterHandler("giftNotice", HandleNetMsg_UpdategiftNotice);
            NetManager.GetInstance().RegisterHandler("giftProp", HandleNetMsg_UpdategiftProp);

            NetManager.GetInstance().RegisterHandler("shareNotice", HandleNetMsg_UpdateshareNotice);
            NetManager.GetInstance().RegisterHandler("shareMode", HandleNetMsg_UpdateshareMode);

            NetManager.GetInstance().RegisterHandler("shareBindingNotice", HandleNetMsg_ShareBindingNotice);

            NetManager.GetInstance().RegisterHandler("shareClearingNotice", HandleNetMsg_ShareClearingNotice);

            NetManager.GetInstance().RegisterHandler("activityAwardNotice", HandleNetMsg_ActivityAwardNotice);

            //NetManager.GetInstance().RegisterHandler("accountProtectionNotice", HandleNetMsg_AccountProtectionNotice);

            Init();
        }

        void OnEnable()
        {
            Debug.Log("MainPanelController.OnEnable()");
            AppManager.Get().RefreshLocalization();
            FreshPayModeRelationIcons();            
        }

        public void Init()
        {
            transform.Find("GoldArea/textGold").GetComponent<Text>().text = Convert.ToString(GVars.user.gameGold);
            transform.Find("ExpeGoldArea/textExpeGold").GetComponent<Text>().text = Convert.ToString(GVars.user.expeGold);
            AppManager.Get().RefreshLocalization();
            if (GVars.user.username == "yunwei1")
            {
                this.transform.Find("ShowIP").gameObject.SetActive(true);
                this.transform.Find("ShowIP").GetComponent<Text>().text = "这个包使用的IP是:" + GVars.IPAddress_Game;
            }
            else
            {
                this.transform.Find("ShowIP").gameObject.SetActive(false);
            }

            layerOrder = 15;

            CheckDeadLineProp();
            FreshUserInfo();
            if(AppManager.Get().special)
                SetBtnMoreAndMorePanel(false);

            //transform.FindChild("btnProperSecurity/redsign").gameObject.SetActive(false);
        }

        void Update()
        {

        }

        /// <summary>
        /// 检测快到期的道具并提示
        /// </summary>
        private void CheckDeadLineProp()
        {
            foreach (int propId in GVars.ownedProps.Keys)
            {
                if (GVars.ownedProps[propId].RemainTime <= 86400000 && GVars.ownedProps[propId].IsShow == true)
                {
                    Transform tip = (Transform)Instantiate(_tipPropDeadLinePrefab);
                    tip.parent = transform;
                    tip.localPosition = new Vector3(0, 0, 0);
                    tip.localScale = Vector3.one;
                    tip.GetComponent<TipPropDeadLine>().ShowUI(propId);
                }
            }
            
        }

        #region Handle Buttons Click

        //测试通用弹框按钮
        public void OnBtnClick_TestAlert()
        {
            Debug.Log("OnBtnClick_TestAlert");
            AlertDialog.GetInstance().ShowDialog("测试alertDialog");
        }

        public void OnBtnClick_AddGold(GameObject obj)
        {
            if (GVars.user.type == 1) //游客用户止步
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游客账号无法操作此功能" : "Guest accounts can't operate this function", false, HideMorePanel);
                return;
            }
            //UserCompleteController.UserCompleteNoPrompt = false;
            if (GVars.payMode == 1)
            {
                //商店开启
                _openPanel("StorePanel");
            }
            else
            {
                //商店没有开启
                RechargeProcess();
            }
            HideMorePanel();
        }

        public void OnBtnClick_AddExpeGold()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            string content = GVars.language == "zh" ? "体验币低于10000可申请，每次申请补足10000币" : "Tickets less than 10000 may apply for each application make up 10000 tickets";
            AlertDialog.Get().ShowDialog(content,true,()=> {
                if(GVars.expeGold<10000)
                    NetManager.Get().Send("gcuserService/getExpeGold", new object[] { });
            });

            HideMorePanel();
        }

        public void OnBtnClick_Store()
        {
            if (GVars.user.type == 1) //游客用户止步
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游客账号无法操作此功能" : "Guest accounts can't operate this function");
                return;
            }
            //UserCompleteController.UserCompleteNoPrompt = false;
            if (GVars.payMode == 1)
            {
                //商店开启
                _openPanel("StorePanel");
            }
            else
            {
                //商店没有开启
                RechargeProcess();
            }
            HideMorePanel();
        }

        public void OnBtnClick_SafeBox()
        {
            if (GVars.user.type == 1) //游客用户止步
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游客账号无法操作此功能" : "Guest accounts can't operate this function");
                return;
            }
            //GVars.user.safeBox = 1;//测试
            if (GVars.user.safeBox == 1)
            {
                //玩家设置过口令
                _openPanel("SafeBoxPwdCheckPanel");
            }
            else
            {
                //玩家未设置过口令
                _openPanel("SafeBoxPwdSetPanel");

            }
            HideMorePanel();
        }

        public void OnBtnClick_LotteryCity()
        {
            if (GVars.user.type == 1) //游客用户止步
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游客账号无法操作此功能" : "Guest accounts can't operate this function", false, HideMorePanel);
                return;
            }
            //UserCompleteController.UserCompleteNoPrompt = false;
            if (GVars.payMode == 1)
            {
                //彩票城开启
                _openPanel("LotteryCityPanel");
            }
            else
            {
                //彩票城没有开启
                CashProcess("cash");
            }
            HideMorePanel();
        }

        private void RefreshNewMailSign(object o)
        {
            int NoReadCount =(int) o;
            m_newUserMail.gameObject.SetActive(!(NoReadCount==0));
            m_newUserMail.transform.Find("Text").GetComponent<Text>().text = NoReadCount.ToString();
        }

        private void RefreshShareRedSign(object o)
        {
            bool isShow = (bool)o;
            transform.Find("bottomArea/btnShare/RedSign").gameObject.SetActive(isShow);
            transform.parent.Find("SharePanel/TwoTab/RedSign").gameObject.SetActive(isShow);
        }

        private void RefreshCampRedSign(object o)
        {
            bool isShow = (bool)o;
            transform.Find("bottomArea/btnCampaign/RedSign").gameObject.SetActive(isShow);
        }

        public void RefreshProperSecurityRedsign(object obj)
        {
            bool isShow = (bool)obj;
            transform.Find("btnProperSecurity/redsign").gameObject.SetActive(isShow);
        }
        public void OnBtnClick_Mail()
        {
            Debug.Log("OnBtnClick_Mail");
            NetManager.GetInstance().Send("gcuserService/getEmailList", new object[] { });//    
            //m_newUserMail.gameObject.SetActive(false);
            _openPanel("MailPanel");
            HideMorePanel();
        }

        public void OnBtnClick_Rank()
        {
            Debug.Log("OnBtnClick_Rank");
            NetManager.GetInstance().Send("gcuserService/getRankList", new object[] { 1,1 });//      
            _openPanel("RankPanel");
            HideMorePanel();
        }

        public void OnBtnClick_Campagin()
        {
            if (GVars.user.type == 1) //游客用户止步
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游客账号无法操作此功能" : "Guest accounts can't operate this function", false, HideMorePanel);
                return;
            }
            //if (GVars.user.promoterId != 0)
            //{
            //    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "只有直属会员才可参加活动" : "Only dir-members can participate activities", false, HideMorePanel);
            //    return;
            //}

            Debug.Log("OnBtnClick_Campagin");

            NetManager.GetInstance().Send("activityService/getActivityInfo", new object[] {  });//            
            HideMorePanel();
        }

        private void HandleNetMsg_UpdateActivityInfo(object[] obj)
        {
            if(this.transform.parent.Find("CampaignPanel").gameObject.activeSelf)
            {
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "活动状态已修改" : "Active state has been modified", false, () =>
                {
                    this.transform.parent.Find("CampaignPanel").gameObject.SetActive(false);
                });
            }
        }

        private void HandleNetMsg_GetActivityInfo(object[] obj)
        {
            Dictionary<string, object> objs = obj[0] as Dictionary<string, object>;
            object o = (object)objs["activityInfo"];
            ActivityInfo ai = ActivityInfo.CreateWithDic((Dictionary<string, object>)o);
            if(ai.activityStatus==1)
            {
                NetManager.GetInstance().Send("gcuserService/getActivity", new object[] { 1 });//      
                _openPanel("CampaignPanel");
            }
            else
            {
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "活动尚未开启" : "The activity has not yet opened");
            }

            
        }

        public void OnBtnClick_Recharge()
        {
            if (GVars.user.type == 1) //游客用户止步
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游客账号无法操作此功能" : "Guest accounts can't operate this function", false, HideMorePanel);
                return;
            }
            Debug.Log("OnBtnClick_Recharge");
            _openPanel("RechargePanel");
            HideMorePanel();
        }

        public void OnBtnClick_Award()
        {
            Debug.Log("OnBtnClick_Award");
            _openPanel("AwardPanel");
            HideMorePanel();
        }

        public void OnBtnClick_UserIcon()
        {
            Debug.Log("OnBtnClick_UserIcon");
            _openPanel("UserSettingsPanel");
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_UserInfoPanel, 0);
            
            HideMorePanel();
        }

        public void OnBtnClick_More()
        {
            Debug.Log("OnBtnClick_More");
            SetBtnMoreAndMorePanel(!isMorePanelShow);
            //_openPanel("UserSettingsPanel");
            //var target = transform.Find("MorePanel").gameObject;
            //target.SetActive(!target.activeInHierarchy);
            //transform.Find("bottomArea/btnMore/Image").gameObject.SetActive(!target.activeSelf);
            //transform.Find("bottomArea/btnMore/Image_xia").gameObject.SetActive(target.activeSelf);
        }


        public void OnBtnClick_Announcement()
        {
            Debug.Log("OnBtnClick_Announcement");
            NetManager.GetInstance().Send("gcuserService/getNotice", new object[] { });
            _openPanel("AnnouncementPanel");
            HideMorePanel();
        }

        public void OnBtnClick_ProperSecurity()
        {
            if (GVars.user.type == 1) //游客用户止步
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游客账号无法操作此功能" : "Guest accounts can't operate this function", false, HideMorePanel);
                return;
            }
            Debug.Log("OnBtnClick_ProperSecurity");
            NetManager.GetInstance().Send("gcuserService/getAccountProtectionStatus", new object[] { });
            _openPanel("ProperSecurityPanel");
            HideMorePanel();
        }

        public void OnBtnClick_Setting()
        {
            Debug.Log("OnBtnClick_Setting");
            _openPanel("SettingPanel");
            HideMorePanel();
        }

        public void OnBtnClick_Share()
        {
            Debug.Log("OnBtnClick_Share");
            _openPanel("SharePanel");
            HideMorePanel();

        }

        public void OnBtnClick_Dealer()
        {
            Debug.Log("OnBtnClick_Dealer");
            MeshUICamera.Get().ClickDealer();
        }

        public void OnBtnClick_DeliverRedEnvelope()
        {
            Debug.Log("OnBtnClick_DeliverRedEnvelope");
            _openPanel("DeliverRedEnvelopePanel");
            HideMorePanel();
        }

        public void OnBtnClick_CharityHall()
        {
            Debug.Log("OnBtnClick_CharityHall");
            _openPanel("CharityHallPanel");
            HideMorePanel();
        }

        //public void OnBtnClick_Campaign()
        //{
        //    Debug.Log("OnBtnClick_Campaign");
        //    _openPanel("CampaignPanel ");

        //}
        //

        #endregion

        private void OpenCashPanel(object obj)
        {
            _openPanel("CashPanel");
            HideMorePanel();
        }

        private void OpenRechargePanel(object obj)
        {
            _openPanel("RechargePanel");
            HideMorePanel();
        }

        private void OpenStorePanel(object obj)
        {
            int tabIndex = (int)obj;
            _openPanel("StorePanel");
            transform.parent.Find("StorePanel").GetComponent<StorePanelController>().FocusTabByIndex(tabIndex);
            HideMorePanel();
        }

        private void CloseStorePanel(object obj)
        {
            var target = transform.parent.Find("StorePanel");
            if (target != null) target.gameObject.SetActive(false);
        }
        
        private void OpenLotteryCityPanel(object obj)
        {
            _openPanel("LotteryCityPanel");
            HideMorePanel();
        }

        private void CloseLotteryCityPanel(object obj)
        {
            var target = transform.parent.Find("LotteryCityPanel");
            if (target != null) target.gameObject.SetActive(false);
        }
        /// <summary>
        /// 通知打开保险箱密码验证界面
        /// </summary>
        /// <param name="obj"></param>
        private void OpenSafeBoxPwdCheckPanel(object obj)
        {
            _openPanel("SafeBoxPwdCheckPanel");
            HideMorePanel();
        }
        private void OpenSafeBoxPanel(object obj)
        {
            _openPanel("SafeBoxPanel");
            HideMorePanel();
        }

        private void OpenUserCompletePanel(object obj)
        {
            ArrayList args = (ArrayList)obj;
            _openPanel("UserCompletePanel");
            transform.parent.Find("UserCompletePanel/Alertbg/Toggle").GetComponent<Toggle>().isOn = false;
            transform.parent.Find("UserCompletePanel").GetComponent<UserCompleteController>().FromArgs = args;
            HideMorePanel();
        }
        
        void _openPanel(string panelName)
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            var target = transform.parent.Find(panelName);
            if (target != null) target.gameObject.SetActive(true);
        }

        public void RechargeProcess()
        {
            if (GVars.user.card == "-1" && UserCompleteController.UserCompleteNoPrompt == false)
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_UserCompletePanel, new ArrayList { "Store" });
            else
                _openPanel("RechargePanel");
            HideMorePanel();
        }

        public void CashProcess(string from)
        {
            if (GVars.user.card == "-1" && UserCompleteController.UserCompleteNoPrompt == false)
            {
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_UserCompletePanel, new ArrayList { from });
            }
            else
            {
                //用户完善了资料
                _openPanel("CashPanel");

            }
            HideMorePanel();
        }

        private void FreshGoldAndLottery(object obj)
        {
            transform.Find("GoldArea/textGold").GetComponent<Text>().text = Convert.ToString(GVars.user.gameGold);
            transform.Find("ExpeGoldArea/textExpeGold").GetComponent<Text>().text = Convert.ToString(GVars.user.expeGold);
            if (UserInfoViewController.Get() != null) UserInfoViewController.Get().RefreshGoldAndExp();
        }

        public void UpdateBtnStoreStatus()
        {
            //GVars.useRaffleStore;
        }

        public void FreshUserInfo()
        {
            Debug.Log("FreshUserInfo");
            m_textUserNickName.text = Convert.ToString(GVars.user.nickname);
            m_textLevel.text = "LV." + Convert.ToString(GVars.user.level);
            m_textLevelName.text = Utils.GetLevelName(GVars.user.level,"zh");// 
            m_imageUserIcon.sprite = m_userIconDataConfig.list[GVars.user.photoId].sprite;
            if (UserInfoViewController.Get() != null) UserInfoViewController.Get().RefreshLevelText();
        }

        public void FreshPayModeRelationIcons()
        {
            if(GVars.payMode == 1)
            {
                transform.Find("bottomArea/btnStore").gameObject.SetActive(true);
                transform.Find("bottomArea/btnRaffleStore").gameObject.SetActive(true);
                transform.Find("bottomArea/btnExchange").gameObject.SetActive(false);
            }
            else
            {
                transform.Find("bottomArea/btnStore").gameObject.SetActive(false);
                transform.Find("bottomArea/btnRaffleStore").gameObject.SetActive(false);
                transform.Find("bottomArea/btnExchange").gameObject.SetActive(true);
            }
        }

        bool isMorePanelShow = false;
        void SetBtnMoreAndMorePanel(bool show)
        {
            isMorePanelShow = show;
            var target = transform.Find("MorePanel").gameObject;
            target.SetActive(show);
            transform.Find("bottomArea/btnMore/Image").gameObject.SetActive(!show);
            transform.Find("bottomArea/btnMore/Image_xia").gameObject.SetActive(show);
        }

        //更多界面已经不存在了
        private void HideMorePanel()
        {
            if(!isMorePanelShow) return;
            SetBtnMoreAndMorePanel(false);
            //var target = transform.Find("MorePanel").gameObject;
            //target.SetActive(false);
            //transform.Find("btnMore/Image").gameObject.SetActive(true);
            //transform.Find("btnMore/Image_xia").gameObject.SetActive(false);
        }

        private void HandleNetMsg_ChargeNotice(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            Debug.Log(obj);
            //bool isSuccess = (bool)obj["success"];
            //if (isSuccess)
            {

                //GVars.user.gameGold = Convert.ToInt32(obj["gameGold"]);
                int type = Convert.ToInt32(obj["type"]);
                //AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_GoldAndLottery);
                string[] zh_notice = { "充值成功", "兑换成功" };
                string[] en_notice = { "Recharge successful", "Exchange successful" };
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? zh_notice[type] : en_notice[type]);

            }
        }

        /// <summary>
        /// 彩票城兑奖/兑奖
        /// </summary>
        /// <param name="objs"></param>
        private void HandleNetMsg_exchargeFalseNotice(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            Debug.Log(obj);
            bool isSuccess = (bool)obj["success"];
            int messageStatus = Convert.ToInt32(obj["messageStatus"]);
            if (!isSuccess)
            {
                if (GVars.payMode == 1)
                {
                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "领取失败，彩票已经返还" : "reveive failed,lottery has returned");
                }
                else
                {
                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "兑换失败" : "cash failed");
                }
            }
        }

        private void HandleNetMsg_rechargeFalseNotice(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            Debug.Log(obj);
            bool isSuccess = (bool)obj["success"];
            int messageStatus = Convert.ToInt32(obj["messageStatus"]);
            if (!isSuccess)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "充值失败" : "recharge failed");

            }
        }

        private void HandleNetMsg_newLotteryNotice(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            GVars.user.lottery = Convert.ToInt32(obj["lottery"]);
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_GoldAndLottery);
        }

        private void HandleNetMsg_newGameGoldNotice(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            GVars.user.gameGold = Convert.ToInt32(obj["gameGold"]);
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_GoldAndLottery);
        }

        private void HandleNetMsg_NewExpeGold(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            GVars.user.expeGold = Convert.ToInt32(obj["newExpeGold"]);
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_GoldAndLottery);
        }
        
        private void HandleNetMsg_userAwardNotice(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            int awardGameGold = Convert.ToInt32(obj["gameGold"]);
            AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? string.Format("赠送{0}游戏币", awardGameGold) : string.Format("award {0} game gold", awardGameGold));
        }

        private void HandleNetMsg_userUnOverflow(object[] objs)
        {
            GVars.user.overflow = 0;//取消暴机状态
        }

        private void HandleNetMsg_userOverflow(object[] objs)
        {
            GVars.user.overflow = 1;//跟新暴机
        }

        //private void HandleNetMsg_DingFen(object[] objs)
        //{
        //    GVars.ScoreOverflow = true;
        //}
        private void HandleNetMsg_NewUserActivity(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            int activityType = (int)obj["activityType"];
            Debug.Log(LogHelper.Red("activityType: " + activityType));
            //刷新活动界面
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_UseCampaignPanel,activityType);
        }

        private void HandleNetMsg_NewFinishActivity(object[] objs)
        {
            //结束的活动
        }

        private void HandleNetMsg_LastGameScore(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            int score = (int)obj["score"];

            GVars.ScoreOverflow = false;
            NetManager.GetInstance().Send("gcuserService/replyLastGameScore", new object[] { GVars.user.id });
            if (score == 0) return;
            AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? string.Format("上局的得分为{0}", score) : string.Format("Previous round scores{0}", score));
        }

        private void HandleNetMsg_ReplyLastGameScore(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            bool success = (bool)obj["success"];
            Debug.Log(LogHelper.Green("replyLastGameScore"));
        }

        //商城赠送功能的开启和关闭
        private void HandleNetMsg_UpdategiftMode(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            int giftMode = (int)obj["giftMode"];
            GVars.giftMode = giftMode;
        }

        private void HandleNetMsg_UpdategiftNotice(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            string username = (string)obj["username"];
            int gameGold = (int)obj["gameGold"];
            string time = (string)obj["time"];
            string note = (string)obj["note"];

            //_openPanel("GiftRemindPanel");
            GameObject go = Instantiate(GiftRemindPanel);
            var canvas = go.GetComponent<Canvas>();

            //canvas.sortingLayerName = "UI";
            go.transform.SetParent(this.transform.parent);
            canvas.sortingLayerID = go.layer;
            //go.transform.localPosition = Vector3.zero;
            go.transform.localScale = Vector3.one;
            go.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
            go.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
            go.transform.GetComponent<GiftRemindPanelController>().Init(username, gameGold, time, note);
            go.GetComponent<Canvas>().sortingOrder = layerOrder;
            layerOrder++;
            //canvas.cachedSortingLayerValue = canvas.sortingLayerID;
            //Debug.Log(LogHelper.Orange("cachedSortingLayerValue: " + canvas.cachedSortingLayerValue));
            //Debug.Log(LogHelper.Orange("sortingLayerID: " + canvas.sortingLayerID));
            //Debug.Log(LogHelper.Orange("sortingLayerName: " + canvas.sortingLayerName));
            //Debug.Log(LogHelper.Orange("sortingOrder: " + canvas.sortingOrder));
            //Debug.Log(LogHelper.Orange("go.layer: " + go.layer));
            go.SetActive(true);
            //Canvas.ForceUpdateCanvases();
        }

        private void HandleNetMsg_UpdategiftProp(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            GVars.ownedProps.Clear();
            object[] propObjs = (object[])obj["prop"];
            for (int i = 0; i < propObjs.Length; i++)
            {
                Dictionary<string, object> propObj = propObjs[i] as Dictionary<string, object>;
                GVars.ownedProps.Add(Convert.ToInt32(propObj["propId"]), new ownShopProp(Convert.ToInt32(propObj["propId"]), Convert.ToInt64(propObj["remainTime"]), (bool)propObj["show"]));
            }

            for (int i = 9; i < 12; i++)
            {
                if (GVars.ownedProps.ContainsKey(i) == true)
                {
                    AppManager.GetInstance().Notify(UIGameMsgType.UINotify_FreshDealer, i - 8);
                    break;
                }
                else if (i == 11)
                {
                    AppManager.GetInstance().Notify(UIGameMsgType.UINotify_FreshDealer, 0);
                }
            }
        }

        private void HandleNetMsg_UpdateshareNotice(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            int awardGold = (int)obj["awardGold"];
            int gameGold = (int)obj["gameGold"];
            GVars.user.gameGold = gameGold;
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_GoldAndLottery);
            AlertDialog.GetInstance().ShowDialog(string.Format(GVars.language == "zh" ? "已为您自动结算通过分享获得的{0}游戏币，请查收" : "Automatic settlement harvest {0} coins from sharing rewards,please check", awardGold));
        }

        private void HandleNetMsg_ShareBindingNotice(object[] objs)
        {
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_ShareRedSign, true);
        }

        private void HandleNetMsg_ShareClearingNotice(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            int ShareAwardGold = (int)obj["awardGold"];
            AlertDialog.GetInstance().ShowDialog(string.Format(GVars.language == "zh" 
                ? "祝贺您，您在邀请朋友的活动中获得了{0}币，已发送至您的邮件中" 
                : "Congratulations , you get {0} coins from inviting activity and go check your mail"
                , ShareAwardGold));
        }

        private void HandleNetMsg_ActivityAwardNotice(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            int type = (int)obj["type"];
            this.transform.Find("bottomArea/btnCampaign/Redsign").gameObject.SetActive(type == 1);
        }

        //private void HandleNetMsg_AccountProtectionNotice(object[] objs)
        //{            
        //    IDictionary dic = (IDictionary)objs[0];
        //    int state = (int)dic["state"];
        //    if (state == 0)//已填写号码 
        //    {
        //        AppManager.Get().ShowNotice();
        //    }
        //    else if (state == 1)
        //    {
        //        OnBtnClick_ProperSecurity();
        //        RefreshProperSecurityRedsign(true);
        //        AppManager.Get().isShowAnnouce = true;
        //    }
        //    else
        //    {
        //        AppManager.Get().ShowNotice();
        //        RefreshProperSecurityRedsign(true);
        //    }
        //}


        private void HandleNetMsg_UpdateshareMode(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            GVars.shareMode = (int)obj["shareMode"];
            GVars.specialMark = (int)obj["specialMark"];
            AppManager.Get().Notify(UIGameMsgType.UINotify_Fresh_SharePanel, GVars.shareMode);
        }

        private void HandleNetMsg_UpdateRunStatus(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;

            object gameRunStatus = obj["gameRunStatus"];
            InnerGameManager.Get().UpdateGameRunStatus(gameRunStatus);
        }

        /// <summary>
        /// 后台更改保险箱密码给客服端的主动提示消息
        /// </summary>
        /// <param name="objs"></param>
        private void HandleNetMsg_ModifySafeBoxPwd(object[] objs)
        {
            //Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;

            AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "口令修改成功" : "The password of safe box has been modified.");
        }

        private void HandleNetMsg_NewUserLevel(object[] obj)
        {
            Dictionary<string, object> objs = obj[0] as Dictionary<string, object>;
            int newLevel = (int)objs["newLevel"];

            GVars.user.level = newLevel;
            FreshUserInfo();
        }

        private void HandleNetMsg_NewUserMail(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            int notReadCount = (int)obj["notReadCount"];

            RefreshNewMailSign(notReadCount);
        }

        public void HandleNetMsg_QuitToLogin(object[] objs)
        {
            GVars.lockRelogin = true;
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            int type = (int)obj["type"];
            NetManager.Get().Disconnect();
            AppManager.Get().ForbidNetDownHint();
            GVars.lockReconnect = true;
            GVars.ScoreOverflow = false;//异常状态重置顶分

            if (transform.parent.Find("RedBonusRain").gameObject.activeSelf)
            {
                RedBonusControl._redBonusControl.MaintenanceRedBonusClose();
            }

            switch (type){
                case 1:                    
                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "系统维护" : "Server maintenance", false, QuitToLogin);
                    break;
                case 2:
                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被冻结" : "This account has been frozen", false, QuitToLogin);
                    break;
                case 3:
                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被删除" : "Your account has been deleted", false, QuitToLogin);
                    break;
                case 4:
                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "该账号已在别处登录，您被迫下线" : "The account has been logged in elsewhere", false, QuitToLogin);
                    break;
                case 5:
                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "后台修改了密码" : "The password has been modified", false, QuitToLogin);
                    break;
                case 33:
                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游客登录功能已关闭" : "Tourists landing has been closed", false, QuitToLogin);
                    break;
                default:
                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请重新登录" : "Please ReLogin", false, QuitToLogin);
                    break;
            }

        }

        private void HandleNetMsg_InputPassword(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            int type = (int)obj["nType"];
            int GameScore = (int)obj["nGameScore"];

           this.transform.parent.Find("Excharge").GetComponent<BackgroundControl>().Init(type, GameScore);
        }
        private void HandleNetMsg_CancelWaitingExpiryUser(object[] objs)
        {
            AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "上级取消了本次操作" : "The superior canceled the operation", false, () =>
            {
                this.transform.parent.Find("Excharge").gameObject.SetActive(false);
            });
            
        }

        void HandleNetMsg_AddExpeGold(object[] objs)
        {
            //Destroy(transform.gameObject);
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            Debug.Log(obj);
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                GVars.user.expeGold = (int)obj["expeGold"];
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_GoldAndLottery);
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "申请成功" : "Application successful");

            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                Debug.Log(msgcode);
                switch (msgcode)
                {
                    case 15:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "体验币低于10000方可申请" : "Tickets less than 10000 may apply for each application make up 10000 tickets");
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "登录失败" : "login false");
                        break;
                }
            }
        }

        //重新异步加载 主场景
        private void QuitToLogin()
        {
            //Application.LoadLevelAsync(Application.loadedLevelName);
            //SceneManager.LoadSceneAsync(Application.loadedLevelName);
            SceneManager.LoadSceneAsync(0);
        }
    }
}


