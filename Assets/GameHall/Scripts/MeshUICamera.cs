﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using DG.Tweening;

namespace WMCY.GameHall
{
    public class MeshUICamera : MonoBehaviour
    {
        //private Vector3 m_posNormal;
        //private Vector3 m_posLogin;
        private float m_sizeNormal;
        private float m_sizeLogin;
        private Camera dealerCamera;
        private static MeshUICamera _instance;
        private GameObject m_activeDealer;

        void Awake()
        {
            //m_posNormal = transform.position;
            //m_posLogin = new Vector3(403f,45.44f,-23.86f);
            dealerCamera = GetComponent<Camera>();
            m_sizeNormal = dealerCamera.orthographicSize;
            m_sizeNormal = 4f;
            m_sizeLogin = 5.2f;
            _instance = this;
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_FreshDealer, this, FreshDealer);
        }

        public static MeshUICamera Get()
        {
            return _instance;
        }


        private void FreshDealer(object obj)
        {
            string[] dealerName = {"dealer00","dealer01","dealer02","dealer03"};
            int dealerIndex = (int)obj;
            if(dealerIndex == -1)
            {
                dealerIndex = 0;
                dealerCamera.orthographicSize = m_sizeLogin;
            }
            else
            {
                dealerCamera.orthographicSize = m_sizeNormal;
            }
            for (int i = 0; i < 4; i ++ )
            {
                var go = transform.Find(dealerName[i]).gameObject;
                go.SetActive(i == dealerIndex ? true : false);
                m_activeDealer = i == dealerIndex ? go : m_activeDealer;
            }
            switch(dealerIndex)
            {
                case 0:
                    SoundManager.Get().PlaySound(SoundType.BG_First);
                    break;
                case 1:
                    SoundManager.Get().PlaySound(SoundType.BG_Second);
                    break;
                case 2:
                    SoundManager.Get().PlaySound(SoundType.BG_Third);
                    break;
                case 3:
                    SoundManager.Get().PlaySound(SoundType.BG_Four);
                    break;
            }
                
        }

        public void ClickDealer()
        {
            if(m_activeDealer)
            {
                m_activeDealer.GetComponent<BeautyDealer>().Greet();
            }
        }
    }

}
