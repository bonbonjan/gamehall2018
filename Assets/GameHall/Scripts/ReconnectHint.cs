﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using WMCY.GameHall.Common;


namespace WMCY.GameHall
{


    public class ReconnectHint : MB_Singleton<ReconnectHint>
    {
        private GameObject _goContainer;
        [SerializeField]
        private Text _textInfo;
        [SerializeField]
        private GameObject _goUI;

        private Coroutine _coTextInfoAni = null;

        void Awake()
        {
            if (_instance == null)
            {
                SetInstance(this);
                PreInit();
            }
            else
            {

            }
        }

        void Start()
        {
            //Show();
        }

        public void PreInit()
        {
            _goContainer = gameObject;
        }

        public void Show()
        {
            if (_goContainer.activeSelf) return;
            //LockManager.Lock("Esc");
            Debug.Log(LogHelper.Magenta("ReconnectHint Show"));

            _goContainer.SetActive(true);
            _setUIEnable(false);
            _coTextInfoAni = StartCoroutine(_textInfoAni());
        }

        public void Hide()
        {
            //Debug.Log("ReconnectHint Hide");
            Debug.Log(LogHelper.Magenta("ReconnectHint Hide"));

            //LockManager.UnLock("Esc");
            _goContainer.SetActive(false);
            if (_coTextInfoAni != null) StopCoroutine(_coTextInfoAni);
            _coTextInfoAni = null;
        }

        private void _setUIEnable(bool isEnable)
        {
            _goUI.SetActive(isEnable);
        }

        public void OnBtnBg_Click()
        {
            //_setUIEnable(true);
            //Hide();
        }

        void Update()
        {

        }

        IEnumerator _textInfoAni()
        {
            int count = 0;
            yield return new WaitForSeconds(0.0f);
            _setUIEnable(true);
            while (true)
            {
                string prefix = GVars.language == "zh" ? "网络断开，正在重连 " : "Reconnecting to the network ";
                string postfix = "";
                count++;
                count = count % 8;
                for (int i = 0; i < count; i++)
                {
                    //postfix += i == 3 ? " " : "·";
                    postfix += i == 3 ? " " : "‧";
                }

                _textInfo.text = prefix + postfix;
                yield return new WaitForSeconds(0.3f);
            }

            //yield break;
            //_coTextInfoAni = null;
        }
    }
}
