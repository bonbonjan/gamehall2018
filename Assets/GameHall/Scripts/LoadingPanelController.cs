﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

using WMCY.GameHall.Net;
using WMCY.GameHall.Common;

namespace WMCY.GameHall
{


public class LoadingPanelController : MonoBehaviour
{

    [SerializeField]
    Slider m_slider;
    bool m_allowLoadingFinish = false;

    void Start()
    {
        if (_check())
        {
            StartCoroutine(_waitForPrepared());
        }
        AppManager.Get().Register(UIGameMsgType.UINotify_Allow_LoadingFinish, this, (obj) => { SetAllowLoadingFinish(true); });
        this.transform.Find("Text").GetComponent<Text>().text = GVars.isShowHallName?Application.productName:"";
    }

    bool _check()
    {
        //Debug.Assert(m_slider);
        if (m_slider == null)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
            Debug.LogError("sth wrong!");
            return false;
        }
        return true;



    }

    IEnumerator _waitForPrepared()
    {
        Debug.Log("_waitForPrepared");
        Debug.Log(m_allowLoadingFinish);
        float duration = 1f;
        //yield return new WaitForSeconds(0.1f);
        yield return null;
        float beginTime = Time.time;
        //float timeTrace = 0f;
        bool waitAllow = !m_allowLoadingFinish;
        Debug.Log("waitAllow: "+waitAllow);
        while(Time.time<beginTime+duration)
        {
            m_slider.value = Mathf.Lerp(0,m_slider.maxValue,(Time.time-beginTime)/duration);
            yield return null;

            if (!GVars.lockRelogin && (GVars.isStartedFromGame || GVars.isStartedFromLuaGame))
            {
                if (waitAllow && m_allowLoadingFinish)
                {
                    gameObject.SetActive(false);
                    yield break;
                }
            }
        }

        while(m_allowLoadingFinish == false)
        {
            yield return null;
        }


        gameObject.SetActive(false);
        if (!GVars.lockRelogin &&(GVars.isStartedFromGame || GVars.isStartedFromLuaGame))
        {

        }
        else
        {
            AppManager.GetInstance().ShowLoginPanel();

        }

    }

    void Update()
    {

    }

    public void SetAllowLoadingFinish(bool value)
    {
        Debug.Log("SetAllowLoadingFinish");
        m_allowLoadingFinish = value;
    }
}

}
