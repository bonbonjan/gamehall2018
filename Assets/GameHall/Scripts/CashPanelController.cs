﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;

namespace WMCY.GameHall
{
    public class CashPanelController : MonoBehaviour
    {
        private int m_cashCount = 0;

        void Awake()
        {

        }

        void Start()
        {
            NetManager.GetInstance().RegisterHandler("excharge", HandleNetMsg_Excharge);
        }

        void OnEnable()
        {
            ResetView();
            //SwitchIntoEnglish();
        }

        void Update()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
            {
                transform.Find("titleBg/title").GetComponent<Text>().text = "兑奖";
                transform.Find("cashItem/itemName").GetComponent<Text>().text = "兑奖数目:";
                transform.Find("cashItem/input/Placeholder").GetComponent<Text>().text = "输入游戏币数量";
                transform.Find("noteItem/itemName").GetComponent<Text>().text = "备注信息:";
                transform.Find("noteItem/input/Placeholder").GetComponent<Text>().text = "输入备注信息";
                transform.Find("password/itemName").GetComponent<Text>().text = "游戏密码:";
                transform.Find("password/input/Placeholder").GetComponent<Text>().text = "输入游戏密码";
                transform.Find("sureBtn/Text").GetComponent<Text>().text = "确定";
            }
            else
            {
                transform.Find("titleBg/title").GetComponent<Text>().text = "Exchange";
                transform.Find("cashItem/itemName").GetComponent<Text>().text = "Amount:";
                transform.Find("cashItem/input/Placeholder").GetComponent<Text>().text = "Input Amount";
                transform.Find("noteItem/itemName").GetComponent<Text>().text = "Remark:";
                transform.Find("noteItem/input/Placeholder").GetComponent<Text>().text = "Input Remark";
                transform.Find("password/itemName").GetComponent<Text>().text = "Password:";
                transform.Find("password/input/Placeholder").GetComponent<Text>().text = "Input Password";
                transform.Find("sureBtn/Text").GetComponent<Text>().text = "Confirm";
            }
            

        }

        void ResetView()
        {
            transform.Find("cashItem/input").GetComponent<InputField>().text = "";
            transform.Find("phoneItem/input").GetComponent<InputField>().text="";
            transform.Find("noteItem/input").GetComponent<InputField>().text = "";
            transform.Find("password/input").GetComponent<InputField>().text = "";
        }

        public void OnBtnClick_Close()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            gameObject.SetActive(false);
        }

        public void OnBtnClick_Sure()
        {
            string num = transform.Find("cashItem/input").GetComponent<InputField>().text;
            string phone = transform.Find("phoneItem/input").GetComponent<InputField>().text;
            string note = transform.Find("noteItem/input").GetComponent<InputField>().text;
            string pwd = transform.Find("password/input").GetComponent<InputField>().text;
            if (num == "")
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "兑换额度不能为空" : "The amount cannot be blank", false, () =>
                {
                    ResetView();
                });
                return;
            }
            else if (Convert.ToInt32(num) == 0)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "兑换额度不能为零" : "The amount cannot be 0", false, () =>
                {
                    ResetView();
                });
                return;
            }
            else if (Convert.ToInt32(num) < 0)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "兑奖额度输入有误" : "The amount cannot be less than 0", false, () =>
                {
                    ResetView();
                });
                return;
            }
            else if(phone=="")
            {
                AlertDialog.GetInstance().ShowDialog("请输入您的手机号码");
                return;
            }
            else if (pwd == "")
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游戏密码不能为空" : "please enter the password", false, () =>
                {
                    if (Convert.ToInt64(num) > GVars.user.gameGold)
                    {
                        ResetView();
                    }
                });
                return;
            }
            
            if (Convert.ToInt64(num) > GVars.user.gameGold)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游戏币不足" : "Coin shortage", false, () =>
                {
                    ResetView();
                });
                return;
            }
            NetManager.GetInstance().Send("gcuserService/excharge", new object[] { Convert.ToInt64(num), phone, note, pwd });
            SoundManager.Get().PlaySound(SoundType.Common_Click);
        }

        public void CashNumChanged()
        {
            string num = transform.Find("cashItem/input").GetComponent<InputField>().text;
            if (num.Contains("-"))
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "兑奖额度输入有误" : "Illegal number");
            }
            else if (num != "")
            {
                num = transform.Find("cashItem/input").GetComponent<InputField>().text = Convert.ToInt64(num) > 100000 ? "100000" : num;
                m_cashCount = Convert.ToInt32(num);
            }
            else
            {
                m_cashCount = 0;
            }
            UpdateCashcount();
            transform.Find("cashItem/equalMoney").GetComponent<Text>().text = string.Format(GVars.language == "zh" ? "兑换{0}元宝" : "Amount to {0} yuan", m_cashCount);
            
        }

        void UpdateCashcount()
        {
            transform.Find("cashItem/input").GetComponent<InputField>().text = m_cashCount.ToString();
        }

        void HandleNetMsg_Excharge(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            Debug.Log(obj);
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                gameObject.SetActive(false);
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "发送成功（兑奖申请正在处理，稍后将提示您处理结果）" : "Send Successful！Your exchange application is being processed,later we will prompt you the result");
            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                Debug.Log(msgcode);
                switch (msgcode)
                {
                    case 2:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号或密码错误" : "Account name or password error, please enter again");
                        break;
                    case 3:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码错误" : "password error, please enter again");
                        break;
                    case 6:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被冻结" : "This account has been frozen");
                        break;
                    case 7:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "系统维护" : "Server maintenance");
                        break;
                    case 11:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请勿重复申请" : "Please do not repeat the application");
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "兑奖失败" : "exchange failed");
                        break;
                }
            }
        }
    }
}