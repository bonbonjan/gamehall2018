﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall;
public class QuestionsViewController : MonoBehaviour
{
    public static QuestionsViewController instance;
    private QuestionInfo[] questions;
    public  int  i_security ;
    [SerializeField]
    private  Sprite[] _arrLock;
    [SerializeField]
    private Image img_Lock;
    [SerializeField]
    private Text text_Prompt;
    [SerializeField]
    private Text text_setHint;
    [SerializeField]
    private GameObject go_set;

    [SerializeField]
    private Text m_tempText1;
    [SerializeField]
    private Text m_tempText2;
    [SerializeField]
    private Text m_Q1;
    [SerializeField]
    private Text m_Q2;
    [SerializeField]
    private InputField m_answer1;
    [SerializeField]
    private InputField m_answer2;
    [SerializeField]
    private GameObject m_questionPanel;
    [SerializeField]
    private Text text_Hint;
    [SerializeField]
    private GameObject security;
    [SerializeField]
    private GameObject m_dropList1;
    [SerializeField]
    private GameObject m_dropList2;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
       
    }

    void Start ()
    {
        i_security = GVars.user.security;
        NetManager.GetInstance().RegisterHandler("SecuritySet", Handle_Questions);
        init();
	
	}
	
	void Update () 
    {
	
	}
    public void init()
    {
        i_security = GVars.user.security;
        Debug.Log("set: "+i_security);
        //questions = new QuestionInfo[2];
        _ShowSafetyPanelByState(i_security);
        
    }

    public void Send_Questions(Dictionary<int ,object> dic)
    {
        object[] args=new object[]{dic};
        NetManager.GetInstance().Send("gcsecurityService/SecuritySet", args);
        Debug.Log("Send_Questions");
 
    }

    public void Handle_Questions(object[] args)
    {
        bool bsuccess = false;
        string msg = null;
        Dictionary<string, object> dic = new Dictionary<string, object>();
        dic = args[0] as Dictionary<string, object>;
        bsuccess = (bool)dic["success"];
        Debug.Log("success:" + bsuccess);
        if (bsuccess)
        {
            i_security = 1;
            GVars.user.security = 1;
            AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密保问题设置成功" : "Setting Success", false, () => {
                m_dropList1.transform.Find("choose").GetComponent<ChooseController>().ResetQuestionObject();
                m_dropList2.transform.Find("choose").GetComponent<ChooseController>().ResetQuestionObject();
            });
            _UpdateSafePanelState();

            m_questionPanel.SetActive(false);
        } 
        else
        {
            msg = (string)dic["msg"];
            AlertDialog.GetInstance().ShowDialog(msg);
        }
    }

    public void OnBtnClick_Sure()
    {
        QuestionInfo info1 = new QuestionInfo();
        QuestionInfo info2 = new QuestionInfo();

        info1.ID = m_dropList1.transform.Find("choose").GetComponent<ChooseController>().currentIndex;
        info1.Question = m_Q1.text;
        info1.Answer = m_answer1.text;
        info2.ID = m_dropList2.transform.Find("choose").GetComponent<ChooseController>().currentIndex;
        info2.Question = m_Q2.text;
        info2.Answer = m_answer2.text;
        Dictionary<int, object> dic = new Dictionary<int, object>();
        dic.Add(1,info1);
        dic.Add(2, info2);

        Debug.Log("info1.ID: " + info1.ID);
        Debug.Log("info2.ID: " + info2.ID);

        if(info1.Question =="" || info2.Question =="")
        {
            AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请选择密保问题" : "Please choose the security question");
            return;
        }

        //输入内容检测
        if (info1.Question == info2.Question)
        {
            AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密保问题不能相同" : "The security question cannot be the same");
        }
        else if (info1.Answer == ""||info2.Answer=="")
        {
            AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "答案格式错误" : "Answer format error");
        }
        else if (info1.Answer.Length >20 || info2.Answer.Length>20)
        {
            AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "答案格式错误" : "Answer format error");
        }
        else
        {
            Send_Questions(dic);
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            Debug.Log("问题ID：" + info1.ID + "问题：" + info1.Question + " 答案： " + info1.Answer);
            Debug.Log("问题ID：" + info2.ID + "问题：" + info1.Question + " 答案： " + info2.Answer);
        }       
    }

    private void _ShowSafetyPanelByState(int  set)
    {
        if (set==1)
        {
            img_Lock.sprite = _arrLock[1];
            text_Prompt.text = GVars.language == "zh" ? "已设置" : "HaveSet";
            text_Prompt.font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
           // text_setHint.text = GVars.language == "zh" ? "修改密保" : "Modify Security Question";
            GameObject hint_temp = Instantiate(text_Hint,go_set.transform.position,text_Hint.transform.localRotation).gameObject;
          //  hint_temp.transform.parent = this.transform;
            go_set.gameObject.SetActive(false);
            text_Hint.transform.localPosition = new Vector3(190, -9, 0);
            //text_Hint.gameObject.SetActive(false);
        }
        else
        {
            img_Lock.sprite = _arrLock[0];
            text_Prompt.text = GVars.language == "zh" ? "未设置" : "NotSet";
            text_Prompt.font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            text_setHint.text = GVars.language == "zh" ? "设置密保" : "Set Security Question";
            text_setHint.fontSize = GVars.language == "zh" ? 36 : 20;
            text_setHint.font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
        }
    }

    public void ResetSafePanel()
    {
        img_Lock.sprite = _arrLock[0];
        text_Prompt.text = GVars.language == "zh" ? "未设置" : "NotSet";
        text_Prompt.font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
        text_setHint.text = GVars.language == "zh" ? "设置密保" : "Set Security Question";
        text_setHint.fontSize = GVars.language == "zh" ? 36 : 20;
        text_setHint.font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
        go_set.gameObject.SetActive(true);
        text_Hint.transform.localPosition = new Vector3(190, 60, 0);
    }

    private void _UpdateSafePanelState()
    {
        if (i_security == 1)
        {
            img_Lock.sprite = _arrLock[1];
            text_Prompt.text = GVars.language == "zh" ? "已设置" : "HaveSet";
            text_Prompt.font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            GameObject hint_temp = Instantiate(text_Hint, go_set.transform.position, text_Hint.transform.localRotation).gameObject;
           
            go_set.gameObject.SetActive(false);
            text_Hint.transform.localPosition = new Vector3(190,-9,0);
            //text_Hint.gameObject.SetActive(false);
            //hint_temp.transform.parent = security.transform;
        }
    }

    public void OnBtnClick_DropList(int index)
    {
        if (index == 1)
        {
            m_dropList1.SetActive(true);
            m_dropList2.SetActive(false);
        }
        if (index == 2)
        {
            m_dropList1.SetActive(false);
            m_dropList2.SetActive(true);
        }
    }
}
