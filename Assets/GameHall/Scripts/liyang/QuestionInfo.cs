﻿using UnityEngine;
using System.Collections;

public class QuestionInfo 
{
    public int ID;
    public string Question;
    public string Answer;
}
