﻿/*
 *  author: liyang&jiangwen
 *  creation date: 2016/3/28
 *  modified by: jiangwen
 *  modified date: 2016/5/13
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using WMCY.GameHall.Net;
using WMCY.GameHall;
public class UserInfoViewController : MonoBehaviour
{
    private static UserInfoViewController instance;

    public static UserInfoViewController Get()
    {
        return instance;
    }

    public GameObject[] chooseText;
    [SerializeField]
    private InputField input_nickName;
    [SerializeField]
    private Sprite[] spr_icons;
    [SerializeField]
    private Text text_accont;
    [SerializeField]
    private Text text_level;
    [SerializeField]
    private Toggle[] toggle_Sexs;
    [SerializeField]
    private Button btn_icon;
    [SerializeField]
    private Text text_Gold;
    [SerializeField]
    private Text text_ExpCoin;
    [SerializeField]
    private InputField m_OldPassword;
    [SerializeField]
    private InputField m_newPassword;
    [SerializeField]
    private InputField m_againPassword;
    private string m_sex = null;
    // private string _strNick;
    private int m_iIconIndex;
    private int m_tempindex = 9;
    private string m_tempsex = null;
    private string m_tempname = "";
    private int m_userType;
    private string m_newPW;
    private string m_oldPW;
    private Image img_icon;
    [SerializeField]
    bool isFaker = false;
    [SerializeField]
    UserIconDataConfig m_userIconDataConfig;
    public System.Action onUserInfoChanged;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        NetManager.GetInstance().RegisterHandler("updateUserInfo", HandleNetMsg_UpdateUserInfo);
        NetManager.GetInstance().RegisterHandler("updateUserPwd", HandleNetMsg_UpdatePassword);
    }

    void Start()
    {
        AppManager.GetInstance().Register(UIGameMsgType.UINotify_Reset_UserInfoPanel,this, ResetUserInfoPanel);
        AppManager.GetInstance().Register(UIGameMsgType.UINotify_Fresh_UserInfoPanel, this, FreshUI);
        FreshUI(1);
    }

    public void FreshUI(object obj)
    {
        Debug.Log("FreshUIFreshUI: " + obj);
        init();
        QuestionsViewController.instance.init();
    }

    private void init()
    {
        img_icon = btn_icon.GetComponent<Image>();
        _InitUserInfo();
    }

    private void _InitUserInfo()
    {
        if (isFaker)
        {
            text_ExpCoin.text = "88888";
            text_Gold.text = "66666";
            text_accont.text = "test";
            m_sex = "女";
            input_nickName.text = "小贺子";
            m_userType = 0;
            text_level.text = "LV.1 新手 ";
            m_iIconIndex = 0;
            if (m_sex == "男")
            {
                toggle_Sexs[0].isOn = true;
                toggle_Sexs[1].isOn = false;
            }
            else
            {
                toggle_Sexs[1].isOn = true;
                toggle_Sexs[0].isOn = false;
            }
        }
        else
        {
            text_ExpCoin.text = GVars.user.expeGold.ToString();
            text_Gold.text = GVars.user.gameGold.ToString();
            text_accont.text = GVars.accountName;
            if (GVars.user.sex == "男") m_sex = "男";
            else m_sex = "女";
            input_nickName.text = GVars.user.nickname;

            m_userType = GVars.user.type;
            text_level.text = "LV." + GVars.user.level + " " + Utils.GetLevelName(GVars.user.level);

            toggle_Sexs[0].isOn = false;
            toggle_Sexs[1].isOn = false;

            if (GVars.user.sex == "男")
            {
                toggle_Sexs[0].isOn = true;
                toggle_Sexs[1].isOn = false;

                Debug.Log("男 GVars.user.sex: " + GVars.user.sex);
            }
            else 
            {
                toggle_Sexs[0].isOn = false;
                toggle_Sexs[1].isOn = true;
                
                Debug.Log("女 GVars.user.sex: " + GVars.user.sex);
            }
            m_iIconIndex = GVars.user.photoId;
            Debug.Log(LogHelper.Blue("m_iIconIndex1102: " + m_iIconIndex));
            //img_icon.sprite = spr_icons[m_iIconIndex];
            img_icon.sprite = m_userIconDataConfig.list[m_iIconIndex ].sprite;
            Transform iconsParent = transform.Find("PanelGroup/PersonalData/Icons/IconPanel/bg/Image");
            for (int i = 0; i < 4; i++)
            {
                iconsParent.Find("btn_" + i + "/Image").GetComponent<Image>().sprite = m_userIconDataConfig.list[i].sprite;
                if (i == m_iIconIndex)
                {
                    iconsParent.Find("btn_" + i + "/Text").gameObject.SetActive(true);
                }
            }
            Debug.Log(" m_iIconIndex: " + m_iIconIndex);
        }

    }
    # region Net Method
    public void RefreshLevelText()
    {
        text_level.text = "LV." + GVars.user.level + " " + Utils.GetLevelName(GVars.user.level);
    }

    public void RefreshGoldAndExp()
    {
        text_ExpCoin.text = GVars.user.expeGold.ToString();
        text_Gold.text = GVars.user.gameGold.ToString();
    }

    public void Send_UserInfo(string nickname, string sex, int photoId)
    {
        object[] args = new object[] { nickname, sex, photoId };
        NetManager.GetInstance().Send("gcuserService/updateUserInfo", args);
    }

    public void Send_ChangePassword(string _strOld, string _strNew)
    {
        object[] args = new object[] { _strOld, _strNew };
        NetManager.GetInstance().Send("gcuserService/updateUserPwd", args);
        Debug.Log("Send_ChangePassword: " + "old: " + _strOld + " new: " + _strNew);

    }

    public void HandleNetMsg_UpdateUserInfo(object[] args)
    {
        Debug.Log("HandleNetMsg__UpdateUserInfo");
        bool bsuccess = false; //是否修改成功
        //string errorCode = null;// 如果否，原因。
        Dictionary<string, object> dic = new Dictionary<string, object>();
        dic = args[0] as Dictionary<string, object>;
        bsuccess = (bool)dic["success"];
        // errorCode = (string)dic["errorCode"];
        Debug.Log("success:" + bsuccess);
        if (bsuccess)
        {
            m_tempindex = m_iIconIndex;
            m_tempname = input_nickName.text;
            m_tempsex = m_sex;
            
            AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "修改成功" : "Change sucess", false, () => { ResetUserInfoPanel(true); });
            GVars.user.photoId = m_iIconIndex;
            GVars.user.nickname = input_nickName.text;
            GVars.user.sex = m_sex;
            AppManager.Get().Notify(UIGameMsgType.UINotify_Fresh_UserInfo);
        }
        // else AlertDialog.GetInstance().ShowDialog("错误代码：" + errorCode);
    }

    private void ResetUserInfoPanel(object obj)
    {
        Debug.Log("ResetUserInfoPanel: "+obj);

        foreach (GameObject g in chooseText)
        {
            g.SetActive(false);
        }

        bool bo = (bool)obj;
        m_OldPassword.text = "";
        m_newPassword.text = "";
        m_againPassword.text = "";
        if (bo) return;
        QuestionsViewController.instance.ResetSafePanel();       
    }

    public void HandleNetMsg_UpdatePassword(object[] args)
    {
        Debug.Log("HandleNetMsg_UpdatePassword");

        bool bsuccess = false;
        string msg = null;
        Dictionary<string, object> dic = new Dictionary<string, object>();
        dic = args[0] as Dictionary<string, object>;
        bsuccess = (bool)dic["success"];
        Debug.Log("success:" + bsuccess);
        if (bsuccess) AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码修改成功" : "Password Change sucess", false, () => { ResetUserInfoPanel(true); });
        else
        {
            msg = (string)dic["msg"];
            AlertDialog.GetInstance().ShowDialog(msg);
        }
    }

    # endregion

    # region UI Method
    public void OnYoukeForbiddenClick(int type)
    {
        Debug.Log("OnYoukeForbiddenClick");
        if (m_userType == 1)
        {
            if (type == 0)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游客账号无法修改密码" : "Only Vip Can Change Password", false, ResetUserPanelGroup);
            }
            if (type == 1)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游客账号无法进行设置" : "Guest account can not be set", false, ResetUserPanelGroup);
            }
        }
        else 
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
        };
    }

    public void OnPlayMusic()
    {
        SoundManager.Get().PlaySound(SoundType.Common_Click);
    }

    public void ResetUserPanelGroup()
    {
        SoundManager.Get().PlaySound(SoundType.Common_Click);
        Debug.Log("ResetUserPanelGroup");
        this.transform.Find("PanelGroup/UserInfoPanel").GetComponent<Toggle>().isOn = true;
        this.transform.Find("PanelGroup/PasswordPanel").GetComponent<Toggle>().isOn = false;
        this.transform.Find("PanelGroup/SafetyPanel").GetComponent<Toggle>().isOn = false;
        this.transform.Find("PanelGroup/PersonalData").gameObject.SetActive(true);
    }

    public void ChangeSex()
    {
        SoundManager.Get().PlaySound(SoundType.Common_Click);
        if (toggle_Sexs[1].isOn == true)
        {
            m_sex = "女";
        }
        else
        {
            m_sex = "男";
        }
        Debug.Log("changeSex: " + m_sex);

    }

    public void ChangeNickName()
    {
        // 检测输入格式
        //如果输入格式正确
        //_strNick = nickName.text;
        //Debug.Log("changeNickName："+_strNick);
    }

    public void ChangeIcon(IconButtonsInfo iconinfo)
    {
        SoundManager.Get().PlaySound(SoundType.Common_Click);
        img_icon.sprite = iconinfo.transform.Find("Image").GetComponent<Image>().sprite;
        //img_icon.sprite = iconinfo.gameObject.GetComponent<Image>().sprite;
        m_iIconIndex = iconinfo.index;
        foreach (GameObject g in chooseText)
        {
            g.SetActive(false);
        }
        chooseText[m_iIconIndex].SetActive(true);
        Debug.Log("changeIcon: " + m_iIconIndex);
    }

    public void UpdateGoldNum()
    {
        text_ExpCoin.text = GVars.user.expeGold.ToString();
        text_Gold.text = GVars.user.gameGold.ToString();
    }

    public void ChangePassword()
    {
        m_newPW = m_newPassword.text;
        m_oldPW = m_OldPassword.text;
    }

    public void OnPanelClose()
    {
        ResetUserPanelGroup();

        bool rightFormat = InputCheck.CheckChangeNickName(input_nickName.text, () => {
            if (input_nickName)
                input_nickName.text = GVars.user.nickname;
        });

        if (rightFormat)
        {
            this.gameObject.SetActive(false);
            Debug.Log(string.Format("m_iIconIndex: {0}, m_tempindex: {1}, photoId: {2}", m_iIconIndex, m_tempindex, GVars.user.photoId));
            Debug.Log("nickname: " + input_nickName.text + " usernick: " + m_tempname + " index: " + m_iIconIndex + "photoid: " + m_tempindex);
            if (input_nickName.text == m_tempname && m_iIconIndex == m_tempindex && m_sex == m_tempsex) return;                                       //没有作出修改
            else if (input_nickName.text == GVars.user.nickname && m_iIconIndex == GVars.user.photoId && m_sex == GVars.user.sex) return;
            else
            {
                Send_UserInfo(input_nickName.text, m_sex, m_iIconIndex+1);
                GVars.user.photoId = m_iIconIndex ;
                GVars.user.nickname = input_nickName.text;
                GVars.user.sex = m_sex;
                Debug.Log("UIGameMsgType.UINotify_Fresh_UserInfo");
                AppManager.Get().Notify(UIGameMsgType.UINotify_Fresh_UserInfo);
            }

        }

        
    }

    public void OnBtnClick_PasswordOK() //步骤 1.格式没错 2.新密码两次输入一样  3.服务器检测成功  
    {

        bool rightFormat = false;
        rightFormat = InputCheck.CheckPassWord(m_newPassword.text);
        if (rightFormat == true)
        {
            ChangePassword();
            if (m_againPassword.text == m_newPW)
            {
                if (m_OldPassword.text == m_newPassword.text)
                {
                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "新旧密码重复，请重新输入" : "Repeat Password,Reset Please.");
                    return;
                }
                else
                {
                    Send_ChangePassword(m_oldPW, m_newPW);
                    SoundManager.Get().PlaySound(SoundType.Common_Click);
                }
            }
            else
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码输入不匹配" : "Two input is inconsistent");
            }
        }
        else {

            
        }

    }
    # endregion

}
