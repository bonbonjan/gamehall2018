﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using System;

namespace WMCY.GameHall
{
    public class RedBonus : MonoBehaviour
    {
        float time;

        // Use this for initialization
        void Start()
        {
        }

        public void clickRedBonus()
        {
            //if (GVars.lockSend) return;
            Debug.Log("********name********: " + transform.name);
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Click_RedBonus,Convert.ToInt32(transform.name));
            Destroy(this.gameObject);
        }

        void Update()
        {
            time = Time.deltaTime;
            if (transform.localPosition.y > -1650)
            {
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y - time * 150, 0);
            }
            else
            {
                transform.localPosition = new Vector3(UnityEngine.Random.Range(-590, 590), 535, 0);

            }

        }


    }

}
