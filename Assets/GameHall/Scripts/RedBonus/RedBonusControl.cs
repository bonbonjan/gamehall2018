﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using System;

namespace WMCY.GameHall
{
    public class RedBonusControl : MonoBehaviour
    {
        public GameObject redBonus;
        public static RedBonusControl _redBonusControl = null;
        int bonusCount;     //领取次数
        int bonusGold;
        bool hasInitialize = false;
        ArrayList redBonuses = new ArrayList();

        void Awake()
        {
            Application.targetFrameRate = 60;           //所有平台设置为60帧
            if (_redBonusControl == null)
            {
                _redBonusControl = this;
            }
            GameObject button = transform.Find("TestButton").gameObject;
            GameObject button2 = transform.Find("TestButton2").gameObject;
            button.GetComponent<Button>().onClick.AddListener(TestBonus);
            button2.GetComponent<Button>().onClick.AddListener(TestBonus2);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Click_RedBonus, this, ClickRedBonus);
        }

        void Start()
        {
            NetManager.GetInstance().RegisterHandler("getBonus", HandleNetMsg_GetBonus);
        }

        public void redBonusStart()
        {
            //transform.GetComponent<UIPanel>().enabled = true;
            // BackGround.SetActive(true);
            redBonusInitialize();
            //transform.parent.Find("Panel/LoadingPanel").gameObject.SetActive(false);
            //transform.parent.Find("Panel/LoginPanel").gameObject.SetActive(false);
            //transform.parent.Find("Panel/NetDateTip").gameObject.SetActive(false);
            //transform.parent.Find("Panel/OtherPanel").gameObject.SetActive(false);
            //transform.parent.Find("Panel/RegisterPanel").gameObject.SetActive(false);
            //transform.parent.Find("Panel/TipPanel").gameObject.SetActive(false);
            //transform.parent.Find("Panel/SupplementPanel").gameObject.SetActive(false);
            //transform.parent.Find("Panel/GamePanel/MailPanel").gameObject.SetActive(false);
        }

        float randomNum = 300;

        void redBonusInitialize()
        {
            if (!hasInitialize)
            {
                bonusCount = 0;
                GameObject Collection = new GameObject();
                Collection.transform.parent = transform;
                Collection.name = "Collection";
                Collection.transform.localScale = new Vector3(1, 1, 1);
                Collection.transform.localPosition = new Vector3(0, 0, 0);
                for (int i = 0; i < 30; i++)
                {
                    GameObject _redBonus = GameObject.Instantiate(redBonus) as GameObject;
                    _redBonus.transform.parent = transform.Find("Collection");
                    _redBonus.transform.localScale = new Vector3(1, 1, 1);
                    _redBonus.name = Convert.ToString(i);
                    if (randomNum > 0)
                        randomNum = UnityEngine.Random.Range(-590, -1);
                    else
                        randomNum = UnityEngine.Random.Range(1, 590);
                    _redBonus.transform.localPosition = new Vector3(randomNum, 535 + i * 160, 0);
                    _redBonus.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, UnityEngine.Random.Range(65, -65)));
                    redBonuses.Add(_redBonus);
                }
                hasInitialize = true;
            }
        }

        void ClickRedBonus(object obj)
        {
            if (GVars.lockSend) return;
            if (bonusCount < 3)
            {
                //GameObject.Destroy((GameObject)redBonuses[(int)obj]);
                Debug.Log("getBonusgetBonusgetBonus");
                NetManager.GetInstance().Send("gcuserService/getBonus", new object[] { });
                GVars.lockSend = true;
            }
        }

        public void redBonusStop()
        {
            redBonusClose();
            transform.Find("RedBonus_No").gameObject.SetActive(false);
            transform.Find("RedBonus_Yes").gameObject.SetActive(false);
            transform.gameObject.SetActive(false);
            AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "红包雨已经下完" : "The red packet money rain has finished"); ;     //提示红包雨已经下完了
        }

        public void MaintenanceRedBonusClose()
        {
            redBonusClose();
            transform.Find("RedBonus_No").gameObject.SetActive(false);
            transform.Find("RedBonus_Yes").gameObject.SetActive(false);
            transform.gameObject.SetActive(false);
        }

        public void redBonusClose()
        {
            hasInitialize = false;
            for (int i = 0; i < redBonuses.Count; i ++ )
            {
                GameObject.Destroy((GameObject)redBonuses[i]);
            }
            redBonuses.Clear();
        }

        void TestBonus()
        {
            redBonusStart();
        }

        void TestBonus2()
        {
            redBonusClose();
        }

        IEnumerator waitForEffect()
        {
            yield return new WaitForSeconds(2.5f);
            transform.Find("RedBonus_Yes/gameGoldAdd").gameObject.SetActive(bonusGold > 0 ? true : false);
            transform.Find("RedBonus_Yes/gameGoldAdd").GetComponent<Text>().text = string.Format("+{0}{1}",bonusGold,GVars.language == "zh" ?"游戏币":"coins");
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_GoldAndLottery);
            //if (bonusGold==0)
            //{
            //    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "很遗憾，红包是空的" : "Sorry，the red is empty");
            //}
            if (bonusCount >= 3)
            {
                redBonusClose();
            }
            yield return new WaitForSeconds(2.5f);
            transform.Find("RedBonus_No").gameObject.SetActive(false);
            transform.Find("RedBonus_Yes").gameObject.SetActive(false);
            transform.Find("RedBonus_Yes/gameGoldAdd").gameObject.SetActive(false);
            if (bonusCount >= 3)
            {
                transform.gameObject.SetActive(false);
            }
        }

        void HandleNetMsg_GetBonus(object[] objs)
        {
            Debug.Log("##########bonusCount############: " + bonusCount);
            GVars.lockSend = false;
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            Debug.Log(obj);
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                bonusGold = Convert.ToInt32(obj["bonusGold"]);
                if (bonusGold==-1)
                {
                    MaintenanceRedBonusClose();
                    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "本次红包已被抢完，下次可不要错过哦！" : "The red is finished, don't miss it next time!");
                    return;
                }
                GVars.user.gameGold = Convert.ToInt32(obj["gameGold"]);
                transform.Find("RedBonus_Yes").gameObject.SetActive(bonusGold == 0 ? false : true);
                transform.Find("RedBonus_No").gameObject.SetActive(bonusGold == 0 ?  true: false);
                bonusCount++;
                StartCoroutine("waitForEffect");
            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                Debug.Log(msgcode);
                switch (msgcode)
                {
                    case 20:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "领红包超过3次，不能再领" : "Receive red packets more than 3 times,Not receive again");
                        redBonusClose();
                        transform.gameObject.SetActive(false);
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "领取失败" : "Receive failed");
                        break;
                }
            }
        }

    }

}
