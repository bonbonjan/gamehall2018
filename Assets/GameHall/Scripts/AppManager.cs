using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using System.Text.RegularExpressions;
using System.Net;

namespace WMCY.GameHall
{
    public delegate void NotifyCallBack(object obj);

    public struct NotifyObject
    {
        public UnityEngine.Object Instance;
        public NotifyCallBack CallBack;
    }

    public class AppManager : MB_Singleton<AppManager>
    {

        public bool devScene;
        [SerializeField]
        LoadingPanelController m_loadingPanel;
        [SerializeField]
        LoginPanelController m_loginPanel;
        [SerializeField]
        MainPanelController m_mainPanel;
        [SerializeField]
        bool m_isEnglish = false;
        [SerializeField]
        DataConfig config;
        bool m_lastIsEnglish = false;
        Transform m_xformCanvas;
        public bool localIP = false;
        bool m_firstAwake = true;
        public Font zh_font_title;
        public Font zh_font;
        public Font en_font_title;
        public Font en_font;
        public bool special = false;//标识特殊版本（如展会版）
        public static bool initNet = true;
        public bool isShowAnnouce = false;

        void Awake()
        {
            Debug.Log("AppManager:Awake");
            if (SceneManager.GetActiveScene().name.Contains("MainScene"))
            {
                if (Screen.orientation != ScreenOrientation.LandscapeLeft)
                {
                    var oldOrientation = Screen.orientation;
                    Screen.orientation = ScreenOrientation.LandscapeLeft;
                    Debug.LogFormat("change orientation from {0} to {1}", oldOrientation, Screen.orientation);
                }
            }
            if (m_firstAwake)
            {
                SetInstance(this);
                if (devScene)
                {
                    Debug.Log("AppManager> use <color=magenta>devScene</color> mode");
                    DataFaker.Init();
                }
                else
                {
                    Debug.Log(LogHelper.Magenta("AppManager> use <color=magenta>product</color> mode"));
                }
            }
            m_firstAwake = false;

            Screen.sleepTimeout = SleepTimeout.NeverSleep;//大厅常亮
            Debug.Log(LogHelper.Yellow("bundleID: " + Application.identifier));

            SafeTools.Init(config);
        }


        bool netDownHintLocker = false;//为true时，不显示断线重连提示

        public void ForbidNetDownHint()
        {
            Debug.Log("ForbidNetDownHint");
            netDownHintLocker = true;
        }

        void Start()
        {
            Init();
        }

        bool _hasInited = false;
        void Init()
        {
            if (_hasInited) return;
            _hasInited = true;
            Debug.Log(LogHelper.Blue("Application.systemLanguage: " + Application.systemLanguage));
            if (Application.systemLanguage == SystemLanguage.Chinese || Application.systemLanguage == SystemLanguage.ChineseSimplified || Application.systemLanguage == SystemLanguage.ChineseTraditional)
            {
                GVars.language = "zh";
            }
            else
            {
                Debug.Log(LogHelper.Magenta("language: " + Application.systemLanguage));
                GVars.language = "en";
            }
            RefreshLocalization();
            if (initNet)
                InitNet();
            _basicInit();
            if (!devScene) _initPanels();

            //NetManager.GetInstance().RegisterHandler("netDown", (args) =>
            //{
            //    if (args != null && (Exception)args[0] != null)
            //    {
            //        Debug.Log(args.Length);
            //        Debug.Log(args[0]);
            //        Debug.Log("" + args[0] == null);
            //    }
            //    netDownLocker = true;
            //    if (netDownLocker == true) return;

            //    AlertDialog.Get().ShowDialog("网络断开，退出重连", false, () =>
            //    {
            //        //SceneManager.LoadScene("GameHall");
            //        HideAllPanels();
            //        AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Reset_UserInfoPanel, false);
            //        NetManager.GetInstance().Disconnect();
            //        AppManager.Get().InitNet();
            //        AppManager.GetInstance().ShowLoginPanel();
            //        AppManager.GetInstance().Notify(UIGameMsgType.UINotify_FreshDealer, 0);
            //        netDownLocker = false;
            //    });
            //}
            //);

            NetManager.GetInstance().RegisterHandler("netDown", Handle_NetDown);
            NetManager.GetInstance().RegisterHandler("reLogin", HandleNetMsg_ReLogin);
            NetManager.GetInstance().RegisterHandler("international", HandleNetMsg_International);//
            NetManager.GetInstance().RegisterHandler("checkVersion", HandleNetMsg_CheckVersion);
            NetManager.GetInstance().RegisterHandler("quitToLogin", m_mainPanel.HandleNetMsg_QuitToLogin);
            m_loginPanel.Init();


        }

        public void ShowNotice()
        {
            m_loginPanel.ShowAnnouce();
        }
        public void ShowProperSecurity()
        {
            m_mainPanel.OnBtnClick_ProperSecurity();
        }
        //public void RefreshRedSign(bool isShow)
        //{
        //    m_mainPanel.RefreshProperSecurityRedsign(isShow);
        //}


        void OnValidate()
        {
            if (m_lastIsEnglish != m_isEnglish)
            {
                string lang = m_isEnglish ? "en" : "zh";
                GVars.language = lang;
                I2Localization.SetCurLanguage(lang);
                RefreshLocalization();
            }
            m_lastIsEnglish = m_isEnglish;
        }

        public void RefreshLocalization()
        {
            I2LocalizeText[] texts = FindObjectsOfType<I2LocalizeText>();
            foreach (I2LocalizeText text in texts)
            {
                text.Refresh();
            }

            I2LocalizeImage[] images = FindObjectsOfType<I2LocalizeImage>();
            foreach (I2LocalizeImage image in images)
            {
                image.Refresh();
            }
        }

        public bool hasLogined = false;
        public bool isConnecting = false;//是否在尝试连接中
        bool _isReconnecting = false;//是否重连中
        bool m_hasVersionChecked = false;
        private IEnumerator _connectAndLoginCoroutine(bool willLogin = false)
        {
            m_hasVersionChecked = false;
            isConnecting = true;
            Debug.Log(string.Format("_isReconnecting: {0}, isConnected: {1}, isReady: {2}, willLogin: {3}",
                _isReconnecting, NetManager.GetInstance().isConnected, NetManager.GetInstance().isReady, willLogin));

            if (!hasLogined)
            {
                //第一次登录，断线重连和超时检测更严格
                NetManager.GetInstance().connectMaxTimes = 10;
                NetManager.GetInstance().connectMaxTimeout = 5f;
            }

            if (!_isReconnecting)
            {
                InitData();
            }
            else
            {
                NetManager.GetInstance().Disconnect();
                yield return new WaitForSeconds(0.5f);
            }
            Debug.Log("GVars.IPAddress: " + GVars.IPAddress);
            NetManager.Get().autoReady = true;
            NetManager.GetInstance().Connect(GVars.IPAddress, GVars.IPPort);
            Debug.Log("wait connected");

            while (!NetManager.GetInstance().isConnected)
            {
                yield return null;
            }

            while (!NetManager.GetInstance().isReady)
            {
                yield return null;
            }

            if (!willLogin)
            {
                ReconnectSuccess();
                isConnecting = false;
                yield break;
            }
            float waitTime = _isReconnecting ? 0.2f : 0.2f;
            yield return new WaitForSeconds(waitTime);

            //NetManager.GetInstance().SendVersion();
            //while (!m_hasVersionChecked)
            //{
            //    yield return null;
            //}

            if (hasLogined)
            {
                Send_ReLogin();
            }
            else
            {
                //Send_Login();
                //if (!_isReconnecting) StartCoroutine(_loadingLoginTimeout());
                Send_Login(GVars.isTourist);
                yield return null;
            }
            if (!_isReconnecting) StartCoroutine(_loadingLoginTimeout());
            isConnecting = false;
            yield break;
        }

        public void Send_Login(bool isTourist=false)
        {
            if (!Can_Send()) return;
            GVars.lockSend = true;
            GVars.isTourist = isTourist;//默认非游客账号
            if (isTourist)
            {
                //游客
                GVars.isTourist = true;
                NetManager.Get().Send("gcuserService/visitorLogin", new object[] { GVars.username, GVars.nickname });
            }
            else
            {
                //非游客
                NetManager.Get().Send("gcuserService/login", new object[] { GVars.username, GVars.pwd });
            }

            if (!_isReconnecting) StartCoroutine(_loadingLoginTimeout());
        }

        public void Send_ReLogin()
        {
            NetManager.Get().Send("gcuserService/reLogin", new object[] { GVars.username, GVars.pwd });
        }

        void HandleNetMsg_ReLogin(object[] objs)
        {
            Debug.Log(LogHelper.NetHandle("HandleNetMsg_ReLogin"));
            GVars.lockSend = false;
            GVars.lockRelogin = false;

            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            bool result = (bool)obj["reLogin"];
            if (ReconnectHint.GetInstance() != null) ReconnectHint.GetInstance().Hide();
            if (result)
            {
                //添加user字段用于重练后刷新
                GVars.user = User.CreateWithDic(obj["user"] as Dictionary<string, object>);
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_GoldAndLottery);//刷新金币和体验币
                AppManager.Get().Notify(UIGameMsgType.UINotify_Fresh_UserInfo);//刷新等级
                
                GVars.lockReconnect = false;
                GVars.lockQuit = false;
                netDownHintLocker = false;
                Debug.Log(LogHelper.Magenta("relogin success"));
            }
            else
            {
                GVars.lockReconnect = true;
                GVars.lockQuit = true;
                //netDownHintLocker = true;
                Debug.Log(LogHelper.Magenta("relogin failure"));

                if (m_xformCanvas.Find("RedBonusRain").gameObject.activeSelf)
                {
                    RedBonusControl._redBonusControl.MaintenanceRedBonusClose();
                }

                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "断线重连失败" : "Reconnection failure", false, () =>
                {
                    SceneManager.LoadSceneAsync(0);
                });
            }
        }

        public void Handle_RecLoginOrVisitorLogin()
        {

            hasLogined = true;
            GVars.lockSend = false;
            _isReconnecting = false;
            GVars.lockRelogin = false;//清除重连锁定

            //AppManager.Get().hasLogined = true;
            //不是第一次登录，断线重连和超时检测宽松
            NetManager.GetInstance().connectMaxTimes = 50;
            NetManager.GetInstance().connectMaxTimeout = 30f;
#if UNITY_IOS && !UNITY_EDITOR
        NetManager.GetInstance().connectMaxTimes = 25;
        NetManager.GetInstance().connectMaxTimeout = 20f;
#endif
#if UNITY_ANDROID && !UNITY_EDITOR
        NetManager.GetInstance().connectMaxTimes = 30;
        NetManager.GetInstance().connectMaxTimeout = 20f;
#endif
            StopCoLogin();
            if (ReconnectHint.GetInstance() != null) ReconnectHint.GetInstance().Hide();
        }

        void StopCoLogin()
        {
            if (_coLogin != null)
            {
                StopCoroutine(_coLogin);
                _coLogin = null;
                isConnecting = false;
            }

        }

        public void Handle_LoginFailed()
        {
            m_loadingPanel.gameObject.SetActive(false);
            m_loginPanel.gameObject.SetActive(true);
            hasLogined = false;
        }

        public bool Can_Send()
        {
            bool ret = !GVars.lockSend && NetManager.Get().isReady;

            return ret;
        }

        void InitData()
        {           
            if (localIP)
            {
                GVars.IPAddress = "192.168.1.63";//范广凯
                GVars.IPAddress = "192.168.1.64";//沈浩
                GVars.IPAddress = "192.168.1.62";//张曼曼
                //GVars.IPAddress = "192.168.1.127";//郑磊
                //GVars.IPAddress = "192.168.1.63";//马缔
                //GVars.IPAddress = "192.168.1.126";//张建
                GVars.IPAddress = "192.168.1.60";//胡志强
                GVars.IPPort = 8877;
            }
            else
            {
                //GVars.IPAddress = "121.40.187.208";
                //GVars.IPAddress = "118.178.138.253";
                //GVars.IPAddress = "121.40.187.208";
                //GVars.IPAddress = DoGetHostAddresses("edfdi.swncd88.vip");
                //GVars.IPAddress = "47.88.148.142";
                //GVars.IPAddress = "118.178.134.108";
                //Debug.Log(LogHelper.Magenta("封测版本"));
                //GVars.IPAddress = "114.55.179.109";
                try
                {
                    GVars.IPAddress = SafeTools.GetIp();
                    GVars.IPAddress = DoGetHostAddresses(GVars.IPAddress);
                    GVars.IPPort = 8877;
                }
                catch(Exception e)
                {
                    Debug.LogError(e);
                }
                if (special)
                {
                    GVars.IPAddress = "";
                    Debug.Log("GVars.IPAddress: " + GVars.IPAddress);
                    Debug.Log(LogHelper.Magenta("封测版本"));
                }
            }

            GVars.IPAddress_Game = SafeTools.GetIp(); 
        }

        public void InitNet(bool switchUser = false)
        {
            try
            {
                netDownHintLocker = switchUser;
                InitData();
                NetManager.GetInstance().Connect(GVars.IPAddress, GVars.IPPort);
            }
            catch
            { }
            _coLogin = StartCoroutine(CheckStartupParams());
        }

        void _initPanels()
        {
            Debug.Log("_initPanels");
            //string rootName = "Canvas/Root";
            //rootName = Application.loadedLevelName;
            //string sceneName = SceneManager.GetActiveScene().name;
            //string rootName = (sceneName == "MainScene_bug" || sceneName == "MainScene_old" || sceneName == "MainScene_test") ? "Canvas" : "Canvas/Root";
            //Transform xformCanvas = GameObject.Find(rootName).transform;
            //Transform xformCanvas = GameObject.Find("Canvas/Root")!=null?GameObject.Find("Canvas/Root").transform:GameObject.Find("Canvas").transform;
            //AlertDialog.SetInstanceByGameObject(xformCanvas.Find("AlertDialog").gameObject).PreInit();


            m_loadingPanel.gameObject.SetActive(true);
            m_loginPanel.gameObject.SetActive(false);
            m_mainPanel.gameObject.SetActive(false);
            if (AlertDialog.GetInstance() != null) AlertDialog.GetInstance().gameObject.SetActive(false);

        }

        public void HideAllPanels()
        {
            Debug.Log("HideAllPanels");
            string[] panels = {
                                  "LoadingPanel",
                                  "MainPanel",
                                  "AlertDialog",
                                  "CashPanel",
                                  "CashOperatePanel",
                                  "LotteryCityPanel",
                                  "RegisterPanel",
                                  "UserSettingsPanel",
                                  "SafeBoxPanel",
                                  "SafeBoxPwdSetPanel",
                                  "SafeBoxPwdCheckPanel",
                                  "FakePanel",
                                  "StorePanel",
                                  "RankPanel",
                                  "RechargePanel",
                                  "AwardPanel",
                                  "UserCompletePanel",
                                  "RecoverPasswordPanel",
                                  "CampaignPanel",
                                  "SettingPanel",
                                  "AnnouncementPanel",
                                  "SharePanel",
                                  "MailPanel",
                                  "NoticeTip",
                                  "RedBonusRain"
                              };
            foreach (var panel in panels)
            {
                //var target = m_xformCanvas.Find("Root/" + panel);
                var target = m_xformCanvas.Find(panel);
                if (target != null)
                {
                    target.gameObject.SetActive(false);
                }
                else
                {
                    Debug.Log(panel + " not find");
                }

            }
        }

        void _basicInit()
        {
            Debug.Log("_basicInit");
            m_xformCanvas = GameObject.Find("Canvas/Root") != null ? GameObject.Find("Canvas/Root").transform : GameObject.Find("Canvas").transform;
            Debug.Log(m_xformCanvas);
            AlertDialog.SetInstanceByGameObject(m_xformCanvas.Find("AlertDialog").gameObject).PreInit();
            GameObject noticeTip = m_xformCanvas.Find("NoticeTip").gameObject;
            noticeTip.SetActive(false);
            noticeTip.GetComponent<NoticeTipController>().PreInit();
            ReconnectHint.SetInstanceByGameObject(m_xformCanvas.Find("ReconnectHint").gameObject).PreInit();
            if (ReconnectHint.GetInstance() != null) ReconnectHint.GetInstance().Hide();
            NetManager.GetInstance().RegisterHandler("scrollMessage", noticeTip.GetComponent<NoticeTipController>().HandleNetMsg_Notice);
            GameObject AnnouncementPanel = m_xformCanvas.Find("AnnouncementPanel").gameObject;
            NetManager.GetInstance().RegisterHandler("getNotice", AnnouncementPanel.GetComponent<AnnouncePanelController>().HandleNetMsg_Announce);

            NetManager.GetInstance().RegisterHandler("timeout", (args) =>
            {
                Debug.Log(LogHelper.Magenta("timeout>  id: " + (int)args[0]));
            });
            NetManager.GetInstance().RegisterHandler("heart timeout", (args) =>
            {
                Debug.Log(LogHelper.Magenta("heart timeout"));
                Handle_NetDown(new object[] { new Exception("heart timeout") });
            });
        }

        void Update()
        {
            NetManager.GetInstance().connectTimeCount += Time.deltaTime;
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Alpha1) && Input.GetKey(KeyCode.LeftShift))
            {
                GetPanel("NoticeTip").GetComponent<NoticeTipController>().AddMessage("asdfasfdd萨法十分发生大幅杀跌发送旅客放假了思考的房间里看书记录卡司法解释了的空间123");
                Debug.Log(LogHelper.Magenta("AddMessage: "));
            }
            if (Input.GetKeyDown(KeyCode.N) && Input.GetKey(KeyCode.LeftShift))
            {
                //调试断网
                NetManager.GetInstance().OnDestroy();
                Debug.Log(LogHelper.Magenta("KeyCode.N@ " + Time.time));
            }
            if (Input.GetKeyDown(KeyCode.S) && Input.GetKey(KeyCode.LeftShift))
            {
                //调试丢弃发送报文
                NetManager.GetInstance().dropSend = !NetManager.GetInstance().dropSend;
                Debug.Log(LogHelper.Magenta("dropSend: " + (NetManager.GetInstance().dropSend ? "On" : "Off")));
            }
            if (Input.GetKeyDown(KeyCode.D) && Input.GetKey(KeyCode.LeftShift))
            {
                //调试丢弃接收报文
                NetManager.GetInstance().dropDispatch = !NetManager.GetInstance().dropDispatch;
                Debug.Log(LogHelper.Magenta("dropDispatch: " + (NetManager.GetInstance().dropDispatch ? "On" : "Off")));
            }

#endif
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                QuitApp();
            }
        }

        public void ShowLoginPanel()
        {
            m_loginPanel.gameObject.SetActive(true);
            SoundManager.Get().PlaySound(SoundType.BG_First);
        }

        public void ShowMainPanel()
        {
            Debug.Log("ShowMainPanel");
            m_mainPanel.gameObject.SetActive(true);
            m_mainPanel.Init();
        }

        private Dictionary<UIGameMsgType, HashSet<NotifyObject>> m_notifys
        = new Dictionary<UIGameMsgType, HashSet<NotifyObject>>();
        /// <summary>
        /// 注册消息;
        /// </summary>
        /// <param name="type">消息类型;</param>
        /// <param name="obj">注册对象;</param>
        /// <param name="callBack">回调函数;</param>
        /// <param name="times">注册次数;</param>
        public void Register(UIGameMsgType type, UnityEngine.Object obj, NotifyCallBack callBack, int times = 0)
        {
            if (!m_notifys.ContainsKey(type))
            {
                m_notifys[type] = new HashSet<NotifyObject>();
            }

            if (times > 0)
            {
                HashSet<NotifyObject> list = m_notifys[type];
                if (list.Count >= times)
                {
                    return;
                }
            }
            NotifyObject notify = new NotifyObject();
            notify.Instance = obj;
            notify.CallBack = callBack;
            m_notifys[type].Add(notify);
        }

        /// <summary>
        /// 发射消息;
        /// </summary>
        /// <param name="Type">消息类型;</param>
        /// <param name="obj">参数;</param>
        public void Notify(UIGameMsgType Type, object obj = null)
        {
            ClearInvalidNOtify();
            if (!m_notifys.ContainsKey(Type))
            {
                return;
            }
            HashSet<NotifyObject> list = m_notifys[Type];
            foreach (NotifyObject notify in list)
            {
                if (null != notify.Instance)
                {
                    notify.CallBack(obj);
                }
            }
        }

        /// <summary>
        /// 清除消息;
        /// </summary>
        private void ClearInvalidNOtify()
        {
            foreach (var pair in m_notifys)
            {
                HashSet<NotifyObject> set = pair.Value;
                HashSet<NotifyObject> intersectSet = null;
                foreach (NotifyObject notify in set)
                {
                    if (null == notify.Instance)
                    {
                        if (null == intersectSet)
                        {
                            intersectSet = new HashSet<NotifyObject>();
                        }
                        intersectSet.Add(notify);
                    }
                }
                if (null != intersectSet)
                {
                    set.ExceptWith(intersectSet);
                }
            }
        }

        public GameObject GetPanel(string panelName)
        {
            if (m_xformCanvas == null)
            {
                m_xformCanvas = GameObject.Find("Canvas/Root") != null ? GameObject.Find("Canvas/Root").transform : GameObject.Find("Canvas").transform;
            }

            return m_xformCanvas.Find(panelName).gameObject;
        }

        public void QuitApp()
        {
            AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "是否退出大厅" : "Quit the gaming center？", true, () =>
                  {
                      NetManager.Get().Disconnect();
#if !UNITY_EDITOR
                Application.Quit();
#endif
#if UNITY_EDITOR
                      UnityEditor.EditorApplication.isPlaying = false;
#endif
                  });
        }

        Coroutine _coLogin = null;
        public void Handle_NetDown(object[] args)
        {
#if UNITY_EDITOR
            Debug.Log(LogHelper.Magenta("Handle_NetDown@: " + Time.time));
            //Debug.Break();
#endif
            Debug.Log(string.Format("isReady:{0}, lockSend:{1}, lockQuit:{2}, lockReconnect:{3}, netDownLocker:{4}, connectCount: {5}",
    NetManager.GetInstance().isReady, GVars.lockSend, GVars.lockQuit, GVars.lockReconnect, netDownHintLocker, NetManager.GetInstance().connectCount));

            isConnecting = false;
            if (NetManager.GetInstance().isReady)
            {
                if (ReconnectHint.GetInstance() != null) ReconnectHint.GetInstance().Hide();
                Debug.Log("isReady");
                return;
            }
            if (GVars.lockQuit || GVars.lockReconnect)
            {
                Debug.Log("locked");
                return;
            }
            _isReconnecting = true;

            Exception ex = null;
            if (args != null)
                ex = args[0] as Exception;
            Debug.Log(ex);
            if (Application.isEditor && false)
            {
                string content = string.Format(GVars.language == "zh" ? "网络连接已断开，错误代码为 [{0}]" : "Net Error [{0}]", ex == null ? "9999" : ex.Message);
                //LockManager.Lock("HandleNetDown");
                AlertDialog.GetInstance().ShowDialog(content, false, () =>
                {
                    //QuitApp();
                    QuitToLogin();
                });
            }

            StopCoLogin();
            if ((NetManager.GetInstance().connectCount > NetManager.GetInstance().connectMaxTimes && NetManager.GetInstance().connectTimeCount > NetManager.GetInstance().connectMaxTimeout / 2)
    || NetManager.GetInstance().connectTimeCount > NetManager.GetInstance().connectMaxTimeout)
            {
                //重连超时
                _timeoutQuit();
            }
            else
            {
                //允许重连
                Debug.Log(string.Format("lockRelogin: {0}, hasLogined: {1}, fromGame: {2}, fromLua: {3}",
                    GVars.lockRelogin, hasLogined, GVars.isStartedFromGame, GVars.isStartedFromLuaGame));
                _coLogin = StartCoroutine(_connectAndLoginCoroutine(!GVars.lockRelogin && (hasLogined || GVars.isStartedFromGame || GVars.isStartedFromLuaGame)));
                //Send_ReLogin();
                if (ReconnectHint.GetInstance() != null && !netDownHintLocker) ReconnectHint.GetInstance().Show();
            }
        }

        public bool CheckStartConnectAndLogin(bool willLogin = false)
        {
            Debug.Log("CheckStartConnectAndLogin");
            bool ret = false;
            Debug.Log(string.Format("isConnecting: {0}, lockRelogin: {1}, willLogin: {2}, netIsReady: {3}", isConnecting, GVars.lockRelogin, willLogin, NetManager.Get().isReady));
            GVars.lockQuit = false;
            GVars.lockReconnect = false;
            GVars.lockSend = false;
            if (!NetManager.Get().isReady)
            {
                ret = true;
                if (!isConnecting)
                    _coLogin = StartCoroutine(_connectAndLoginCoroutine(!GVars.lockRelogin && willLogin));
                return ret;
            }
            return ret;
        }

        void ReconnectSuccess()
        {
            Debug.Log("ReconnectSuccess");
            if (ReconnectHint.GetInstance() != null) ReconnectHint.GetInstance().Hide();
            _isReconnecting = false;
            netDownHintLocker = false;
            GVars.lockSend = false;
            //if (_coLogin != null)
            //{
            //    StopCoroutine(_coLogin);
            //    _coLogin = null;
            //}
            StopCoLogin();
            //GVars.lockReconnect = false;
            //GVars.lockQuit = false;
        }
        private void _timeoutQuit()
        {
            Debug.Log(string.Format("connectCount: {0}, connectTimeCount: {1}", NetManager.GetInstance().connectCount, NetManager.GetInstance().connectTimeCount));
            Debug.Log(string.Format("connectMaxTimes: {0}, connectMaxTimeout: {1}", NetManager.GetInstance().connectMaxTimes, NetManager.GetInstance().connectMaxTimeout));
            string content = GVars.language == "zh" ? "网络连接错误，请检查网络连接" : "Unable to connect to server，Please check your network connection";
            if (ReconnectHint.GetInstance() != null) ReconnectHint.GetInstance().Hide();
            GVars.lockQuit = true;
            GVars.lockReconnect = true;
            //PrepareQuitGame();

            if (m_xformCanvas.Find("RedBonusRain").gameObject.activeSelf)
            {
                RedBonusControl._redBonusControl.MaintenanceRedBonusClose();
            }
            {
                Debug.Log("m_xformCanvas  Find RedBonusRain");
            }

            AlertDialog.GetInstance().ShowDialog(content, false, () =>
            {
                initNet = false;

                QuitToLogin();
                GVars.lockQuit = false;
                GVars.lockReconnect = false;

            });
        }

        private IEnumerator _loadingLoginTimeout()
        {
            //Debug.Log(LogHelper.Magenta("_loadingLoginTimeout"));
            yield return new WaitForSeconds(5f);
            if (hasLogined || NetManager.GetInstance().useFake)
            {

            }
            else
            {
                Debug.Log(LogHelper.Magenta("_loadingLoginTimeout"));
                //_timeoutQuit();
                GVars.lockSend = false;
                Handle_NetDown(new object[] { new Exception("loading timeout") });
            }
        }
        //public Get
        //Dictionary<Type, object> singletonDic;
        //public T GetSingleton<T>()
        //{
        //    return singletonDic[T]
        //}


        //lua子游戏加载大厅scene
        public void LoadGameHall()
        {
            SceneManager.LoadScene("GameHall");
        }

        static InnerGame m_curInnerGame;//保存最后执行的子游戏
        public InnerGame InnerGame()
        {
            return m_curInnerGame;
        }

        //进入Lua子游戏时执行
        public void OnEnterLuaGame()
        {
            NetManager.Get().Disconnect();
        }

        public void OnBackFromLuaGame()
        {

        }

        //检查子游戏返回大厅时传递的参数
        public IEnumerator CheckStartupParams()
        {
            Debug.Log("CheckStartupParams");
            NetManager.Get().autoReady = false;


            yield return 2;//等待ios .mm 代码给unity发送消息
            if (!hasLogined)
            {
#if UNITY_ANDROID && !UNITY_EDITOR
            AndroidJavaClass jc;//调用java工程
            AndroidJavaObject jo;
            jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
            //GVars.isStartedFromGame = jo.Get<bool>("mIsFromGame");
            GVars.isStartedFromGame = jo.Call<bool>("isFromGame");
            if (GVars.isStartedFromGame)
            {
                string language = jo.Get<string>("mLanguage");
                GVars.language = language.Equals("1") ? "en" : "zh";
                GVars.username = jo.Get<string>("mUserId");
                GVars.pwd = jo.Get<string>("mPwd");

                string strParams = jo.Call<string>("getStartParams");
                string[] paramsArray = strParams.Split(' ');
                Debug.Log("paramsArray.Length: " + paramsArray.Length);
                GVars.language = paramsArray[2].Equals("1") ? "en" : "zh";
                GVars.username = paramsArray[0];
                GVars.pwd = paramsArray[1];
            }
#endif
#if UNITY_IOS
            //string user_Name = IOSGameStart.GetSingleton().GetPassedUserName();
			//string user_Pwd = IOSGameStart.GetSingleton().GetPassedPwd();
            //username =IOSGameStart.GetSingleton().Mserials;
            //nickname =IOSGameStart.GetSingleton().mModel;
            //IOSGameStart.GetSingleton().mLoginParam


                try
                {
                    string queryStr = IOS_Agent.GetStartQuery();
                    //Debug.Log(queryStr);
                    if (queryStr != null && queryStr.Length > 0)
                    {
                        //string query = "abc+NAME=我是XX+PWD=456123+IP=127.0.0.1+LAN=8080";
                        string name = new Regex(@"(?<=\+NAME\=)\S+?(?=\+)").Match(queryStr).ToString();
                        string pwd = new Regex(@"(?<=\+PWD\=)\S+?(?=\+)").Match(queryStr).ToString();
                        string language = new Regex(@"(?<=\+LAN\=)\S+$").Match(queryStr).ToString();
                        string test = new Regex(@"(?<=\+TEST\=)\S+?(?=\+)").Match(queryStr).ToString();
                        Debug.Log(name);
                        //Debug.Log(pwd);
                        Debug.Log(test);
                        GVars.isStartedFromGame = name.Length>4;

                        GVars.username = name;
                        GVars.pwd = pwd;
                        GVars.language = language.Equals("1") ? "en" : "zh";
                    }
                    else
                    {

                    }
                    //Debug.Log(string.Format("QueryStr:{0}", IOS_Agent.GetStartQuery()));
                    Debug.Log(string.Format("SchemeStr:{0}", IOS_Agent.GetStartScheme()));
                    Debug.Log(string.Format("SourceApp:{0}", IOS_Agent.GetStartSourceApp()));
                }
                catch (Exception ex)
                {
                    Debug.LogError(ex);
                }
#endif
                I2Localization.SetCurLanguage(GVars.language);
                RefreshLocalization();
            }

            while (!NetManager.Get().isConnected)
            {
                yield return null;
            }
            netDownHintLocker = false;
            Send_CheckVersion();
            while (m_versionOK == false)
            {
                yield return null;
            }

            NetManager.Get().autoReady = true;
            NetManager.Get().SendPublicKey();

            if (!GVars.lockRelogin && (GVars.isStartedFromGame || GVars.isStartedFromLuaGame))
            {
                StartCoroutine(m_loginPanel.DirectLogin());
            }

            yield break;
        }

        bool m_versionOK = false;
        void Send_CheckVersion()
        {
            string version = Application.version;
#if UNITY_EDITOR_OSX
            version = "9.1.2";
#endif
            NetManager.Get().Send("gcuserService/checkVersion", new object[] { version, 0 });
        }

        //void Send_QuitHall()
        //{
        //    GVars.lockQuit = true;
        //    GVars.lockReconnect = true;
        //    GVars.lockSend = true;
        //    NetManager.Get().Send("gcuserService/checkVersion", new object[] {});
        //}

        private void HandleNetMsg_International(object[] objs)
        {
            Debug.Log(LogHelper.NetHandle("HandleNetMsg_International"));
            Dictionary<string, object> dic = objs[0] as Dictionary<string, object>;
            bool international = (bool)dic["international"];
            GVars.isInternational = international;
        }

        void HandleNetMsg_CheckVersion(object[] objs)
        {
            Debug.Log(LogHelper.NetHandle("HandleNetMsg_CheckVersion"));
            Dictionary<string, object> dic = objs[0] as Dictionary<string, object>;
            bool hasNewVersion = (bool)dic["haveNewVersion"];
            if (hasNewVersion)
            {
                GVars.lockQuit = true;
                GVars.lockReconnect = true;
                GVars.lockSend = true;
                //netDownHintLocker = false;
                string urlIOS = (string)dic["downloadWindows"];
                string urlAndroid = (string)dic["downloadAndroid"];
#if UNITY_EDITOR
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "有新版本" : "New version", false, () =>
                {
                    UnityEditor.EditorApplication.isPlaying = false;
                });
                return;
#endif
                string msg = GVars.language == "zh" ? "注：为保证您的用户体验，客户端必须使用最新版本，否则无法正常游戏" : "Note: to ensure that your user experience, the client must use the latest version, otherwise unable to run the game";
                AlertDialog.Get().ShowDialog(msg, false, () =>
                {
                    AlertDialog.Get().Hide();
                    StartCoroutine(Utils.DelayCall(0.1f, () =>
                    {
                        Application.Quit();
                    }));
#if UNITY_IOS && !UNITY_EDITOR
                    Application.OpenURL(urlIOS);
#endif
#if UNITY_ANDROID && !UNITY_EDITOR
                    Application.OpenURL(urlAndroid);
#endif
                    Application.Quit();
                    //Send_QuitHall();
                });
            }
            else
            {
                m_versionOK = true;
                if (dic.ContainsKey("downloadWindows"))
                {
                    GVars.downloadStr = (string)dic["downloadWindows"];
                }
            }
        }

        //重新异步加载 主场景
        public void QuitToLogin()
        {
            //Application.LoadLevelAsync(Application.loadedLevelName);
            //SceneManager.LoadSceneAsync(Application.loadedLevelName);
            SceneManager.LoadSceneAsync(0);
        }

        private string DoGetHostAddresses(string hostname)
        {
            IPAddress ip;
            if (IPAddress.TryParse(hostname, out ip))
                return ip.ToString();
            else
                return Dns.GetHostEntry(hostname).AddressList[0].ToString();
        }

        void OnDestroy()
        {
            GVars.isStartedFromLuaGame = false;
            GVars.isStartedFromGame = false;
            GVars.lockQuit = false;
            GVars.lockReconnect = false;
            GVars.lockSend = false;
            PlayerPrefs.Save();
        }
    }

}
