﻿using UnityEngine;
using System.Collections;

namespace WMCY.GameHall.Common
{


    public class MB_Singleton<T> : MonoBehaviour where T : MB_Singleton<T>
    {
        protected static T _instance;
        //private bool isRegisted = false;

        public static T GetInstance()
        {
            return _instance;
        }

        //参考炉石单例的命名
        public static T Get()
        {
            return _instance;
        }

        public static void SetInstance(T t, bool force = false)
        {
            if (_instance == null || force)
            {
                _instance = t;
            }
        }

        //SetInstanceWithGameObject
        public static T SetInstanceByGameObject(GameObject go, bool force = false)
        {

            if (_instance == null || force)
            {
                _instance = go.GetComponent<T>();
            }
            return _instance;
        }

        public virtual void InitSingleton()
        {
            return;
        }

        public virtual void Release()
        {
            return;
        }
    }
}


