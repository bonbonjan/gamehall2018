﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using DG.Tweening;

namespace WMCY.GameHall
{

    public class TipBuyGameCurrency : MonoBehaviour
    {
        private int m_itemIndex;
        void Awake()
        {
            
        }

        void Start()
        {
            SwitchIntoEnglish();
            NetManager.GetInstance().RegisterHandler("bugByGameGold", HandleNetMsg_Buy);
        }

        void Update()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
                return;
            transform.Find("btnSure/Text").GetComponent<Text>().text = "Sure";
            transform.Find("btnClose/Text").GetComponent<Text>().text = "Cancel";
        }

        /*
         * itemIndex : 1-4
         * */
        public void ShowUI(int itemIndex)
        {
            m_itemIndex = itemIndex;
            transform.Find("tip").GetComponent<Text>().text = string.Format(GVars.language == "zh" ? "确认购买{0}游戏币吗？" : "Confirm to buy {0} coins?", Mathf.Pow(10, itemIndex));
        }

        public void OnBtnClick_Sure()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            NetManager.GetInstance().Send("gcshopService/bugByGameGold", new object[] { m_itemIndex, 1, "" });
        }

        public void OnBtnClick_Cancel()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            Destroy(transform.gameObject);

        }

        void HandleNetMsg_Buy(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            Debug.Log(obj);
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                Destroy(transform.gameObject);
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请等候系统进行处理" : "Please wait for the system process");

            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                Debug.Log(msgcode);
                switch (msgcode)
                {
                    case 2:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号或密码错误" : "Account name or password error, please enter again");
                        break;
                    case 3:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码错误" : "password error, please enter again");
                        break;
                    case 6:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被冻结" : "This account has been frozen");
                        break;
                    case 7:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "系统维护" : "Server maintenance");
                        break;
                    case 11:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请勿重复申请" : "Please do not repeat the application");
                        break;
                    case 12:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "余额不足" : "Your balance is not enough");
                        break;
                    case 22:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "后台未开启此项功能" : "Background did not open this feature");
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "处理失败" : "process failed");
                        break;
                }
            }
        }
    }
}