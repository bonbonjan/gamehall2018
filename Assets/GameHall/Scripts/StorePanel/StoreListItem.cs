﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;

namespace WMCY.GameHall
{


    public class StoreListItem : MonoBehaviour
    {
        private int m_itemId;
        //private int m_tabIndex;//1:游戏币 2：道具 3：装饰
        public Transform _itemDesPrefab;
        [SerializeField]
        ShopPropList _shopProplist;
        void Awake()
        {
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Fresh_OwnedProps, this, FreshOwnedProps);
        }

        void Start()
        {
        }

        void OnEnable()
        {
            SwitchIntoEnglish();
        }

        void OnDisable()
        {
            
        }

        void Update()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
                return;
            transform.Find("ItemNum").GetComponent<Text>().fontSize = 25;
            transform.Find("ItemNum").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("buyBtn/Text").GetComponent<Text>().fontSize = 30;
            transform.Find("remainTime/Text").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial",25);
        }

        private void FreshOwnedProps(object obj)
        {
            if (GVars.ownedProps.ContainsKey(m_itemId))
            {
                transform.Find("used").gameObject.SetActive(true);
                transform.Find("remainTime").gameObject.SetActive(true);
                transform.Find("remainTime/Text").GetComponent<Text>().text = string.Format("{0}{1}{2}{3}{4}",GVars.language == "zh" ? "剩余：" : "Countdown:", Mathf.FloorToInt(GVars.ownedProps[m_itemId].RemainTime / 86400000), GVars.language == "zh" ? "天" : " d ",Mathf.FloorToInt((GVars.ownedProps[m_itemId].RemainTime % 86400000) / 3600000), GVars.language == "zh" ? "小时" : " h");
            }
            else
            {
                transform.Find("used").gameObject.SetActive(false);
                transform.Find("remainTime").gameObject.SetActive(false);
            }
        }
        /*
         * itemId : 1-11
         * */
        public void ShowUI(int itemId)
        {
            m_itemId = itemId;
            transform.Find("ItemNum").GetComponent<Text>().text = GVars.language == "zh" ? _shopProplist.list[m_itemId - 1].zh_name : _shopProplist.list[m_itemId - 1].en_name;
            if (m_itemId <= 4)
            {
                transform.Find("buyBtn/Text").GetComponent<Text>().text = string.Format("{0}{1}",_shopProplist.list[m_itemId - 1].price, GVars.language == "zh" ?"元宝":"Gold");
            }
            else
            {
                transform.Find("buyBtn/Text").GetComponent<Text>().text = string.Format("{0}{1}",_shopProplist.list[m_itemId - 1].price, GVars.language == "zh" ?"币":"Coins");
            }
            transform.Find("Item").GetComponent<RawImage>().texture = Resources.Load("Icon/"+_shopProplist.list[m_itemId - 1].iconName) as Texture;
            if (GVars.ownedProps.ContainsKey(m_itemId))
            {
                transform.Find("used").gameObject.SetActive(true);
                transform.Find("remainTime").gameObject.SetActive(true);
                transform.Find("remainTime/Text").GetComponent<Text>().text = string.Format("{0}{1}{2}{3}{4}", GVars.language == "zh" ? "剩余：" : "Countdown:", Mathf.FloorToInt(GVars.ownedProps[m_itemId].RemainTime / 86400000), GVars.language == "zh" ? "天" : " d ", Mathf.FloorToInt((GVars.ownedProps[m_itemId].RemainTime % 86400000) / 3600000), GVars.language == "zh" ? "小时" : " h");
            }
        }

        public void OnBtnClick_ShowDes()
        {
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Close_StoreItemInfo);
            transform.Find("guang").gameObject.SetActive(true);
            //创建新的信息界面拷贝
            Transform itemDes = (Transform)Instantiate(_itemDesPrefab);
            itemDes.parent = transform;
            itemDes.localPosition = new Vector3(0, 0, 0);
            itemDes.localScale = Vector3.one;
            itemDes.GetComponent<ItemDes>().ShowUI(m_itemId);

        }

        public void OnBtnClick_Buy()
        {
            if (GVars.user.card == "-1" && UserCompleteController.UserCompleteNoPrompt == false && UserCompleteController.UserTempNoPrompt == false)
            {
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_UserCompletePanel, new ArrayList { "Store", m_itemId });
                return;
            }
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Close_StoreItemInfo);
            if (GVars.ownedProps.ContainsKey(m_itemId))
            {
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_ContinueBuyShopItems, m_itemId);
            }
            else
            {
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_BuyShopItems, m_itemId);
            }
            SoundManager.Get().PlaySound(SoundType.Common_Click);
        }
    }
}