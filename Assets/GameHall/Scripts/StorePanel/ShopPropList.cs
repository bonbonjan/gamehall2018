﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ShopProp  {
    public string zh_name;
    public string en_name;
    public string zh_des;
    public string en_des;
    public int price;
    public string iconName;
	
}

[CreateAssetMenu()]
public class ShopPropList : ScriptableObject
{
    public List<ShopProp> list;
}
