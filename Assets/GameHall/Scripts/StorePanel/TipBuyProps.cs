﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using DG.Tweening;

namespace WMCY.GameHall
{


    public class TipBuyProps : MonoBehaviour
    {
        private int m_itemId;
        private int m_buyCount = 0;
        [SerializeField]
        private int presentLevel = 5;
        [SerializeField]
        ShopPropList _shopProplist;
        void Awake()
        {
            
        }

        void Start()
        {
            NetManager.GetInstance().RegisterHandler("bugByGameGold", HandleNetMsg_Buy);
            //NetManager.GetInstance().RegisterHandler("presentByGameGold", HandleNetMsg_Present);
            NetManager.GetInstance().RegisterHandler("giftByGameGold", HandleNetMsg_PresentBack);
        }

        void OnEnable()
        {
            //SwitchIntoEnglish();
        }

        void Update()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
                return;
            transform.Find("tip").GetComponent<Text>().fontSize = 28;
            transform.Find("tip").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("btnSure/Text").GetComponent<Text>().text = "Sure";
            transform.Find("InputPassword/Placeholder").GetComponent<Text>().text = "please input password";
        }

        /*
         * itemId: 5-11
         * */
        public void ShowUI(int itemId)
        {
            m_itemId = itemId;
            m_buyCount = _shopProplist.list[m_itemId - 1].price > GVars.user.gameGold ? 0 : 1;
            UpdateBuycount();
            UpdateTopTip();
            if(GVars.giftMode==0)
            {
                this.transform.Find("btnPresent").gameObject.SetActive(false);
                this.transform.Find("btnSure").localPosition = new Vector3(0, -187.7f, 0);
            }
            else
            {
                this.transform.Find("btnPresent").gameObject.SetActive(true);
                this.transform.Find("btnSure").localPosition = new Vector3(150, -187.7f, 0);
            }
        }
        public void OnBtnClick_Present()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            if (_shopProplist.list[m_itemId - 1].price > GVars.user.gameGold)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游戏币不足" : "Coin shortage");
                return;
            }
            string pwd = transform.Find("InputPassword").GetComponent<InputField>().text;
            if (m_buyCount == 0)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入正确的赠送数量" : "Please input correct num");
                return;
            }
            if (pwd == "")
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入密码" : "Please input password");
                return;
            }
            if(pwd != GVars.pwd)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码错误" : "password error");
                return;
            }
            //if (GVars.user.level < presentLevel)
            //{
            //    AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "等级不足" : "");
            //    return;
            //}
            //NetManager.GetInstance().Send("gcshopService/presentByGameGold", new object[] { m_itemId, m_buyCount, pwd });

            this.transform.Find("PresentPanel").gameObject.SetActive(true);
        }

        //private void HandleNetMsg_Present(object[] obj)
        //{
        //    Dictionary<string, object> objs = obj[0] as Dictionary<string, object>;
        //    bool success = (bool)objs["success"];
        //    if(success)
        //    {
        //        this.transform.Find("PresentPanel").gameObject.SetActive(true);
        //    }
        //    else
        //    {
        //        int msgCode = (int)objs["msgCode"];
        //        string msg = (string)objs["msg"];
        //        switch (msgCode)
        //        {
        //            case 1:
        //                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入密码" : "Please input password");
        //                break;
        //            default:
        //                AlertDialog.GetInstance().ShowDialog(msg);
        //                break;
        //        }
        //    }
            
        //}

        public void OnBtnClick_PresentSure()
        {
            string username = this.transform.Find("PresentPanel/bg/UserName").GetComponent<InputField>().text;
            string repeat = this.transform.Find("PresentPanel/bg/Repeat").GetComponent<InputField>().text;
            string remarks = this.transform.Find("PresentPanel/bg/Beizhu").GetComponent<InputField>().text;
            string pwd = this.transform.Find("InputPassword").GetComponent<InputField>().text;
            int length = username.Length;
            if(username=="")
            {
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "请输入对方账号" : "Please enter the other account");
                return;
            }
            if (InputCheck.CheckUserName(username))
            {
                if (username != repeat)
                {
                    AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "两次输入不一致" : "The two input are different");
                    return;
                }
                if (username == GVars.user.username)
                {
                    AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "无法赠送给自己" : "Can not give yourself");
                    return;
                }

                NetManager.GetInstance().Send("gcshopService/giftByGameGold", new object[] { m_itemId, m_buyCount, pwd, username, remarks, });
            }
            
        }

        private void HandleNetMsg_PresentBack(object[] obj)
        {
            Dictionary<string, object> objs = obj[0] as Dictionary<string, object>;
            bool success = (bool)objs["success"];
            if(success)
            {
                //处理金币    
                GVars.user.gameGold = (int)objs["gameGold"];
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_GoldAndLottery);
                Destroy(transform.gameObject);
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "赠送成功" : "Presented successfully");
            }
            else
            {
                int msgCode = (int)objs["msgCode"];
                //string msg = (string)objs["msg"];
                switch (msgCode)
                {
                    case 2:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "用户名不存在" : "The account does not exist");
                        break;
                    case 3:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码错误" : "password error");
                        break;
                    case 12:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游戏币不足" : "Coin is insufficient");
                        break;
                    case 23:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账目异常" : "");
                        break;
                    case 24:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "等级不足" : "Insufficient level");
                        break;
                    case 25:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "今日赠送个数已达上限，请明日再试" : "Today,the maximum number of gifts is reached,please try again tomorrow");
                        break;
                    case 27:
                        AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "无法赠送给自己" : "Can not give yourself");
                        break;
                    default:
                        Debug.Log(LogHelper.Yellow("msgCode: " + msgCode));
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "赠送失败" : "");
                        break;
                }
            }
        }

        public void OnbtnClick_PresentClose()
        {
            this.transform.Find("PresentPanel/bg/UserName").GetComponent<InputField>().text = "";
            this.transform.Find("PresentPanel/bg/Repeat").GetComponent<InputField>().text = "";
            this.transform.Find("PresentPanel/bg/Beizhu").GetComponent<InputField>().text = "";
            this.transform.Find("PresentPanel").gameObject.SetActive(false);
        }

        public void OnBtnClick_Sure()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            if (_shopProplist.list[m_itemId - 1].price > GVars.user.gameGold)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游戏币不足" : "Coin shortage");
                return;
            }
            string pwd = transform.Find("InputPassword").GetComponent<InputField>().text;
            if (m_buyCount == 0)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入正确的购买数量" : "Please input correct num");
                return;
            }
            if (pwd == "")
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请输入密码" : "Please input password");
                return;
            }
            NetManager.GetInstance().Send("gcshopService/bugByGameGold", new object[] { m_itemId, m_buyCount, pwd });
        }

        public void OnBtnClick_Cancel()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            Destroy(transform.gameObject);
        }

        public void OnBtnClick_Add()
        {
            if ((m_buyCount + 1) * _shopProplist.list[m_itemId - 1].price > GVars.user.gameGold)
                m_buyCount = _shopProplist.list[m_itemId - 1].price > GVars.user.gameGold ? 0 : 1;
            else
                m_buyCount += 1;
            SoundManager.Get().PlaySound(SoundType.Click_PlusMinus);
            UpdateBuycount();
        }

        public void OnBtnClick_Reduce()
        {
            if (m_buyCount <= 0)
            {
                m_buyCount = 0;
                
            }
            else if (m_buyCount == 1)
            {
                m_buyCount = Convert.ToInt32(GVars.user.gameGold / _shopProplist.list[m_itemId - 1].price);
            }
            else
            {
                m_buyCount -= 1;
            }
            SoundManager.Get().PlaySound(SoundType.Click_PlusMinus);
            UpdateBuycount();
        }

        void UpdateBuycount()
        {
            transform.Find("InputBuyCount").GetComponent<InputField>().text = m_buyCount.ToString();
        }

        public void UpdateTopTip()
        {
            string num = transform.Find("InputBuyCount").GetComponent<InputField>().text;
            if (num.Contains("-"))
            {
                //AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "非法数字" : "Illegal number");
                m_buyCount = Convert.ToInt32(GVars.user.gameGold / _shopProplist.list[m_itemId - 1].price);
            }
            else
            {
                m_buyCount = Convert.ToInt32(num);
            }
            if (m_buyCount * _shopProplist.list[m_itemId - 1].price > GVars.user.gameGold)
            {
                //AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "游戏币不足" : "Coin shortage");
                m_buyCount = Convert.ToInt32(GVars.user.gameGold / _shopProplist.list[m_itemId - 1].price);
            }
            else if (m_buyCount < 0)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "非法数字" : "Illegal number");
                m_buyCount = Convert.ToInt32(GVars.user.gameGold / _shopProplist.list[m_itemId - 1].price);
            }
            UpdateBuycount();
            transform.Find("tip").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
            transform.Find("tip").GetComponent<Text>().fontSize = GVars.language == "zh" ? 33 : 26;
            transform.Find("tip").GetComponent<Text>().text = string.Format(GVars.language == "zh" ? "确认花费<size=38><color=#E4C751>{0}币</color></size>购买{1}么？" : "Confirm to spend <size=38><color=#E4C751>{0} coins</color></size> to buy {1}？", m_buyCount * _shopProplist.list[m_itemId - 1].price, GVars.language == "zh" ? _shopProplist.list[m_itemId - 1].zh_name : _shopProplist.list[m_itemId - 1].en_name);
        }

        void HandleNetMsg_Buy(object[] objs)
        {
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                Destroy(transform.gameObject);
                GVars.user.gameGold = (int)obj["gameGold"];
                GVars.user.lottery = (int)obj["lottery"];
                //接收道具信息
                GVars.ownedProps.Clear();
                object[] propObjs = (object[])obj["prop"];
                for (int i = 0; i < propObjs.Length; i++)
                {
                    Dictionary<string, object> propObj = propObjs[i] as Dictionary<string, object>;
                    GVars.ownedProps.Add(Convert.ToInt32(propObj["propId"]), new ownShopProp(Convert.ToInt32(propObj["propId"]), Convert.ToInt64(propObj["remainTime"]), false));
                }
                for (int i = 9; i < 12; i++)
                {
                    if (GVars.ownedProps.ContainsKey(i) == true)
                    {
                        AppManager.GetInstance().Notify(UIGameMsgType.UINotify_FreshDealer, i - 8);
                        break;
                    }
                    else if (i == 11)
                    {
                        AppManager.GetInstance().Notify(UIGameMsgType.UINotify_FreshDealer, 0);
                    }
                }
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_GoldAndLottery);
                AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Fresh_OwnedProps);
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "购买成功" : "Buy success!");
            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                Debug.Log(msgcode);
                switch (msgcode)
                {
                    case 2:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码错误" : "Account name or password error, please enter again");
                        break;
                    case 3:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码错误" : "password error, please enter again");
                        break;
                    case 6:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被冻结" : "This account has been frozen");
                        break;
                    case 7:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "系统维护" : "Server maintenance");
                        break;
                    case 11:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请勿重复申请" : "Please do not repeat the application");
                        break;
                    case 12:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "余额不足" : "Your balance is not enough");
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "购买失败" : "buy failed");
                        break;
                }
            }
        }
    }
}