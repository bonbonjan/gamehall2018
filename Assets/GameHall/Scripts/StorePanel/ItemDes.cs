﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using DG.Tweening;

namespace WMCY.GameHall
{


    public class ItemDes : MonoBehaviour
    {
        private int m_itemId;
        [SerializeField]
        ShopPropList _shopProplist;
        void Awake()
        {
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Close_StoreItemInfo, this, CloseItemInfo);
        }

        void Start()
        {
            SwitchIntoEnglish();
            Tweener tweener = transform.GetComponent<RectTransform>().DOScale(0.3f, 0.2f);
            tweener.SetDelay(3f);
            tweener.SetUpdate(true);
            tweener.SetEase(Ease.Linear);
            tweener.OnComplete(delegate { Destroy(transform.gameObject); transform.parent.Find("guang").gameObject.SetActive(false); });
        }

        void OnEnable()
        {
            
        }

        void Update()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
                return;
            transform.Find("frame/itemName").GetComponent<Text>().fontSize = 28;
            transform.Find("frame/itemName").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("frame/implement").GetComponent<Text>().fontSize = 20;
            transform.Find("frame/implement").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
            transform.Find("frame/desText").GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 25);
        }

        public void ShowUI(int itemId)
        {
            m_itemId = itemId; 
            if (m_itemId <= 4)
            {
                transform.Find("frame/implement").gameObject.SetActive(false);
            }
            else
            {
                transform.Find("frame/implement").GetComponent<Text>().text = string.Format(GVars.language == "zh" ?"赠{0}彩票":"Giving {0} ticket", m_itemId <= 8 ? 50:50);//(int)Mathf.Pow(10, m_itemId - 4) : 5 * (int)Mathf.Pow(10, m_itemId - 8));
            }
            transform.Find("frame/itemName").GetComponent<Text>().text = GVars.language == "zh" ? _shopProplist.list[m_itemId - 1].zh_name : _shopProplist.list[m_itemId - 1].en_name;
            transform.Find("frame/desText").GetComponent<Text>().text = GVars.language == "zh" ? _shopProplist.list[m_itemId - 1].zh_des : _shopProplist.list[m_itemId - 1].en_des;
            transform.Find("frame/itemImage").GetComponent<RawImage>().texture = Resources.Load("Icon/" + _shopProplist.list[m_itemId - 1].iconName) as Texture;
            FixFrameSize();
            FixFramePos();
        }

        private void CloseItemInfo(object obj)
        {
            Destroy(transform.gameObject);
            transform.parent.Find("guang").gameObject.SetActive(false);
        }

        private void FixFramePos()
        {
            
            float frame_x = -122;
            float frame_y = 0;

            RectTransform desBgRect = transform.Find("frame/desBg").GetComponent<RectTransform>();
            RectTransform frameRect = transform.Find("frame").GetComponent<RectTransform>();

            frame_y = -desBgRect.localPosition.y + transform.Find("arrow").localPosition.y + desBgRect.sizeDelta.y - 3f;

            float offset_Lx = transform.parent.localPosition.x + transform.parent.parent.localPosition.x - desBgRect.sizeDelta.x / 2 + 640;
            float offset_Rx = transform.parent.localPosition.x + transform.parent.parent.localPosition.x + desBgRect.sizeDelta.x / 2 - 640;
            float offset_x = offset_Lx < 0 ? -offset_Lx : offset_Rx > 0 ? -offset_Rx : 0;

            frame_x = -(desBgRect.localPosition.x + desBgRect.sizeDelta.x / 2) + offset_x;
            transform.Find("frame").localPosition = new Vector3(frame_x, frame_y, 0);
            //if (m_itemId <= 4)
            //{
            //    return;
            //}
            //if (GVars.language == "en")
            //{
            //    if (transform.parent.localPosition.x > 700)
            //    {
            //        transform.FindChild("frame").localPosition -= new Vector3(360, frame_y, 0);
            //    }
            //    else if (transform.parent.localPosition.x < 250)
            //    {
            //        transform.FindChild("frame").localPosition += new Vector3(25, frame_y, 0);
            //    }
            //}
            //else
            //{
            //    if (transform.parent.position.x > 60)
            //    {
            //        transform.FindChild("frame").localPosition -= new Vector3(220, frame_y, 0);
            //    }
            //    else if (transform.parent.position.x > -60)
            //    {
            //        transform.FindChild("frame").localPosition -= new Vector3(100, frame_y, 0);
            //    }
            //}
        }

        private void FixFrameSize()
        {
            float implement_W = transform.Find("frame/implement").gameObject.activeSelf ? transform.Find("frame/implement").GetComponent<Text>().preferredWidth : 0;
            transform.Find("frame/desText").GetComponent<RectTransform>().sizeDelta = new Vector2(300 + implement_W, 1);
            float implement_H = transform.Find("frame/desText").GetComponent<Text>().preferredHeight;
            transform.Find("frame/desBg").GetComponent<RectTransform>().sizeDelta = new Vector2(380 + implement_W, 150 + implement_H);
            transform.Find("frame/desText").GetComponent<RectTransform>().sizeDelta = new Vector2(300 + implement_W, implement_H);
            transform.Find("frame/desBg/gang").GetComponent<RectTransform>().sizeDelta = new Vector2(3, 300 + implement_W);
            transform.Find("frame/desBg/gang").GetComponent<RectTransform>().localPosition = new Vector2(190 + implement_W / 2, -92);
            //if (m_itemId <= 4)
            //{
            //    transform.FindChild("frame/desBg").GetComponent<RectTransform>().sizeDelta = new Vector2(300, 210.7f);
            //    transform.FindChild("frame/desBg/gang").GetComponent<RectTransform>().sizeDelta = new Vector2(3, 200);
            //    transform.FindChild("frame/desText").GetComponent<RectTransform>().sizeDelta = new Vector2(280f, 70.5f);
            //}
            //else if (m_itemId <= 11)
            //{
            //    if (GVars.language == "en")
            //    {
            //        transform.FindChild("frame/desBg").GetComponent<RectTransform>().sizeDelta = new Vector2(610, 210.7f);
            //        transform.FindChild("frame/desBg/gang").GetComponent<RectTransform>().sizeDelta = new Vector2(3, 495);
            //        transform.FindChild("frame/desText").GetComponent<RectTransform>().sizeDelta = new Vector2(535f, 70.5f);
            //        transform.FindChild("frame/implement").localPosition = new Vector3(355, 239.8f, 0);
            //        transform.FindChild("frame/itemName").localPosition = new Vector3(50, 239.8f, 0);
                    
            //    }
            //    else
            //    {
            //        float implement_W = transform.FindChild("frame/implement").GetComponent<Text>().preferredWidth;
            //        float implement_H = transform.FindChild("frame/implement").GetComponent<Text>().preferredHeight;
            //        transform.FindChild("frame/desBg").GetComponent<RectTransform>().sizeDelta = new Vector2(380 + implement_W, 60 + implement_H);
            //        transform.FindChild("frame/desText").GetComponent<RectTransform>().sizeDelta = new Vector2(300 + implement_W, 70.5f);
            //        transform.FindChild("frame/desBg/gang").GetComponent<RectTransform>().sizeDelta = new Vector2(3, 300 + implement_W);
            //        transform.FindChild("frame/desBg/gang").GetComponent<RectTransform>().localPosition = new Vector2(0,10);
            //    }
            //}

        }

    }
}