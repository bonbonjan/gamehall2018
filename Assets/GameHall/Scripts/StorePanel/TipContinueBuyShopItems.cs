﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;
using DG.Tweening;

namespace WMCY.GameHall
{

    public class TipContinueBuyShopItems : MonoBehaviour
    {
        private int m_itemId;
        [SerializeField]
        ShopPropList _shopProplist;
        void Awake()
        {
            
        }

        void Start()
        {
            SwitchIntoEnglish();
        }

        void Update()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
                return;
            transform.Find("btnSure/Text").GetComponent<Text>().text = "Buy";
        }

        /*
         * itemId : 5-11
         * */
        public void ShowUI(int itemId)
        {
            m_itemId = itemId;
            transform.Find("tip").GetComponent<Text>().text = string.Format(GVars.language == "zh" ? "您的{0}道具仅剩{1}天，是否继续购买?" : "You have {1} day validity of {0} , whether to buy again?", GVars.language == "zh" ? _shopProplist.list[m_itemId - 1].zh_name : _shopProplist.list[m_itemId - 1].en_name, "" + Mathf.CeilToInt(GVars.ownedProps[m_itemId].RemainTime / 86400000f));
        }

        public void OnBtnClick_Sure()
        {
            Destroy(transform.gameObject);
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Close_StoreItemInfo);
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Open_BuyShopItems, m_itemId);
        }

        public void OnBtnClick_Close()
        {
            Destroy(transform.gameObject);
        }

        public void OnBtnClick_Fader()
        {

        }

    }
}