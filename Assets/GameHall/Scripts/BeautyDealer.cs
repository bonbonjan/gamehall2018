﻿using UnityEngine;
using System.Collections;

public class BeautyDealer : MonoBehaviour
{

    Animator animator;
    MeshRenderer renderer;
    Coroutine coPlayControl;
    Coroutine coPlayGreet;
    bool greeting = false;
    void Awake()
    {

    }

    void OnEnable()
    {
        if (animator == null)
            animator = GetComponent<Animator>();
        if (renderer == null)
            renderer = GetComponent<MeshRenderer>();
        //renderer.sortingOrder = 99;//会导致层级错乱
        greeting = false;
        if (coPlayControl != null) StopCoroutine(coPlayControl);
        //coPlayControl = StartCoroutine(PlayControl());
        coPlayControl = StartCoroutine(PlayControl2());
    }

    void Start()
    {
    }

    IEnumerator PlayControl()
    {
        Debug.Log("PlayControl");
        while (true)
        {
            int times = Random.Range(2, 3 + 1);
            //Debug.Log("BeautyDealer>times: " + times);
            for (int i = 0; i < times; i++)
            {
                while (greeting)
                {
                    yield return null;
                }
                yield return PlayState("Standby_1");
                yield return null;
            }
            while (greeting)
            {
                yield return null;
            }
            yield return PlayState("Standby_2");
        }
        yield break;
    }

    IEnumerator PlayControl2()
    {
        Debug.Log("PlayControl2");
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(10f, 20f));
            //animator.SetTrigger("greet");
        }
    }

    IEnumerator PlayState(string state)
    {
        //animator.Stop();
        animator.Play("");
        animator.Play(state);
        AnimatorStateInfo info = animator.GetCurrentAnimatorStateInfo(0);
        //Debug.Log(string.Format("playstate [{0}], length: {1}s",state,info.length));
        yield return new WaitForSeconds(info.length);
        //while(animator.)
        //{
        //    yield return null;
        //}
        //yield return new WaitForSeconds(0.5f);
    }

    public void Greet()
    {
        if (coPlayGreet != null) StopCoroutine(coPlayGreet);
        coPlayGreet = StartCoroutine(PlayGreet());
    }

    IEnumerator PlayGreet()
    {
        //if (coPlayControl != null) StopCoroutine(coPlayControl);
        //animator.SetBool("greet", true);
        animator.SetTrigger("greet");
        yield return 2;
        animator.ResetTrigger("greet");
        yield break;
        //yield return new WaitForSeconds(5);
        //yield return StartCoroutine(PlayState("Standby_2"));
        //animator.SetBool("greet", false);
        //coPlayControl = StartCoroutine(PlayControl());
    }
}
