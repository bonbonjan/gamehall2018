﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;

namespace WMCY.GameHall
{
    public class StorePanelController : MonoBehaviour
    {
        public Transform m_listItemPrefab;
        public Transform _tipBuyGameCurrency;
        public Transform _tipBuyProps;
        public Transform _tipContinueBuyShopItems;
        private Transform m_listContent;
        private int m_curTabIndex = 0;
        void Awake()
        {
            m_listContent = transform.Find("listPanel/listScrollView/Viewport/Content");
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Fresh_GoldAndLottery, this, FreshGoldAndLottery);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Open_BuyShopItems, this, OpenBuyShopItems);
            AppManager.GetInstance().Register(UIGameMsgType.UINotify_Open_ContinueBuyShopItems, this, OpenContinueBuyShopItems);
            
        }

        void OnEnable()
        {
            transform.Find("topBg/goldItem/num").GetComponent<Text>().text = Convert.ToString(GVars.user.gameGold);
            transform.Find("topBg/lotteryItem/num").GetComponent<Text>().text = Convert.ToString(GVars.user.lottery);
            FocusTabByIndex(1);
            //SwitchIntoEnglish();
        }

        void OnDisable()
        {
            m_curTabIndex = 0;
            UserCompleteController.UserTempNoPrompt = false;
        }

        /*
         * 清除列表
         */
        void DestroyList()
        {
            for (int i = 0; i < m_listContent.childCount; i++)
            {
                DestroyObject(m_listContent.GetChild(i).gameObject);
            }
        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
                return;
            transform.Find("TitleBg/Title").GetComponent<Text>().text = "Mall";
            transform.Find("TitleBg/Title").GetComponent<Text>().fontSize = 30;
            transform.Find("tab/tab1/Text").GetComponent<Text>().text = "Coins";
            transform.Find("tab/tab1/tabHighlight/Text").GetComponent<Text>().text = "Coins";
            transform.Find("tab/tab2/Text").GetComponent<Text>().text = "Props";
            transform.Find("tab/tab2/tabHighlight/Text").GetComponent<Text>().text = "Props";
            transform.Find("tab/tab3/Text").GetComponent<Text>().text = "Beauty dealers";
            transform.Find("tab/tab3/tabHighlight/Text").GetComponent<Text>().text = "Beauty dealers";

        }

        /*
         * 更新列表
         */
        void UpdateList()
        {
            int[] listItemNum = {4,4,3 };
            DestroyList();
            m_listContent.localPosition = new Vector3(-574, 146, 0);
            for (int i = 0; i < listItemNum[m_curTabIndex - 1]; i++)
            {
                Transform listItem = (Transform)Instantiate(m_listItemPrefab);
                listItem.parent = m_listContent;
                listItem.localPosition = new Vector3(m_curTabIndex < 3 ? 150 + 300 * i : 200 + 370 * i, -146, 0);
                listItem.localScale = Vector3.one;
                listItem.name = ""+i;
                listItem.GetComponent<StoreListItem>().ShowUI(i + 1 + (m_curTabIndex - 1) * 4);
            }
            float rectW = listItemNum[m_curTabIndex - 1] * 300;
            float rectH = m_listContent.GetComponent<RectTransform>().rect.height;
            m_listContent.GetComponent<RectTransform>().sizeDelta = new Vector2(rectW, rectH);
        }

        void Update()
        {

        }

        public void OnBtnClick_Fader()
        {
            AppManager.GetInstance().Notify(UIGameMsgType.UINotify_Close_StoreItemInfo); 
        }

        public void OnBtnClick_Close()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            gameObject.SetActive(false);
        }

        public void OnBtnClick_Return()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            gameObject.SetActive(false);

        }

        public void OnBtnClick_Tab1()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            FocusTabByIndex(1);
        }
        public void OnBtnClick_Tab2()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            FocusTabByIndex(2);

        }
        public void OnBtnClick_Tab3()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            FocusTabByIndex(3);
        }

        public void FocusTabByIndex(int index)
        {
            if (m_curTabIndex == index)
            {
                return;
            }
            m_curTabIndex = index;
            UpdateList();  
            for (int i = 1; i < 4; i ++ )
            {
                transform.Find(string.Format("tab/tab{0}/tabHighlight", i)).gameObject.SetActive(i == index ? true : false);
            }
        }

        private void FreshGoldAndLottery(object obj)
        {
            transform.Find("topBg/goldItem/num").GetComponent<Text>().text = Convert.ToString(GVars.user.gameGold);
            transform.Find("topBg/lotteryItem/num").GetComponent<Text>().text = Convert.ToString(GVars.user.lottery);
        }

        private void OpenBuyShopItems(object obj)
        {
            Transform tipPanel = (Transform)Instantiate((int)obj <= 4 ? _tipBuyGameCurrency : _tipBuyProps);
            tipPanel.parent = transform;
            tipPanel.localPosition = new Vector3(0, 216, 0);
            tipPanel.localScale = Vector3.one;
            if ((int)obj <= 4)
            {
                tipPanel.GetComponent<TipBuyGameCurrency>().ShowUI((int)obj);
            }
            else
            {
                tipPanel.GetComponent<TipBuyProps>().ShowUI((int)obj);
            }
        }

        private void OpenContinueBuyShopItems(object obj)
        {
            Transform tipPanel = (Transform)Instantiate(_tipContinueBuyShopItems);
            tipPanel.parent = transform;
            tipPanel.localPosition = new Vector3(0, 216, 0);
            tipPanel.localScale = Vector3.one;
            tipPanel.GetComponent<TipContinueBuyShopItems>().ShowUI((int)obj);
        }
        
    }
}