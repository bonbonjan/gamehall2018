﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class FixedResolution : MonoBehaviour
{

    public int designWidth = 1280;
    public int desginHeight = 720;
    public Camera uiCamera;
    public Canvas rootCanvas;
    private float _designAspect;
    private RectTransform _canvasRectTransform;
    private int _lastWidth;
    private int _lastHeight;
    void Awake()
    {
        _designAspect = designWidth * 1f / desginHeight;
        //Debug.Log("FixedResolution: awake");
        _lastWidth = Screen.width;
        _lastHeight = Screen.height;
        _adjust();
    }


    void Start()
    {
        //Debug.Log("FixedResolution: Start", this);
    }


    void Update()
    {

#if UNITY_EDITOR
        if (Time.frameCount % 100 == 0)
        {
            //Debug.Log("Update");
        }
        if (_lastWidth != Screen.width || _lastHeight != Screen.height)
        {
            _adjust();
        }

        //_ResetCanvas();
        //ResetCamera();
#endif
    }

    void _adjust()
    {
        float size = desginHeight / 200f;
        float screenAspect = Screen.width * 1f / Screen.height;
        if (Mathf.Abs(Screen.width * desginHeight - Screen.height * designWidth) * 1f / (Screen.width * desginHeight) < 0.01f)
        {
            size = desginHeight / 200f;
            //Debug.Log("Adjust> approximately equal");
        }
        else if (screenAspect > _designAspect)
        {
            size = desginHeight / 200f;
            //Debug.Log("Adjust> screenAspect is bigger");
        }
        else
        {
            size = desginHeight * _designAspect / (200 * screenAspect);
            //Debug.Log("Adjust> designAspect is bigger");
        }

        if (uiCamera != null)
        {

            if (uiCamera.orthographic)
            {

                uiCamera.orthographicSize = size;
            }
            else
            {

            }
        }

    }

    public void _resetCanvas()
    {
        if (rootCanvas != null)
        {
            if (_canvasRectTransform == null)
            {
                _canvasRectTransform = rootCanvas.GetComponent<RectTransform>();
            }
            rootCanvas.renderMode = RenderMode.WorldSpace;
            rootCanvas.transform.position = Vector3.zero;
            rootCanvas.scaleFactor = 0.01f;
            _canvasRectTransform.sizeDelta = new Vector2(designWidth, desginHeight);
        }
    }

    public void _resetCamera()
    {
        if (uiCamera != null)
        {
            uiCamera.transform.position = Vector3.forward * (-1000f);
            uiCamera.orthographic = true;
            Debug.Log("ResetCamera");
        }
    }



}
