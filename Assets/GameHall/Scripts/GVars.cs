﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace WMCY.GameHall
{
    /// <summary>
    /// Global variables,全局变量
    /// </summary>
    public static class GVars
    {
        //public static int InnerGameCount = 9;//子游戏个数
        public static string IPAddress = "";//"120.27.163.31";//121.40.232.222//112.124.119.178//192.168.1.76
        public static string IPAddress_Game = "";
        public static int IPPort = 0;//10016;
        public static string versionURL = "";
        public static string versionCode = "";//仅IOS版本有用，其它版本会被重新置空
        public static string language = "zh";//"zh";//"en"
        public static string username = "";//"wan006";
        public static string pwd = "";//"qwerty1";
        public static string nickname = "";//游客登陆时客户端生成昵称，发送给服务器
        public static string netVersion = "1.2.14";//通信报文中携带的版本约定

        //？可能有的变量放在User中更合适
        public static bool isTourist = false;//是否游客
        public static bool isOverflow = false;//是否帐号爆机
        public static int payMode = 1;//1:商城 2:直接充值
        public static bool useRaffleStore = false;//是否开启彩票城
        public static int pwdSafeBox = 0;   //保险柜是否设置过口令
        public static bool hasSetUpProtectQuestion = false;//是否设置了密保问题
        public static int gold = 0;//游戏币
        public static int expeGold = 0;//体验币
        public static int raffle = 0;//彩票数量
        public static int savedGameGold = 0; //保险柜的游戏币数量
        public static int savedLottery = 0; //保险柜的彩票数量
        public static Dictionary<int, ownShopProp> ownedProps = new Dictionary<int, ownShopProp>(); //拥有的道具id和时效

        public static bool isInternational = true;//true表示国际版 false表示国内版

        public static int[] orders = new int[] {11, 3, 1, 4, 7, 0, 6, 2, 5};

        public static string[] QianpaoName = { "同类炸弹", "Target Bomb" };//千炮 GameType 
        public static string[] WanpaoName = { "万炮专版", "Likui Fish" };//万炮 GameType

        public static bool isShowHallName = true;

        //public static int OverFishType = 0;//万炮捕鱼3个模式 0牛魔王 1钻石渔夫 2御龙传说

        public static User user;
        public static string downloadStr = "";

        public static int giftMode = 0;//是否开启赠送功能	0 关闭赠送模式   1 开启

        public static int shareMode = 2;//是否开启分享功能	0 开启分享模式   1 关闭
        public static int specialMark = 2;//是否是特殊账号      0-正常账号 1-特殊账号

        public static bool ScoreOverflow = false;//玩家顶分状态
        public static string gamesJsonFilePath = "Assets/Resources/Saves/games.json";
        public const string ABDir = "AB";           //素材目录 
        static bool localDown = false;
        public static bool isStartedFromLuaGame = false;                        //是否从lua游戏返回
        public static bool isStartedFromGame = false;                           //是否从其他子游戏返回
        public const string touristPwd = "123456";
        public const string touristAccountPrefix_cn = "游客";
        public const string touristAccountPrefix_en = "tourist";
        public static bool lockSend = false;//通信发送中，禁止用户触发新的发送请求，适用于req等待resp模型，在req的同时置为true,在resp清为false
        public static bool lockQuit = false;//客户端将退出，不允许进行意外的操作
        public static bool lockReconnect = false;//禁止再次重连
        public static bool lockRelogin = false;//禁止重连后自动登陆

        //用户账号显示名称
        public static string accountName
        {
            get {
                string prefix = "";
                if (GVars.isTourist)
                {
                    prefix = GVars.language == "zh" ? touristAccountPrefix_cn : touristAccountPrefix_en;
                }
                return prefix + GVars.username;
            }
        }
        public static string downBaseUrl
        {
            get
            {
#if UNITY_EDITOR
                if (localDown)
                    return "file://" + Path.GetFullPath(Path.Combine(Application.dataPath, "../AB/")).Replace('\\', '/');
#endif
                return "http://192.168.1.80:8080/";
            }
        }
        public static string dataPath
        {
            get
            {
                if (Application.isMobilePlatform)
                {
                    return Application.persistentDataPath + "/";
                }

//                if (Application.platform == RuntimePlatform.OSXEditor)
//                {
//                    int i = Application.dataPath.LastIndexOf('/');
//                    return Application.dataPath.Substring(0, i + 1);
//                }

                //return "D:/GameHall/";
                //return "E:/GameHall/";
                string ret = Path.GetFullPath(Path.Combine(Application.dataPath, "../Down/")).Replace('\\', '/');
                return ret;
            }
        }

        public static string wwwDataPath
        {
            get
            {
                string ret;
                if (Application.isEditor)
                    ret = "file://" + dataPath;
                else if (Application.isMobilePlatform || Application.isConsolePlatform)
                    ret = "file:///" + Application.persistentDataPath + "/";
                else // For standalone player.
                    ret = "file://" + dataPath;
                return ret;
            }
        }

        public static string platformDir
        {
            get
            {
#if UNITY_EDITOR_WIN
                return "Win";
#endif
#if UNITY_IOS
                return "iOS";
#endif
#if UNITY_ANDROID
                return "Android";
#endif
#if UNITY_STANDALONE_WIN
                return "Win";
#endif
                return "";
            }
        }

        public static string GetOSDir()
        {
#if UNITY_STANDALONE
            return "Win";
#elif UNITY_ANDROID
            return "Android";
#elif UNITY_IPHONE
        return "iOS";
#else
        return "";
#endif
        }



        static GVars()
        {

        }
    }

    /// <summary>
    /// 针对UI层的消息类型;
    /// </summary>
    public enum UIGameMsgType
    {
        UINotify_Close_StoreItemInfo,                           // 关闭商城信息弹框;
        UINotify_Open_StorePanel,                               // 打开商城界面;
        UINotify_Close_StorePanel,                              // 关闭商城界面;
        UINotify_Open_CashPanel,                                // 打开兑奖界面
        UINotify_Open_RechargePanel,                            // 打开充值界面;
        UINotify_Open_BuyShopItems,                             // 打开购买商店物品界面;
        UINotify_Open_ContinueBuyShopItems,                     // 打开继续购买商店物品界面;
        UINotify_Open_LotteryCityPanel,                         // 打开彩票城界面;
        UINotify_Close_LotteryCityPanel,                        // 关闭彩票城界面;
        UINotify_Open_BuyLotteryItems,                          // 打开购买彩票城物品界面;
        UINotify_Open_SafeBoxPwdCheckPanel,                     // 打开保险柜密码设置界面;
        UINotify_Open_SafeBoxPanel,                             // 打开保险柜界面;
        UINotify_Open_ReveiveAddressPanel,                      // 打开接收地址界面;
        UINotify_Open_UserCompletePanel,                        // 打开用户资料是否完善界面;
        UINotify_Fresh_GoldAndLottery,                          // 更新玩家金币和彩票数量;
        UINotify_Fresh_OwnedProps,                              // 更新玩家拥有的道具;
        UINotify_Fresh_UserInfo,                                // 更新玩家头像名称等级等信息;
        UINotify_Click_RedBonus,                                // 点击红包操作;
        UINotify_FreshDealer,                                   // 更新大厅荷官
        UINotify_Fresh_LoginUserName,                           // 注册成功后，更新登录界面用户名
        UINotify_Reset_UserInfoPanel,                           // 切换后重置个人信息界面
        UINotify_Fresh_UserInfoPanel,                           // 进入主界面后更新个人信息界面
        UINotify_Allow_LoadingFinish,                           // 允许loading结束，进入主界面
        UINotify_Register_Invoke,                                   //注册时 询问是否注册成功
        UINotify_Fresh_UseCampaignPanel,                    //刷新活动界面
        UINotify_Fresh_UseMailSign,                             //刷新未读邮件个数
        UINotify_Fresh_SharePanel,                                  //刷新分享界面
        UINotify_Fresh_ShareRedSign,                                //刷新分享红点状态
        UINotify_Fresh_CampRedSign,                                 //刷新活动红点状态
        UINotify_Fresh_ProperSecuritySign,                          //刷新财产保护红点状态
    }

    /// <summary>
    /// 玩家已有的商城道具
    /// </summary>
    public class ownShopProp 
    {
        public int Id;  //道具id
        public long RemainTime; //剩余时间
        public bool IsShow; //是否需要弹框提示将要到期

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="id"></param>
        /// <param name="remainTime"></param>
        /// <param name="ishow"></param>
        public ownShopProp(int id,System.Int64 remainTime,bool ishow)
        {
            Id = id;
            RemainTime = remainTime;
            IsShow = ishow;
        }
    }

    /*
     * 术语表：glossary table
     * 红包雨 BonusRain
     * 子游戏 InnerGame
     * 美人荷官 BeautyDealer
     * 
     */


    /*
     * 图层说明关系
     * 视口外蒙黑层 layer order: 999
     * 弹框 layer order: 99
     * 断线提示框 layer order:98
     * 红包雨 layer order: 80
     * 
     * 
     * 
     * 
     */
}

