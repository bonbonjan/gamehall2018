﻿using UnityEngine;
using System.Collections;

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using WMCY.GameHall.Net;

namespace WMCY.GameHall
{
    public class NoticeTipController : MonoBehaviour
    {
        [SerializeField]
        Text m_textMsg;
        Queue<string> m_msgQueue;
        Font m_font;
        bool isInited = false;

        void Awake()
        {
            //if (_instance != null) return;
            //SetInstance(this);
            PreInit();
        }

        void Start()
        {
        }

        public void PreInit()
        {
            if (isInited) return;
            //NetManager.GetInstance().RegisterHandler("scrollMessage", HandleNetMsg_Notice); //注册滚动消息
            m_msgQueue = new Queue<string>();
            m_font = m_textMsg.font;
            isInited = true;
        }

        void Update()
        {

        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        bool m_isScrolling = false;
        public IEnumerator Scrolling()
        {
            float beginX = 450;
            float leftX = -400;
            while (m_msgQueue.Count > 0)
            {
                float duration = 10f;
                float speed = 200;
                int loop = 3;
                string msg = m_msgQueue.Dequeue();
                m_textMsg.text = msg;
                float textWdith = GetTextWidth();
                Debug.Log(string.Format("textWdith: {0}", textWdith));
                Vector3 pos = m_textMsg.rectTransform.localPosition;
                Debug.Log(string.Format("localPosition: {0}, positon: {1}, pivot: {2}", pos, m_textMsg.rectTransform.position, m_textMsg.rectTransform.pivot));
                float distance = beginX - leftX + textWdith + 10f;
                duration = distance / speed;
                m_isScrolling = true;
                while (loop-- > 0)
                {
                    Debug.Log(loop);
                    m_textMsg.rectTransform.localPosition = new Vector3(beginX, pos.y, pos.z);
                    m_textMsg.rectTransform.DOLocalMoveX(beginX - distance, duration).SetEase(Ease.Linear);
                    yield return new WaitForSeconds(duration);
                }
                //float x = m_textMsg.
                //m_textMsg.rectTransform.DOLocalMoveX();
                yield return null;
            }
            m_isScrolling = false;
            Hide();
            yield break;
        }

        public float GetTextWidth()
        {
            return m_textMsg.preferredWidth;
            float width = 0f;
            string msg = "测试";
            Font font = m_textMsg.font;
            font.RequestCharactersInTexture(msg);
            CharacterInfo characterInfo;
            for (int i = 0; i < msg.Length; i++)
            {
                font.GetCharacterInfo(msg[i], out characterInfo);
                width += characterInfo.advance;
            }
            return width;
        }

        public void AddMessage(string msg)
        {
            m_msgQueue.Enqueue(msg);
            if (!gameObject.activeSelf)
            {
                Show();
            }
            if (m_isScrolling) return;
            AppManager.Get().StartCoroutine(Scrolling());
        }

        public void ChangeScene()
        {
            float posY = 0f;
            switch ("")
            {
                case "MajorGame":
                    posY = 314;
                    break;
                case "MaryGame":
                    posY = 227;
                    break;
                case "DiceGame":
                    posY = 250;
                    break;
                case "RoomSelectionView":
                    posY = 250;
                    break;
                case "DeskSelectionView":
                    posY = 250;
                    break;
                default:
                    posY = 250;
                    break;
            }
            var position = transform.localPosition;
            transform.localPosition = new Vector3(position.x, posY, position.z);
        }

        public void HandleNetMsg_Notice(object[] args)
        {
            Debug.Log(LogHelper.NetHandle("HandleNetMsg_Notice"));
            Dictionary<string, object> dic = args[0] as Dictionary<string, object>;
            string msg = (string)dic["content"];
            Debug.Log(LogHelper.NetHandle(string.Format("msg: {0}", msg)));
            AddMessage(msg);
        }

    }
}