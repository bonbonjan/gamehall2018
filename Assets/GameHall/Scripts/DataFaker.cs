﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace WMCY.GameHall
{
    public static class DataFaker
    {

        public static void MakeFakeUser()
        {
            Debug.Log(LogHelper.Magenta("*** DataFaker:MakeFakeUser ***"));

            Dictionary<string, object> user = new Dictionary<string, object>();
            user.Add("id", 12345);
            user.Add("username", "FakeUser");
            user.Add("nickname", "FakeUser nick");
            user.Add("password", "FakeUser nick");
            user.Add("sex", "shemale");
            user.Add("level", 10);
            user.Add("gameGold", 19999);
            user.Add("expeGold", 999);
            user.Add("lottery", 1);
            user.Add("photoId", 3);
            user.Add("overflow", 0);
            user.Add("type", 0);
            user.Add("security", 0);
            user.Add("safeBox", 1);
            user.Add("promoterName", "FakeUser推广员");

            GVars.user = User.CreateWithDic(user);
        }

        public static void Init()
        {
            Debug.Log(LogHelper.Magenta("*** DataFaker:Init ***"));
            MakeFakeUser();
        }



    }
}

