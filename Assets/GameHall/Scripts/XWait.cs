﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class XWait
{
    List<float> _list;
    MonoBehaviour _mb;
    float _timeCount;
    float _totalTime;
    float _elapsedTime;
    public XWait(MonoBehaviour mb)
    {
        _list = new List<float>();
        _mb = mb;
        _timeCount = 0;
        _totalTime = 0;
        //elapsedTime = 0;
    }

    public XWait Begin()
    {
        _elapsedTime = Time.time;
        return this;
    }

    public float Stop()
    {
        float ret = Time.time - _elapsedTime;
        Debug.Log(string.Format("elapsed: {0}s, totalWait: {1}s", ret, _totalTime));
        return ret;
    }

    public Coroutine WaitForSeconds(float sec,bool hasAdd= false)
    {
        Coroutine co = _mb.StartCoroutine(_wait(sec,hasAdd));

        return co;
    }

    public bool TestShouldYield(float sec)
    {
        _timeCount += sec;
        return _timeCount> _totalTime;
    }

    private IEnumerator _wait(float sec,bool hasAdd = false)
    {

        if (!hasAdd) _timeCount += sec;

        while (_totalTime < _timeCount)
        {
            _totalTime += Time.deltaTime;
            yield return null;
        }

        //Debug.Log("totalTime " + _totalTime);
        //Debug.Log("_timeCount " + _timeCount);
        //yield break;
    }

    private float _getTotalTime()
    {

        return 0f;
    }
}