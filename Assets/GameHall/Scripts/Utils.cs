﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Collections.Generic;

public class Utils
{

    /**
 * 获取等级名称
 */
    public static string GetLevelName(int level, string lang = "zh")
    {
        string[] names = { "新手", "学徒", "新秀", "新贵", "高手", "达人", "精英", "专家", "大师", "宗师", "盟主", "传奇", "赌王", "赌圣", "赌神", "至尊" };
        string[] names_en = { "Novice", "Trainee", "Talent", "Upstart", "Ace", "Artist", "Elite", "Expert", "Master", "Godfather", "Chief", "Legend", "King", "Saint", "God", "Extreme" };
        //string[] names_en = { "新手", "学徒", "新秀", "新贵", "高手", "达人", "精英", "专家", "大师", "宗师", "盟主", "传奇", "赌王", "赌圣", "赌神", "至尊" };

        if (level == 0)
        {
            if (lang == "en")
                return names_en[0];
            else
                return names[0];
        }

        if (lang == "zh")
            return names[level - 1];
        else
            return names_en[level - 1];
    }

    public static void TrySetActive(GameObject go, bool active)
    {
        if (go != null)
            go.SetActive(active);
    }

    public static void TrySetText(UnityEngine.UI.Text text, string str)
    {
        if (text != null)
            text.text = str;
    }

    public static int[,] GetRandomMatrix3x5()
    {
        int[,] matrix = new int[3, 5];
        for (int row = 0; row < 3; row++)
        {
            for (int col = 0; col < 5; col++)
            {
                //int index = row * 5 + col;
                matrix[row, col] = Random.Range(1, 10);
            }
        }
        return matrix;
    }

    public static string PrintIntArray(int[] array, string separator = ", ")
    {
        string ret = "[";
        int length = array.Length;
        for (int i = 0; i < length; i++)
        {
            ret += i == length - 1 ? array[i].ToString() : array[i] + separator;
        }
        ret += "]";
        return ret;
    }

    public static string PrintInt2DArray(int[,] array, bool multiLine = false)
    {
        //Debug.Log(string.Format("array.GetUpperBound(0): [{0}]", array.GetUpperBound(0)));
        //Debug.Log(string.Format("array.GetUpperBound(1): [{0}]", array.GetUpperBound(1)));
        //Debug.Log(string.Format("array.GetLength(0): [{0}]", array.GetLength(0)));
        //Debug.Log(string.Format("array.GetLength(1): [{0}]", array.GetLength(1)));
        //Debug.Log(string.Format(" array.Length: [{0}]", array.Length));

        string ret = multiLine ? "[\n" : "[";
        int length = array.Length;
        for (int row = 0; row <= array.GetUpperBound(0); row++)
        {
            string rowStr = multiLine ? "  [" : "[";
            for (int col = 0; col <= array.GetUpperBound(1); col++)
            {
                rowStr += col == array.GetUpperBound(1) ? array[row, col].ToString() : array[row, col] + ", ";
            }
            rowStr += "]";
            ret += rowStr;
            ret += row == array.GetUpperBound(0) ? "" : ",";
            ret += multiLine ? "\n" : "";
        }
        ret += "]";
        return ret;
    }

    public static string PrintInt2DJaggedArray(int[][] array, bool multiLine = false)
    {
        //Debug.Log(string.Format("array.GetUpperBound(0): [{0}]", array.GetUpperBound(0)));
        //////Debug.Log(string.Format("array.GetUpperBound(1): [{0}]", array.GetUpperBound(1)));
        //Debug.Log(string.Format("array.GetLength(0): [{0}]", array.GetLength(0)));
        //////Debug.Log(string.Format("array.GetLength(1): [{0}]", array.GetLength(1)));
        //Debug.Log(string.Format(" array.Length: [{0}]", array.Length));

        string ret = multiLine ? "[\n" : "[";
        int length = array.Length;

        for (int row = 0; row < length; row++)
        {
            string rowStr = multiLine ? "  [" : "[";
            int[] innerArray = array[row];
            int innerLength = innerArray.Length;
            //Debug.Log(innerLength);
            for (int col = 0; col < innerLength; col++)
            {
                rowStr += col == innerLength - 1 ? innerArray[col].ToString() : innerArray[col] + ", ";
            }
            rowStr += "]";
            ret += rowStr;
            ret += row == length - 1 ? "" : ",";
            ret += multiLine ? "\n" : "";
        }
        ret += "]";
        return ret;
    }

    public static int[][] ConvertInt2DArrayToJagged(int[,] inArray)
    {
        int length1 = inArray.GetLength(0);
        int length2 = inArray.GetLength(1);
        int[][] ret = new int[length1][];
        for (int i = 0; i < length1; i++)
        {
            int[] array = new int[length2];
            for (int j = 0; j < length2; j++)
            {
                array[j] = inArray[i, j];
            }
            ret[i] = array;
        }
        return ret;
    }

    public static int[,] ConvertIntJaggedTo2DArray(int[][] inArray, int dimension0Size, int dimension1Size)
    {
        int[,] ret = new int[dimension0Size, dimension1Size];
        for (int i = 0; i < dimension0Size; i++)
        {
            for (int j = 0; j < dimension1Size; j++)
            {
                ret[i, j] = inArray[i][j];
            }
        }
        return ret;
    }

    //public static void Delayed(float time)
    //{

    //}

    

    public static IEnumerator DelayCall(float delay, System.Action action)
    {
        if (delay > 0f)
        {
            yield return new WaitForSeconds(delay);
        }
        action();
    }

    public static IEnumerator WaitCall( System.Func<bool> judge, System.Action action)
    {
        while (judge() == false)
        {
            yield return null;
        }
        action();
    }

    public static T[] ConvertObjectToArray<T>(object obj)
    {
        object[] objs = obj as object[];
        int length = objs.Length;
        T[] ret = new T[length];
        for (int i = 0; i < length; i++)
        {
            ret[i] = (T)objs[i];
        }
        return ret;
    }


    public static T[][] ConvertObjectToJaggedArray<T>(object obj)
    {
        object[] objs = obj as object[];
        int length = objs.Length;
        T[][] ret = new T[length][];
        for (int i = 0; i < length; i++)
        {
            object[] objs2 = objs[i] as object[];
            int length2 = objs2.Length;
            ret[i] = new T[length2];
            for (int j = 0; j < length2; j++)
            {
                ret[i][j] = (T)objs2[j];
            }
        }

        return ret;
    }

    public static T[,] ConvertObjectTo2DArray<T>(object obj, int innerSize)
    {
        object[] objs = obj as object[];
        int length = objs.Length;
        T[,] ret = new T[length, innerSize];
        for (int i = 0; i < length; i++)
        {
            object[] objs2 = objs[i] as object[];
            int length2 = objs2.Length;
            if (length2 != innerSize)
            {
                Debug.LogError("innerSize is not correct!");
            }
            for (int j = 0; j < innerSize; j++)
            {
                ret[i, j] = (T)objs2[j];
            }
        }

        return ret;
    }

    //public static System.Array 
    public static IEnumerator WaitFrameAction(int frame, System.Action action)
    {
        yield return frame;
        action();
    }

    public static IEnumerator WaitSecondsAction(float seconds, System.Action action)
    {
        //Debug.Log("seconds: " + seconds);
        yield return new WaitForSeconds(seconds);
        action();
    }

    public static IEnumerator WaitSecondsAction(float seconds, System.Action<int> action, int param1, XWait xwait = null)
    {
        //Debug.Log("seconds: " + seconds);
        if (xwait == null)
        {
            yield return new WaitForSeconds(seconds);
        }
        else
        {
            yield return xwait.WaitForSeconds(seconds);
        }
        action(param1);
    }
}


