﻿/*
 *  author: jiangwen
 *  creation date: 2016/3/30
 *  modified by: jiangwen
 *  modified date: 2016/5/16
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using WMCY.GameHall.Net;

namespace WMCY.GameHall
{
    public class RankPanelController : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_rankButton;
        public static int rankButtonCount = GVars.orders.Length + 2;
        [SerializeField]
        private Transform grid;//按钮Grid

        public GameObject[] rankContent;//等级和财富Content
        public GameObject[] gameRankContent;//游戏的content
        //public Sprite[] spriteHead;//头像图片
        [SerializeField]
        private GameObject m_rankGrid;//等级和财富Grid
        [SerializeField]
        private GameObject m_gameRankGrid;//游戏的grid
        [SerializeField]
        private int m_gameIndex = 0;//当前选择的排行榜
        //[SerializeField]
        //private int m_dwmType = 1;
        [SerializeField]
        private GameObject m_DayWeekMonthButton;//日周月排行
        [SerializeField]
        private UserIconDataConfig m_userIconDataConfig;
        [SerializeField]
        private InnerGameConfig m_InnerGameConfig;

        List<GameObject> rankButtonGroup = new List<GameObject>();

        void Awake()
        {
            grid = this.transform.Find("Rank_bg/GameTypeButton/GameTypeButton_bg/Grid");

            NetManager.GetInstance().RegisterHandler("getRankList", HandleNetMsg_GameRankType);//注册请求游戏类型、财富、等级
        }

        void Init()
        {
            for (int i = 0; i < rankButtonCount; i++)
            {
                GameObject go = Instantiate(m_rankButton, Vector3.zero, Quaternion.identity) as GameObject;
                rankButtonGroup.Add(go);
                go.name = i.ToString();
                if (i == 0)
                {
                    go.transform.Find("Text").GetComponent<Text>().text = GVars.language == "zh" ? "财富榜" : "Wealth list";
                    go.transform.Find("Image/Text").GetComponent<Text>().text = GVars.language == "zh" ? "财富榜" : "Wealth list";
                }
                else if (i == 1)
                {
                    go.transform.Find("Text").GetComponent<Text>().text = GVars.language == "zh" ? "等级榜" : "Level list";
                    go.transform.Find("Image/Text").GetComponent<Text>().text = GVars.language == "zh" ? "等级榜" : "Level list";
                }
                else
                {
                    go.transform.Find("Text").GetComponent<Text>().text = GVars.language == "zh" ? m_InnerGameConfig[GVars.orders[i - 2]].name_cn : m_InnerGameConfig[GVars.orders[i - 2]].name_en;
                    go.transform.Find("Image/Text").GetComponent<Text>().text = GVars.language == "zh" ? m_InnerGameConfig[GVars.orders[i - 2]].name_cn : m_InnerGameConfig[GVars.orders[i - 2]].name_en;
                }

                go.transform.Find("Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;
                go.transform.Find("Image/Text").GetComponent<Text>().font = GVars.language == "zh" ? AppManager.Get().zh_font : AppManager.Get().en_font;

                go.transform.SetParent(grid);
                go.transform.localScale = new Vector3(1, 1, 1);
                go.transform.localPosition = new Vector3(0, 0, 0);
                //go.GetComponent<RankButtonItem>().OnSendSelect = InitRankItem;
                //给生成的按钮添加点击事件
                go.GetComponent<Button>().onClick.AddListener(() =>
                {
                    Debug.Log("go.name: " + go.name);
                    int index = int.Parse(go.name);
                    m_gameIndex = index < 2 ? index + 1 : m_InnerGameConfig[GVars.orders[index - 2]].rankType;//int.Parse(go.name.Substring(go.name.Length - 1, 1));
                    rankButtonToggle(index);
                    InitRankItem(m_gameIndex);
                });

            }
            rankButtonToggle(0);
            grid.GetComponent<RectTransform>().sizeDelta = new Vector2(227, rankButtonCount * 111);
            grid.transform.localPosition = new Vector3(0, (4 - rankButtonCount) * 111 - 85, 0);
        }

        void OnEnable()
        {
            Debug.Log("OnEnable");
            foreach (var rb in rankButtonGroup)
            {
                Destroy(rb);
            }
            rankButtonGroup.Clear();

            Init();
        }

        //发送请求日、周、月排行
        public void OnBtnClick_ChangeType(int index)
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            //m_dwmType = index + 1;
            NetManager.GetInstance().Send("gcuserService/getRankList", new object[] { m_gameIndex, index + 1 });//           
        }

        //发送请求游戏类型、财富、等级
        public void InitRankItem(int index)
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            m_DayWeekMonthButton.transform.Find("DayButton").GetComponent<Toggle>().isOn = true;
            m_DayWeekMonthButton.transform.Find("WeekButton").GetComponent<Toggle>().isOn = false;
            m_DayWeekMonthButton.transform.Find("MonthButton").GetComponent<Toggle>().isOn = false;
            Debug.Log("fasonglqingqiubaowen: " + index);
            NetManager.GetInstance().Send("gcuserService/getRankList", new object[] { index, 1 });//      
        }

        private void HandleNetMsg_GameRankType(object[] objs)
        {
            Dictionary<string, object> rankScore = objs[0] as Dictionary<string, object>;
            int type = (int)rankScore["type"];
            Debug.Log("type: " + type);

            if (type < 3)
            {

                object[] rankList = (object[])rankScore["rankList"];
                int length = rankList.Length;

                if (length == 0) return;
                WealthLevel[] wl = new WealthLevel[length];
                for (int i = 0; i < length; i++)
                {
                    wl[i] = WealthLevel.CreateWithDic((Dictionary<string, object>)rankList[i]);
                }
                //List<TopRank> tre = new List<TopRank>();WealthLevel
                //tre = rankScore["TrList"] as List<TopRank>;
                ShowWealthLevel(wl, type);
            }
            else if (type > 2)
            {
                //DayWeekMonthButton.transform.Find("DayButton").GetComponent<Toggle>().isOn = dwmType == 1?true:false;
                //DayWeekMonthButton.transform.Find("WeekButton").GetComponent<Toggle>().isOn = dwmType == 2 ? true : false;
                //DayWeekMonthButton.transform.Find("MonthButton").GetComponent<Toggle>().isOn = dwmType == 3 ? true : false;


                object[] rankList = (object[])rankScore["rankList"];
                int length = rankList.Length;

                //if (length == 0) return;
                TopRank[] top = new TopRank[length];
                for (int i = 0; i < length; i++)
                {
                    top[i] = TopRank.CreateWithDic((Dictionary<string, object>)rankList[i]);
                }
                //List<TopRank> tre = new List<TopRank>();
                //tre = rankScore["TrList"] as List<TopRank>;
                ShowGameRank(top);
            }
        }

        private void ShowWealthLevel(WealthLevel[] wl, int type)
        {
            int count = 0;
            //for(int i=0;i<wl.Length;i++)
            //{
            //    if(wl[i].gameGoldOrLevel==0)
            //    {
            //        count++;
            //    }
            //}


            int length = wl.Length - count;

            m_rankGrid.SetActive(true);
            m_gameRankGrid.SetActive(false);
            m_DayWeekMonthButton.SetActive(false);

            if (length <= 4)
            {
                m_rankGrid.transform.Find("Grid").GetComponent<RectTransform>().sizeDelta = new Vector2(953, 495);
            }
            if (length > 4)
            {
                m_rankGrid.transform.Find("Grid").GetComponent<RectTransform>().sizeDelta = new Vector2(953, 495 + (length - 4) * 75);
                m_rankGrid.transform.Find("Grid").GetComponent<RectTransform>().localPosition = new Vector3(0, -80 * (length - 4), 0);
            }
            foreach (GameObject go in rankContent)
            {
                go.SetActive(true);
            }

            for (int i = 0; i < rankContent.Length; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    rankContent[j].GetComponent<RankItem>().Init(m_userIconDataConfig, wl[j], type);
                }
                for (int k = length; k < rankContent.Length; k++)
                {
                    rankContent[k].SetActive(false);
                }
            }
        }

        private void ShowGameRank(TopRank[] tr)
        {
            Debug.Log("aaaaaaaaaaaaaaaaaaa");
            int length = tr.Length;

            m_rankGrid.SetActive(false);
            m_gameRankGrid.SetActive(true);
            m_DayWeekMonthButton.SetActive(true);

            if (length <= 4)
            {
                m_gameRankGrid.transform.Find("GameGrid").GetComponent<RectTransform>().sizeDelta = new Vector2(953, 428);
            }
            if (length > 4)
            {
                m_gameRankGrid.transform.Find("GameGrid").GetComponent<RectTransform>().sizeDelta = new Vector2(953, 428 + (length - 4) * 90);
                m_gameRankGrid.transform.Find("GameGrid").GetComponent<RectTransform>().localPosition = new Vector3(0, -65 * (length - 4), 0);
            }
            foreach (GameObject go in gameRankContent)
            {
                go.SetActive(true);
            }

            for (int i = 0; i < gameRankContent.Length; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    gameRankContent[j].GetComponent<GameRankItem>().Init(tr[j]);
                }
                for (int k = length; k < gameRankContent.Length; k++)
                {
                    gameRankContent[k].SetActive(false);
                }
            }
        }

        public void rankButtonToggle(int i)
        {
            foreach (GameObject go in rankButtonGroup)
            {
                go.transform.Find("Image").gameObject.SetActive(false);
            }

            rankButtonGroup[i].transform.Find("Image").gameObject.SetActive(true);
        }

        //IEnumerator DelayCall(float delay, System.Action action)
        //{
        //    yield return  new  WaitForSeconds(delay);
        //    action();
        //}

        public void OnBtnClick_Close()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            Debug.Log(gameObject.name + ">>OnBtnClick_Close:");
            rankButtonToggle(0);
            foreach (GameObject go in rankContent)
            {
                go.SetActive(true);
            }
            foreach (GameObject go in gameRankContent)
            {
                go.SetActive(true);
            }
            gameObject.SetActive(false);
        }

        public void OnBtnClick_Return()
        {
            Debug.Log(gameObject.name + ">>OnBtnClick_Return:");
            gameObject.SetActive(false);

        }
    }
}