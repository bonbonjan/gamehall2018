﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using WMCY.GameHall.Net;
using WMCY.GameHall.Common;

namespace WMCY.GameHall
{


    public class RechargePanelController : MonoBehaviour
    {
        void Awake()
        {
            Debug.Log("<color=green>Awake</color>");
            
        }

        void Start()
        {
            NetManager.GetInstance().RegisterHandler("recharge", HandleNetMsg_Recharge);
        }

        void OnEnable()
        {
            ResetView();
            //SwitchIntoEnglish();
        }

        void Update()
        {

        }

        void SwitchIntoEnglish()
        {
            if (GVars.language == "zh")
            {
                transform.Find("titleBg/title").GetComponent<Text>().text = "充值";
                transform.Find("rechargeItem/itemName").GetComponent<Text>().text = "充值数目:";
                transform.Find("rechargeItem/input/Placeholder").GetComponent<Text>().text = "输入数目";
                transform.Find("noteItem/itemName").GetComponent<Text>().text = "备注信息:";
                transform.Find("noteItem/input/Placeholder").GetComponent<Text>().text = "输入备注信息";
                transform.Find("sureBtn/Text").GetComponent<Text>().text = "确定";
            }
            else
            {
                transform.Find("titleBg/title").GetComponent<Text>().text = "Recharge";
                transform.Find("rechargeItem/itemName").GetComponent<Text>().text = "Amount:";
                transform.Find("rechargeItem/input/Placeholder").GetComponent<Text>().text = "Input Amount";
                transform.Find("noteItem/itemName").GetComponent<Text>().text = "Remark:";
                transform.Find("noteItem/input/Placeholder").GetComponent<Text>().text = "Input Remark";
                transform.Find("sureBtn/Text").GetComponent<Text>().text = "Confirm";
            }
                       
        }

        void ResetView()
        {
            transform.Find("rechargeItem/input").GetComponent<InputField>().text = "";
            transform.Find("noteItem/input").GetComponent<InputField>().text = "";
        }

        public void OnBtnClick_Close()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            gameObject.SetActive(false);
        }

        public void OnBtnClick_Sure()
        {
            string Num = transform.Find("rechargeItem/input").GetComponent<InputField>().text;
            if(Num=="")
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请填入数字" : "Please fill in the numbers");
                return;
            }
            int rechargeNum = Convert.ToInt32(Num);
            string note = transform.Find("noteItem/input").GetComponent<InputField>().text;
            if (rechargeNum > 100000 || rechargeNum < 1)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "充值额度范围为：1-100000" : "Recharge limit:1-10000");
                return;
            }
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            NetManager.GetInstance().Send("gcuserService/recharge", new object[] { rechargeNum, note });
        }

        public void RechargeNumChanged()
        {
            string num = transform.Find("rechargeItem/input").GetComponent<InputField>().text;
            transform.Find("rechargeItem/equalCurrency").GetComponent<Text>().text = string.Format(GVars.language == "zh" ? "共{0}游戏币" : "Amount to {0} game gold", (num != "" ? Convert.ToInt64(num) : 0));
            
        }

        void HandleNetMsg_Recharge(object[] objs)
        {
            gameObject.SetActive(false);
            Dictionary<string, object> obj = objs[0] as Dictionary<string, object>;
            bool isSuccess = (bool)obj["success"];
            if (isSuccess)
            {
                AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "发送成功（充值申请正在处理，稍后将提示您处理结果）" : "Send Successful！Your recharge application is being processed,later we will prompt you the result");
            }
            else
            {
                int msgcode = (int)obj["msgCode"];
                switch (msgcode)
                {
                    case 2:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号或密码错误" : "Account name or password error, please enter again");
                        break;
                    case 3:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "密码错误" : "password error, please enter again");
                        break;
                    case 6:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "账号被冻结" : "This account has been frozen");
                        break;
                    case 7:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "系统维护" : "Server maintenance");
                        break;
                    case 11:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "请勿重复申请" : "Please do not repeat the application");
                        break;
                    default:
                        AlertDialog.GetInstance().ShowDialog(GVars.language == "zh" ? "申请失败" : "processed failed");
                        break;
                }
            }
        }
    }
}