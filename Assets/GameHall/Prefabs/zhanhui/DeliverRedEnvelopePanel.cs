﻿using UnityEngine;
using System.Collections;
namespace WMCY.GameHall
{


    public class DeliverRedEnvelopePanel : MonoBehaviour
    {


        void Awake()
        {

        }

        void Start()
        {

        }

        public void OnBtnClose_Click()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            this.gameObject.SetActive(false);
        }

        public void OnBtnGetRewards_Click()
        {
            UnOpened();
        }

        public void OnBtnQQ_Click()
        {
            UnOpened();
        }

        public void OnBtnWeChat_Click()
        {
            UnOpened();
        }

        void UnOpened()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "敬请期待" : "Coming soon");
        }

    }
}