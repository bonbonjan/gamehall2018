﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace WMCY.GameHall
{


    public class CharityHallPanel : MonoBehaviour
    {

        [SerializeField]
        Transform template;
        [SerializeField]
        Transform listParent;
        [SerializeField]
        Sprite[] userIcons;
        List<Transform> itemList = new List<Transform>();
        void Awake()
        {

        }

        void Start()
        {
            //Init();
        }

        void Update()
        {

        }

        void OnEnable()
        {
            Init();
        }

        string FormatInfo(DonateEntity item)
        {
            string ret;
            if (GVars.language == "zh")
            {
                string reason = "";
                string str = item.reason;
                int rowLimit = 23;

                {
                    do
                    {
                        int pos = Mathf.Min(rowLimit, str.Length);
                        reason += str.Substring(0, pos);
                        str = str.Substring(pos);
                        if (str.Length > 0)
                            reason += "\n               ";
                    }
                    while (str.Length > 0);
                }
                Debug.LogFormat("{0},{1}", item.reason.Length, reason.Length);
                ret = string.Format("<color=#E5C752FF>姓名：{0}   性别：{1}   年龄：{2}</color>\n<color=#F0E3ACFF>地址：{3}\n资助缘由：{4}\n公开账户：{5}\n已收到善款：{6}元</color>",
                    item.name, item.gender == 1 ? "男" : "女", item.age, item.address, reason, item.account, item.total);
            }
            else
            {

                string reason = "";
                string str = item.reason;
                int rowLimit = 38;

                {
                    do
                    {
                        int pos = Mathf.Min(rowLimit, str.Length);
                        reason += str.Substring(0, pos);
                        str = str.Substring(pos);
                        if (str.Length > 0)
                            reason += "\n              ";
                    }
                    while (str.Length > 0);
                }
                Debug.LogFormat("{0},{1}", item.reason.Length, reason.Length);
                ret = string.Format("<color=#E5C752FF>Name：{0}   Gender：{1}   Age：{2}</color>\n<color=#F0E3ACFF>Address：{3}\nReasons for donation：{4}\nPublic account：{5}\nHas received a donation：{6} RMB</color>",
                    item.name, item.gender == 1 ? "male" : "female", item.age, item.address, item.reason, item.account, item.total);

            }
            return ret;
        }

        void Init()
        {
            ClearAll();
            DonateEntity[] donateItems;
            if (GVars.language == "zh")
            {

                donateItems = new DonateEntity[] {
                   new DonateEntity {iconUrl="", iconId="", name="张三", gender=1, age = "45", address="XX省XX市XX区XX路XX号XX栋XX室",
                       reason ="女儿患重病，现急需手术，因手术费用巨大，无法承担其所有费用，希望好心人能够给予帮助",
                       account ="622208XXXXXXXXXXXXX    XX银行",total="123456"},
                   new DonateEntity {iconUrl="", iconId="", name="李四", gender=0, age = "36", address="XX省XX市XX区XX路XX号XX栋XX室",
                       reason ="因家里经济条件差，无力担负子女上学的费用，希望能得到社会上好心人的帮助",
                       account ="622700XXXXXXXXXXXXX     XX银行",total="123456"},
                   new DonateEntity {iconUrl="", iconId="", name="王五", gender=1, age = "37", address="XX省XX市XX区XX路XX号XX栋XX室",
                       reason ="儿子患白血病需要进行手术，因经济条件有限，无法支付手术的全部费用，希望能寻求到大家的帮助",
                       account ="622848XXXXXXXXXXXXX    XX银行",total="123456"}
                   //new DonateEntity {iconUrl="", iconId="", name="", gender="", age = "", address="",reason="",account="",total=""}

                };
            }
            else
            {
                donateItems = new DonateEntity[] {
                   new DonateEntity {iconUrl="", iconId="", name="Zhangsan", gender=1, age = "45", address="Room XX,buliding XX,No.XX,XX Road,XX Prov.,China",
                       reason ="Daughter suffering from serious illness，needed urgent operation，Because the operation cost a lot, so unable to pay all of the costs of operation，Hope that good hearted people can give help",
                       account ="622208XXXXXXXXXXXXX    XX Bank",total="123456"},
                   new DonateEntity {iconUrl="", iconId="", name="Lisi", gender=0, age = "36", address="Room XX,buliding XX,No.XX,XX Road,XX Prov.,China",
                       reason ="Family economic conditions are poor, can not afford the cost of children to school，hope to get the help of the good hearted people in society",
                       account ="622700XXXXXXXXXXXXX     XX Bank",total="123456"},
                   new DonateEntity {iconUrl="", iconId="", name="Wangwu", gender=1, age = "37", address="Room XX,buliding XX,No.XX,XX Road,XX Prov.,China",
                       reason ="Son suffering from leukemia need surgery，family economic conditions are limited, can not pay the full cost of surgery,hope to get everyone's help",
                       account ="622848XXXXXXXXXXXXX    XX Bank",total="123456"}
                   //new DonateEntity {iconUrl="", iconId="", name="", gender="", age = "", address="",reason="",account="",total=""}

                };
            }
            template.gameObject.SetActive(false);
            for (int i = 0; i < donateItems.Length; i++)
            {
                CreateItem(donateItems[i],i, i == donateItems.Length - 1);
            }
        }

        void CreateItem(DonateEntity item, int index, bool isLastOne = false)
        {
            var newItemObj = Instantiate<Transform>(template);
            newItemObj.SetParent(listParent);
            newItemObj.localScale = Vector3.one;
            newItemObj.localPosition = Vector3.zero;
            var btnDonate = newItemObj.Find("Donate");
            btnDonate.GetComponent<Button>().onClick.AddListener(() => { UnOpened(); });
            var info = newItemObj.Find("Info");
            var textCtrl = info.GetComponent<Text>();
            textCtrl.font = GVars.language == "zh" ? AppManager.Get().zh_font:AppManager.Get().en_font;
            textCtrl.text = FormatInfo(item);
            var headIcon = newItemObj.Find("headIcon");
            var headIconImage = headIcon.GetComponent<Image>();
            headIconImage.sprite = item.gender == 1 ? userIcons[1] : userIcons[0];
            var separator = newItemObj.Find("separator");
            if (isLastOne)
                separator.gameObject.SetActive(false);
            itemList.Add(newItemObj);
            newItemObj.gameObject.SetActive(true);
        }

        void ClearAll()
        {
            for (int i = 0; i < itemList.Count; i++)
            {
                Destroy(itemList[i].gameObject);
            }
            itemList.Clear();
        }

        void UnOpened()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "敬请期待" : "Coming soon");
        }

        public void OnBtnClose_Click()
        {
            SoundManager.Get().PlaySound(SoundType.Common_Click);
            this.gameObject.SetActive(false);
        }
    }

    public class DonateEntity
    {
        public string iconUrl;
        public string iconId;
        public string name;
        public int gender;
        public string age;
        public string address;
        public string reason;
        public string account;
        public string total;
    }
}