﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu()]
public class InnerGameConfig : ScriptableObject
{

    public List<InnerGame> list;
    public InnerGame this[int index]
    {
        get
        {
            return list[index];
        }
    }
}
