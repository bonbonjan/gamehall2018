﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class InnerGame{
    public string name_cn;
    public string name_en;
    public int id;
    public int rankType;
    public string icon;
    public string color;
    public Sprite spriteIcon;
    public Sprite spriteColor;
    public bool isLuaGame;
    public string RunStatusKey;//
    public int RunStatus;//游戏开发状态 0关闭 1开启
    public string action;
    public string package;//包名，表示名
    public string schema;//ios URL schema
    [System.NonSerialized]
    public InnerGameState state;
    [System.NonSerialized]
    public int totalSize;//apk或者ipa包大小，应该不会超过4G，所以不用long型
    [System.NonSerialized]
    public int savedSize;
    [System.NonSerialized]
    public float progress;
    [System.NonSerialized]
    public string url;
    [System.NonSerialized]
    public string version;
    [System.NonSerialized]
    public string latestVersion;
    [System.NonSerialized]
    public float speed;
    [System.NonSerialized]
    public float downloadDuration;
    [System.NonSerialized]
    public float downSize;
}


public enum InnerGameState
{
    Default,//默认状态，未下载，未安装
    ReadyInstall,//已下载未安装
    Waiting,//在下载列表中，尚未下载
    Downloading,//下载中
    Installing,//安装中
    OK,//可以使用
    NeedUpdate,//已安装，但有新版本
    Paused//暂停中
}
