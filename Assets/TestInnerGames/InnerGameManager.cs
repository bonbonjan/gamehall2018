﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using BestHTTP;
using System;
using UnityEngine.Assertions;
using WMCY.GameHall;
using WMCY.GameHall.Net;


public class InnerGameManager : MonoBehaviour
{

    [SerializeField]
    InnerGameConfig m_innerGameConfig;
    [SerializeField]
    GameObject m_innerGamePrefab;
    List<InnerGameItem> m_itemList;
    Transform m_xformGrid;
    bool isInit = false;
    //[SerializeField]
    public bool debug_Uninstall = false;
    bool multiDown = true;//允许同时下载
    //#if UNITY_ANDROID
    public AndroidJavaClass jc;//调用java工程
    public AndroidJavaObject jo;
    //#endif

    private static int gameCount = 9;

    string m_userName;
    string m_pwd;
    string m_ip;
    string m_language;
    string m_downDir;
    static InnerGameManager m_instance;
    List<InnerGameItem> m_downloadTaskList;
    void Awake()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
		jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
#endif
        m_instance = this;

        m_itemList = new List<InnerGameItem>();
        m_xformGrid = transform.Find("Viewport/InnerGames");
        InitInnerGameConfig();
        Init();
    }

    void Start()
    {
        //m_itemList = new List<InnerGameItem>();
        //m_xformGrid = transform.Find("Viewport/InnerGames");
        NetManager.Get().RegisterHandler("gameLoadAddress", HandleNetMsg_CheckGameVersion);
        //Init();
        //DeleteKey();
    }

    void Update()
    {
#if UNITY_IOS && !UNITY_EDITOR
        if (shouldCheckIOSGameStatus)
        {
            StartCoroutine(_CheckIOSGameStatus());
        }
#endif
    }

    bool shouldCheckIOSGameStatus = true;
    IEnumerator _CheckIOSGameStatus()
    {
        shouldCheckIOSGameStatus = false;
        yield return new WaitForSeconds(3);
        shouldCheckIOSGameStatus = true;
        m_itemList.ForEach(item =>
        {
            CheckGameStatus(item);
        });
        yield break;
    }

    public static InnerGameManager Get()
    {
        return m_instance;
    }

    void InitInnerGameConfig()
    {
        m_innerGameConfig[12].name_cn = GVars.QianpaoName[0];
        m_innerGameConfig[12].name_en = GVars.QianpaoName[1];
        m_innerGameConfig[13].name_cn = GVars.WanpaoName[0];
        m_innerGameConfig[13].name_en = GVars.WanpaoName[1];
    }

    void DeleteKey()
    {
        for(int i=0;i<GVars.orders.Length;i++)
        {
            int GameID = m_itemList[GVars.orders[i] - 1].GetInnerGame().id;
            string savedKey = GameID + "_SavedSize";
            string totalKey = GameID + "_TotalSize";

            PlayerPrefs.DeleteKey(savedKey);
            PlayerPrefs.DeleteKey(totalKey);

            Debug.Log(string.Format("deleteKey:savedKey:{0},totalKey:{1}", savedKey,totalKey));
        }
    }

    public void Init()
    {
        if (isInit) return;
        isInit = true;
        m_instance = this;

        m_userName = "";
        m_pwd = "";
        m_ip = "";
        m_language = "0";//Chinese
        //return;
        m_downDir = Application.persistentDataPath + "/download/";
        if (Application.platform == RuntimePlatform.WindowsEditor)
            m_downDir = Path.GetFullPath(Application.dataPath + "/../download/").Replace('\\', '/'); ;
        Debug.Log("downDir: " + m_downDir);
        if (!Directory.Exists(m_downDir))
            Directory.CreateDirectory(m_downDir);
        m_downloadTaskList = new List<InnerGameItem>();

        foreach (Transform item in m_xformGrid)
        {
            Destroy(item.gameObject);
        }
        //int[] orders = new int[] {1,2,3,4,5,6,7,8,9 };
        int[] orders = GVars.orders;
        //orders = new int[] { 1, 2, 9, 8, 4, 6, 3, 7, 5 };
        //orders = new int[] { 4, 2, 5, 8, 1, 7, 3, 6, 9 };
        //if (AppManager.Get() && AppManager.Get().special)
        //    orders = new int[] { 2, 4, 5, 1, 8, 7, 9, 3, 6 };
        //for (int i = 0; i < GVars.InnerGameCount/*m_innerGameConfig.list.Count*/; i++)
        //{
        //    m_itemList.Add(null);
        //}
        for (int i = 0; i < orders.Length; i++)
        {
            var game = m_innerGameConfig.list[orders[i]];
            var item = CreateGameItem();
            item.Init(game);
            CheckGameStatus(item);
            m_itemList.Add(item);
            //m_itemList[orders[i]] = item;
            //Debug.Log("m_itemList init: " + m_itemList[i].GetInnerGame().name_cn);
        }
        //UpdateGameRunStatus(runStatus);
        //foreach (var game in m_innerGameConfig.list)
        //{
        //    var item = CreateGameItem();
        //    item.Init(game);
        //    CheckGameStatus(item);
        //    m_itemList.Add(item);
        //}
        Debug.LogFormat("total game:{0} active game:{1}", m_itemList.Count, orders);

    }

    public void UpdateGameRunStatus(object args)
    {
        CheckStatus(args);
    }

    void CheckStatus(object args)
    {
        for (int i = 0; i < GVars.orders.Length; i++)
        {
            var item = m_itemList[i].GetInnerGame();
            string statusKey = item.RunStatusKey;
            if (((Dictionary<string, object>)args).ContainsKey(statusKey))
                item.RunStatus = (int)((Dictionary<string, object>)args)[statusKey];

            Debug.Log(LogHelper.Yellow(string.Format("[{0}]:{1}", item.name_cn, item.RunStatus)));
        }
    }

    public InnerGameItem CreateGameItem()
    {
        var go = Instantiate<GameObject>(m_innerGamePrefab);
        go.transform.SetParent(m_xformGrid, false);
        go.SetActive(true);
        //go.transform.localScale = Vector3.one;
        //go.transform.localPosition = Vector3.zero;
        return go.GetComponent<InnerGameItem>();
    }

    public void AndroidNoticeUnity(string packageName)
    {
        string packN = packageName.Substring(8, packageName.Length-8);
        Debug.Log("AndroidNoticeUnity packageName: " + packN);
        int CurrentGameID = 0;
        var CurrentItem=m_itemList[0];
        for(int i=0;i<GVars.orders.Length;i++)
        {
            var gameConfig = m_innerGameConfig.list[GVars.orders[i]];
            if (gameConfig.package == packN)
            {
                CurrentGameID = gameConfig.id;
                Debug.Log("gameid: " + CurrentGameID);                
            }
            Debug.Log(string.Format("packageName{0} : {1},id : {2}", i, gameConfig.package, gameConfig.id));
        }

        for (int i = 0; i < m_itemList.Count;i++ )
        {
            if (m_itemList[i].GetInnerGame().id == CurrentGameID)
            {
                CurrentItem = m_itemList[i];
                Debug.Log("CurrentItem id: " + CurrentItem.GetInnerGame().id);
            }
        }

            if (CurrentGameID == 0)
            {
                Debug.Log("异常情况！gameID不可为0");
            }
        string key_savedSize = CurrentGameID + "_SavedSize";
        string key_totalSize = CurrentGameID + "_TotalSize";
        PlayerPrefs.DeleteKey(key_savedSize);
        PlayerPrefs.DeleteKey(key_totalSize);
        CurrentItem.ChangeState(InnerGameState.OK);
        Debug.Log(string.Format("Name:{0},id:{1},state:{2}", CurrentItem.name, CurrentItem.GetInnerGame().id, CurrentItem.GetInnerGame().state));

        string filePath = m_downDir + CurrentGameID + ".apk";

#if UNITY_ANDROID
        jo.Call("DeleteInstallApk", filePath);
#endif
        //try 
        //{
        //    File.Delete(filePath);
        //}
        //catch(Exception e)
        //{
        //    Debug.Log("Exception类型: " + e);
        //}

        Debug.Log(string.Format("filePath:{0},delete", filePath));

    }

    public void CheckGameStatus(InnerGameItem item)
    {
        var game = item.GetInnerGame();
        int gameId = game.id;
        Debug.Log("CheckGameStatus: " + gameId);
        string key_savedSize = gameId + "_SavedSize";
        string key_totalSize = gameId + "_TotalSize";
        game.state = InnerGameState.Default;
        game.totalSize = 1024 * 1024;
        string filePath = m_downDir + gameId + ".apk";

        //lua游戏
        if (game.isLuaGame)
        {
            //检查存放目录、索引文件是否有效

            //读档
            //item.ChangeState(InnerGameState.OK);
            item.ChangeState(InnerGameState.Default);
            StartCoroutine(CheckGameStatus_Lua(item));
            item.gameObject.SetActive(false);//8.3.2隐藏消消乐
            return;
        }
        //TODO:检查游戏是否已经安装
#if UNITY_EDITOR
        game.state = InnerGameState.OK;
        item.ChangeState(game.state);
        return;
#endif

#if UNITY_IOS
		if(IOS_Agent.CanOpenUrl(game.schema))
		{
			game.state = InnerGameState.OK;
			Debug.Log(game.name_cn+" is Ok");
		}
		else
		{
			game.state = InnerGameState.Default;
		}
		item.ChangeState(game.state);
		return;
#endif
#if UNITY_ANDROID
        if (jc != null)
        {
            string versionName = "";
            string latestVersion = "";
            try
            {
                AndroidJavaObject _jo = jo.Call<AndroidJavaObject>("getTargetPackageInfo", game.package);
                //Debug.Log(_jo);
                versionName = jo.Call<string>("getPackageVersionName", _jo);
                game.version = versionName;
                //TEST
                latestVersion = versionName;
                Debug.Log(versionName);

            }
            catch (Exception e)
            {

            }
            if (versionName.Equals(""))
            {
                //未安装
                game.state = InnerGameState.Default;
                if (PlayerPrefs.HasKey(key_savedSize) && PlayerPrefs.HasKey(key_totalSize))
                {
                    int totalSize = PlayerPrefs.GetInt(key_totalSize);
                    int savedSize = PlayerPrefs.GetInt(key_savedSize);

                    Debug.Log("versionName.Equals ");

                    if (File.Exists(filePath))
                    {
                        FileInfo fileInfo = new FileInfo(filePath);

                        Debug.Log(string.Format("totalSize:{0},savedSize:{1},realSize:{2}", totalSize, savedSize, fileInfo.Length));

                        if (totalSize == fileInfo.Length)
                        {
                            game.state = InnerGameState.ReadyInstall;
                            //InstallGame(filePath);
                            return;
                        }
                    }
                    else
                    {
                        Debug.Log("File not exist!");
                    }
                }
            }
            else if (versionName.Equals(latestVersion))
            {
                //已经是最新版本
                game.state = InnerGameState.OK;
            }
            else
            {
                //需要更新
                game.state = InnerGameState.NeedUpdate;
            }

        }

        if (game.state == InnerGameState.OK)
        {
            //如果版本正确，无需进一步检测
            item.ChangeState(game.state);
            return;
        }
#endif

        //进一步检查存档、文件
        if (PlayerPrefs.HasKey(key_savedSize) && PlayerPrefs.HasKey(key_totalSize))
        {
            int totalSize = PlayerPrefs.GetInt(key_totalSize);
            int savedSize = PlayerPrefs.GetInt(key_savedSize);

            game.totalSize = totalSize;
            game.savedSize = savedSize;
            game.downSize = savedSize;
            game.progress = savedSize / (float)totalSize;
            game.state = InnerGameState.Paused;

            if (File.Exists(filePath))
            {
                FileInfo fileInfo = new FileInfo(filePath);
                Debug.Log(string.Format("saved:{0}, real:{1}", savedSize, fileInfo.Length));
                Assert.IsTrue(fileInfo.Length == savedSize);
            }
            else
            {
                goto Reset_Game;
            }
        }
        else
        {

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            goto Reset_Game;
        }



        item.ChangeState(game.state);
        return;
    Reset_Game:
        Debug.Log("Reset_Game");
        game.state = InnerGameState.Default;
        game.savedSize = 0;
        game.downSize = 0;
        game.totalSize = 1;
        PlayerPrefs.DeleteKey(key_savedSize);
        PlayerPrefs.DeleteKey(key_totalSize);
        game.progress = 0;
    }

    public IEnumerator CheckGameStatus_Lua(InnerGameItem item)
    {
        var game = item.GetInnerGame();
        int gameId = game.id;
        string url = game.url;
        Debug.Log("CheckGameStatus_Lua: " + game.id);
        //url = @"http://xingli1hao.com/watermargin/watermargin.apk";
        //url = @"http://192.168.1.64:8080/WaterMargin7.1.9.apk";
        //url = Fake_GameDownUrl(game);
        //Debug.LogFormat("url:{2}, length:{0}, index:{1}", url.Length, url.LastIndexOf('/', url.Length - 1), url);
        //string baseUrl = url.Substring(0, url.LastIndexOf('/', url.Length - 1) + 1);
        //Debug.Log("baseUrl: " + baseUrl);
        string gameName = game.action;
        string ABSaveDir = GVars.dataPath + gameName + "/" + GVars.platformDir + "/";
        Debug.Log("ABSaveDir: " + ABSaveDir);
        string ABIndexFilePath = ABSaveDir + "files.txt";
        //string listUrl = baseUrl + "files.txt";
        if (!Directory.Exists(ABSaveDir))
        {
            Directory.CreateDirectory(ABSaveDir);
        }
        //yield break;
        string listContent = "";
        //todo:检查文件是否存在，特征是否一致
        if (!File.Exists(ABIndexFilePath))
        {
            item.ChangeState(InnerGameState.Default);
            Debug.LogFormat("ABIndexFilePath:{0} not exsit", ABIndexFilePath);
            yield break;
        }


        listContent = File.ReadAllText(ABIndexFilePath, System.Text.Encoding.UTF8);
        Debug.LogFormat("listContent:{0}", listContent);
        string[] listItems = listContent.Split('\n');
        Debug.LogFormat("listItems.Length:{0}", listItems.Length);
        string[] publishInfos = listItems[0].Split('#');
        var time = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(Convert.ToInt64(publishInfos[2]));
        Debug.LogFormat("version:{0}, filecount:{1}, time:{2}", publishInfos[0], publishInfos[1], time);
        game.version = publishInfos[0];
        for (int i = 1; i < listItems.Length; i++)
        {
            if (string.IsNullOrEmpty(listItems[i])) continue;
            string[] keyValue = listItems[i].Split('|');

            string f = keyValue[0];
            string outfile = (ABSaveDir + f).Trim();
            string path = Path.GetDirectoryName(outfile);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            //string fileUrl = baseUrl + f;
            //todo:检查文件是否存在，特征是否一致
            if (!File.Exists(outfile))
            {
                item.ChangeState(InnerGameState.Default);
                Debug.LogFormat("outfile:{0} not exsit", outfile);
                yield break;
            }
        }
        item.ChangeState(InnerGameState.OK);
        yield break;
    }

    public void StartGame(InnerGameItem item)
    {
        if (item.GetInnerGame().isLuaGame)
        {
            GameLoading
            .Create()
            .SetAwakeTask(() =>
            {
                Debug.Log("awakeTask @" + Time.frameCount);
                GameLoading.Get().LoadLoadingPrefab(2 - 1);
            })
            .Load();
            GameLoading.Get().SetHallSceneName(SceneManager.GetActiveScene().name);
            AppManager.Get().OnEnterLuaGame();
            return;
        }
#if UNITY_ANDROID && !UNITY_EDITOR
        StartApp_Android(item);
#endif
#if UNITY_IOS && !UNITY_EDITOR
        //StartGame_IOS(item.GetInnerGame().id);
        StartApp_IOS(item);
#endif
#if UNITY_EDITOR
        Application.OpenURL("www.bing.cn");
#endif
    }

    public void StartGame(int gameId)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        jo.Call("startFish", m_userName, m_pwd, gameId, m_ip, m_language);
#endif
#if UNITY_IOS && !UNITY_EDITOR
        StartGame_IOS(gameId);
#endif
#if UNITY_EDITOR
        Application.OpenURL("www.bing.cn");
#endif
    }

    public void StartApp_Android(InnerGameItem item)
    {
        InnerGame game = item.GetInnerGame();
        string method = "startApp";
        string key = "userMessage";
        string userName = GVars.username;
        string pwd = GVars.pwd;
        string language = GVars.language.Equals("zh") ? "0" : "1";
        string ip = GVars.IPAddress_Game;// GVars.IPAddress;
        string action = game.action;
        string javaClassName = jo.Call<string>("getClassSimpleName");
        string javaFullClassName = jo.Call<string>("getClassFullName");
        string hallPackageName = Application.identifier;
        //hallPackageName = javaFullClassName.Replace("." + javaClassName, "");
        Debug.Log("Application.bundleIdentifier: " + Application.identifier);
        Debug.Log("action: " + game.action);
        Debug.Log("getPackageName: " + jo.Call<string>("getPackageName"));
        Debug.Log("getClassSimpleName: " + jo.Call<string>("getClassSimpleName"));
        Debug.Log("getClassFullName: " + jo.Call<string>("getClassFullName"));
        //ip = "114.215.206.19";
        //userName = "wanmei001";
        //pwd = "qwerty1";
        //method = "startApp";
        //action = "com.xingli.waterlaction";
        //if (pwd.Length == 0) pwd = "123";
        string value = GenerateStartAppValue_Android(userName, pwd, language, ip, hallPackageName);
        Debug.Log(string.Format("value: [{0}]", value));
        jo.Call(method, action, key, value, true);
    }

    public void StartApp_IOS(InnerGameItem item)
    {
        Debug.Log("StartApp_IOS");
        InnerGame game = item.GetInnerGame();
        string userName = GVars.username;
        string pwd = GVars.pwd;
        string language = GVars.language.Equals("zh") ? "0" : "1";
        string ip = GVars.IPAddress;
        Debug.Log(userName);
        Debug.Log(pwd);
        Debug.Log(language);
        Debug.Log(ip);
        string DomainName = GVars.IPAddress_Game;
        string paras = GenerateStartAppValue_IOS(DomainName, userName, pwd, language, ip);
        string headStr = game.schema;
        string url = headStr + paras;
        Debug.Log(url);
        Debug.Log(WWW.EscapeURL(url));
        Application.OpenURL(url);
        //Application.OpenURL(WWW.EscapeURL(url));
        Application.Quit();
    }

    public void StartGame_IOS(int gameId)
    {
        string headStr = "";
        //string paras = string.Format("+NAME={0}+PWD={1}+IP={2}+LAN={3}", m_userName, m_pwd, m_ip, m_language);
        string paras = string.Format("+NAME={0}+PWD={1}+IP={2}+LAN={3}", GVars.username, GVars.pwd, GVars.IPAddress, GVars.language.Equals("en") ? "1" : "0");
        string url = headStr + paras;
        switch (gameId)
        {
            case 0:
                headStr = "LuckyLion://www.xingli.com?";
                break;
            case 1:
                headStr = "MoneyFish://www.xingli.com?";
                break;
            case 2:
                headStr = "DanTiao://www.xingli.com?";
                break;
            case 3:
                headStr = "WanFish://www.xingli.com?";
                break;
            case 4:
                headStr = "BeautyFish://www.xingli.com?";
                break;
            case 5:
                headStr = "QueYiMen://www.xingli.com?";
                break;
            case 6:
                headStr = "Joy://www.xingli.com?";
                break;
            case 7:
                headStr = "WaterMargin://www.xingli.com?";
                break;
            default:
                headStr = "www.bing.cn";
                break;

        }
        Application.OpenURL(url);
        Application.Quit();

    }

    public void IsGameInstalled(int gameId)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
     jo.Call<string>("HasBeenInstall", gameId);
#endif
#if UNITY_IOS && !UNITY_EDITOR
#endif
#if UNITY_EDITOR
#endif

    }

    public void InstallGame(string path)
    {
        if (RuntimePlatform.Android == Application.platform)
            jo.Call("installApp", path);
    }

    public void UninstallGame(InnerGameItem item)
    {
        var game = item.GetInnerGame();
        if (RuntimePlatform.Android == Application.platform && item.GetInnerGame().isLuaGame == false)
        {
            jo.Call("uninstallApp", "package:" + game.package);
        }
        if (item.GetInnerGame().isLuaGame)
        {
            string gameName = game.action;
            string ABSaveDir = GVars.dataPath + gameName + "/" + GVars.platformDir + "/";
            if (Directory.Exists(ABSaveDir))
            {
                try
                {
                    Directory.Delete(ABSaveDir, true);
                    Debug.LogFormat("delete [id:{0}, name:{1}] success", game.id, game.name_cn);
                }
                catch (Exception ex)
                {
                    Debug.LogError(ex);
                }
            }
        }
        item.ChangeState(InnerGameState.Default);
    }

    HTTPRequest m_httpRequest = null;
    public void Download(InnerGameItem item)
    {
        var game = item.GetInnerGame();
        int gameId = game.id;
        string url = game.url;
        Debug.LogFormat("Download: {0}", gameId);
        //url = @"http://xingli1hao.com/watermargin/watermargin.apk";
        //url = @"http://192.168.1.64:8080/WaterMargin7.1.9.apk";
        //url = Fake_GameDownUrl(game);
        if (Application.platform != RuntimePlatform.Android && game.isLuaGame == false)
        {
            Application.OpenURL(game.url);
            return;
        }

        string filePath = m_downDir + gameId + ".apk";
        if (string.IsNullOrEmpty(url))
        {
            Debug.LogErrorFormat("empty url");
            return;
        }
        var request = new HTTPRequest(new Uri(url), DownloadCallback);
        m_httpRequest = request;
        item.SetHTTPRequest(request);
        string key_savedSize = gameId + "_SavedSize";
        //PlayerPrefs.DeleteKey(key_savedSize);//?test code
        bool newDown = true;

        if (PlayerPrefs.HasKey(key_savedSize))
        {
            if(File.Exists(filePath))
            {
                var fileInfo = new FileInfo(filePath);
                var savedSize = PlayerPrefs.GetInt(key_savedSize);
                if (null != fileInfo)
                {
                    Debug.LogFormat("fileInfo.Length:{0}, savedSize:{1}", fileInfo.Length, savedSize);
                    if (fileInfo.Length == savedSize)
                    {
                        newDown = false;
                    }
                }
            }
        }

        if (newDown == false)
        {
            Debug.Log(string.Format("恢复下载游戏[id:{0},name:{1}] total:{2}, progress:{3:0.00}, saved: {4}", gameId, game.name_cn, game.totalSize, game.progress, game.savedSize));
            request.SetRangeHeader(PlayerPrefs.GetInt(key_savedSize));
        }
        else
        {
            File.Delete(filePath);
            Debug.Log(string.Format("下载游戏[id:{0},name:{1},url:{2}]", gameId, game.name_cn, game.url));
            PlayerPrefs.SetInt(key_savedSize, 0);
        }


#if !BESTHTTP_DISABLE_CACHING && (!UNITY_WEBGL || UNITY_EDITOR)
        request.DisableCache = true;
#endif
        request.UseStreaming = true;
        request.StreamFragmentSize = 1 * 1024 * 1024;
        request.Tag = item;
        request.OnProgress = OnDownloadProgress;
        request.Send();
        item.lastDownloaded = 0;
        item.lastProgressTime = 0;
    }

    public IEnumerator Download_AB(InnerGameItem item)
    {
        var game = item.GetInnerGame();
        int gameId = game.id;
        string url = game.url;
        Debug.Log("Download_AB: " + game.id);
        //url = @"http://xingli1hao.com/watermargin/watermargin.apk";
        //url = @"http://192.168.1.64:8080/WaterMargin7.1.9.apk";
        //url = Fake_GameDownUrl(game);
#if UNITY_EDITOR
        url = url.Replace("Android", "Win");
#endif
        Debug.LogFormat("url:{2}, length:{0}, index:{1}", url.Length, url.LastIndexOf('/', url.Length - 1), url);
        string baseUrl = url.Substring(0, url.LastIndexOf('/', url.Length - 1) + 1);
        Debug.Log("baseUrl: " + baseUrl);
        string gameName = game.action;
        string ABSaveDir = GVars.dataPath + gameName + "/" + GVars.platformDir + "/";
        Debug.Log("ABSaveDir: " + ABSaveDir);
        string ABIndexFilePath = ABSaveDir + "files.txt";
        string listUrl = baseUrl + "files.txt";
        if (!Directory.Exists(ABSaveDir))
        {
            Directory.CreateDirectory(ABSaveDir);
        }
        //yield break;
        string listContent = "";
        //todo:检查文件是否存在，特征是否一致
        if (File.Exists(ABIndexFilePath))
        {
            File.Delete(ABIndexFilePath);
        }
        //开始下载此文件
        if (!File.Exists(ABIndexFilePath))
        {
            WWW wwwList = new WWW(listUrl);
            yield return wwwList;
            if (wwwList.error != null)
            {
                Debug.LogError("wwwList cannot download");
                yield break;
            }
            File.WriteAllBytes(ABIndexFilePath, wwwList.bytes);
            listContent = wwwList.text;
        }
        else
        {
            listContent = File.ReadAllText(ABIndexFilePath, System.Text.Encoding.UTF8);
        }
        Debug.LogFormat("listContent:{0}", listContent);
        string[] listItems = listContent.Split('\n');
        Debug.LogFormat("listItems.Length:{0}", listItems.Length);
        string[] publishInfos = listItems[0].Split('#');
        var time = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(Convert.ToInt64(publishInfos[2]));
        Debug.LogFormat("version:{0}, filecount:{1}, time:{2}", publishInfos[0], publishInfos[1], time);
        game.version = publishInfos[0];
        for (int i = 1; i < listItems.Length; i++)
        {
            if (string.IsNullOrEmpty(listItems[i])) continue;
            string[] keyValue = listItems[i].Split('|');

            string f = keyValue[0];
            string outfile = (ABSaveDir + f).Trim();
            string path = Path.GetDirectoryName(outfile);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string fileUrl = baseUrl + f;
            //todo:检查文件是否存在，特征是否一致
            if (File.Exists(outfile))
            {
                File.Delete(outfile);
            }
            //开始下载此文件
            WWW www2 = new WWW(fileUrl); yield return www2;
            if (www2.isDone)
            {
                File.WriteAllBytes(outfile, www2.bytes);
            }
            if (www2.error != null)
            {
                Debug.LogErrorFormat("file item [{0}] cannot download", f);
            }
        }

        item.ChangeState(InnerGameState.OK);

        yield break;
    }

    private void AddToWaitDLTask(InnerGameItem gameItem)
    {
        m_downloadTaskList.Add(gameItem);
    }


    private void DownloadAbort()
    {
        var request = m_httpRequest;
        if (request != null && request.State < HTTPRequestStates.Finished)
        {
            request.OnProgress = null;
            request.Callback = null;
            request.Abort();
        }
    }

    private bool IsDownloading()
    {
        return m_httpRequest == null ? false : true;
    }

    public void OnBtnClick_GameItem(InnerGameItem item)
    {
        var game = item.GetInnerGame();
        int gameId = game.id;
        //todo:根据状态选择进行不同的行为
        if (game.state == InnerGameState.Default)
        {
            //Debug.Log("");
            //return;

            //if (multiDown == false && IsDownloading())//只允许一个个下载
            //{
            //    item.ChangeState(InnerGameState.Waiting);
            //    AddToWaitDLTask(item);
            //}
            //else
            //{
            //    item.ChangeState(InnerGameState.Downloading);
            //    if (game.isLuaGame)
            //    {
            //        StartCoroutine(Download_AB(item));
            //    }
            //    else
            //    {
            //        Download(item);
            //    }
            //}


            Debug.Log(LogHelper.Red("game.name_cn: " + game.name_cn + " game.id: " + game.id + "  game.RunStatus: " + game.RunStatus));

            if (game.RunStatus == 0)
            {
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "游戏暂未开放" : "The game is not yet open");
                return;
            }


            string version = "-1";
            Send_CheckGameVersion(game.id, version);
            return;

        }
        else if(game.state==InnerGameState.ReadyInstall)
        {
            Debug.Log("InnerGameState.ReadyInstall");
            string key_savedSize = gameId + "_SavedSize";
            string key_totalSize = gameId + "_TotalSize";
            string filePath = m_downDir + gameId + ".apk";
            Debug.Log(string.Format("key_savedSize:{0},key_totalSize:{1}", PlayerPrefs.HasKey(key_savedSize), PlayerPrefs.HasKey(key_totalSize)));
            Debug.Log(gameId + ".apk是否存在:" + File.Exists(filePath));
            if (PlayerPrefs.HasKey(key_savedSize) && PlayerPrefs.HasKey(key_totalSize))
            {
                int totalSize = PlayerPrefs.GetInt(key_totalSize);
                int savedSize = PlayerPrefs.GetInt(key_savedSize);

                if (File.Exists(filePath))
                {
                    FileInfo fileInfo = new FileInfo(filePath);

                    Debug.Log(string.Format("APKExist totalSize:{0},savedSize:{1},realSize:{2}", totalSize, savedSize, fileInfo.Length));

                    if (totalSize == fileInfo.Length)
                    {
                        InstallGame(filePath);
                        Debug.Log("InstallGame: " + gameId);
                        return;
                    }
                }
                else
                {
                    Debug.Log("File Not Exists!");
                }
            }
        }
        else if (game.state == InnerGameState.Waiting)
        {
            Debug.Log(string.Format("游戏[id:{0},name:{1}] 已经存在于下载列表", game.id, game.name_cn));
            AlertDialog.Get().ShowDialog(string.Format(GVars.language == "zh" ? "游戏[{0}]已经存在于下载列表" : "The [{0}] has been in the download list", GVars.language == "zh" ? game.name_cn : game.name_en));
        }
        else if (game.state == InnerGameState.Downloading)
        {
            Debug.Log(string.Format("游戏[id:{0},name:{1}] 正在下载中", game.id, game.name_cn));

            //AlertDialog.Get().ShowDialog(string.Format("游戏[id:{0},name:{1}] 正在下载中", game.id, game.name_cn));
            if (!game.isLuaGame)
            {
                AlertDialog.Get().ShowDialog(string.Format(GVars.language == "zh" ? "是否暂停[{0}]的下载" : "Stop downloading the [{0}]？", GVars.language == "zh" ? game.name_cn : game.name_en), true, () =>
                {
                    var request = item.GetHTTPRequest();
                    if (request != null && request.State < HTTPRequestStates.Finished)
                    {
                        request.OnProgress = null;
                        request.Callback = null;
                        request.Abort();
                    }
                    item.SetHTTPRequest(null);
                    item.ChangeState(InnerGameState.Paused);
                });
            }
        }
        else if (game.state == InnerGameState.Installing)
        {
            Debug.Log(string.Format("游戏[id:{0},name:{1}] 正在安装中", game.id, game.name_cn));
        }
        else if (game.state == InnerGameState.OK)
        {
            Debug.Log(string.Format("游戏[id:{0},name:{1}] 可以运行", game.id, game.name_cn));

            Debug.Log(LogHelper.Red("game.name_cn: " + game.name_cn + " game.id: " + game.id + "  game.RunStatus: " + game.RunStatus));

            if (game.RunStatus == 0)
            {
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "游戏暂未开放" : "The game is not yet open");
                return;
            }

            if (GVars.ScoreOverflow == true)//顶分状态
            {
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "请等待上局游戏结束" : "Please wait for the end of a game");
                return;
            }
            else if (GVars.user.overflow == 1)//暴机状态
            {
                AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "您的帐号已爆机，请及时兑奖，否则无法正常游戏" : "Your account is too high,please go to the reception desk and redeem prizes");
                return;
            }

            //StartGame(gameId);
            //StartGame(item);
            string version = "";
            if (!game.isLuaGame)
            {
                CheckGameStatus(item);
            }
            if (RuntimePlatform.Android == Application.platform)
            {
                version = game.version;
            }
            else if (RuntimePlatform.IPhonePlayer == Application.platform)
            {
                version = "-2";
            }
            else
            {
                version = "-1";
            }
            if (game.isLuaGame)
            {
                version = game.version;
            }
            //if (false && game.isLuaGame)
            //{
            //    GameLoading
            //        .Create()
            //        .SetAwakeTask(() =>
            //        {
            //            Debug.Log("awakeTask @" + Time.frameCount);
            //            GameLoading.Get().LoadLoadingPrefab(2 - 1);
            //        })
            //    .Load();
            //    GameLoading.Get().SetHallSceneName(SceneManager.GetActiveScene().name);
            //    AppManager.Get().OnEnterLuaGame();
            //    return;
            //}
            Send_CheckGameVersion(game.id, version);
        }
        else if (game.state == InnerGameState.NeedUpdate)
        {
            Debug.Log(string.Format("游戏[id:{0},name:{1}] 需要更新", game.id, game.name_cn));
        }
        else if (game.state == InnerGameState.Paused)
        {
            Debug.Log(string.Format("游戏[id:{0},name:{1}] 准备恢复下载", game.id, game.name_cn));
            string version = "-1";
            Send_CheckGameVersion(game.id, version);
            return;
            //if (multiDown == false && IsDownloading())
            //{
            //    item.ChangeState(InnerGameState.Waiting);
            //    AddToWaitDLTask(item);
            //}
            //else
            //{
            //    item.ChangeState(InnerGameState.Downloading);
            //    Download(item);
            //}
        }
    } 


    void DownloadCallback(HTTPRequest req, HTTPResponse resp)
    {
        InnerGameItem item = req.Tag as InnerGameItem;
        var game = item.GetInnerGame();
        int gameId = game.id;
        string status;
        string filePath = m_downDir + gameId + ".apk";

        List<byte[]> fragments;
        Debug.Log(string.Format("state:{0}", req.State));
        string key_totalSize = gameId + "_TotalSize";
        string key_savedSize = gameId + "_SavedSize";
        switch (req.State)
        {
            case HTTPRequestStates.Processing:

                if (!PlayerPrefs.HasKey(key_totalSize))
                {
                    string value = resp.GetFirstHeaderValue("content-length");
                    if (!string.IsNullOrEmpty(value))
                    {
                        int total = int.Parse(value);
                        PlayerPrefs.SetInt(key_totalSize, total);
                        Debug.Log(string.Format("totalSize:{0}", value));
                        Debug.Log(filePath);
                        game.totalSize = total;
                    }
                }
                fragments = resp.GetStreamedFragments();
                using (FileStream fs = new FileStream(filePath, FileMode.Append))
                    foreach (byte[] data in fragments)
                    {
                        fs.Write(data, 0, data.Length);
                        game.savedSize += data.Length;
                    }
                PlayerPrefs.SetInt(key_savedSize, game.savedSize);
                //game.progress = game.savedSize / (float)game.totalSize;
                item.UpdateProgress();
                break;
            case HTTPRequestStates.Finished:

                if (resp.IsSuccess)
                {
                    if (resp.IsStreamingFinished)
                    {
                        status = "Streaming finished!";

                        //PlayerPrefs.DeleteKey(key_savedSize);
                        fragments = resp.GetStreamedFragments();
                        Debug.Log(fragments == null);
                        if (fragments != null)
                        {
                            using (FileStream fs = new FileStream(filePath, FileMode.Append))
                            {
                                //Debug.Log(fs == null);
                                foreach (byte[] data in fragments)
                                {
                                    fs.Write(data, 0, data.Length);
                                    game.savedSize += data.Length;
                                }
                            }
                        }


                        //PlayerPrefs.Save();
                        m_httpRequest = null;
                        item.SetHTTPRequest(null);
                        InstallGame(filePath);
                        Debug.LogFormat("保存地址:{0}", filePath);
                        item.ChangeState(InnerGameState.OK);
                    }
                    else
                        status = "Processing";
                }
                else
                {
                    status = string.Format("Request finished Successfully, but the server sent an error. Status Code: {0}-{1} Message: {2}",
                                                    resp.StatusCode,
                                                    resp.Message,
                                                    resp.DataAsText);
                    Debug.Log(status);
                    m_httpRequest = null;
                    item.SetHTTPRequest(null);
                    ReDownload(item);
                }
                break;
            case HTTPRequestStates.Error:
                status = "Request Finished with Error! " + (req.Exception != null ? (req.Exception.Message + "\n" + req.Exception.StackTrace) : "No Exception");
                Debug.LogError(status);
                m_httpRequest = null;
                item.SetHTTPRequest(null);
                ReDownload(item);
                break;
            case HTTPRequestStates.Aborted:
                status = "Request Aborted!";
                Debug.Log(status);
                m_httpRequest = null;
                item.SetHTTPRequest(null);
                break;
            case HTTPRequestStates.ConnectionTimedOut:
                status = "Connection Timed Out!";
                Debug.LogError(status);
                m_httpRequest = null;
                item.SetHTTPRequest(null);
                ReDownload(item);
                break;
            case HTTPRequestStates.TimedOut:
                status = "Processing the request Timed Out!";
                Debug.LogError(status);
                m_httpRequest = null;
                item.SetHTTPRequest(null);
                ReDownload(item);
                break;
        }
        Debug.Log(string.Format("id:{0}, total:{1}, progress:{2:0.00}, saved:{3}", gameId, game.totalSize, game.progress, game.savedSize));
    }

    IEnumerator ReDownload(InnerGameItem item)
    {
        yield return new WaitForSeconds(1f);
        Download(item);
        yield break;
    }

    //int m_lastDownloaded;
    //float m_lastProgressTime;
    void OnDownloadProgress(HTTPRequest req, int downloaded, int length)
    {
        if (req == null) return;
        InnerGameItem item = req.Tag as InnerGameItem;
        float duration = Time.time - item.lastProgressTime;
        float speed = (downloaded - item.lastDownloaded) / (duration * 1024);
        item.lastProgressTime = Time.time;
        Debug.Log(string.Format("total:{0}, downloaded:{1}, speed:{2:0.0}KB/s", length, downloaded, speed));
        var game = item.GetInnerGame();
        game.downSize += downloaded - item.lastDownloaded;
        item.lastDownloaded = downloaded;
        int totalSize = Math.Max(game.totalSize, length);
        game.progress = game.downSize / (float)totalSize;
        item.UpdateProgress();
    }


    //生成启动子游戏时用的参数
    string GenerateStartAppValue_Android(string userName, string pwd, string language, string ip, string hallPackageName, string port = "", bool newVersion = false)
    {
        string ret = "";
        long timeMIlls = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
        string timeStr = (timeMIlls / 1000).ToString();
        if (newVersion)
        {
            ret = string.Join(" ", new string[] { userName, pwd, hallPackageName, ip, port, language, timeStr });
        }
        else
        {

            ret = string.Join(" ", new string[] { userName, pwd, hallPackageName, ip, language, timeStr });
        }
        return ret;
    }

    string GenerateStartAppValue_IOS(string doMain, string userName, string pwd, string language, string ip, string hallPackageName = "", string port = "", bool newVersion = false)
    {
        string ret = "";
        if (newVersion)
        {

        }
        else
        {
            ret = string.Format("www.xingli.com?+Domain={4}+NAME={0}+PWD={1}+IP={2}+LAN={3}", userName, pwd, ip, language, doMain);
        }
        return ret;
    }

    void Send_CheckGameVersion(int gameId, string version)
    {
        NetManager.GetInstance().Send("gcuserService/gameLoadAddress", new object[] { gameId, version });
    }

    void HandleNetMsg_CheckGameVersion(object[] args)
    {
        Debug.Log("HandleNetMsg_CheckGameVersion");
        Dictionary<string, object> dic;// = new Dictionary<string, object>();
        dic = args[0] as Dictionary<string, object>;
        int gameType = (int)dic["gameType"];
        var gameItem = GetInnerGameItemByType(gameType);
        var game = gameItem.GetInnerGame();
        Debug.Log(game.name_cn + game.isLuaGame);
        if (dic.ContainsKey("haveNewVersionIDFlag"))
        {
            bool haveNewVersion = (bool)dic["haveNewVersionIDFlag"];
            if (haveNewVersion)
            {
                string versionCode = (string)dic["versionCode"];
                string downloadWindowsAddr = (string)dic["downloadWindows"];
                string downloadAndroidAddr = (string)dic["downloadAndroid"];

                if (game.isLuaGame)
                {
                    Debug.Log("luaGame");
                    if (RuntimePlatform.IPhonePlayer == Application.platform || RuntimePlatform.OSXEditor == Application.platform || RuntimePlatform.OSXPlayer == Application.platform)
                    {
                        game.url = downloadWindowsAddr;
                    }
                    else
                    {
                        game.url = downloadAndroidAddr;
                    }
                    if (game.state == InnerGameState.OK)
                    {

                        AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "检测到游戏有新版本，是否更新" : "Updated version in order to guarantee your gaming experience,the client must be the latest version", true, () =>
                        {
                            gameItem.ChangeState(InnerGameState.Downloading);
                            StartCoroutine(Download_AB(gameItem));

                        });
                    }
                    else
                    {
                        Debug.Log("down luaGame");
                        gameItem.ChangeState(InnerGameState.Downloading);
                        StartCoroutine(Download_AB(gameItem));
                    }
                }
                else if (RuntimePlatform.Android == Application.platform)
                {
                    game.url = downloadAndroidAddr;
                    System.Action doing = () =>
                        {
                            if (!multiDown && IsDownloading())
                            {
                                gameItem.ChangeState(InnerGameState.Waiting);
                                AddToWaitDLTask(gameItem);
                            }
                            else
                            {
                                gameItem.ChangeState(InnerGameState.Downloading);
                                Download(gameItem);
                            }
                        };
                    if (game.state == InnerGameState.OK)
                    {
                        gameItem.ChangeState(InnerGameState.Default);
                        AlertDialog.Get().ShowDialog(GVars.language == "zh" ? "检测到游戏有新版本，是否更新" : "Updated version in order to guarantee your gaming experience,the client must be the latest version", true, doing);
                    }
                    else
                    {
                        doing();
                    }

                }
                else if (RuntimePlatform.WindowsEditor == Application.platform)
                {
                    //GetInnerGameByType(gameType).url = downloadWindowsAddr;
                    //StartGame(gameItem);
                    GetInnerGameByType(gameType).url = downloadAndroidAddr;
                    Application.OpenURL(GetInnerGameByType(gameType).url);
                }
                else if (RuntimePlatform.IPhonePlayer == Application.platform)
                {
                    //GetInnerGameByType(gameType).url = downloadWindowsAddr;
                    //StartGame(gameItem);
                    GetInnerGameByType(gameType).url = downloadWindowsAddr;
                    Application.OpenURL(GetInnerGameByType(gameType).url);
                }

                return;
            }
            else
            {
                goto RunGame;
            }
        }

    RunGame:
        try
        {
            StartGame(GetInnerGameItemByType(gameType));
        }
        catch (Exception ex)
        {
            Debug.LogError(ex);
        }

    }

    InnerGameItem GetInnerGameItemByType(int gameType)
    {
        foreach (InnerGameItem item in m_itemList)
        {
            if (item.GetInnerGame().id == gameType)
            {
                return item;
            }
        }
        //gameType: 0:大厅， 8:水浒传
        //return m_itemList[gameType - 1];
        return null;
    }

    InnerGame GetInnerGameByType(int gameType)
    {
        foreach (InnerGameItem item in m_itemList)
        {
            if (item.GetInnerGame().id == gameType)
            {
                return item.GetInnerGame();
            }
        }

        //return m_itemList[gameType - 1].GetInnerGame();
        return null;
    }

    int GetGameType(InnerGameItem item)
    {
        return item.GetInnerGame().id;
    }

    void OnDestroy()
    {
        PlayerPrefs.Save();
        DownloadAbort();
    }

    string Fake_GameDownUrl(InnerGame game)
    {
        string ret = "";
        //string baseUrl = game.isLuaGame ? "http://192.168.1.64:8081/" : "http://192.168.1.64:8080/";
        string baseUrl = game.isLuaGame ? "http://120.55.112.157:8081/hall8/luagame/" : "http://120.55.112.157:8081/hall8/test/apks/";
        string fileName = "";
        switch (game.id)
        {
            case 1:
                fileName = "1_LuckyLion/LuckyLion__1.1.9.apk";
                break;
            case 2:
                fileName = "2_Fish/Fish__1.2.22.apk";
                break;
            case 3:
                fileName = "3_DanTiao/DanTiao__1.1.27.apk";
                break;
            case 4:
                fileName = "4_OverFish/OverFish__1.2.12.apk";
                break;
            case 5:
                fileName = "5_QQMermaid/QQMermaid__1.1.16.apk";
                break;
            case 6:
                fileName = "6_LackSuit/LackSuit__1.1.28.apk";
                break;
            case 7:
                fileName = "7_JoyBullFighting/JoyBullFighting__1.1.14.apk";
                break;
            case 8:
                fileName = "8_WaterMargin/WaterMargin__7.1.11.apk";
                break;
            case 9:
                fileName = "Game_02/" + GVars.platformDir + "/V1.0.0" + "/files.txt";
                break;
            default:
                Debug.Log("unknow gameid" + game.id);
                break;
        }
        ret = baseUrl + fileName;
        return ret;
    }

    public void Refresh_UninstallBtnState()
    {
        foreach (var item in m_itemList)
        {
            if (item != null && (item.GetInnerGame().state == InnerGameState.OK && (Application.platform != RuntimePlatform.IPhonePlayer || item.GetInnerGame().isLuaGame == true)))
            {
                item.SetDisplayUninstallBtn(debug_Uninstall);
            }
        }
    }
}
