﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BestHTTP;
using WMCY.GameHall;
public class InnerGameItem : MonoBehaviour
{
    InnerGame m_innerGame;
    [SerializeField]
    GameObject m_goWait;
    [SerializeField]
    GameObject m_goEmpty;
    [SerializeField]
    GameObject m_goProgress;
    [SerializeField]
    GameObject m_goPause;
    [SerializeField]
    Image m_imageProgress;
    HTTPRequest m_httpRequest = null;
    public int lastDownloaded;
    public float lastProgressTime;
    void Awake()
    {
        //m_httpRequest = null;
    }

    void Start()
    {
        //StartCoroutine(AniDownloading());
        //StartCoroutine(TestDownloading());
        //StartCoroutine(Test_LayoutInfo());
        transform.Find("btnUninstall").GetComponent<Button>().onClick.AddListener(OnBtnUninstall_Click);
    }

    public void Init(InnerGame innerGame)
    {
        m_innerGame = innerGame;
        Text text = transform.Find("labelBg/Text").GetComponent<Text>();
        text.text = GVars.language=="zh"? innerGame.name_cn: innerGame.name_en;
        text.fontSize = GVars.language == "zh" ? 28 : 22;
        I2LocalizeText i2Text = text.GetComponent<I2LocalizeText>();
        i2Text.SetValues(innerGame.name_cn, innerGame.name_en);
        transform.Find("icon").GetComponent<Image>().sprite = innerGame.spriteIcon;
        transform.Find("color").GetComponent<Image>().sprite = innerGame.spriteColor;
        GetComponent<Button>().onClick.AddListener(OnItem_Click);
        ChangeState(InnerGameState.Default);
    }

    public InnerGame GetInnerGame()
    {
        return m_innerGame;
    }

    public void OnItem_Click()
    {
        Debug.Log(string.Format("OnItem_Click: {0}, state:{1}", m_innerGame.name_cn, m_innerGame.state));
        if (InnerGameState.Default == m_innerGame.state)
        {
            //StartCoroutine(Test_WholeLifetime());
        }
        if (InnerGameState.OK != m_innerGame.state)
        {
            //return;
        }
        InnerGameManager.Get().OnBtnClick_GameItem(this);
    }

    public void OnBtnUninstall_Click()
    {

        InnerGameManager.Get().UninstallGame(this);
    }

    public void UpdateProgress(float newProgress)
    {
        m_imageProgress.fillAmount = newProgress;
    }

    public void UpdateProgress()
    {
        m_imageProgress.fillAmount = m_innerGame.progress;
    }

    private IEnumerator AniDownloading()
    {
        m_imageProgress.fillAmount = 0.25f;
        float speed = -360;
        while (true)
        {
            m_imageProgress.rectTransform.Rotate(new Vector3(0, 0, speed * Time.deltaTime));
            yield return null;
        }
        //yield break;
    }

    public void ChangeState(InnerGameState newState)
    {
        Debug.LogFormat("id:{2}, old:{0}, new:{1}", m_innerGame.state, newState,m_innerGame.id);
        SetDisplayUninstallBtn(false);
        switch (newState)
        {
            case InnerGameState.Default:
                m_goEmpty.transform.parent.gameObject.SetActive(true);
                m_goEmpty.SetActive(true);
                m_goWait.SetActive(false);
                m_goProgress.SetActive(false);
                m_goPause.SetActive(false);
                break;
            //case InnerGameState.ReadyInstall:
            //    m_goEmpty.transform.parent.gameObject.SetActive(false);
            //    if (InnerGameManager.Get().debug_Uninstall && (Application.platform != RuntimePlatform.IPhonePlayer&& m_innerGame.isLuaGame == false))
            //    {
            //        SetDisplayUninstallBtn(true);
            //    }
            //    break;
            case InnerGameState.Waiting:
                m_goEmpty.transform.parent.gameObject.SetActive(true);
                m_goEmpty.SetActive(false);
                m_goWait.SetActive(true);
                m_goProgress.SetActive(false);
                m_goPause.SetActive(false);
                break;
            case InnerGameState.Downloading:
                m_goEmpty.transform.parent.gameObject.SetActive(true);
                m_goEmpty.SetActive(false);
                m_goWait.SetActive(false);
                m_goProgress.SetActive(true);
                m_goPause.SetActive(false);
                if (Application.platform == RuntimePlatform.IPhonePlayer || m_innerGame.isLuaGame)
                {
                    StartCoroutine(AniDownloading());
                }
                else
                {
                    UpdateProgress();
                }
                break;
            case InnerGameState.Installing:
                break;
            case InnerGameState.OK:
                m_goEmpty.transform.parent.gameObject.SetActive(false);
                if (InnerGameManager.Get().debug_Uninstall && (Application.platform != RuntimePlatform.IPhonePlayer&& m_innerGame.isLuaGame == false))
                {
                    SetDisplayUninstallBtn(true);
                }

                break;
            case InnerGameState.NeedUpdate:
                break;
            case InnerGameState.Paused:
                m_goEmpty.transform.parent.gameObject.SetActive(true);
                m_goEmpty.SetActive(false);
                m_goWait.SetActive(false);
                m_goProgress.SetActive(true);
                m_goPause.SetActive(true);
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                }
                else
                {
                    UpdateProgress();
                }
                break;
        }
        m_innerGame.state = newState;
    }


    #region Test Region
    private IEnumerator Test_Downloading()
    {
        float duration = 5f;
        float time = Time.time;
        while (Time.time < time + duration)
        {
            UpdateProgress((Time.time - time) / duration);
            yield return null;
        }
    }

    private IEnumerator Test_WholeLifetime()
    {
        float time = Time.time;
        yield return new WaitForSeconds(1f);
        ChangeState(InnerGameState.Waiting);
        yield return new WaitForSeconds(1f);
        ChangeState(InnerGameState.Downloading);
        yield return StartCoroutine(Test_Downloading());
        ChangeState(InnerGameState.OK);
        yield break;

    }

    private IEnumerator Test_LayoutInfo()
    {
        yield return new WaitForSeconds(2f);
        Debug.Log(m_innerGame.id);
        var rectXForm = GetComponent<RectTransform>();
        string msg = string.Format("localScale:{0}, localPosition:{1}, lossyScale:{2}", rectXForm.localScale, rectXForm.localPosition, rectXForm.lossyScale);
        Debug.Log(msg);
        yield break;
    }

    public void SetHTTPRequest(HTTPRequest req)
    {
        m_httpRequest = req;
    }

    public HTTPRequest GetHTTPRequest()
    {
        return m_httpRequest;
    }

    public bool IsDownloading()
    {
        return m_httpRequest == null ? false : true;
    }

    void OnDestroy()
    {
        DownloadAbort();
    }

    private void DownloadAbort()
    {
        var request = m_httpRequest;
        if (request != null && request.State < HTTPRequestStates.Finished)
        {
            request.OnProgress = null;
            request.Callback = null;
            request.Abort();
        }
    }

    public void SetDisplayUninstallBtn(bool value)
    {
        transform.Find("btnUninstall").gameObject.SetActive(value);
    }

    #endregion
}
