﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;


public class GameLoading : MonoBehaviour
{

    public static string loadingSceneName = "";
    static GameLoading g_instance;
    //[SerializeField]
    //List<GameObject> m_loadingPrefabs;
    public PrefabsSO prefabs;
    bool keepPrevious = true;

    public System.Action awakeTask;
    public System.Action startTask;

    void Awake()
    {
        Debug.LogFormat("GameLoading.Awake @{0} @id {1}", Time.frameCount, GetInstanceID());
        //transform.parent.Find("")
        
        //Debug.Log("GetInstanceID: " + this.GetInstanceID());
        DontDestroyOnLoad(this);
        gameObject.name = "GameLoading";

        if (g_instance == null)
        {
            g_instance = this;
        }
        else
        {
            if (keepPrevious)
            {
                if (this != g_instance)
                {
                    Debug.Log("Destroy new one");
                    Destroy(gameObject);
                    //return;
                }
            }
            else
            {
                if (this == g_instance)
                {
                    Debug.Log("Destroy old one");
                    Destroy(g_instance.gameObject);
                    g_instance = this;
                }
            }
        }

        Debug.LogFormat("awakeTask is null: {0}, {1}", g_instance.awakeTask == null, this.GetInstanceID());
        if (g_instance.awakeTask != null)
        {
            g_instance.awakeTask();
            g_instance.awakeTask = null;
        }
    }

    void Start()
    {
        Debug.LogFormat("GameLoading.Start @{0}", Time.frameCount);
        Screen.orientation = ScreenOrientation.Portrait;
        //gameObject.name = "GameLoading";

        if (startTask != null)
        {
            startTask();
            startTask = null;
        }
    }

    public static void LoadLoadingScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public static void LoadLoadingSceneAsync(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName);
    }

    public GameLoading Load()
    {
        SceneManager.LoadScene("_Scenes/GameLoading");
        return this;
    }

    public GameObject LoadLoadingPrefab(int index)
    {
        //var newGo = GameObject.Instantiate<GameObject>(m_loadingPrefabs[index]);
        EnsurePrefabs();
        var newGo = GameObject.Instantiate<GameObject>(prefabs[index]);
        return newGo;
    }

    public void EnsurePrefabs()
    {
        if (prefabs == null)
        {
            prefabs = Resources.Load<PrefabsSO>("LoadingPrefabs");
        }
    }

    public static GameLoading Get()
    {
        return g_instance;
    }

    public static GameLoading GetInstance()
    {
        return g_instance;
    }

    public static GameLoading Create()
    {
        GameObject newGo;
        //newGo = GameObject.Instantiate<GameObject>(null);
        newGo = new GameObject();
        return newGo.AddComponent<GameLoading>();
    }

    public GameLoading SetAwakeTask(System.Action task)
    {
        Debug.LogFormat("GameLoading.SetAwakeTask @{0}", Time.frameCount);
        awakeTask = task;
        return this;
    }

    public GameLoading SetStartTask(System.Action task)
    {
        Debug.LogFormat("GameLoading.SetStartTask @{0}", Time.frameCount);
        startTask = task;
        return this;
    }

    void OnDestory()
    {
        Debug.LogFormat("GameLoading.OnDestory @{0}", Time.frameCount);
        //if(this == g_instance)
            g_instance = null;
    }

    string m_hallSceneName = "TestSceneTransfer";
    public void SetHallSceneName(string hallSceneName)
    {
        m_hallSceneName = hallSceneName;
    }

    public string GetHallSceneName()
    {
        return m_hallSceneName;
    }

}
