﻿using UnityEngine;
using System.Collections;

public class TopCoroutine : MonoBehaviour {

    static TopCoroutine g_instance;
    void Awake()
    {
        g_instance = this;
    }
	void Start () {
	
	}
	
	void Update () {
	
	}

    public static TopCoroutine Get()
    {
        if (null == g_instance)
            return Create();
        else
            return g_instance;
    }

    public static TopCoroutine Create()
    {
        var go = new GameObject();
        go.name = "TopCoroutine";
        return go.AddComponent<TopCoroutine>();

    }
}
