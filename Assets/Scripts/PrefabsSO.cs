﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[CreateAssetMenu()]
public class PrefabsSO : ScriptableObject {
    public List<GameObject> prefabs;
    public GameObject this[int index]
    {
        get{
            return prefabs[index];
        }
    }

}
