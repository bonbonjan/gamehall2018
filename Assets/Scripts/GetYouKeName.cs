﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace YouKePlayer
{
    public struct Point
    {
        private int x;
        private int y;
        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Point)) return false;
            Point p = (Point)obj;
            return x == p.x & y == p.y;
        }

        public override int GetHashCode()
        {
            return ShiftAndWrap(x.GetHashCode(), 2) ^ y.GetHashCode();
        }

        private int ShiftAndWrap(int value, int positions)
        {
            positions = positions & 0x1F;
            uint number = BitConverter.ToUInt32(BitConverter.GetBytes(value), 0);
            uint wrapped = number >> (32 - positions);
            return BitConverter.ToInt32(BitConverter.GetBytes((number << positions) | wrapped), 0);
        }
    }


    public class Point1
    {
        public int Acs(string character)
        {
            if (character.Length <= 1)
            {
                System.Text.ASCIIEncoding asciiEncoding = new System.Text.ASCIIEncoding();
                int intAsciiCode = (int)asciiEncoding.GetBytes(character)[0];
                return (intAsciiCode);
            }
            else
            {
                throw new Exception("Character is not valid");
            }
        }


    }
    public class Point2
    {
        public int[] ConStr(string personId)
        {
            if (personId.Length <= 16)
            {
                personId = personId.ToString().PadRight(16, '0');
                char[] temp = personId.ToCharArray();
                int[] temp1 = new int[8]; int j = 0;
                Point1 pt1 = new Point1();

                for (int i = 0; i < temp.Length; i = i + 2)
                {

                    string d = temp[i].ToString();
                    int a = pt1.Acs(d);
                    string e = temp[i + 1].ToString();
                    int b = pt1.Acs(e);
                    Point pt = new Point(a, b);
                    int f = pt.GetHashCode() % 10;
                    temp1[j] = f;
                    j++;
                }
                return temp1;

            }
			else if(personId.Length <= 20)
			{
				personId = personId.ToString().PadRight(20, '0');
				char[] temp = personId.ToCharArray();
				int[] temp1 = new int[personId.Length/2]; int j = 0;
				Point1 pt1 = new Point1();
				
				for (int i = 0; i < temp.Length; i = i + 2)
				{
					
					string d = temp[i].ToString();
					int a = pt1.Acs(d);
					string e = temp[i + 1].ToString();
					int b = pt1.Acs(e);
					Point pt = new Point(a, b);
					int f = pt.GetHashCode() % 10;
					temp1[j] = f;
					j++;
				}
				return temp1;
			}
			else if(personId.Length <= 40)
			{
				personId = personId.ToString().PadRight(40, '0');
				char[] temp = personId.ToCharArray();
				int[] temp1 = new int[personId.Length/2]; int j = 0;
				Point1 pt1 = new Point1();
				
				for (int i = 0; i < temp.Length; i = i + 2)
				{
					
					string d = temp[i].ToString();
					int a = pt1.Acs(d);
					string e = temp[i + 1].ToString();
					int b = pt1.Acs(e);
					Point pt = new Point(a, b);
					int f = pt.GetHashCode() % 10;
					temp1[j] = f;
					j++;
				}
				return temp1;
			}
            else
            {
                throw new Exception("PersonId is not valid");
            }
        }
    }
    public class YouKePlayerName
    {
		public string mSerials="";
        public string GetYouKeName(string namestr)
        {
            Point2 str = new Point2();
            int[] temp2 = str.ConStr(namestr);
            string name="";
            foreach (int p in temp2)
               name += p.ToString();
			for(int i = 0 ; i < 3 ;i ++)
			{
				if (name.Length>=20)
				{
					Point2 str1 = new Point2();
					int[] temp3 = str1.ConStr(name);
					name="";
					foreach (int p in temp3)
						name += p.ToString();
				}
			}

            //Console.ReadKey();
			mSerials=name;
            //Debug.Log("youke=" + "游客" + name);
            return "游客" + name;
        }

       
    }
}

