﻿using UnityEngine;
using System.Collections;

public class ShowFPS : MonoBehaviour
{

    public float f_UpdateInterval = 0.5F;

    private float f_LastInterval;

    private int i_Frames = 0;

    private float f_Fps;
    bool _onGUIInit = false;

    void Start()
    {
        //Application.targetFrameRate=60;

        f_LastInterval = Time.realtimeSinceStartup;

        i_Frames = 0;
    }

    void OnGUI()
    {
        if (!_onGUIInit)
        {
            GUIStyle style = GUI.skin.GetStyle("label");
            style.fontSize = 30;
            GUI.skin.font = Font.CreateDynamicFontFromOSFont("微软雅黑", 50);
            _onGUIInit = true;
        }
        GUI.Label(new Rect(0, 200, 200, 200), "FPS:" + f_Fps.ToString("f2"));
    }

    void Update()
    {
        ++i_Frames;

        if (Time.realtimeSinceStartup > f_LastInterval + f_UpdateInterval)
        {
            f_Fps = i_Frames / (Time.realtimeSinceStartup - f_LastInterval);

            i_Frames = 0;

            f_LastInterval = Time.realtimeSinceStartup;
        }
    }
}