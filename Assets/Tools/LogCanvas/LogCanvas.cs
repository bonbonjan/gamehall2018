﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/*
 * author: shen.hf
 * company: WMCY@hf
 * date: 2016/05/18
 * version: 0.1
 */
namespace LogCanvas
{
    public class LogCanvas : MonoBehaviour
    {
        [SerializeField]
        Transform m_xformDrag;
        [SerializeField]
        Text textLog;
        [SerializeField]
        Transform xformContent;
        [SerializeField]
        GameObject templateItem;

        List<LogItem> logItemList;

        const int MaxLogNum = 100;
        static LogCanvas _instance;
        void Awake()
        {
            Debug.Log("LogCanvas Awake");
            if (_instance != null)
            {
                Debug.Log("LogCanvas Awake again");
                Destroy(this.gameObject);
                return;
            }
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
            logItemList = new List<LogItem>();
            templateItem.SetActive(false);
            Application.logMessageReceived += HandleLog;
        }

        void Start()
        {
            //for (int i = 0; i < 2; i++)
            //{
            //    HandleLog(string.Format("日志{0}", i), "", LogType.Log);
            //}
        }

        void Update()
        {

        }

        public LogCanvas Get()
        {
            return _instance;
        }



        private string output = "";
        private string stack = "";
        void OnEnable()
        {
            //Application.logMessageReceived += HandleLog;
        }

        void OnDisable()
        {
            //Application.logMessageReceived -= HandleLog;
        }

        void OnDestroy()
        {
            //Application.logMessageReceived -= HandleLog;
        }

        void HandleLog(string logString, string stackTrace, LogType type)
        {
            output = logString;
            stack = stackTrace;
            textLog.text = logString+'\n'+stackTrace;
            AddLog(logString, stackTrace, type);
        }

        //not work
        public void OnDrag(PointerEventData data)
        {
            //Debug.Log("OnDrag");
            GetComponent<RectTransform>().pivot.Set(0, 0);
            transform.position = Input.mousePosition;
            //SetDraggedPosition(data);
        }

        private void SetDraggedPosition(PointerEventData data)
        {
            /*
            RectTransform plane;
            if (data.pointerEnter != null && data.pointerEnter.transform as RectTransform != null)
            {

                plane = data.pointerEnter.transform as RectTransform;
                Debug.Log("plane:" + plane.name);
            }
            return;
            var rt = m_xformDrag as RectTransform;
            Vector3 globalMousePos;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(plane, out globalMousePos, data.position, data.pressEventCamera))
            {
                rt.position = globalMousePos;
                rt.rotation = transform.rotation;
            }
             */
        }

        void AddLog(string logString, string stackTrace, LogType type)
        {
            if (type != LogType.Log && logString.StartsWith("Trying to add"))
                return;
            var newItemGo = Instantiate<GameObject>(templateItem);
            newItemGo.transform.SetParent(xformContent, false);
            //newItemGo.transform.localPosition = Vector3.zero;
            //newItemGo.transform.localScale = Vector3.one;
            newItemGo.SetActive(true);
            var newItem = new LogItem();
            newItem.logString = logString;
            newItem.stackTrace = stackTrace;
            newItem.type = type;
            newItem.go = newItemGo;
            newItem.text = newItemGo.transform.Find("Text").GetComponent<Text>();
            newItem.text.text = logString;
            logItemList.Add(newItem);
            if (logItemList.Count > MaxLogNum)
            {
                Destroy(logItemList[MaxLogNum - 1].go);
                logItemList.RemoveAt(MaxLogNum - 1);
            }
        }

        public void OnBtnClick_Control()
        {
            //Debug.Log("OnBtnClick_Control");
            var target = transform.Find("RootPanel").gameObject;
            var text = transform.Find("btnControl/Text").GetComponent<Text>();
            target.SetActive(!target.activeSelf);
            text.text = target.activeSelf ? "隐藏" : "显示";
        }

        public void OnBtnClick_Close()
        {
            transform.Find("RootPanel").gameObject.SetActive(false);
        }

        public void OnBtnClick_Pause(Text text)
        {
            if (text == null) return;
            if (text.text == "暂停")
            {
                Application.logMessageReceived -= HandleLog;
                text.text = "继续";
            }
            else
            {
                Application.logMessageReceived += HandleLog;
                text.text = "暂停";

            }
        }

        public void OnBtnClick_Clear()
        {
            while (logItemList.Count > 0)
            {
                Destroy(logItemList[0].go);
                logItemList.RemoveAt(0);
            }
        }

        public void OnBtnClick_Test()
        {
            Debug.Log("这是一条测试信息\n行二\n行三");
        }

        public void OnBtnClick_SetBgColor(Text text)
        {
            if (text == null) return;
            Image bg = transform.Find("RootPanel/bg").GetComponent<Image>();
            if (text.text == "背景半透明")
            {
                bg.color = new Color(1f, 1f, 1f, .4f);
                text.text = "背景不透明";
            }
            else
            {
                bg.color = new Color(1f, 1f, 1f, 1f);
                text.text = "背景半透明";

            }
        }

        public void OnBtnClick_Quit()
        {
            Application.logMessageReceived -= HandleLog;
            Destroy(this.gameObject);
        }
    }

    public class LogItem
    {
        public string logString;
        public string stackTrace;
        public LogType type;
        public GameObject go;
        public Text text;
    }
}
