﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace LogCanvas
{
    public class LogHand : Dragable,IPointerClickHandler
    {
        void Awake()
        {
            base.Awake();
            GetComponent<Button>().onClick.AddListener(OnClick);
        }

        void Start()
        {
            base.Start();
        }

        public void OnClick()
        {
            //Debug.Log("OnClick" + isDraging);
            if (isDraging) return;
            if (follower != null)
                follower.gameObject.SetActive(true);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            //Debug.Log("OnPointerClick"+isDraging);

        }

    }

}
