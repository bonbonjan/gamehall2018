﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace LogCanvas
{
    public class Dragable : MonoBehaviour, IPointerDownHandler, IDragHandler
    {
        private Vector2 originalLocalPointerPos;
        private RectTransform rectTransform;
        private Vector3 originalLocalPos;
        public RectTransform follower;
        private bool isRealTime;//实时移动
        private Vector2 lastLocalPointPos;
        protected bool isDraging;

        protected void Awake()
        {
            rectTransform = transform as RectTransform;
            originalLocalPos = rectTransform.localPosition;
        }

        protected void Start()
        {

        }

        protected void Update()
        {
            //isDraging = false;
        }

        public void OnPointerDown(PointerEventData data)
        {
            //Debug.Log("OnPointerDown");
            originalLocalPos = rectTransform.localPosition;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform.parent as RectTransform, data.position, null, out originalLocalPointerPos);
            lastLocalPointPos = originalLocalPointerPos;
            isDraging = false;
        }


        public void OnDrag(PointerEventData data)
        {
            isDraging = true;
            Vector2 localPointerPos;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform.parent as RectTransform, data.position, null, out localPointerPos))
            {
                Vector3 offsetToOriginal = localPointerPos - originalLocalPointerPos;
                Vector3 offsetToLast = localPointerPos - lastLocalPointPos;
                lastLocalPointPos = localPointerPos;
                rectTransform.localPosition = originalLocalPos + offsetToOriginal;
                //if(isRealTime)
                //{
                //    originalLocalPointerPos = localPointerPos;
                //    originalLocalPos = rectTransform.localPosition;
                //}
                if (follower != null)
                    follower.localPosition += offsetToLast;
            }

        }




    }
}

